<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Home_model');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
       	$this->load->view('new/header',$data,true);
  }
    
	function index(){
		$this->load->view('new/header');
    $this->load->view('new/about');
    $this->load->view('new/footer');
	}
}
