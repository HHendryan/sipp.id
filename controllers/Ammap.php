<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ammap extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('map_model');
	}
		
	public function getKab(){
		$data['query'] = $this->map_model->getKab();
		$this->load->view('new/map_view', $data);
	}
	
	public function getOneKab($id){
		$data['query'] = $this->map_model->getOneKab($id);
		$this->load->view('new/map_view', $data);
	}
	
	public function getProvince() {
        $data['query'] = $this->map_model->getProv();
		$this->load->view('new/map_view', $data);
	}
	
	public function getOneProvince($id){
		$data['query'] = $this->map_model->getOneProv($id);
		$this->load->view('new/map_view', $data);
	}
}