<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Home_model');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }
    
	function index(){
        $data['paslon'] = $this->db->query("SELECT * from m_paslon a
            inner join m_pengusung b on a.id_paslon = b.id_paslon
            where id_parpol = 3 and length(a.id_area) = 2")->result();
        $i=0;
        foreach ($data['paslon'] as $key => $value) {
            $cek = $this->paslon_win($value->id_paslon);
            // var_dump($cek);
            if($cek == $value->id_paslon){

                $data['paslon'][$i]->status = 1;
            }else{
                $data['paslon'][$i]->status = 0;
            }
        $i++;
        }
		
		// var_dump($data);
		$this->load->view('home',$data);
	}
	
	function getprovince($id){ 
		$where_id="";
		if($id>0){
			
			$where_id = " where id = ".$id;
		}
		
		$data = $this->db->query("select * from m_provinces $where_id")->result_array();
		//var_dump($data);
		echo  json_encode($data);
	}

    function get_paslon_detail($id){
        $data = $this->db->get_where('m_paslon', array('id_paslon' => $id))->row();
        echo json_encode($data);
    }

    function paslon_win($id_paslon){
        $paslon = $this->db->get_where('m_paslon', array('id_paslon' => $id_paslon))->row();
        $id_area = $paslon->id_area;
        $list_paslon = $this->db->query("SELECT a.id_paslon, a.nama_kepala, c.kode_parpol,c.name, d.* from m_paslon a 
            inner join m_pengusung b on b.id_paslon = a.id_paslon
            inner join m_parpol c on c.id_parpol=b.id_parpol
            inner join m_jumlah_kursi d on d.id_area=a.id_area
            where a.id_area = '$id_area'")->result_array();
        foreach ($list_paslon as $key => $value) {
            $data[$value['id_paslon']]['jumlah_kursi'] = 0;
        }
        foreach ($list_paslon as $key => $value) {
            $kode = $value['kode_parpol'];
            if(!isset($value[$kode])){
                $jum = 0;
            }else{
                $jum = $value[$kode];
            }
            $data[$value['id_paslon']]['jumlah_kursi'] = $data[$value['id_paslon']]['jumlah_kursi']+$jum;
        }
        $menang = max($data);
        $menang2 = array_search($menang,$data);
        return $menang2;
    }
}
