<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        // $this->load->model('Home_model');
        // $this->load->library('datatables');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
        $this->load->view('template/menu',$data,true);
    }
    
    function index(){
        $data['tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
        $data['intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
        $data['total'] = $data['tangible']+$data['intangible'];
        $data['persen_tangible'] = round(($data['tangible']/$data['total'])*100,2) ;
        $data['persen_intangible'] = round(($data['intangible']/$data['total'])*100,2) ;
    	$this->load->view('profile',$data);
    }
}
