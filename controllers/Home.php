<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    //constructor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Home_model');
        // $this->load->library('datatables');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
       	$this->load->view('new/header',$data,true);
    }

    public function index()
    {

        $data = $this->Home_model->get_data_home();
        $this->load->view('new/header');
        $this->load->view('new/home', $data);
        $this->load->view('new/footer');
    }

    public function dashboard()
    {
        // Load Session Library
        $this->load->library('session');

        $this->load->view('new/header');
        $this->load->view('new/dashboard');
        $this->load->view('new/footer');
    }

    public function test($id_area){
        $panjang = strlen($id_area);
        if($panjang <= 2){
            $data = $this->db->get_where('m_provinces', array('id' => $id_area))->row();    
            $data->tipe = 'provinsi';
        }else{
            $data = $this->db->get_where('m_regencies', array('id' => $id_area))->row();
            $data->tipe = 'kabupaten';
        }
        // var_dump($data);
        $this->session->set_flashdata('item', $data);
        redirect("dashboard");
    }
	
	public function detail_data($id_area){
        $panjang = strlen($id_area);
        if($panjang <= 2){
            $data = $this->db->get_where('m_provinces', array('id' => $id_area))->row();    
            $data->tipe = 'provinsi';
        }else{
            $data = $this->db->get_where('m_regencies', array('id' => $id_area))->row();
            $data->tipe = 'kabupaten';
        }
        // var_dump($data);
        $this->session->set_flashdata('item', $data);
        redirect("dashboard");
    }
	
    function getprovince($id){ 
        $table_name = "";
		$where_id="";
        if($id>0){
            $table_name ="m_regencies";
            $where_id = " where province_id = ".$id;
        }else{
			$table_name ="m_provinces";
            
		}
        
        $data = $this->db->query("select * from $table_name $where_id")->result_array();
        //var_dump($data);
        echo  json_encode($data);
    }

    function test2(){
        $ci = get_instance();
        var_dump($ci);
    }

}
