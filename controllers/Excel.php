<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Survei_model');
        // $this->load->library('db');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }


    function index(){
    }   
	function export($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);	
		$judul = urldecode($judul);
		if($nama_survey != 'NULL'){
			$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama' => $nama_survey))->row('id'); 
		}
		
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_pilih b ', $tipe_survey_.' = b.id', 'left');
        if($nama_survey <> "ALL"){
        	if($nama_survey != 'NULL'){
				$this->db->where($tipe_survey, $id_survey);
			}else{
				$this->db->where($tipe_survey, 0);
			}
        }
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();
		
		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"Date: ");  
		// xlsWriteLabel(2,0,"Keyword: Keyword");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	function export_alias($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);
		$judul = urldecode($judul);
		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama_alias' => $nama_survey))->row('id');
		if(!$id_survey){
			$id_survey = 0;
		}
		$tipe_survey_ = 'a.'.$tipe_survey;

		$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_pilih b ', $tipe_survey_.' = b.id', 'left');
        if($nama_survey <> "ALL"){
			$this->db->where($tipe_survey, $id_survey);
        }
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();

		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"");  
		// xlsWriteLabel(2,0,"");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	function export_pengeluaran($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$judul = urldecode($judul);
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama_alias' => $nama_survey))->result(); 
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_pilih b ', $tipe_survey_.' = b.id', 'left'); 
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		}
		if($nama_survey <> "ALL"){
			$i=0;
	        $where = '';
	        $length = count($id_survey);
	        foreach ($id_survey as $key => $value) {
	        	if($length == 1){
	        			$where .= "$tipe_survey=$value->id";	
	        	}else{
	        		if($i == 0){
	        			$where .= "( $tipe_survey=$value->id OR ";	
	        		}
	        		if($length == $i+1){
	        			$where .= "$tipe_survey=$value->id)";
	        		}else{
	        			$where .= "$tipe_survey=$value->id OR ";
	        		}	
	        	}        	
	        $i++;
	        } 
	        $this->db->where($where);
        }
		

		$data = $this->db->get()->result();

		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// // $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"");  
		// xlsWriteLabel(2,0,"");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	function export_gubernur($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$judul = urldecode($judul);
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_paslon = $this->db->get_where('m_paslon', array('kepala_alias' => $nama_survey))->row('id_paslon');

		$tipe_survey_ = 'a.'.$tipe_survey;

		$this->db->select("a.transId as id,a.nama,a.alamat,a.telpRumah,a.hp,a.jabatan,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama_kepala as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_paslon b ', 'a.p7_id = b.id_paslon', 'left');
        if($nama_survey <> 'ALL'){
        	$this->db->where('a.p7_id', $id_paslon);
        }
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();

		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// // $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"");  
		// xlsWriteLabel(2,0,"");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	function export_gubernur2($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$judul = urldecode($judul);
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_paslon = $this->db->get_where('m_paslon', array('kepala_alias' => $nama_survey))->row('id_paslon');

		$tipe_survey_ = 'a.'.$tipe_survey;

		$this->db->select("a.transId as id,a.nama,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama_kepala as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_paslon b ', 'a.p7_id = b.id_paslon', 'left');
        if($nama_survey <> 'ALL'){
        	$this->db->where('a.p7_id', $id_paslon);
        }
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();
		var_dump($data);
		// include APPPATH.'third_party/fs_export_to_excel.php';
		
		// //pengaturan nama file
		// $namaFile = $tipe_survey.".xls";
		// //pengaturan judul data
		// // // $judul = $tipe_survey;
		// //baris berapa header tabel di tulis
		// $tablehead = 5;
		// //baris berapa data mulai di tulis
		// $tablebody = 6;
		// //no urut data
		// $nourut = 1;
		 
		// //penulisan header
		// header("Pragma: public");
		// header("Expires: 0");
		// header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		// header("Content-Type: application/force-download");
		// header("Content-Type: application/octet-stream");
		// header("Content-Type: application/download");
		// header("Content-Disposition: attachment;filename=".$namaFile."");
		// header("Content-Transfer-Encoding: binary ");
		 
		 
		// xlsBOF();
		 
		// xlsWriteLabel(0,0,$judul);  
		// // xlsWriteLabel(1,0,"");  
		// // xlsWriteLabel(2,0,"");  
		 
		// //header
		// $kolomhead = 0;
		// xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		// xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		// xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		// //body
		// foreach ($data as $key => $value) { 
		// 	$kolombody = 0;
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
		// 	xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
		// 	$tablebody++;
		// 	$nourut++;
		// }
		// xlsEOF();
		// exit();
	}
	function export_organisasi($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$judul = urldecode($judul);
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		if($nama_survey == 'Tidak Tahu Tidak Menjawab'){
			$nama_survey = 'Tidak tahu/Tidak Menjawab';
		}

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama' => $nama_survey))->row('id'); 
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		if($nama_survey != 'Lainnya'){
			$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi");
		}else{
			$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, a.p16Catatan as informasi");
		}
		
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_pilih b ', $tipe_survey_.' = b.id', 'left');
        if($nama_survey <> 'ALL'){
        	$this->db->where($tipe_survey, $id_survey);
        }
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();

		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"");  
		// xlsWriteLabel(2,0,"");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	function export_sosmed($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$judul = urldecode($judul);
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama' => $nama_survey))->row('id'); 
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, $tipe_survey_ as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->where($tipe_survey_.' !=', '');
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		} 
		$data = $this->db->get()->result();

		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"");  
		// xlsWriteLabel(2,0,"");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}

	function export_change($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah,$judul){
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);	
		$judul = urldecode($judul);

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama' => $nama_survey))->row('id');
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$this->db->select("a.transId as id,a.nama,a.telpRumah,a.hp,a.jabatan,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi");
        $this->db->from('trans_survey_clean a');
        $this->db->join('m_pilih b ', 'a.p4 = b.id', 'left');
        $this->db->where('a.p3', 31);
        $this->db->where('a.p4 !=', 48);
		if($nama_survey <> "ALL"){
			$this->db->where($tipe_survey, $id_survey);
        }
		if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('a.id_provinsi', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('a.id_kabupaten', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('a.id_kecamatan', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('a.id_kelurahan', $id_wilayah);
        	}
		}
		$data = $this->db->get()->result();
		
		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = $tipe_survey.".xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,$judul);  
		// xlsWriteLabel(1,0,"Date: ");  
		// xlsWriteLabel(2,0,"Keyword: Keyword");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Jabatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Alamat");              
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->nama);              
			xlsWriteLabel($tablebody,$kolombody++,$value->hp);              
			xlsWriteLabel($tablebody,$kolombody++,$value->jabatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->alamat);              
			xlsWriteLabel($tablebody,$kolombody++,$value->provinsi);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kotaKabupaten);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kecamatan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->desaKelurahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();
	}
	
	function detail_influencer_new($partai, $ketokohan, $tipe_ketokohan, $tipe_wilayah, $id_wilayah){
		$partai = urldecode($partai);
		$ketokohan = urldecode($ketokohan);
		$tipe_ketokohan = urldecode($tipe_ketokohan);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		if($tipe_ketokohan == 1){
			$tipe_ketokohan = 'tangible';
		}else{
			$tipe_ketokohan = 'intangible';
		}

		$where = '';
		if($id_wilayah != 0){
			if($tipe_wilayah == 'provinsi'){
				$where = "and id_provinces=$id_wilayah";
			}else if($tipe_wilayah == 'kabupaten'){
				$where = "and id_city=$id_wilayah";
			}else if($tipe_wilayah == 'kecamatan'){
				$where = "and id_districts=$id_wilayah";
			}else if($tipe_wilayah == 'kelurahan'){
				$where = "and id_village=$id_wilayah";
			}	
		}

		$this->db->select("a.id_intangible as id,a.name, a.phone, g.nama as pertokohan, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, a.informasi_tambahan,b.alias as partai");
        $this->db->from('intangible a');
        $this->db->join('m_parpol b ', 'a.inclination=b.id_parpol', 'inner');
        $this->db->join('m_provinces c ', 'a.id_provinces=c.id', 'left');
        $this->db->join('m_regencies d ', 'a.id_city=d.id', 'left');
        $this->db->join('m_districts e ', 'a.id_districts=e.id', 'left');
        $this->db->join('m_villages f ','a.id_village=f.id', 'left');
        $this->db->join('m_pertokohan g ', 'a.id_pertokohan=g.id_pertokohan', 'left');
        $this->db->where('a.type', $tipe_ketokohan);
        if($partai == 'Netral'){

        }else if($partai == 'Afiliasi Politik'){

        }else{
        	
        }
        
        $this->db->where('a.is_deleted', '0');
        if($id_wilayah != 0){
        	if($tipe_wilayah == 'provinsi'){
        		$this->db->where('id_provinces', $id_wilayah);
        	}else if($tipe_wilayah == 'kabupaten'){
        		$this->db->where('id_city', $id_wilayah);
        	}else if($tipe_wilayah == 'kecamatan'){
        		$this->db->where('id_districts', $id_wilayah);
        	}else if($tipe_wilayah == 'kelurahan'){
        		$this->db->where('id_village', $id_wilayah);
        	}
		}

		if($partai <> "ALL"){
			if($partai == 'Netral'){
				$this->db->where('a.inclination', 0);
	        }else if($partai == 'Afiliasi Politik'){
	        	$this->db->where('a.inclination !=', 0);
	        }else{
	        	$this->db->where('b.alias', $partai);
	        }
		}

		if($ketokohan != 'ALL'){
			$this->db->where('a.id_pertokohan', $ketokohan);
		}

		$data = $this->db->get()->result();
		
		include APPPATH.'third_party/fs_export_to_excel.php';
		
		//pengaturan nama file
		$namaFile = "Detail Influencer.xls";
		//pengaturan judul data
		// $judul = $tipe_survey;
		//baris berapa header tabel di tulis
		$tablehead = 5;
		//baris berapa data mulai di tulis
		$tablebody = 6;
		//no urut data
		$nourut = 1;
		 
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");
		 
		 
		xlsBOF();
		 
		xlsWriteLabel(0,0,'DETAIL INFLUENCER');  
		// xlsWriteLabel(1,0,"Date: ");  
		// xlsWriteLabel(2,0,"Keyword: Keyword");  
		 
		//header
		$kolomhead = 0;
		xlsWriteLabel($tablehead,$kolomhead++,"Nama");              
		xlsWriteLabel($tablehead,$kolomhead++,"HP");              
		xlsWriteLabel($tablehead,$kolomhead++,"Ketokohan");                          
		xlsWriteLabel($tablehead,$kolomhead++,"Provinsi");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kabupaten");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kecamatan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Kelurahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Informasi Tambahan");              
		xlsWriteLabel($tablehead,$kolomhead++,"Partai");              

		//body
		foreach ($data as $key => $value) { 
			$kolombody = 0;
			xlsWriteLabel($tablebody,$kolombody++,$value->name);              
			xlsWriteLabel($tablebody,$kolombody++,$value->phone);              
			xlsWriteLabel($tablebody,$kolombody++,$value->pertokohan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->prov);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kab);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kec);              
			xlsWriteLabel($tablebody,$kolombody++,$value->kel);              
			xlsWriteLabel($tablebody,$kolombody++,$value->informasi_tambahan);              
			xlsWriteLabel($tablebody,$kolombody++,$value->partai);          
			
			$tablebody++;
			$nourut++;
		}
		xlsEOF();
		exit();

	}
}
