<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maps extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Home_model');
        $this->load->library('datatables');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
       	$this->load->view('template/menu',$data,true);
    }
    
	function index(){
		$this->load->view('maps');
	}

	function navigation(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$data['jum_jeniskelamin'] = $this->db->query("SELECT sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin")->row();
		$data['jum_usia'] = $this->db->query("select SUM(jml_1) as jml_1,SUM(jml_2) as jml_2,SUM(jml_3) as jml_3,SUM(jml_4) as jml_4,SUM(jml_5) as jml_5 FROM sum_umur")->row();
		$data['nasional_2014'] = $this->db->query("SELECT COUNT(*) as jumlah,b.nama from trans_survey a 
									inner join m_pilih b on a.p5=b.id
									where  a.p5 <> 162 and a.p5 <> 163 and a.p5 <> 236
									group by p5;")->result();
		$data['nasional_2014_a'] = $this->db->query("SELECT COUNT(*) as jumlah,b.nama from trans_survey a  inner join m_pilih b on a.p5=b.id group by p5;")->result();
		$data['nasional_2019'] = $this->db->query("SELECT COUNT(*) as jumlah,b.nama from trans_survey a 
									inner join m_pilih b on a.p6=b.id
									where  a.p6 <> 227 and a.p6 <> 225 and a.p6 <> 226 and a.p6 <> 218 and a.p6 <> 206 and a.p6 <> 205 
									group by p6 order by jumlah desc limit 10;")->result();
		$data['nasional_sum_survey'] = $this->db->query("select count(*) as jml from  trans_survey;")->result();
		
		$total14 = 0;
		$total19 = 0;
		$total14a = 0;
		foreach ($data['nasional_2014'] as $key => $value) {
				$total14 += $value->jumlah;
		}
		foreach ($data['nasional_2014_a'] as $key => $value) {
				$total14a += $value->jumlah;
		}
		foreach ($data['nasional_2019'] as $key => $value) {
				$total19 += $value->jumlah;
		}
		$i=0;
		foreach ($data['nasional_2014'] as $key => $value) {
			$data['nasional_2014'][$i]->jumlah = (float)round(($value->jumlah/$total14)*100,2);
		$i++;
		}

		$i=0;
		foreach ($data['nasional_2014_a'] as $key => $value) {
			$data['nasional_2014_a'][$i]->jumlah = (float)round(($value->jumlah/$total14)*100,2);
		$i++;
		}

		$i=0;
		foreach ($data['nasional_2019'] as $key => $value) {
			$data['nasional_2019'][$i]->jumlah = (float)round(($value->jumlah/$total19)*100,2);
		$i++;
		}

		$this->load->view('navigation',$data);
	}
	
	function get_data(){
		
		$wil = $this->input->post('wil');
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$data['fauzan'] = $id;
		$data['fauzan2'] = $nama;
		
		$where_clause="";
		$where_id = "";
		$where_id_prov = "";
		$where_id_area = "";
		$where_id_area2 = "";
		$where_id_prov2 = "";
		if($wil <> "All" && $wil <> ""){ 
			$where_clause = "where ".$wil." = '".$id."'";
			$where_id = " and area = '".$id."'";
			$where_id_area = " and id_area = '".$id."'";
			$where_id_area2 = " and b.id_area = '".$id."'";
			$where_id_prov= " and id_provinces = '".$id."'";
			$where_id_prov2= " and provinsi LIKE '%".$nama."%' ";
			
		}else{
			$where_id_area2 = " and b.id_area = '0'";
		}
		$data['totall'] = $this->db->query("SELECT COUNT(1) as jml from trans_survey where 1=1 ".$where_id_area)->row('jml');
		$data['jum_jeniskelamin'] = $this->db->query("SELECT sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin ".$where_clause)->row();
		$data['jum_usia'] = $this->db->query("select SUM(jml_1) as jml_1,SUM(jml_2) as jml_2,SUM(jml_3) as jml_3,SUM(jml_4) as jml_4,SUM(jml_5) as jml_5 FROM sum_umur ".$where_clause)->row();
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'".$where_id_prov)->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'".$where_id_prov)->row('jml');
		
		
		//NETRAL DAN PILIH
		$result = $this->db->query("SELECT sum(jml) as jml from sum_grafik_kecondongan where alias = 'NETRAL' and tipe='1'
									UNION select sum(jml) as jml from sum_grafik_kecondongan where alias != 'NETRAL' ".$where_id." and tipe='1';")->result();
		$netral = $result[0]->jml;
		$pilih = $result[1]->jml;
		$total = $netral + $pilih;
		$data['jum_netral'] = (float)round(($netral/$total)*100,2);  
		$data['jum_pilih'] = (float)round(($pilih/$total)*100,2);
		
		
		$result = $this->db->query("SELECT a.transId, a.p1, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p1= b.id AND b.jenis='P1' ".$where_id_prov2." GROUP BY a.p1 order by jml desc")->result();
		if($result){
			$i=0;
			$total = 0;
			foreach ($result as $key => $value) {
				$total += $value->jml;
			}
			foreach ($result as $key => $value) {
				
				$vname = $value->nama;
				 if (strlen($vname) > 20){ 
					$vname = substr($vname, 0, 20) . '...';
				 }
				
				$chart[$i]['name'] = $vname;
				$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
				$chart[$i]['color'] = '#'.$this->random_color();
				$i++;
			}	
		}else{
			$chart[0]['name'] = '';
			$chart[0]['points'] = 0;
			$chart[0]['color'] = '';
		}
		$data['chart'] = $chart;
		
		
		$result = $this->db->query("SELECT a.transId, a.p2, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p2= b.id AND b.jenis='P2' ".$where_id_prov2." GROUP BY a.p2 order by jml desc")->result();
		if($result){
			$i=0;
			$total = 0;
			foreach ($result as $key => $value) {
				$total += $value->jml;
			}
			foreach ($result as $key => $value) {
				
				$vname = $value->nama;
				 if (strlen($vname) > 20){ 
					$vname = substr($vname, 0, 20) . '...';
				 }
				
				$chart2[$i]['name'] = $vname;
				$chart2[$i]['points'] = (float)round(($value->jml/$total)*100,2);
				$chart2[$i]['color'] = '#'.$this->random_color();
				$i++;
			}	
		}else{
			$chart2[0]['name'] = '';
			$chart2[0]['points'] = 0;
			$chart2[0]['color'] = '';
		}
		$data['chart2'] = $chart2;
		
		$result = $this->db->query("SELECT a.id_paslon, a.nama_kepala,a.nama_wakil,a.area,a.id_area,COUNT(1) AS jml, d.color 
									FROM m_paslon a, trans_survey b , m_pengusung c, m_parpol d
									WHERE a.id_paslon = b.id_paslon 
									AND a.`id_paslon` = c.`id_paslon`
									AND c.`id_parpol` = d.`id_parpol`
									".$where_id_area2."
									GROUP BY b.id_paslon ORDER BY jml DESC")->result();
		
		if($result)
		{
			$i=0;
			$total = 0;
			foreach ($result as $key => $value) {
				$total += $value->jml;
			}
			foreach ($result as $key => $value) {
				
				$vname = $value->nama_kepala;
				 if (strlen($vname) > 20){ 
					$vname = substr($vname, 0, 20) . '...';
				 }
				$cek = $this->db->query("SELECT * from m_pengusung where id_parpol = 3 and id_paslon=$value->id_paslon")->row();
				if($cek){
					$color = '#da251c';
				}else{
					$color =  $value->color;
 				}
				$chart3[$i]['name'] = $vname;
				$chart3[$i]['points'] = (float)round(($value->jml/$total)*100,2);
				$chart3[$i]['color'] = $color;
				$i++;
			}
		}else{

			$chart3[0]['name'] = '';
			$chart3[0]['points'] = 0;
			$chart3[0]['color'] = '';
		}
			
		$data['chart3'] = $chart3;

		if (strlen($id)==2) {
			$result = $this->db->query("SELECT a.p7 as nama , count(*) as jml FROM trans_survey a where (a.p7='netral'||a.p7='Belum Tahu' || a.p7='rahasia' || a.p7='belum menentukan' || a.p7='tidak tahu' || a.p7='tidak tau' || a.p7='belum tau' || a.p7='LIHAT NANTI' || a.p7='TIDAK MENJAWAB' || a.p7='BELUM MENENTUKAN PILIHAN' || a.p7='lihat visi misi nya' || a.p7='belum memilih' ) GROUP BY a.p7 ORDER BY jml DESC")->result();
		} else if (strlen($id)>2) {
			$result = $this->db->query("SELECT a.p8 as nama, count(*) as jml FROM trans_survey a where (a.p8='netral'||a.p8='Belum Tahu' || a.p8='rahasia' || a.p8='belum menentukan' || a.p8='tidak tahu' || a.p8='tidak tau' || a.p8='belum tau' || a.p8='LIHAT NANTI' || a.p8='TIDAK MENJAWAB' || a.p8='BELUM MENENTUKAN PILIHAN' || a.p8='lihat visi misi nya' || a.p8='belum memilih' ) GROUP BY a.p8 ORDER BY jml DESC")->result();
		}

		if ($result) {
			$i=0;
			$total = 0;
			foreach ($result as $key => $value) {
				$total += $value->jml;
			}

			foreach ($result as $key => $value) {
				$vname = $value->nama;
				$chart3_[$i]['name'] = $vname;
				$chart3_[$i]['points'] = (float)round(($value->jml/$total)*100,2);
				$chart3_[$i]['color'] = '#'.$this->random_color();
				$i++;
			}
		} else{
			$chart3_[0]['name'] = '';
			$chart3_[0]['points'] = 0;
			$chart3_[0]['color'] = '';
		}
			
		$data['chart3_'] = $chart3_;
		

		//chart 4 partai yang akan dipilih 2014
		$chart2014[0]['nama'] ='';
		$chart2014[0]['points'] = 0;
		$chart2014[0]['color'] = '';
		$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama, c.color from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND (b.nama NOT LIKE '%rahasia%' AND b.nama NOT LIKE '%belum%' AND b.nama NOT LIKE '%tidak%'AND b.nama NOT LIKE '%lupa%' ) inner join m_parpol c on c.alias=b.nama ".$where_id_prov2." GROUP BY a.p3 order by jml desc")->result();
		
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		
			
		foreach ($result as $key => $value) {
			
			 $vname = $value->nama;
			 if (strlen($vname) > 20){ 
				$vname = substr($vname, 0, 20) . '...';
			 }
			 
			$chart2014[$i]['nama'] = $vname;
			$chart2014[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart2014[$i]['color'] = $value->color;
			$chart2014[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart2014'] = $chart2014;
		
		
		$chart2014_[0]['nama'] ='';
		$chart2014_[0]['points'] = 0;
		$chart2014_[0]['color'] = '';
		// partai yang akan dipilih 2014 lain2
		$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND (a.p3=40 OR a.p3=41 OR a.p3=42 OR a.p3=43 OR a.p3=44 OR a.p3=45 OR a.p3=235) ".$where_id_prov2." GROUP BY a.p3 order by jml desc")->result();
		
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		
			
		foreach ($result as $key => $value) {
			
			 $vname = $value->nama;
			 if (strlen($vname) > 20){ 
				$vname = substr($vname, 0, 20) . '...';
			 }
			 
			$chart2014_[$i]['nama'] = $vname;
			$chart2014_[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			if ($value->nama=='PDI-P') {
				$chart2014_[$i]['color'] = '#da251c';
			} else if($value->nama=='NASDEM') {
				$chart2014_[$i]['color'] = '#f6ab02';
			} else if($value->nama=='PKB') {
				$chart2014_[$i]['color'] = '#00893a';
			} else if($value->nama=='PKS') {
				$chart2014_[$i]['color'] = '#000000';
			} else if($value->nama=='GOLKAR') {
				$chart2014_[$i]['color'] = '#ffed00';
			} else if($value->nama=='GERINDRA') {
				$chart2014_[$i]['color'] = '#f8f2f2';
			} else if($value->nama=='DEMOKRAT') {
				$chart2014_[$i]['color'] = '#155ca8';
			} else if($value->nama=='PAN') {
				$chart2014_[$i]['color'] = '#000080';
			} else if($value->nama=='PPP') {
				$chart2014_[$i]['color'] = '#00923f';
			} else if($value->nama=='HANURA') {
				$chart2014_[$i]['color'] = '#f08519';
			} else if($value->nama=='PBB') {
				$chart2014_[$i]['color'] = '#044d32';
			} else if($value->nama=='PKPI') {
				$chart2014_[$i]['color'] = '#e62129';
			} else if($value->nama=='PERINDO') {
				$chart2014_[$i]['color'] = '#28166f';
			} else {
				$chart2014_[$i]['color'] = '#'.$this->random_color();
			}
			$chart2014_[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart2014_'] = $chart2014_;
		
		
		
		
		
		
		//chart 4 partai yang akan dipilih 2019
		$chart2019[0]['nama'] ='';
		$chart2019[0]['points'] = 0;
		$chart2019[0]['color'] = '';
		$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND (b.nama NOT LIKE '%rahasia%' AND b.nama NOT LIKE '%belum%' AND b.nama NOT LIKE '%tidak%' AND b.nama NOT LIKE '%lupa%' ) ".$where_id_prov2." GROUP BY a.p4 order by jml desc")->result();
		
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		
			
		foreach ($result as $key => $value) {
			
			 $vname = $value->nama;
			 if (strlen($vname) > 20){ 
				$vname = substr($vname, 0, 20) . '...';
			 }
			 
			$chart2019[$i]['nama'] = $vname;
			$chart2019[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			if ($value->nama=='PDI-P') {
				$chart2019[$i]['color'] = '#da251c';
			} else if($value->nama=='NASDEM') {
				$chart2019[$i]['color'] = '#f6ab02';
			} else if($value->nama=='PKB') {
				$chart2019[$i]['color'] = '#00893a';
			} else if($value->nama=='PKS') {
				$chart2019[$i]['color'] = '#000000';
			} else if($value->nama=='GOLKAR') {
				$chart2019[$i]['color'] = '#ffed00';
			} else if($value->nama=='GERINDRA') {
				$chart2019[$i]['color'] = '#f8f2f2';
			} else if($value->nama=='DEMOKRAT') {
				$chart2019[$i]['color'] = '#155ca8';
			} else if($value->nama=='PAN') {
				$chart2019[$i]['color'] = '#000080';
			} else if($value->nama=='PPP') {
				$chart2019[$i]['color'] = '#00923f';
			} else if($value->nama=='HANURA') {
				$chart2019[$i]['color'] = '#f08519';
			} else if($value->nama=='PBB') {
				$chart2019[$i]['color'] = '#044d32';
			} else if($value->nama=='PKPI') {
				$chart2019[$i]['color'] = '#e62129';
			} else if($value->nama=='PERINDO') {
				$chart2019[$i]['color'] = '#28166f';
			} else {
				$chart2019[$i]['color'] = '#'.$this->random_color();
			}
			$chart2019[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart2019'] = $chart2019;
		
		//chart 4 partai yang akan dipilih 2019 lain2
		$chart2019_[0]['nama'] ='';
		$chart2019_[0]['points'] = 0;
		$chart2019_[0]['color'] = '';
		$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND  (a.p4=60 OR a.p4=61 OR a.p4=62 OR a.p4=63 OR a.p4=64 OR a.p4=65) ".$where_id_prov2." GROUP BY a.p4 order by jml desc")->result();
		
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		
			
		foreach ($result as $key => $value) {
			
			 $vname = $value->nama;
			 if (strlen($vname) > 20){ 
				$vname = substr($vname, 0, 20) . '...';
			 }
			 
			$chart2019_[$i]['nama'] = $vname;
			$chart2019_[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			if ($value->nama=='PDI-P') {
				$chart2019_[$i]['color'] = '#da251c';
			} else if($value->nama=='NASDEM') {
				$chart2019_[$i]['color'] = '#214aa0';
			} else if($value->nama=='PKB') {
				$chart2019_[$i]['color'] = '#00893a';
			} else if($value->nama=='PKS') {
				$chart2019_[$i]['color'] = '#000000';
			} else if($value->nama=='GOLKAR') {
				$chart2019_[$i]['color'] = '#fbce00';
			} else if($value->nama=='GERINDRA') {
				$chart2019_[$i]['color'] = '#d53c32';
			} else if($value->nama=='DEMOKRAT') {
				$chart2019_[$i]['color'] = '#2899d5';
			} else if($value->nama=='PAN') {
				$chart2019_[$i]['color'] = '#1c52a0';
			} else if($value->nama=='PPP') {
				$chart2019_[$i]['color'] = '#00923f';
			} else if($value->nama=='HANURA') {
				$chart2019_[$i]['color'] = '#ef7f1a';
			} else if($value->nama=='PBB') {
				$chart2019_[$i]['color'] = '#016436';
			} else if($value->nama=='PKPI') {
				$chart2019_[$i]['color'] = '#db261b';
			} else if($value->nama=='PERINDO') {
				$chart2019_[$i]['color'] = '#22287c';
			} else {
				$chart2019_[$i]['color'] = '#'.$this->random_color();
			}
			$chart2019_[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart2019_'] = $chart2019_;
		
 
		
		//partai by provinsi
	
		$data['graph_bawah'] = $this->db->query("SELECT area, alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' ".$where_id." group by alias order by jml_ desc")->result();
		$total_data =0;
		$total_lain =0;
		$k=0;
		
		//echo json_encode($data['graph_bawah']);
		//return;
		
		foreach ($data['graph_bawah'] as $tes) {
			$total_data += $tes->jml_;
			if($k >= 6){
				$total_lain += $tes->jml_;
			}else{
				$data['new_graph'][$k]['poin'] = (int)$tes->jml_;
				$data['new_graph'][$k]['name'] = $tes->alias;
				$data['new_graph'][$k]['color'] = $tes->color;
			}
			$k++;
		}
		if($k >= 6){
			$data['new_graph'][6]['poin'] = $total_lain;
			$data['new_graph'][6]['name'] = 'Lain-Lain';
			$data['new_graph'][6]['color'] = '#fffff';
			$k=7;
		}
			
			
		$data['preferensi'] = $this->db->query("SELECT c.alias AS partai,
												SUM(CASE WHEN b.id = a.p3 THEN 1 ELSE 0 END)a14,
												SUM(CASE WHEN b.id = a.p4 THEN 1 ELSE 0 END)a19
												FROM m_parpol c, m_pilih b, trans_survey a
												WHERE c.alias = b.nama ".$where_id_area."
												GROUP BY c.alias HAVING (a14 > 0 OR a19 > 0) ORDER BY a14 DESC LIMIT 10")->result();
												
		$data['preferensi_jml'] = $this->db->query("SELECT c.alias AS partai,
												SUM(CASE WHEN b.id = a.p3 THEN 1 ELSE 0 END)as a14,
												SUM(CASE WHEN b.id = a.p4 THEN 1 ELSE 0 END)as a19
												FROM m_parpol c, m_pilih b, trans_survey a
												WHERE c.alias = b.nama ".$where_id_area."
												GROUP BY c.alias HAVING (a14 > 0 OR a19 > 0) ORDER BY a14 DESC LIMIT 10")->result();
												
												
		$sum14 = 0;
		$sum19 = 0;
		foreach ($data['preferensi'] as $key => $value) {
			$sum14 = $sum14+$value->a14;
			$sum19 = $sum19+$value->a19;
		}
		$i=0;
			foreach ($data['preferensi'] as $key => $value) {
				$data['preferensi'][$i]->a14 = (float)round(($data['preferensi'][$i]->a14/$sum14)*100,2);
				$data['preferensi'][$i]->a19 = (float)round(($data['preferensi'][$i]->a19/$sum19)*100,2);
				$i++;
			}
		$data['partai'] = $this->db->get('m_parpol')->result();
		$string = '';
		$jum_partai = count($data['partai']);
		$i=0;
		
		foreach ($data['partai'] as $key => $value) {
			$alias = str_replace("-","_",$value->alias);
			$alias = str_replace(" ","_",$alias);
			if($i==$jum_partai-1){
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a14 ELSE 0 END)".$alias."_14,";
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a19 ELSE 0 END)".$alias."_19";
					
			}else{
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a14 ELSE 0 END)".$alias."_14,";
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a19 ELSE 0 END)".$alias."_19,";
			}
			$i++;
		}

		$data['konsisten'] = $this->db->query("SELECT p7,
					".$string."
					FROM(
					SELECT * FROM (
					SELECT c.alias AS partai,p7, -- provinsi,kotaKabupaten,kecamatan,
					SUM(CASE WHEN b.id = a.p3 THEN 1 ELSE 0 END) AS a14,
					SUM(CASE WHEN b.id = a.p4 THEN 1 ELSE 0 END) AS a19
					FROM m_parpol c,m_pilih b,trans_survey a
					WHERE c.alias = b.nama
					AND a.id_paslon > 0
					".$where_id_area."
					GROUP BY c.alias,p7 -- ,provinsi,kotaKabupaten,kecamatan
					)v WHERE a19 <> 0
					)s GROUP BY p7")->result();
					
					
		
		//ketokohan
		$chart_ketokohan[0]['nama'] ='';
		$chart_ketokohan[0]['points'] = 0;
		$chart_ketokohan[0]['color'] = '';
		$result = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a inner join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where 1=1 ".$where_id_prov." and a.is_deleted='0' group by a.id_pertokohan order by jml desc")->result();
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart_ketokohan[$i]['name'] = $value->nama;
			$chart_ketokohan[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart_ketokohan[$i]['color'] = '#'.$this->random_color();
			$i++;
		}
		$data['chart_ketokohan'] = $chart_ketokohan;
		$data['ketokohan_total'] = $total;
		

		
		//SIMULASI
		$data['simulasi'] = $this->simulasi($id);
					
					
		echo json_encode($data);
		
	}

	function get_tree_kabupaten($id){
		$result = $this->db->get_where('m_regencies', array('province_id' => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['lazy'] = TRUE;
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = 'kabupaten';
			$i++;
		}
		echo json_encode($data);
	}

	function get_tree_kecamatan($id){
		$result = $this->db->get_where('m_districts', array('regency_id' => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['lazy'] = TRUE;
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = 'kecamatan';
			$i++;
		}
		echo json_encode($data);
	}

	function get_tree_kelurahan($id){
		$result = $this->db->get_where('m_villages', array('district_id' => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = 'kelurahan';
			$i++;
		}
		echo json_encode($data);
	}

	function get_kabupaten($id){
		$result = $this->db->get_where('m_regencies', array('province_id' => $id))->result();
		$string = '<option value="all">All</option>';
		foreach ($result as $key => $value) {
			$string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
		}
		echo $string;
	}

	function get_kecamatan($id){
		$result = $this->db->get_where('m_districts', array('regency_id' => $id))->result();
		$string = '<option value="all">All</option>';
		foreach ($result as $key => $value) {
			$string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
		}
		echo $string;
	}

	function get_information_($id,$tipe){
		if($tipe == 'provinsi'){
			if($id == 0){
				$data['link'] = 'INDONESIA';
			}else{
				$result = $this->db->get_where('m_provinces', array('id' => $id))->row();
				if($result->name == 'BALI'){
					$result->name = 'BALI, INDONESIA';
				}
				$data['link'] = $result->name;
				$data['paslon'] = $this->table_paslon($result->id); 
			}
			
			
		}else if($tipe == 'kabupaten'){
			$result = $this->db->query("SELECT a.id, a.name as kab, b.name as prov from m_regencies a inner join m_provinces b on b.id=a.province_id where a.id = $id")->row();
			$data['link'] = $result->kab.', '.$result->prov;
			$data['paslon'] = $this->table_paslon($result->id);
		}else if($tipe == 'kecamatan'){
			$result = $this->db->query("SELECT a.id, a.name as kec, b.name as kab, c.name as prov from m_districts a inner join m_regencies b on b.id=a.regency_id inner join m_provinces c on c.id=b.province_id where a.id=$id")->row();
			$data['link'] = $result->kec.', '.$result->kab.', '.$result->prov;
			$data['paslon'] = $this->table_paslon($result->id);
		}else if($tipe == 'kelurahan'){
			$result = $this->db->query("SELECT a.id, a.name as kel, b.name as kec, c.name as kab, d.name as prov from m_villages a inner join m_districts b on b.id=a.district_id inner join m_regencies c on c.id=b.regency_id inner join m_provinces d on d.id=c.province_id where a.id=$id")->row();
			$data['link'] = $result->kel.', '.$result->kec.', '.$result->kab.', '.$result->prov;
			$data['paslon'] = $this->table_paslon($result->id);
		}	
		echo json_encode($data);
	}

	function table_paslon($area){
		$data = $this->db->get_where("m_paslon", array('id_area' => $area))->result();
		$string = '';
		if($data){
			$string .= '<h6 class="element-header">Wilayah : '.$data[0]->area.'</h6>
	                		<div class="table-responsive">
	                    		<table class="table table-lightborder">
	                            	<thead>
	                                	<tr>
	                                    	<th>Pasangan Calon</th>
	                                    	<th>Foto</th>
	                                    	<th>Pengusung</th>
	                                	</tr>
	                            	</thead>
	                            	<tbody>';
	    	foreach ($data as $key => $value) {
	    		$pengusung = $this->db->query("select * from m_pengusung a
									inner join m_parpol b on a.id_parpol=b.id_parpol
									where id_paslon = $value->id_paslon;")->result();
	    		$string .= '<tr>
								<td>'.$value->nama_kepala.' - '.$value->nama_wakil.'</td>
								<td><img src="'.base_url().'img/paslon/'.$value->image_ketua.'"  height="42" width="42"><img src="'.base_url().'img/paslon/'.$value->image_wakil.'"  height="42" width="42"> </td>';
				$string .= '<td>';
				foreach ($pengusung as $value2) {
					$string .= '<img src="'.base_url().'img/logo_parpol/'.$value2->picture.'"  height="42" width="42">';
				}
				
				$string .= '</td>';
				$string .='</tr>';
	    	}
	    	$string .='</tbody></table></div>';
		}
		
    	return $string;
	}
	function table_provinsi(){
		$this->datatables->select("id,name,alias");
		$this->datatables->add_column('button', '<button data-id=$1 class="mr-2 mb-2 btn btn-primary btn-sm btn-search" type="button">Search</button>', 'id');
        $this->datatables->from('m_provinces');

        echo $this->datatables->generate();
	}
	function random_color_part() {
    	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	function random_color() {
	    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

	function marker(){
		$provinsi = $this->db->get('m_provinces')->result();
		foreach ($provinsi as $key => $value) {
			$data[$value->id]['name'] = $value->name;
			$data[$value->id]['id'] = $value->id;
			$data[$value->id]['latitude'] = $value->latitude;
			$data[$value->id]['longitude'] = $value->longitude;
			$data[$value->id]['logo'] = $value->logo;
			$data[$value->id]['jum_tangible'] = 0;
			$data[$value->id]['jum_intangible'] = 0;
		}
		$result = $this->db->query("SELECT b.*, sum(a.intangible) as jum_intangible, sum(a.tangible) as jum_tangible 
									from (SELECT is_deleted, id_provinces, IF(type = 'intangible',1,0) as intangible, IF(type = 'tangible',1,0) as tangible from intangible) as a 
									inner join m_provinces as b on a.id_provinces = b.id
									where a.is_deleted = '0'
									group by a.id_provinces;")->result();

		foreach ($result as $key => $value) {
			$data[$value->id]['jum_tangible'] = $value->jum_tangible;
			$data[$value->id]['jum_intangible'] = $value->jum_intangible;
		}
		echo json_encode($data);
	}

	function graph($id_provinsi){
		$tangible = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a inner join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where a.id_provinces='$id_provinsi' and a.type='tangible' and a.is_deleted = '0' group by a.id_pertokohan")->result();
		$intangible = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a inner join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where a.id_provinces='$id_provinsi' and a.type='intangible' and a.is_deleted = '0' group by a.id_pertokohan")->result();
		$partai = $this->db->query("SELECT a.inclination, b.alias, b.color, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol where a.id_provinces='$id_provinsi' and a.is_deleted = '0' and a.inclination <> 0 group by a.inclination")->result();
		
		$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and id_provinces='$id_provinsi'")->row('jml');
		$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and id_provinces='$id_provinsi'")->row('jml');
		$total_semua = $data['jum_netral']+$data['jum_pilih'];
		$data['persen_jum_netral'] = round(($data['jum_netral']/$total_semua)*100,2);
		$data['persen_jum_pilih'] = round(($data['jum_pilih']/$total_semua)*100,2);
		
		$data['table_condong_umum'] =  '<tr>
		                                	<td class="nowrap">Netral</td>
		                                	<td>'.$data['jum_netral'].'</td>
		                                	<td>'.$data['persen_jum_netral'].' %</td>
		                  				</tr>
		                  				<tr>
		                                	<td class="nowrap">Condong</td>
		                                	<td>'.$data['jum_pilih'].'</td>
		                                	<td>'.$data['persen_jum_pilih'].' %</td>
		                  				</tr>';
		$string_tangible = '';
		$string_intangible = '';
		$string_partai='';
		$total_tangible = 0;
		$total_intangible = 0;
		$total_partai = 0;
		
		//GET JUMLAH
		foreach ($tangible as $key => $value){
			$total_tangible += $value->jml;
		}
		foreach ($intangible as $key => $value){
			$total_intangible += $value->jml;
		}

		foreach ($partai as $key => $value){
			$total_partai += $value->jml;
		}

		$i=0;
		if($tangible != NULL){
			foreach ($tangible as $key => $value){
				$string_tangible .= '<tr>
	                                	<td class="nowrap">'.$value->nama.'</td>
	                                	<td>'.$value->jml.'</td>
	                                	<td>'. round(($value->jml/$total_tangible)*100,2).' %</td>
	                  				</tr>';
	            $chart[$i]['name'] = $value->nama;
				$chart[$i]['points'] = (float)round(($value->jml/$total_tangible)*100,2);
				$chart[$i]['color'] = '#'.$this->random_color();
				$i++;

			}
			$data['tangible'] = $string_tangible;
			$data['tangible_chart'] = $chart;
		}
		
		$i=0;
		if($intangible != NULL){
			foreach ($intangible as $key => $value){
				$string_intangible .= '<tr>
	                                	<td class="nowrap">'.$value->nama.'</td>
	                                	<td>'.$value->jml.'</td>
	                                	<td>'. round(($value->jml/$total_intangible)*100,2).' %</td>
	                  				</tr>';
	            $chart2[$i]['name'] = $value->nama;
				$chart2[$i]['points'] = (float)round(($value->jml/$total_intangible)*100,2);
				$chart2[$i]['color'] = '#'.$this->random_color();
				$i++;
			}
			$data['intangible'] = $string_intangible;
			$data['intangible_chart'] = $chart2;
		}
		




		if($partai != NULL){
			$i=0;
			foreach ($partai as $key => $value) {
				$string_partai .= '<tr>
                                	<td class="nowrap">'.$value->alias.'</td>
                                	<td>'.$value->jml.'</td>
                                	<td>'. round(($value->jml/$total_partai)*100,2).' %</td>
                  				</tr>';

				$chart3[$i]['name'] = $value->alias;
				$chart3[$i]['points'] = (int)$value->jml;
				$chart3[$i]['color'] = $value->color;
				$i++;
			}
			$data['partai'] = $string_partai;
			$data['partai_chart'] = $chart3;
		}
		
		echo json_encode($data);
	}
	function simulasi($id_area){
		
	  	$paslon = $this->db->query("SELECT * FROM m_paslon where id_area = '$id_area' ")->result();
	  	$result = $this->db->query("SELECT * FROM (select a.id_area,a.id_parpol,b.kode_parpol,b.alias,b.picture, a.id_paslon ,c.nama_kepala,c.image_ketua,c.nama_wakil,c.image_wakil  from m_pengusung a inner join m_parpol b on a.id_parpol = b.id_parpol inner join m_paslon c on a.id_paslon = c.id_paslon where c.id_area = '$id_area') a inner join m_jumlah_kursi e on a.id_area = e.id_area")->result_array();
	    $string = '';
	    if(!$paslon || ! $result){

	    }else{
	    	$i=0;
	    	$string .='<thead><tr><th>Partai</th>';
	    	foreach ($paslon as $key => $value) {
	    		
	    		$string .=	'<th>
                            	<img src="'.base_url().'/img/paslon/'.$value->image_ketua.'" height="42" width="42">        
								<img src="'.base_url().'/img/paslon/'.$value->image_wakil.'" height="42" width="42">				
							</th>';
	    	}
	    	$string .= '</tr></thead>';
            $string .= '<tbody>';             
		    foreach ($result as $key => $value) {
		    	$string .= '<tr>';
		    	$string .= '<td><img src="'.base_url().'/img/logo_parpol/'.$value['picture'].'"  height="42" width="42"> '.$value['alias'].'</td>';
		      	$data[$i]['parpol'] = $value['kode_parpol'];
		      $j=1;
		      foreach ($paslon as $value2) {
		        $name = 'paslon'.$j;
		        $name2 = 'jum'.$j;
		        $id = $value2->id_paslon;

		        $data[$i][$name] = $value2->nama_kepala;
		        if(($data[$i]['parpol']==$value['kode_parpol']) and ($id==$value['id_paslon'])){
		          if(isset($result[0][$value['kode_parpol']])) {
		            $data[$i][$name2] = $result[0][$value['kode_parpol']];
		          }else{
		            $data[$i][$name2] = 0;
		          } 
		        }else{
		          $data[$i][$name2] = 0;
		        }
		        $string .= '<td>'.$data[$i][$name2].'</td>';
		        $j++;
		      }
		      $i++;
		      $string .= '</tr>';
		    }
		    $z = 1;
		    foreach ($paslon as $key => $value) {
		    	$jum[$z] = 0;
		    $z++;
		    }
		    $y=0;
		    foreach ($data as $key => $value) {
		    	$z=1;
		    	foreach ($paslon as $key => $value2) {
		    		$nama = 'jum'.$z;
		    		$jum[$z] = $jum[$z]+$data[$y][$nama];
		    		$z++;
		    	}
		    $y++;
		    }
		    $string .= '<tr><td>JUMLAH</td>';
		    $z=1;
		    foreach ($paslon as $key => $value) {
		    	$string .='<td>'.$jum[$z].'</td>';
		    	$z++;
		    }
		    $string .= '</tr>';
		    $string .= '</tbody>';
	    }
	    // var_dump($string);
		return $string;
	}

	function get_top_4($pertokohan,$area,$tipe){
		if($tipe == 'provinsi'){
			$vtipe = 'id_provinces';
		}else if($tipe == 'kabupaten'){
			$vtipe = 'id_city';
		}else if($tipe == 'kecamatan'){
			$vtipe = 'id_districts';
		}else if($tipe == 'kelurahan'){
			$vtipe = 'id_village';
		}else{
			$vtipe = 1;
			$area = 1;
		}

		$pertokohan = urldecode($pertokohan);
		$id_pertokohan = $this->db->get_where('m_pertokohan', array('nama' => $pertokohan))->row('id_pertokohan');
		$data['jum_netral'] = 0;
		$data['jum_pilih'] = 0;
		$chart[0]['name'] = '';
		$chart[0]['points'] = 0;
		$chart[0]['color'] = '';
		$chart[0]['bullet'] = '';

		if($pertokohan == 'all'){
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where a.is_deleted='0' and a.inclination <> 0 and $vtipe='$area' group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and $vtipe='$area'")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and $vtipe='$area'")->row('jml');	
		}else{
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where id_pertokohan =$id_pertokohan and a.is_deleted='0' and a.inclination <> 0 and $vtipe='$area' group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and id_pertokohan =$id_pertokohan and $vtipe='$area'")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and id_pertokohan =$id_pertokohan and $vtipe='$area'")->row('jml');
		}
		
		
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		$total_semua = $data['jum_netral']+$data['jum_pilih'];
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->alias;
			$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart[$i]['color'] = $value->color;
			$chart[$i]['bullet'] = site_url('img/logo_parpol/').$value->picture;
			$i++;
		}
		$data['jum_netral'] = round(($data['jum_netral']/$total_semua)*100,2);
		$data['jum_pilih'] = round(($data['jum_pilih']/$total_semua)*100,2);
		$data['chart'] = $chart;
		$data['ketokohan_total2'] = $total_semua;
		echo json_encode($data);
	}

	function detail_influencer($alias_partai, $pertokohan,$area, $tipe ){
		if($tipe == 'provinsi'){
			$vtipe = 'a.id_provinces';
		}else if($tipe == 'kabupaten'){
			$vtipe = 'a.id_city';
		}else if($tipe == 'kecamatan'){
			$vtipe = 'a.id_districts';
		}else if($tipe == 'kelurahan'){
			$vtipe = 'a.id_village';
		}else{
			$vtipe = 1;
			$area = 1;
		}
	
		if($pertokohan == 'all'){
			$data['data'] = $this->db->query("SELECT a.name, a.phone, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, g.nama as pertokohan, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan
						where b.alias='$alias_partai' and $vtipe='$area' and a.is_deleted = '0';")->result();
		}else{
			$pertokohan = urldecode($pertokohan);
			$id_pertokohan = $this->db->get_where('m_pertokohan', array('nama' => $pertokohan))->row('id_pertokohan');
			$data['data'] = $this->db->query("SELECT a.name, a.phone, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, g.nama as pertokohan, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan
						where b.alias='$alias_partai' and a.id_pertokohan=$id_pertokohan and $vtipe='$area' and a.is_deleted = '0';")->result();
		}
		
		echo json_encode($data);
	}

	function detail_influencer2($alias_partai, $area, $tipe ){
		if($tipe == 'provinsi'){
			$vtipe = 'a.id_provinces';
		}else if($tipe == 'kabupaten'){
			$vtipe = 'a.id_city';
		}else if($tipe == 'kecamatan'){
			$vtipe = 'a.id_districts';
		}else if($tipe == 'kelurahan'){
			$vtipe = 'a.id_village';
		}else{
			$vtipe = 1;
			$area = 1;
		}
	
		$data['data'] = $this->db->query("SELECT a.name, a.phone, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, g.nama as pertokohan, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan
						where b.alias='$alias_partai' and $vtipe='$area' and a.is_deleted = '0';")->result();
		
		echo json_encode($data);
	}
	function test(){
		$partai = $this->db->get('m_parpol')->result();
		$string = '';
		$jum_partai = count($partai);
		$i=0;
		foreach ($partai as $key => $value) {
			$alias = str_replace("-","_",$value->alias);
			$alias = str_replace(" ","_",$alias);
			if($i==$jum_partai-1){
				
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a19 ELSE 0 END)".$alias."_19,";
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a14 ELSE 0 END)".$alias."_14";	
			}else{
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a19 ELSE 0 END)".$alias."_19,";
				$string .= "SUM(CASE WHEN partai = '".$value->alias."' THEN a14 ELSE 0 END)".$alias."_14,";
			}
			$i++;
		}
		$query = "SELECT p7,
					".$string."
					FROM(
					SELECT * FROM (
					SELECT c.alias AS partai,p7, -- provinsi,kotaKabupaten,kecamatan,
					SUM(CASE WHEN b.id = a.p3 THEN 1 ELSE 0 END) AS a14,
					SUM(CASE WHEN b.id = a.p4 THEN 1 ELSE 0 END) AS a19
					FROM m_parpol c,m_pilih b,trans_survey a
					WHERE c.alias = b.nama
					AND a.id_paslon > 0
					GROUP BY c.alias,p7 -- ,provinsi,kotaKabupaten,kecamatan
					)v WHERE a19 <> 0
					)s GROUP BY p7";
		$data = $this->db->query($query)->result();
		echo json_encode($data);


	}
}
