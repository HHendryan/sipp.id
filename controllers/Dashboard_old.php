<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Home_model');
        $this->load->library('datatables');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
       	$this->load->view('template/menu',$data,true);
    }
    
	function index(){
		$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
		$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
		$data['provinsi']=$this->db->get('m_provinces')->result();
		$this->load->view('dashboard',$data);
	}

	function second(){
		$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
		$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
		$data['jum_jeniskelamin'] = $this->db->query("SELECT sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin")->row();
		$data['jum_usia'] = $this->db->query("select SUM(jml_1) as jml_1,SUM(jml_2) as jml_2,SUM(jml_3) as jml_3,SUM(jml_4) as jml_4,SUM(jml_5) as jml_5 FROM sum_umur")->row();
		$data['jum_status'] = $this->db->query("SELECT SUM(jml_kawin) as jml_kawin, SUM(jml_belumkawin) as jml_belumkawin FROM sum_status")->row();

		//NETRAL DAN PILIH
		$result = $this->db->query("SELECT sum(jml) as jml from sum_grafik_kecondongan where alias = 'NETRAL' and tipe='1'
									UNION select sum(jml) as jml from sum_grafik_kecondongan where alias != 'NETRAL' and tipe='1';")->result();
		$netral = $result[0]->jml;
		$pilih = $result[1]->jml;
		$total = $netral + $pilih;
		$data['jum_netral'] = (float)round(($netral/$total)*100,2);  
		$data['jum_pilih'] = (float)round(($pilih/$total)*100,2);

		//DETAIL PARTAI
		$data['influence_detail'] = $this->db->query("SELECT alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' group by alias order by jml_ desc limit 10")->result();
		$total_partai = 0;
		$i=0;
		foreach ($data['influence_detail'] as $key => $value) {
			$total_partai += $value->jml_;
		}
		for ($i=0; $i < 10; $i++) { 
			$data['influence_detail'][$i]->jml_ = (float)round(($data['influence_detail'][$i]->jml_/$total_partai)*100,2);
		}

		//EVERY PROVINCE PARTAI
		$data['province'] = $this->db->query("SELECT area from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' group by area")->result();
		$j=0;
		foreach ($data['province'] as $key => $value) {
			$area = $value->area;
			$name = $this->db->query("SELECT name from m_provinces where id='$area'")->row('name');
			$data['graph_bawah'][$name] = $this->db->query("SELECT area, alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' and area='$area' group by alias order by jml_ desc")->result();
			$total_data =0;
			$total_lain =0;
			$k=0;
			foreach ($data['graph_bawah'][$name] as $tes) {
				$total_data += $tes->jml_;
				if($k >= 6){
					$total_lain += $tes->jml_;
				}else{
					$data['new_graph'][$name][$k]['jumlah'] = $tes->jml_;
					$data['new_graph'][$name][$k]['alias'] = $tes->alias;
					$data['new_graph'][$name][$k]['color'] = $tes->color;
				}
				$k++;
			}
			if($k >= 6){
				$data['new_graph'][$name][6]['jumlah'] = $total_lain;
				$data['new_graph'][$name][6]['alias'] = 'Lain-Lain';
				$data['new_graph'][$name][6]['color'] = '#fffff';
				$k=7;
			}
			

			for ($i=0; $i < $k; $i++) { 
				$data['new_graph'][$name][$i]['jumlah'] = (int)round(($data['new_graph'][$name][$i]['jumlah']/$total_data)*100,2);
			}

			$j++;
		}
		$data['jum_province'] = $j;
		$this->load->view('dashboard_second',$data);
	}

	function get_pertokohan($type){
		$data = $this->db->get_where('m_pertokohan', array('type' => $type))->result();
		$string = '<option value="ALL">ALL</option>';
		$string .= '<option value="SEMUA">Kecondongan Politik Nasional</option>';
		foreach ($data as $key => $value) {
			$string .= '<option value="'.$value->id_pertokohan.'">'.$value->nama.'</option>';
		}
		echo $string;
	}

	function random_color_part() {
    	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	function random_color() {
	    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

	function get_graph_bawah($id_provinsi){
		if($id_provinsi == 'ALL'){
			$result = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a inner join m_pertokohan b on a.id_pertokohan=b.id_pertokohan group by a.id_pertokohan order by jml desc")->result();
		}else{
			$result = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a inner join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where id_provinces=$id_provinsi group by a.id_pertokohan order by jml desc")->result();
		}
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart[$i]['color'] = '#'.$this->random_color();
			$i++;
		}
		$data['chart'] = $chart;
		$data['total'] = $total;
		echo json_encode($data);
	}

	function get_survey_p1($id_provinsi) {
		if ($id_provinsi!='ALL') {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p1, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p1= b.id AND b.jenis='P1' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p1 order by jml desc")->result();
			
		} else {
			$result = $this->db->query("SELECT a.transId, a.p1, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p1= b.id AND b.jenis='P1' GROUP BY a.p1 order by jml desc")->result();
		}
		
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart[$i]['color'] = '#'.$this->random_color();
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_survey_p2($id_provinsi) {
		if ($id_provinsi=='ALL') {
			$result = $this->db->query("SELECT a.transId, a.p2, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p2= b.id AND b.jenis='P2' GROUP BY a.p2 order by jml desc")->result();
		} else {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p2, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p2= b.id AND b.jenis='P2' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p2 order by jml desc")->result();
		}
		
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart[$i]['color'] = '#'.$this->random_color();
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_survey_p3($id_provinsi) {
		if ($id_provinsi=='ALL') {
			$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND a.p3!=40 AND a.p3!=41 AND a.p3!=42 AND a.p3!=43 AND a.p3!=44 AND a.p3!=45 AND a.p3!=235 GROUP BY a.p3 order by jml desc")->result();
		} else {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p3 order by jml desc")->result();
		}
		
		$result1 = $this->db->query("SELECT count(a.p3) as jml from trans_survey a")->row('jml');
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['nama'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$result1)*100,2);
			if ($value->nama=='PDIP') {
				$chart[$i]['color'] = '#fd0002';
			} else if($value->nama=='NASDEM') {
				$chart[$i]['color'] = '#f6ab02';
			} else if($value->nama=='PKB') {
				$chart[$i]['color'] = '#015931';
			} else if($value->nama=='PKS') {
				$chart[$i]['color'] = '#000000';
			} else if($value->nama=='GOLKAR') {
				$chart[$i]['color'] = '#fbce00';
			} else if($value->nama=='GERINDRA') {
				$chart[$i]['color'] = '#d53c32';
			} else if($value->nama=='DEMOKRAT') {
				$chart[$i]['color'] = '#2899d5';
			} else if($value->nama=='PAN') {
				$chart[$i]['color'] = '#1c52a0';
			} else if($value->nama=='PPP') {
				$chart[$i]['color'] = '#00923f';
			} else if($value->nama=='HANURA') {
				$chart[$i]['color'] = '#ef7f1a';
			} else if($value->nama=='PBB') {
				$chart[$i]['color'] = '#016436';
			} else if($value->nama=='PKPI') {
				$chart[$i]['color'] = '#db261b';
			} else if($value->nama=='PERINDO') {
				$chart[$i]['color'] = '#22287c';
			} else {
				$chart[$i]['color'] = '#'.$this->random_color();
			}
			
			$chart[$i]['p3'] = $value->p3;
			$chart[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_survey_p3_2($id_provinsi) {
		if ($id_provinsi=='ALL') {
			$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND (a.p3=40 OR a.p3=41 OR a.p3=42 OR a.p3=43 OR a.p3=44 OR a.p3=45 OR a.p3=235) GROUP BY a.p3 order by jml desc")->result();
		} else {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p3, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p3= b.id AND b.jenis='P3' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p3 order by jml desc")->result();
		}
		
		$result1 = $this->db->query("SELECT count(a.p3) as jml from trans_survey a")->row('jml');
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['nama'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$result1)*100,2);
			$chart[$i]['color'] = '#'.$this->random_color();
			$chart[$i]['p3'] = $value->p3;
			$chart[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_survey_p4($id_provinsi) {
		if ($id_provinsi=='ALL') {
			$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND  a.p4!=60 AND a.p4!=61 AND a.p4!=62 AND a.p4!=63 AND a.p4!=64 AND a.p4!=65 GROUP BY a.p4 order by jml desc")->result();
		} else {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p4 order by jml desc")->result();
		}
	
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['nama'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$result1)*100,2);
			if ($value->nama=='PDIP') {
				$chart[$i]['color'] = '#fd0002';
			} else if($value->nama=='NASDEM') {
				$chart[$i]['color'] = '#f6ab02';
			} else if($value->nama=='PKB') {
				$chart[$i]['color'] = '#015931';
			} else if($value->nama=='PKS') {
				$chart[$i]['color'] = '#000000';
			} else if($value->nama=='GOLKAR') {
				$chart[$i]['color'] = '#fbce00';
			} else if($value->nama=='GERINDRA') {
				$chart[$i]['color'] = '#d53c32';
			} else if($value->nama=='DEMOKRAT') {
				$chart[$i]['color'] = '#2899d5';
			} else if($value->nama=='PAN') {
				$chart[$i]['color'] = '#1c52a0';
			} else if($value->nama=='PPP') {
				$chart[$i]['color'] = '#00923f';
			} else if($value->nama=='HANURA') {
				$chart[$i]['color'] = '#ef7f1a';
			} else if($value->nama=='PBB') {
				$chart[$i]['color'] = '#016436';
			} else if($value->nama=='PKPI') {
				$chart[$i]['color'] = '#db261b';
			} else if($value->nama=='PERINDO') {
				$chart[$i]['color'] = '#22287c';
			} else {
				$chart[$i]['color'] = '#'.$this->random_color();
			}
			$chart[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_survey_p4_2($id_provinsi) {
		if ($id_provinsi=='ALL') {
			$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND  (a.p4=60 OR a.p4=61 OR a.p4=62 OR a.p4=63 OR a.p4=64 OR a.p4=65) GROUP BY a.p4 order by jml desc")->result();
		} else {
			$id_provinsi = str_replace("%20"," ",$id_provinsi);
			
			$result = $this->db->query("SELECT a.transId, a.p4, count(*) as jml, b.nama from trans_survey a inner join m_pilih b on a.p4= b.id AND b.jenis='P4' AND provinsi LIKE '%".$id_provinsi."%' GROUP BY a.p4 order by jml desc")->result();
		}
	
		$result1 = $this->db->query("SELECT count(a.p4) as jml from trans_survey a")->row('jml');

		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		foreach ($result as $key => $value) {
			$chart[$i]['nama'] = $value->nama;
			$chart[$i]['points'] = (float)round(($value->jml/$result1)*100,2);
			$chart[$i]['color'] = '#'.$this->random_color();
			$chart[$i]['jml'] = $result1 ;
			$i++;
		}
		$data['chart'] = $chart;
		echo json_encode($data);
	}

	function get_top_4($id_pertokohan,$type){
		if($type == 1){
			$type = 'tangible';
		}else{
			$type = 'intangible';
		}

		if($id_pertokohan == 'ALL'){
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where a.is_deleted='0' and a.inclination <> 0 and a.type='$type' group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and type='$type'")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and type='$type'")->row('jml');
		}elseif($id_pertokohan=='SEMUA'){
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where a.is_deleted='0' and a.inclination <> 0 group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0")->row('jml');
		}else{
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where id_pertokohan =$id_pertokohan and a.is_deleted='0' and a.inclination <> 0 and a.type='$type' group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and id_pertokohan =$id_pertokohan and type='$type'")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and id_pertokohan =$id_pertokohan and type='$type'")->row('jml');
		}
		
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		$total_semua = $data['jum_netral']+$data['jum_pilih'];
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->alias;
			$chart[$i]['points'] = (float)round(($value->jml/$total)*100,2);
			$chart[$i]['color'] = $value->color;
			$chart[$i]['bullet'] = site_url('img/logo_parpol/').$value->picture;
			$i++;
		}
		$data['jum_netral'] = round(($data['jum_netral']/$total_semua)*100,2);
		$data['jum_pilih'] = round(($data['jum_pilih']/$total_semua)*100,2);
		$data['chart'] = $chart;
		$data['total'] = $total_semua;
		echo json_encode($data);
	}

	function detail_influencer($alias_partai, $ketokohan, $type ){
		if($type == 1){
			$type = 'tangible';
		}else{
			$type = 'intangible';
		}
		if($ketokohan == 'ALL'){
			$data['data'] = $this->db->query("SELECT a.name, a.phone, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, g.nama as pertokohan, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan 
						where b.alias='$alias_partai' and a.type='$type' and a.is_deleted = '0';")->result();
		}else{
			$data['data'] = $this->db->query("SELECT a.name, a.phone, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, g.nama as pertokohan, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan
						where b.alias='$alias_partai' and a.id_pertokohan=$ketokohan and a.type='$type' and a.is_deleted = '0';")->result();
		}
		
		echo json_encode($data);
	}

	function get_chart_by_provinsi($id){
		if ($id != 'ALL') {
			$tangible = $this->db->query("SELECT b.nama, count(*) as jml from intangible a
											INNER JOIN m_pertokohan b on a.id_pertokohan = b.id_pertokohan
											where a.id_provinces=$id and a.is_deleted = '0' and a.type='tangible'
											group by a.id_pertokohan")->result();
			$intangible = $this->db->query("SELECT b.nama, count(*) as jml from intangible a
											INNER JOIN m_pertokohan b on a.id_pertokohan = b.id_pertokohan
											where a.id_provinces=$id and a.is_deleted = '0' and a.type='intangible'
											group by a.id_pertokohan")->result();
		}else{
			$tangible = $this->db->query("SELECT b.nama, count(*) as jml from intangible a
											INNER JOIN m_pertokohan b on a.id_pertokohan = b.id_pertokohan
											where a.is_deleted = '0' and a.type='tangible'
											group by a.id_pertokohan")->result();
			$intangible = $this->db->query("SELECT b.nama, count(*) as jml from intangible a
											INNER JOIN m_pertokohan b on a.id_pertokohan = b.id_pertokohan
											where a.is_deleted = '0' and a.type='intangible'
											group by a.id_pertokohan")->result();
		}

		foreach ($tangible as $key => $value) {
			# code...
		}
	}

	//MAPS
	function maps(){
		$this->load->view('maps');
	}

	function marker(){
		$provinsi = $this->db->get('m_provinces')->result();
		foreach ($provinsi as $key => $value) {
			$data[$value->id]['name'] = $value->name;
			$data[$value->id]['id'] = $value->id;
			$data[$value->id]['latitude'] = $value->latitude;
			$data[$value->id]['longitude'] = $value->longitude;
			$data[$value->id]['logo'] = $value->logo;
			$data[$value->id]['jum_tangible'] = 0;
			$data[$value->id]['jum_intangible'] = 0;
		}
		$result = $this->db->query("SELECT b.*, sum(a.intangible) as jum_intangible, sum(a.tangible) as jum_tangible 
									from (SELECT is_deleted, id_provinces, IF(type = 'intangible',1,0) as intangible, IF(type = 'tangible',1,0) as tangible from intangible) as a 
									inner join m_provinces as b on a.id_provinces = b.id
									where a.is_deleted = '0'
									group by a.id_provinces;")->result();

		foreach ($result as $key => $value) {
			$data[$value->id]['jum_tangible'] = $value->jum_tangible;
			$data[$value->id]['jum_intangible'] = $value->jum_intangible;
		}
		echo json_encode($data);
	}

	function get_detail_influencer($id_prov){
		$data['data'] = $this->Home_model->get_detail_influencer($id_prov);
		echo json_encode($data);

	}

	function datatable_influence($tipe,$condong,$kategori){
		$data['data'] = $this->get_data_influence($tipe,$condong,$kategori);
		echo json_encode($data);
	}

	function get_data_influence($tipe,$condong,$kategori){
		$data=$this->db->get('m_provinces')->result();
		$i=0;
		foreach ($data as $key => $value) {
			// $data[$i]->provinsi = '<a href='."'".site_url("data/influence_kab/$value->id")."'".'>'."".$value->name."".'</a>';
			$data[$i]->provinsi = $value->name;
			$query = "SELECT count(id_intangible) as jml from intangible where is_deleted='0' AND id_provinces = '$value->id'";
			if($tipe != 'all'){
				$query .= " AND type = '$tipe'";
			};
			if($condong != 'all'){
				$query .= " AND inclination = '$condong'";
			};
			if($kategori != 'all'){
				$query .= " AND id_pertokohan = '$kategori'";
			};
			$jum = $this->db->query($query)->row('jml');
			$data[$i]->jum_tangible = $jum;
			$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-id="'.$value->id.'">Detail</button>';
			// $data[$i]->jum_tangible = '<a href="#">'."".$jum."".'</a>';
			$i++;
		}

		return $data;
	}
	function table_kecondongan(){
		$nama_influencer = urldecode($_GET["category"]);
		$id_provinsi = $_GET["area"];
		$id = $this->db->get_where('m_pertokohan', array('nama' => $nama_influencer))->row('id_pertokohan');
		if($id_provinsi=='ALL'){
			$data = $this->db->query("SELECT COUNT(*) as jumlah, b.alias from intangible a
			inner join m_parpol b on a.inclination = b.id_parpol where id_pertokohan = $id
			group by b.id_parpol order by jumlah desc")->result();	
		}else{
			$data = $this->db->query("SELECT COUNT(*) as jumlah, b.alias from intangible a
			inner join m_parpol b on a.inclination = b.id_parpol where id_provinces = '$id_provinsi' and id_pertokohan = $id
			group by b.id_parpol order by jumlah desc")->result();
		}
		
		$total = 0;
		foreach ($data as $key => $value) {
			$total = $total+$value->jumlah;
		}
		$string = '';
		foreach ($data as $key => $value) {
			$string .= '<tr>';
			$string .= '<td class="nowrap">'.$value->alias.'</td>';
			$string .= '<td>'.(float)round(($value->jumlah/$total)*100,2).' %</td>';
			$string .= '</tr>';
		}
		echo json_encode($string);
	}
	function test(){
		//EVERY PROVINCE PARTAI
		$data['province'] = $this->db->query("SELECT area from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' group by area")->result();
		$j=0;
		foreach ($data['province'] as $key => $value) {
			$area = $value->area;
			$name = $this->db->query("SELECT name from m_provinces where id='$area'")->row('name');
			$data['graph_bawah'][$name] = $this->db->query("SELECT area, alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' and area='$area' group by alias order by jml_ desc")->result();
			$total_data =0;
			$total_lain =0;
			$k=0;
			foreach ($data['graph_bawah'][$name] as $tes) {
				$total_data += $tes->jml_;
				if($k >= 6){
					$total_lain += $tes->jml_;
				}else{
					$data['new_graph'][$name][$k]['jumlah'] = $tes->jml_;
					$data['new_graph'][$name][$k]['alias'] = $tes->alias;
					$data['new_graph'][$name][$k]['color'] = $tes->color;
				}
				$k++;
			}
			if($k >= 6){
				$data['new_graph'][$name][6]['jumlah'] = $total_lain;
				$data['new_graph'][$name][6]['alias'] = 'Lain-Lain';
				$data['new_graph'][$name][6]['color'] = '#fffff';
				$k=7;
			}
			

			for ($i=0; $i < $k; $i++) { 
				$data['new_graph'][$name][$i]['jumlah'] = (float)round(($data['new_graph'][$name][$i]['jumlah']/$total_data)*100,2);
			}

			$j++;
		}
		$data['jum_province'] = $j;
		var_dump($data['new_graph']);
	} 
}
