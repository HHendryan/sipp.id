<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Survei_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }


    function index(){
    	$data = $this->Dashboard_model->get_data_dashboard();
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml'); 

		$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
		$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');  
		$data['provinsi']=$this->db->get('m_provinces')->result(); 	
		
		if($this->session->flashdata('item')){
			$data['flashdata'] = $this->session->flashdata('item');
		}else{
			$data['flashdata'] = 0;
		}
    	$this->load->view('new/header');	
    	$this->load->view('new/dashboard',$data);	
    	$this->load->view('new/footer');	
    }   
	
	function load_tab(){ 

		$tab_name = $this->input->post('tab_name');
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$tipe = $this->input->post('tipe');
		$logo = $this->input->post('logo');
		if($tab_name == 'tab_profile'){
			$data = $this->Dashboard_model->get_data_tab_profile($id,$nama,$tipe,$logo);
		}else if($tab_name == 'tab_survei'){
			$data = $this->Survei_model->get_data($id,$nama,$tipe,$logo);
		}else if($tab_name == "tab_ketokohan"){
			$modal_name="modal_ketokohan";
			$data = $this->Dashboard_model->get_modalprofile($modal_name,$id,$nama,$tipe);
		}else if($tab_name == "tab_simulasi"){
			$data = $this->Dashboard_model->get_data_tab_simulasi($id,$nama,$tipe);
		}
		
				
		$data['id'] = $this->input->post('id');
		$data['nama'] = $this->input->post('nama');
		$data['tipe'] = $this->input->post('tipe');
		$data['logo'] = $this->input->post('logo');
		$response = $this->load->view('new/'.$tab_name,$data);
		
		return $response;
		// echo json_encode($data);
		
	}
	
	function load_modal_profile(){ 
		$modal_name = $this->input->post('modal_name');
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$tipe = $this->input->post('tipe');
		
		$data = $this->Dashboard_model->get_modalprofile($modal_name,$id,$nama,$tipe);
		// var_dump($data);  
		$response = $this->load->view('new/'.$modal_name,$data);
		return $response;
	}

	function get_tree_kabupaten($id){
		$data = $this->Dashboard_model->get_tree('kabupaten',$id);
		echo json_encode($data);
	}

	function get_tree_kecamatan($id){
		$data = $this->Dashboard_model->get_tree('kecamatan',$id);
		echo json_encode($data);
	}

	function get_tree_kelurahan($id){
		$data = $this->Dashboard_model->get_tree('kelurahan',$id);
		echo json_encode($data);
	}

	function get_information(){
		$tab_active = $this->input->post('tab_active');
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$tipe = $this->input->post('tipe');
		$logo = $this->input->post('logo');
		if($tab_active == 'tab_profile'){
			$data = $this->Dashboard_model->get_data_tab_profile($id,$nama,$tipe,$logo);
		}else if($tab_active == 'tab_survei'){
			$data = $this->Survei_model->get_data($id,$nama,$tipe,$logo);
		};
		echo json_encode($data);
	}
	
	function about(){
		$this->load->view('new/header');
		$this->load->view('new/about');
		$this->load->view('new/footer');
	}
	
    // function index(){
    // 		$this->load->view('new/index');	
    // }

    // function index2(){
    // 		$this->load->view('new/index2');	
    // }
        

    // function index3(){
    // 		$this->load->view('new/index3');	
    // }

    // function index4(){
    // 		$this->load->view('new/index4');	
    // }

        




	// function index(){
	// 	$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
	// 	$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');
	// 	$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
	// 	$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
	// 	$data['provinsi']=$this->db->get('m_provinces')->result();
	// 	$this->load->view('dashboard',$data);
	// }

	// function second(){
	// 	$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
	// 	$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');
	// 	$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
	// 	$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
	// 	$data['jum_jeniskelamin'] = $this->db->query("SELECT sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin")->row();
	// 	$data['jum_usia'] = $this->db->query("select SUM(jml_1) as jml_1,SUM(jml_2) as jml_2,SUM(jml_3) as jml_3,SUM(jml_4) as jml_4,SUM(jml_5) as jml_5 FROM sum_umur")->row();
	// 	$data['jum_status'] = $this->db->query("SELECT SUM(jml_kawin) as jml_kawin, SUM(jml_belumkawin) as jml_belumkawin FROM sum_status")->row();

	// 	//NETRAL DAN PILIH
	// 	$result = $this->db->query("SELECT sum(jml) as jml from sum_grafik_kecondongan where alias = 'NETRAL' and tipe='1'
	// 								UNION select sum(jml) as jml from sum_grafik_kecondongan where alias != 'NETRAL' and tipe='1';")->result();
	// 	$netral = $result[0]->jml;
	// 	$pilih = $result[1]->jml;
	// 	$total = $netral + $pilih;
	// 	$data['jum_netral'] = (float)round(($netral/$total)*100,2);  
	// 	$data['jum_pilih'] = (float)round(($pilih/$total)*100,2);

	// 	//DETAIL PARTAI
	// 	$data['influence_detail'] = $this->db->query("SELECT alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' group by alias order by jml_ desc limit 10")->result();
	// 	$total_partai = 0;
	// 	$i=0;
	// 	foreach ($data['influence_detail'] as $key => $value) {
	// 		$total_partai += $value->jml_;
	// 	}
	// 	for ($i=0; $i < 10; $i++) { 
	// 		$data['influence_detail'][$i]->jml_ = (float)round(($data['influence_detail'][$i]->jml_/$total_partai)*100,2);
	// 	}

	// 	//EVERY PROVINCE PARTAI
	// 	$data['province'] = $this->db->query("SELECT area from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' group by area")->result();
	// 	$j=0;
	// 	foreach ($data['province'] as $key => $value) {
	// 		$area = $value->area;
	// 		$name = $this->db->query("SELECT name from m_provinces where id='$area'")->row('name');
	// 		$data['graph_bawah'][$name] = $this->db->query("SELECT area, alias, color, SUM(jml) as jml_ from sum_grafik_kecondongan where tipe='1' and alias != 'Netral' and area='$area' group by alias order by jml_ desc")->result();
	// 		$total_data =0;
	// 		$total_lain =0;
	// 		$k=0;
	// 		foreach ($data['graph_bawah'][$name] as $tes) {
	// 			$total_data += $tes->jml_;
	// 			if($k >= 6){
	// 				$total_lain += $tes->jml_;
	// 			}else{
	// 				$data['new_graph'][$name][$k]['jumlah'] = $tes->jml_;
	// 				$data['new_graph'][$name][$k]['alias'] = $tes->alias;
	// 				$data['new_graph'][$name][$k]['color'] = $tes->color;
	// 			}
	// 			$k++;
	// 		}
	// 		if($k >= 6){
	// 			$data['new_graph'][$name][6]['jumlah'] = $total_lain;
	// 			$data['new_graph'][$name][6]['alias'] = 'Lain-Lain';
	// 			$data['new_graph'][$name][6]['color'] = '#fffff';
	// 			$k=7;
	// 		}
			

	// 		for ($i=0; $i < $k; $i++) { 
	// 			$data['new_graph'][$name][$i]['jumlah'] = (int)round(($data['new_graph'][$name][$i]['jumlah']/$total_data)*100,2);
	// 		}

	// 		$j++;
	// 	}
	// 	$data['jum_province'] = $j;
	// 	$this->load->view('dashboard_second',$data);
	// }

	// function get_pertokohan($type){
	// 	$data = $this->db->get_where('m_pertokohan', array('type' => $type))->result();
	// 	$string = '<option value="ALL">ALL</option>';
	// 	$string .= '<option value="SEMUA">Kecondongan Politik Nasional</option>';
	// 	foreach ($data as $key => $value) {
	// 		$string .= '<option value="'.$value->id_pertokohan.'">'.$value->nama.'</option>';
	// 	}
	// 	echo $string;
	// }

	// function random_color_part() {
 //    	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	// }

	// function random_color() {
	//     return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	// }

	function get_pertokohan($type){
		$data = $this->db->get_where('m_pertokohan', array('type' => $type))->result();
		$string = '<option value="ALL">ALL</option>';
		$string .= '<option value="SEMUA">Kecondongan Politik Nasional</option>';
		foreach ($data as $key => $value) {
			$string .= '<option value="'.$value->id_pertokohan.'">'.$value->nama.'</option>';
		}
		echo $string;
	}

	function get_chart(){
		$id_pertokohan = $this->input->post('id_influencer');
		$type = $this->input->post('tipe_influencer');
		$id_wilayah = $this->input->post('id_wilayah');
		$tipe_wilayah = $this->input->post('tipe_wilayah');
		$where = '';
		if($id_wilayah != 0){
			if($tipe_wilayah == 'provinsi'){
				$where = "and id_provinces=$id_wilayah";
			}else if($tipe_wilayah == 'kabupaten'){
				$where = "and id_city=$id_wilayah";
			}else if($tipe_wilayah == 'kecamatan'){
				$where = "and id_districts=$id_wilayah";
			}else if($tipe_wilayah == 'kelurahan'){
				$where = "and id_village=$id_wilayah";
			}	
		}
		
		if($type == 1){
			$type = 'tangible';
		}else{
			$type = 'intangible';
		}
		$data['jum_netral']=0;
		$data['jum_pilih']=0;
		$chart[0]['name'] = '';
		$chart[0]['points'] = '';
		$chart[0]['color'] = '';
		$chart[0]['bullet'] = '';
		if($id_pertokohan == 'ALL'){
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where a.is_deleted='0' and a.inclination <> 0 and a.type='$type' $where group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and type='$type' $where")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and type='$type' $where")->row('jml');
		}else{
			$result = $this->db->query("SELECT b.alias,b.color,b.picture, a.inclination, count(*) as jml from intangible a inner join m_parpol b on a.inclination=b.id_parpol  where id_pertokohan =$id_pertokohan and a.is_deleted='0' and a.inclination <> 0 and a.type='$type' $where group by inclination order by jml desc limit 10")->result();
			$data['jum_netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 and id_pertokohan =$id_pertokohan and type='$type' $where")->row('jml');
			$data['jum_pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 and id_pertokohan =$id_pertokohan and type='$type' $where")->row('jml');
		}
		
		$i=0;
		$total = 0;
		foreach ($result as $key => $value) {
			$total += $value->jml;
		}
		$total_semua = $data['jum_netral']+$data['jum_pilih'];
		foreach ($result as $key => $value) {
			$chart[$i]['name'] = $value->alias;
			$chart[$i]['points'] = $value->jml;
			$chart[$i]['color'] = $value->color;
			$chart[$i]['bullet'] = site_url('img/logo_parpol/').$value->picture;
			$i++;
		}
		if($total_semua > 0){
			$data['jum_netral'] = round(($data['jum_netral']/$total_semua)*100,2);
			$data['jum_pilih'] = round(($data['jum_pilih']/$total_semua)*100,2);	
		}
		
		$data['chart'] = $chart;
		$data['total'] = $total_semua;
		echo json_encode($data);
	}

	function detail_influencer($partai, $ketokohan, $tipe_ketokohan, $tipe_wilayah, $id_wilayah ){
		$partai = urldecode($partai);
		$ketokohan = urldecode($ketokohan);
		$tipe_ketokohan = urldecode($tipe_ketokohan);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		if($tipe_ketokohan == 1){
			$tipe_ketokohan = 'tangible';
		}else{
			$tipe_ketokohan = 'intangible';
		}

		$where = '';
		if($id_wilayah != 0){
			if($tipe_wilayah == 'provinsi'){
				$where = "and id_provinces=$id_wilayah";
			}else if($tipe_wilayah == 'kabupaten'){
				$where = "and id_city=$id_wilayah";
			}else if($tipe_wilayah == 'kecamatan'){
				$where = "and id_districts=$id_wilayah";
			}else if($tipe_wilayah == 'kelurahan'){
				$where = "and id_village=$id_wilayah";
			}	
		}

		if($ketokohan != 'ALL'){
			$where .=" and a.id_pertokohan=$ketokohan";
		}

		$data['data'] = $this->db->query("SELECT a.name, a.phone, g.nama as pertokohan, c.alias as prov, d.name as kab, e.name as kec, f.name as kel, a.informasi_tambahan
						from intangible a 
						inner join m_parpol b on a.inclination=b.id_parpol 
						left join m_provinces c on a.id_provinces=c.id 
						left join m_regencies d on a.id_city=d.id
						left join m_districts e on a.id_districts=e.id
						left join m_villages f on a.id_village=f.id 
						left join m_pertokohan g on a.id_pertokohan=g.id_pertokohan 
						where b.alias='$partai' and a.type='$tipe_ketokohan' and a.is_deleted = '0' $where")->result();
		echo json_encode($data);
	}

	function detail_survey_alias($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah){
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama_alias' => $nama_survey))->row('id');

		$where = '';
		if($id_wilayah != 0){
			$where .= 'and provinsi=$nama_wilayah';
		} 
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$data['data'] = $this->db->query("SELECT a.nama,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi from trans_survey a 
							left join m_pilih b on $tipe_survey_=b.id 
							where $tipe_survey=$id_survey $where")->result();
		echo json_encode($data);
	}

	function detail_survey($nama_survey,$tipe_survey,$tipe_wilayah,$nama_wilayah,$id_wilayah){
		$nama_survey = urldecode($nama_survey);
		$tipe_survey = urldecode($tipe_survey);
		$tipe_wilayah = urldecode($tipe_wilayah);
		$nama_wilayah = urldecode($nama_wilayah);
		$id_wilayah = urldecode($id_wilayah);

		$id_survey = $this->db->get_where('m_pilih', array('jenis' => $tipe_survey,'nama' => $nama_survey))->row('id');

		$where = '';
		if($id_wilayah != 0){
			$where .= 'and provinsi=$nama_wilayah';
		} 
		$tipe_survey_ = 'a.'.$tipe_survey;
		// $tipe_survey__ = 'b.'.$tipe_survey;
		$data['data'] = $this->db->query("SELECT a.nama,a.alamat,a.provinsi,a.kotaKabupaten,a.kecamatan,a.desaKelurahan, b.nama as informasi from trans_survey a 
							left join m_pilih b on $tipe_survey_=b.id 
							where $tipe_survey=$id_survey $where")->result();
		echo json_encode($data);
	}
}
