<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Load_view extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Survei_model');
        $this->load->model('Legislator_model');
        $this->load->model('Simulasi_model');
    }


    function index(){
    }   
	
	function load_tab(){
		$user = $this->session->userdata('teknopol');
		if(!$user){
			echo 'N';
		}else{
			$tab_name = $this->input->post('tab_name');
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$tipe = $this->input->post('tipe');
			$logo = $this->input->post('logo');
			$id = $this->security->xss_clean($id);
			$nama = $this->security->xss_clean($nama);
			$tipe = $this->security->xss_clean($tipe);
			$logo = $this->security->xss_clean($logo);
			$tab_name = $this->security->xss_clean($tab_name);
			$this->set_flash($id,$tipe);
			if($user['role'] == 'provinsi'){
				if($this->Auth_model->role_restriction($id,$tipe)){
					$this->view($id,$nama,$tipe,$logo,$tab_name);
				}else{
					$data['id'] = $id;
					$data['nama'] = $nama;
					$data['tipe'] = $tipe;
					$data['logo'] = $logo;
					$this->load->view('new/no_data',$data);
				}
			}else{
				$this->view($id,$nama,$tipe,$logo,$tab_name);
			}
		}
	}

	function view($id,$nama,$tipe,$logo,$tab_name){
		if($tab_name == 'tab_profile'){
			$data = $this->Dashboard_model->get_data_tab_profile($id,$nama,$tipe,$logo);
		}else if($tab_name == 'tab_survei'){
			$data = $this->Survei_model->get_data($id,$nama,$tipe,$logo);
		}else if($tab_name == "tab_ketokohan"){
			$modal_name="modal_ketokohan";
			$data = $this->Dashboard_model->get_modalprofile($modal_name,$id,$nama,$tipe);
		}else if($tab_name == "tab_simulasi"){
			$data = $this->Simulasi_model->get_data($id,$nama,$tipe);
			// $data = $this->Dashboard_model->get_data_tab_simulasi($id,$nama,$tipe);
		}else if($tab_name =="tab_legislator"){
			$data = $this->Legislator_model->get_data($id,$nama,$tipe);
		}else if($tab_name =="tab_rekomendasi"){
			$data = $this->Simulasi_model->get_rekomendasi($id,$nama,$tipe);
		} else if ($tab_name =="tab_profile_pilpres") {
			$data = $this->Dashboard_model->get_data_tab_profile_pilpres($id,$nama,$tipe,$logo);
		} else if ($tab_name =="tab_peta_suara") {
			$data = $this->Simulasi_model->get_data_pilpres($id,$nama,$tipe);
		} else if ($tab_name =="tab_simulasi_2019") {
			$data = $this->Simulasi_model->get_data_pilpres($id,$nama,$tipe);
		} else if($tab_name == "tab_ketokohan_pilpres"){
			$modal_name="modal_ketokohan";
			$data = $this->Dashboard_model->get_modalprofile($modal_name,$id,$nama,$tipe);
		}else if($tab_name == 'tab_survey_pilpres'){
			$data = $this->Survei_model->get_data($id,$nama,$tipe,$logo);
		}

		// var_dump($data);
							
		$data['id'] = $id;
		$data['nama'] = $nama;
		$data['tipe'] = $tipe;
		$data['logo'] = $logo;
		$user = $this->session->userdata('teknopol');
		$data['user'] = $user;
		$response = $this->load->view('new/'.$tab_name,$data);
		
		return $response;
	}

	function test(){
		$user = $this->session->userdata('teknopol');
		var_dump($user);
	}

	function set_flash($id,$tipe){
		if($id == 0){
			$data = new stdClass();
			$data->id = $id;
			$data->tipe = $tipe;
			$data->name = 'NASIONAL';
			$data->logo = 'nasional.png';

		}else{
			if($tipe == 'provinsi'){
				$table = 'm_provinces';
			}else if ($tipe == 'kabupaten'){
				$table = 'm_regencies';
			}else if($tipe == 'kecamatan'){
				$table = 'm_districts';
			}else if($tipe == 'kelurahan'){
				$table = 'm_villages';
			}

			if ($tipe=='dapil') {
				$data = new stdClass();
				$logo = $this->db->get_where('m_provinces', array('id' => $id))->row('logo');

				$nama = $this->db->query("SELECT nama_dapil FROM m_dapil a  
					where id_dapil=".$id)->row();

				
				$data->tipe = $tipe;
				$data->id = $id;
				$data->logo = $logo;
				$data->name = $nama ;
			} else {
				$data = $this->db->get_where($table, array('id' => $id))->row();    
            	$data->tipe = $tipe;
			}
			
		}
		$this->session->set_flashdata('item', $data);
	}
}
