<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends CI_Controller {

  //constructor
  public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
        $this->load->model('Home_model');

        $data = $this->session->userdata('teknopol');
        if(!$data){
          redirect('');
        }
        $this->load->view('template/menu',$data,true);
    }
    
  function index(){
    $this->load->view('new/header');
    $this->load->view('new/modal');
    $this->load->view('new/footer');
  }



  function index2(){
    $this->load->view('new/header');
    $this->load->view('new/infoprovinsi');
    $this->load->view('new/footer');
  }


  function profile(){
    $this->load->view('new/header');
    $this->load->view('new/userprofile');
    $this->load->view('new/footer');
  }


}
