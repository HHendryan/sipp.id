
<!DOCTYPE html>
<html>
    <head>
        <title>SIPP Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="bower_components/pace/pace.css" rel="stylesheet">
        <link href="css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <?php include 'template/menu.php' ?>
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>
                <div class="content-w">
                    <div class="property-single">
                        <div class="property-media" style="background-image: url(img/header_home.jpg)">
                            <marquee behavior="scroll" direction="left">
                                <h2><font color="#FFFFFF">SELAMAT DATANG DI IPOL INDONESIA - IT RESEARCH AND POLITIC CONSULTANT</font></h2>
                            </marquee>
                        </div>
                        <div class="property-info-w">
                            <div class="property-info-main">
                                <div class="badge badge-red">Teknologi Pemenangan Pemilu</div>
                                <div class="item-features">
                                    <div class="feature">Terencana</div>
                                    <div class="feature">Terukur</div>
                                    <div class="feature">Akurat</div>
                                    <div class="feature">Terkendali</div>
                                    <div class="feature">Terupdate</div>
                                </div>
                                <h1>IPOL INDONESIA</h1>
                                <div class="property-price">
                                    <img src="img/logo_ipol.png" width="182" height="92">
                                </div>
                                <div class="property-features-highlight">
                                    <div class="feature">
                                        <i class="os-icon os-icon-emoticon-smile"></i>
                                        <span>Trust</span>
                                    </div>
                                    <div class="feature">
                                        <i class="os-icon os-icon-bar-chart-up"></i>
                                        <span>Quality</span>
                                    </div>
                                    <div class="feature">
                                        <i class="os-icon os-icon-ui-92"></i>
                                        <span>Quick</span>

                                    </div>
                                </div>
                                <div class="property-description">
                                    <p>Ipol Indonesia adalah sebuah lembaga profesional dibidang IT, Research and Politic Consultant di Indonesia yang mendedikasikan kehadirannya untuk mengubah kepemimpinan yang lebih berkualitas di tingkat eksekutif dan legislatif. Ipol Indonesia berisi pemimpin muda visioner, cerdas, profesional yang berkarakter Trust - Quality - and Quick (Bisa Dipercaya, Berkualitas, dan Bertindak Cepat). Semua personel Ipol mendedikasikan dirinya menjadi Founding Fathers Pencetak Kepemimpinan di Bumi Pertiwi Indonesia, baik di level Walikota/Bupati/Gubernur/Presiden) dan DPRD Kab/Kota/Propinsi dan Pusat.</p>
                                    <p>Sejak Tahun 2009 mengawal pemilu, iPol yang dulunya bernama IP Consultant selalu memprioritaskan keberpihakan pada kandidat yang secara komitmen bisa menjadi harapan baik bagi rakyat. Ipol enggan menjadi konsultan pada calon pemimpin yang berpotensi mencederai hati rakyat. Ipol Indonesia akan selalu hadir mencetak "SEJARAH BARU" dalam kontestasi Pemilu di Indonesia, Baik dalam bentuk software / aplikasi, database, kreatif kampanye, event politik, riset, pergerakan lapangan, dan pengkondisian kemenangan.</p>
                                    <p>"Demokrasi melalui Pilkada atau Pemilu, semestinya dimenangkan oleh pemimpin berkarakter, smart, bermartabat, dan bertanggungjawab. Bersama Ipol Indonesia, kami akan mewujudkan kemenangan dengan manajemen, teknologi moderen dan standard mutu Terencana - Terukur - Akurat - Terkendali - Terbarukan"</p>
                                </div>
                                <div class="property-section">
                                    <div class="property-section-header">Visi dan Misi
                                        <div class="filter-toggle">
                                            <i class="os-icon-minus os-icon"></i>
                                        </div>
                                    </div>
                                    <div class="property-section-body">
                                        <div>
                                            <h6>Visi</h6>
                                            <p>"Menjadi Perusahaan / Lembaga Profesional dan Terdepan Bidang Riset, Komunikasi, Monitoring, dan Konsultan Pemenangan Pemilu Berbasis Teknologi Politik dan Pergerakan Jaringan iPol di Seluruh Indonesia dengan Prinsip Trust - Quality - and Quick."</p>
                                            
                                            <h6>Misi</h6>
                                            <p>"Membantu klien/kandidat untuk dimenangkan dengan cara yang profesional dan cerdas dalam kompetisi pemilu, serta turut serta mengawal perubahan kepemimpinan diberbagai wilayah/daerah untuk perbaikan nasib rakyat di bumi pertiwi Indonesia dengan cara kerja iPol yang Terukur, Terencana, Akurat, Terkendali, dan Terupdate."</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="property-section">
                                    <div class="property-section-header">Location Info
                                        <div class="filter-toggle">
                                            <i class="os-icon-minus os-icon"></i>
                                        </div>
                                    </div>
                                    <div class="property-section-body">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26466.220546263787!2d-118.35266418131052!3d33.98540355993257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2b79824efa317%3A0x29dd07e1734487f2!2sPark+Mesa+Heights%2C+Los+Angeles%2C+CA!5e0!3m2!1sen!2sus!4v1502593931845"
                                            width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="property-info-side">
                                <div class="side-magic" style="background-image: url(img/property2.jpg)">
                                    <div class="fader"></div>
                                    <h4 class="side-magic-title">Petrus Hariyanto</h4>
                                    <div class="side-magic-desc">"Demokrasi melalui Pilkada atau Pemilu, semestinya dimenangkan oleh pemimpin berkarakter, smart, bermartabat, dan bertanggungjawab. Bersama Ipol Indonesia, kami akan mewujudkan kemenangan dengan manajemen, teknologi moderen dan standard mutu Terencana - Terukur - Akurat - Terkendali - Terbarukan."</div>
                                </div>
                                <div class="side-section">
                                    <div class="side-section-header">CORE BUSINESS</div>
                                    <div class="side-section-content">
                                        <div class="property-side-features">
                                            <div class="feature">
                                                <img src="img/research.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Research</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/success.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Success</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/communication.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Communication & Event</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/advocation.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Advocation</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/institute.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Institute</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/technology.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Ipol Technology</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/Intelligent.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Politic Intelligent</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/Media.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Media Monitoring</span>
                                            </div>
                                            <div class="feature">
                                                <img src="img/business.png" width="50" height="50">
                                                <span>&nbsp;&nbsp;Ipol Business</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="side-magic" style="background-image: url(img/petrus.jpg)">
                                    <div class="fader"></div>
                                    <h4 class="side-magic-title">Standar Kerja</h4>
                                    <div class="side-magic-desc">Sistem Informasi Pemenangan Pemilu - Partai Demokrasi Indonesia Perjuangan adalah Grand Desain Sebuah Software Partai dan Sistem Pemenangan Pemilu dalam Satu Paket yang berbasis Database Riil dan Menampilkan Visual apik untuk memonitor dan mengukur kerja partai secara struktural dan efektivitasnya dalam pemenangan pemilu di Indonesia. Sistem informasi ini adalah cara pemenangan modern yang sifatnya bisa diwariskan untuk keberlangsungan jangka Panjang masa depan partai. Kebergantungan dalam Figur Pimpinan disertai Sistem Informasi yang handal dan SDM Profesional akan menjadikan masa depan partai makin menjanjikan dan dominasinya sulit terpatahkan karena interaktif terbangun (mendekatkan semua yang jauh). Ke depan, dengan PDIP � SIPP ini, partai Akan menjadi makin besar dan kokoh dipuncak karena terimplementasi sampai ke bawah.</div>
                                    <a class="side-magic-btn" href="#">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="bower_components/moment/moment.js"></script>
        <script src="bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="bower_components/ckeditor/ckeditor.js"></script>
        <script src="bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="bower_components/dropzone/dist/dropzone.js"></script>
        <script src="bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="bower_components/tether/dist/js/tether.min.js"></script>
        <script src="bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="bower_components/bootstrap/js/dist/util.js"></script>
        <script src="bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="bower_components/bootstrap/js/dist/button.js"></script>
        <script src="bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="bower_components/pace/pace.min.js"></script>
        <script src="js/dataTables.bootstrap4.min.js"></script>
        <script src="js/main.js?version=4.2.0"></script>
    </body>
</html>
