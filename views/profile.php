
<!DOCTYPE html>
<html>
    <head>
        <title>SIPP Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/switchery/switchery.min.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
        <style type="text/css">
            .middle{
                margin: auto;
            }
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <?php include 'template/menu.php' ?>
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>

                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box" style="padding-top: 10px;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="user-profile compact">
                                        <div class="up-head-w" style="background-image:url(img/profile_bg1.jpg)">
                                            <div class="up-social">
                                                <a href="#">
                                                    <i class="os-icon os-icon-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="os-icon os-icon-facebook"></i>
                                                </a>
                                            </div>
                                            <div class="up-main-info">
                                                <h2 class="up-header">PDI PERJUANGAN</h2>
                                                <h6 class="up-sub-header">Sistem Informasi Pemenangan Pemilu</h6>
                                            </div>
                                            <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                                                    <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="up-contents">
                                            <div class="m-b">
                                                <div class="row m-b">
                                                    <div class="col-sm-6 b-r b-b">
                                                        <div class="el-tablo centered padded-v">
                                                            <div class="value"><?php echo $persen_tangible ?> %</div>
                                                            <div class="label">Tangible</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 b-b">
                                                        <div class="el-tablo centered padded-v">
                                                            <div class="value"><?php echo $persen_intangible ?> %</div>
                                                            <div class="label">Intangible</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="padded">
                                                    <div class="os-progress-bar primary">
                                                        <div class="bar-labels">
                                                            <div class="bar-label-left">
                                                                <span>Tangible</span>
                                                            </div>
                                                            <div class="bar-label-right">
                                                                <span class="info"><?php echo $tangible?></span>
                                                            </div>
                                                        </div>
                                                        <div class="bar-level-1" style="width: 100%">
                                                            <div class="bar-level-3" style="width: <?php echo $persen_tangible?>%"></div>
                                                        </div>
                                                    </div>
                                                    <div class="os-progress-bar primary">
                                                        <div class="bar-labels">
                                                            <div class="bar-label-left">
                                                                <span>Intangible</span>
                                                            </div>
                                                            <div class="bar-label-right">
                                                                <span class="info"><?php echo $intangible?></span>
                                                            </div>
                                                        </div>
                                                        <div class="bar-level-1" style="width: 100%">
                                                            <div class="bar-level-3" style="width: <?php echo $persen_intangible?>%"></div>
                                                        </div>
                                                    </div>
                                                    <div class="os-progress-bar primary">
                                                        <div class="bar-labels">
                                                            <div class="bar-label-left">
                                                                <span>Total</span>
                                                            </div>
                                                            <div class="bar-label-right">
                                                                <span class="info"><?php echo $total?></span>
                                                            </div>
                                                        </div>
                                                        <div class="bar-level-1" style="width: 100%">
                                                            <div class="bar-level-3" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <!--START - Customers with most tickets-->
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                            <form class="form-inline justify-content-sm-end">
                                                <select class="form-control form-control-sm rounded">
                                                    <option value="Pending">Pilgub</option>
                                                    <option value="Active">Pileg </option>
                                                    <option value="Cancelled">Pilwali</option>
                                                    <option value="Cancelled">Pilbup</option>
                                                </select>
                                            </form>
                                        </div>
                                        <h6 class="element-header">Calon Gubernur dan Wakll Gubernur</h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>Calon</th>
                                                            <th>Wilayah</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/jabar.jpg">
                                                                    <span>TB Hasanuddin & Anton Charliyan</span>
                                                                </div>
                                                            </td>
                                                            <td> JAWA BARAT</td>                                          
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/kalbar.jpg">
                                                                    <span class="d-none d-xl-inline-block">Karolin Margret & Suryadman Gidot</span>
                                                                </div>
                                                            </td>
                                                            <td> KALIMANTAN BARAT</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/sumut.jpg">
                                                                    <span class="d-none d-xl-inline-block">Djarot Saiful Hidayat & Sihar Sitorus</span>
                                                                </div>
                                                            </td>
                                                            <td> SUMATERA UTARA</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/sumsel.jpg">
                                                                    <span>Dodi Reza & Giri Ramandha Kiemas</span>
                                                                </div>
                                                            </td>
                                                            <td> SUMATERA SELATAN</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/jateng.jpg">
                                                                    <span class="d-none d-xl-inline-block">Ganjar Pranowo &  Gus Yasin</span>
                                                                </div>
                                                            </td>
                                                            <td> JAWA TENGAH</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/kaltim.jpg">
                                                                    <span class="d-none d-xl-inline-block">Safaruddin</span>
                                                                </div>
                                                            </td>
                                                            <td> KALIMANTAN TIMUR</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/papua.jpg">
                                                                    <span class="d-none d-xl-inline-block">John Wempi & Habel Melkias</span>
                                                                </div>
                                                            </td>
                                                            <td> PAPUA</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/malut.jpg">
                                                                    <span class="d-none d-xl-inline-block">Abdul Ghani Kasuba & Al Yasin Ali</span>
                                                                </div>
                                                            </td>
                                                            <td> MALUKU UTARA</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/sulsel.jpg">
                                                                    <span class="d-none d-xl-inline-block">Nurdin Abdullah &  Andi Sudirman</span>
                                                                </div>
                                                            </td>
                                                            <td> SULAWESI SELATAN</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/maluku.jpg">
                                                                    <span class="d-none d-xl-inline-block">Murad Islmail & Barnabas Ormo</span>
                                                                </div>
                                                            </td>
                                                            <td> MALUKU</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/ntt.jpg">
                                                                    <span class="d-none d-xl-inline-block">Marianus Sae & Emilia J Nomleni</span>
                                                                </div>
                                                            </td>
                                                            <td> NUSA TENGGARA TIMUR</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/ntb.jpg">
                                                                    <span class="d-none d-xl-inline-block">Ahyar Abduh & Mori Hanafi</span>
                                                                </div>
                                                            </td>
                                                            <td> NUSA TENGGARA BARAT</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/bali.jpg">
                                                                    <span class="d-none d-xl-inline-block">I Wayan Koster & Tjokorda Oka</span>
                                                                </div>
                                                            </td>
                                                            <td> BALI</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/sultra.jpg">
                                                                    <span class="d-none d-xl-inline-block">Asrun & Hugua</span>
                                                                </div>
                                                            </td>
                                                            <td> SULAWESI TENGGARA</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/jatim.jpg">
                                                                    <span class="d-none d-xl-inline-block">Saifullah Yusuf & Puti Guntur Soekarno</span>
                                                                </div>
                                                            </td>
                                                            <td> JAWA TIMUR</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/lampung.jpg">
                                                                    <span class="d-none d-xl-inline-block">Herman & Sutono</span>
                                                                </div>
                                                            </td>
                                                            <td> LAMPUNG</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="user-with-avatar">
                                                                    <img alt="" src="img/riau.jpg">
                                                                    <span class="d-none d-xl-inline-block">Andi Rachman & Suyatno</span>
                                                                </div>
                                                            </td>
                                                            <td> RIAU</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
            <input type="hidden" id="base" value="<?=base_url()?>">
        </div>
        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>bower_components/switchery/switchery.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>
    </body>
</html>
