
<!DOCTYPE html>
<html>
    <head>
        <title>SIPP Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/switchery/switchery.min.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
        <style type="text/css">
            .middle{
                margin: auto;
            }
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <?php include 'template/menu.php' ?>
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box" style="padding-top: 10px;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="user-profile compact">
                                        <div class="up-head-w" style="background-image:url(img/profile_bg1.jpg)">
                                            <div class="up-social">           
                                            </div>
                                            <div class="up-main-info">
                                                <h2 class="up-header">PDI PERJUANGAN</h2>
                                                <h6 class="up-sub-header">Sistem Informasi Pemenangan Pemilu</h6>
                                            </div>
                                            <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                                                    <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                                                </g>
                                            </svg>
                                        </div>
										<div style="text-align: center;"><img alt="" src="img/ttd_mega.png" width="270" height="60"></div>
                                        <br>
                                        <div class="up-contents">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                  <div class="element-wrapper">
                                                    <h6 class="element-header" style="margin-bottom: 0px;">
                                                      Visi dan Misi
                                                    </h6>
                                                    <div class="element-box">
                                                      <div class="os-tabs-w">
                                                        <div class="os-tabs-controls">
                                                            <ul class="nav nav-tabs smaller">
                                                                <li class="nav-item">
                                                                    <a class="nav-link active show" data-toggle="tab" href="#tab_visi">Visi</a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" data-toggle="tab" href="#tab_misi">Misi</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active show" id="tab_visi">
                                                                <b>Visi Partai adalah keadaan pada masa depan yang diidamkan oleh Partai, dan oleh karena itu menjadi arah bagi perjuangan Partai.<p>
																Berdasarkan amanat pasal 6 Anggaran Dasar Partai PDI Perjuangan adalah :
																Partai adalah:<p><br></b>
																a. alat perjuangan guna membentuk dan membangun karakter bangsa berdasarkan Pancasila 1 Juni 1945;<p>
																b. alat perjuangan untuk melahirkan kehidupan berbangsa dan bernegara yang ber-Ketuhanan, memiliki semangat sosio nasionalisme, dan sosio demokrasi (Tri Sila);<p>
																c. alat perjuangan untuk menentang segala bentuk individualisme dan untuk menghidupkan jiwa dan semangat gotong royong dalam kehidupan bermasyarakat, berbangsa dan bernegara (Eka Sila);<p>
																d. wadah komunikasi politik, mengembangkan dan memperkuat partisipasi politik warga negara; dan<p>
																e. wadah untuk membentuk kader bangsa yang berjiwa pelopor, dan memiliki pemahaman, kemampuan menjabarkan dan melaksanakan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;
                                                            </div>
                                                            <div class="tab-pane" id="tab_misi">
                                                                <b>Misi Partai adalah muatan hidup yang diemban oleh partai, sekaligus menjadi dasar pemikiran atas keberlangsungan eksistensi Partai, sebagaimana diamanatkan dalam pasal 7,8, 9 dan 10 Anggaran Dasar Partai, yaitu :<p><br></b>
																Pasal 7 Partai mempunyai tujuan umum:<p>
																a. mewujudkan cita-cita Proklamasi Kemerdekaan 17 Agustus 1945 sebagaimana dimaksud dalam Pembukaan Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 dalam bentuk mewujudkan masyarakat adil dan makmur dalam bingkai Negara Kesatuan Republik Indonesia yang bersemboyan Bhinneka Tunggal Ika; dan<p>
																b. berjuang mewujudkan Indonesia sejahtera berkeadilan sosial yang berdaulat di bidang politik, berdiri di atas kaki sendiri di bidang ekonomi, dan Indonesia yang berkepribadian dalam kebudayaan.<p>
																   Pasal 8 Partai mempunyai tujuan khusus:
																a. membangun gerakan politik yang bersumber pada kekuatan rakyat untuk mewujudkan kesejahteraan berkeadilan sosial;<p>
																b. membangun semangat, mengkonsolidasi kemauan, mengorganisir tindakan dan kekuatan rakyat, mendidik dan menuntun rakyat untuk membangun kesadaran politik dan mengolah semua tenaga rakyat dalam satu gerakan politik untuk mencapai kemerdekaan politik dan ekonomi;<p>
																c. memperjuangkan hak rakyat atas politik, ekonomi, sosial dan budaya, terutama demi pemenuhan kebutuhan absolut rakyat, yaitu kebutuhan material berupa sandang, pangan, papan dan kebutuhan spiritual berupa kebudayaan, pendidikan dan kesehatan;<p>
																d. berjuang mendapatkan kekuasaan politik secara konstitusional sebagai alat untuk mewujudkan amanat Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 yaitu mewujudkan pemerintahan yang melindungi segenap bangsa Indonesia dan seluruh tumpah darah Indonesia, memajukan kesejahteraan umum, mencerdaskan kehidupan bangsa, serta ikut melaksanakan ketertiban dunia yang berdasarkan kemerdekaan, perdamaian abadi dan keadilan sosial; dan<p>
																e. menggalang solidaritas dan membangun kerjasama internasional berdasarkan spirit Dasa Sila Bandung dalam upaya mewujudkan cita-cita Pembukaan Undang-Undang Dasar Negara Republik Tahun 1945<p>
																Pasal 9 Partai mempunyai fungsi:<p>
																a. mendidik dan mencerdaskan rakyat agar bertanggung jawab menggunakan hak dan kewajibannya sebagai warga negara;<p>
																b. melakukan rekrutmen anggota dan kader Partai untuk ditugaskan dalam struktural Partai, LembagaLembaga Politik dan Lembaga-Lembaga Publik;<p>
																c. membentuk kader Partai yang berjiwa pelopor, dan memiliki pemahaman, kemampuan menjabarkan dan melaksanakan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;<p>
																d. menghimpun, merumuskan, dan memperjuangkan aspirasi rakyat menjadi kebijakan pemerintahan negara;<p>
																e. menghimpun, membangun dan menggerakkan kekuatan rakyat guna membangun dan mencapai cita-cita masyarakat Pancasila; dan<p>
																f. membangun komunikasi politik berlandaskan hakekat dasar kehidupan berpolitik, serta membangun partisipasi politik warga negara.<p>
																   Pasal 10
																Partai mempunyai tugas:<p>
																a. mempertahankan dan mewujudkan cita-cita negara Proklamasi 17 Agustus 1945 di dalam Negara Kesatuan Republik Indonesia;<p>
																b. mempertahankan, menyebarluaskan dan melaksanakan Pancasila sebagai dasar, pandangan hidup, tujuan berbangsa dan bernegara;<p>
																c. menjabarkan, menyebarluaskan dan membumikan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;<p>
																d. menghimpun dan memperjuangkan aspirasi rakyat berdasarkan ideologi Pancasila 1 Juni 1945 dan Undang Undang Dasar Negara Republik Indonesia 1945, serta jalan TRISAKTI sebagai pedoman strategi dan tujuan kebijakan politik Partai;<p>
																e. memperjuangkan kebijakan politik Partai menjadi kebijakan politik penyelenggaraan Negara;<p>
																f. mempersiapkan kader Partai sebagai petugas Partai dalam jabatan politik dan jabatan publik;<p>
																g. mempengaruhi dan mengawasi jalannya penyelenggaraan negara agar senantiasa berdasarkan pada ideologi Pancasila 1 Juni 1945 dan Undang Undang Dasar Negara Republik Indonesia 1945, serta jalan TRISAKTI sebagai pedoman strategi dan tujuan kebijakan politik Partai demi terwujudnya pemerintahan yang kuat, efektif, bersih dan berwibawa;<p>
																h. sebagai poros kekuatan politik nasional wajib berperan aktif dalam menghidupkan spirit Dasa Sila Bandung untuk membangun konsolidasi dan solidaritas antar bangsa sebagai bentuk perlawanan terhadap liberalisme dan individualisme.<p>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <!--START - Customers with most tickets-->
                                    <div class="element-wrapper">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="element-wrapper">
                                                    <h6 class="element-header" style="margin-bottom: 0px;">
                                                      <img alt="" src="<?=base_url()?>img/logo_pilkada.png" width="270" height="60">
                                                    </h6>
                                                    <div class="element-box">
                                                        <div class="os-tabs-w">
                                                            <div class="os-tabs-controls">
                                                                <ul class="nav nav-tabs smaller">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link active show" data-toggle="tab" href="#tab_gubernur">Calon Gubernur</a>
                                                                    </li>
                                                                    <li class="nav-item" style="display: none;">
                                                                        <a class="nav-link" data-toggle="tab" href="#tab_pilwako">Calon Walikota</a>
																	</li>
																	<li class="nav-item" style="display: none;">
                                                                        <a class="nav-link" data-toggle="tab" href="#tab_pilbup">Calon Bupati</a>
																	</li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" data-toggle="tab" href="#tab_jadwal">Jadwal Pilkada</a>	
                                                                    </li>
																	<li class="nav-item">
                                                                        <a class="nav-link" data-toggle="tab" href="#tab_video">Video</a>	
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="tab-pane active show" id="tab_gubernur">
                                                                    <div class="element-box">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="visibility: hidden;">##########</th>
                                                                                        <th>Calon</th>
                                                                                        <th>Wilayah</th>
																						<th>Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php foreach ($paslon as $key => $value) { ?>
                                                                                        <tr>
                                                                                            <td onclick="load_modal(<?php echo $value->id_paslon?>)">

                                                                                                <img src="<?= base_url("img/paslon/$value->image_ketua")?>"  height="32" width="32"><img src="<?= base_url("img/paslon/$value->image_wakil")?>"  height="32" width="32">
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php echo $value->nama_kepala?> & <?php echo $value->nama_wakil?>
                                                                                            </td>
                                                                                            <td> <?php echo $value->area?></td>
                                                                                        <?php 
                                                                                            if($value->status == 1){
                                                                                                $status = '<a class="badge badge-success"><font style="color: #ffffff">Menang</font></a>';
                                                                                            }else{
                                                                                                $status = '<a class="badge badge-danger"><font style="color: #ffffff">Kalah</font></a>';
                                                                                            }
                                                                                        ?>

                                                                                          <td><?php echo $status?></td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
																<div class="tab-pane" id="tab_pilbup">
                                                                    <div class="element-box">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Calon</th>
                                                                                        <th>Wilayah</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/jabar.jpg">
                                                                                                <span class="d-none d-xl-inline-block" data-target="#exampleModal1" data-toggle="modal">TB Hasanuddin & Anton Charliyan</span>
																								</div>
                                                                                        </td>
                                                                                        <td> JAWA BARAT</td>                                          
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/kalbar.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Karolin Margret & Suryadman Gidot</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KALIMANTAN BARAT</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/sumut.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Djarot Saiful Hidayat & Sihar Sitorus</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SUMATERA UTARA</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/sumsel.jpg">
                                                                                                <span>Dodi Reza & Giri Ramandha Kiemas</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SUMATERA SELATAN</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/jateng.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Ganjar Pranowo &  Gus Yasin</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> JAWA TENGAH</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/kaltim.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Safaruddin</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KALIMANTAN TIMUR</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/papua.jpg">
                                                                                                <span class="d-none d-xl-inline-block">John Wempi & Habel Melkias</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PAPUA</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/malut.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Abdul Ghani Kasuba & Al Yasin Ali</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> MALUKU UTARA</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/sulsel.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Nurdin Abdullah &  Andi Sudirman</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SULAWESI SELATAN</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/maluku.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Murad Islmail & Barnabas Ormo</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> MALUKU</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/ntt.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Marianus Sae & Emilia J Nomleni</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> NUSA TENGGARA TIMUR</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/ntb.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Ahyar Abduh & Mori Hanafi</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> NUSA TENGGARA BARAT</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/bali.jpg">
                                                                                                <span class="d-none d-xl-inline-block">I Wayan Koster & Tjokorda Oka</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> BALI</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/sultra.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Asrun & Hugua</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SULAWESI TENGGARA</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/jatim.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Saifullah Yusuf & Puti Guntur Soekarno</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> JAWA TIMUR</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/lampung.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Herman & Sutono</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> LAMPUNG</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/riau.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Andi Rachman & Suyatno</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> RIAU</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab_pilwako">
                                                                    <div class="element-box">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Calon</th>
                                                                                        <th>Wilayah</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block" data-target="#exampleModal1" data-toggle="modal">drh. JALALUDDIN & WAGIMAN</span>
																								</div>
                                                                                        </td>
                                                                                        <td> KOTA SUBULUSSALAM (ACEH)</td>                                          
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H.AFFAN ALFIAN, SE & Drs. SALMAZA, M.AP</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KOTA SUBULUSSALAM (ACEH)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. ANASRI, ST. MT & SABARUDDIN S. S.Pd.I</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KOTA SUBULUSSALAM (ACEH)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span>H. ASMAUDDIN, SE & Hj. ASMIDAR, S.Pd</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KOTA SUBULUSSALAM (ACEH)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">SARTINA, NA, SE. M.Si &  DEDI ANWAR BANCIN, SE</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> KOTA SUBULUSSALAM (ACEH)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Drs.H.HAILULLAH HARAHAP.MM & Drs.H.AMAS MUDA HASIBUAN</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG SIDIMPUAN (SUMATERA UTARA)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. MUHAMMAD ISNANDAR NASUTION, S.Sos & DR. H. ALI PADA HARAHAP, M.Pd</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG SIDIMPUAN (SUMATERA UTARA)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. RUSYDI NASUTION, S.T.P., M.M & Drs. H. ABD. ROSAD LUBIS, MM</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG SIDIMPUAN (SUMATERA UTARA)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">IRSAN EFENDI NASUTION, SH &  Ir. H. ARWIN SIREGAR, MM</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG SIDIMPUAN (SUMATERA UTARA)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. MAHYELDI, SP & HENDRI SEPTA, B.BUS (Acc)., MIB</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Ir. H. EMZALMI, M.Si & H. DESRI AYUNDA, SE, MBA</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Letkol (Purn) SYAMSUAR SYAM.S,Sos & MISLIZA</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">ALI YUSUF, S.Pt & H. ISMED, SH</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SAWAHLUNTO (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">DERI ASTA, SH & ZOHIRIN SAYUTI, SE</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SAWAHLUNTO (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. FAUZI HASAN & H. DASRIAL ERY, SE.MM</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> SAWAHLUNTO (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">FADLY AMRAN, BBA & Drs. ASRUL</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG PANJANG (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
																						<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">H. HENDRI ARNIS & H. EKO FURQANI</span>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> PADANG PANJANG (SUMATERA BARAT)</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">Ir. RAFDI M. SYARIF & AHMAD FADLY, S.Psi</span>
																					</div>
                                                                                        </td>
                                                                                        <td> PADANG PANJANG (SUMATERA BARAT)</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">MAWARDI & TAUFIQ IDRIS</span>
																					</div>
                                                                                        </td>
                                                                                        <td> PADANG PANJANG (SUMATERA BARAT)</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">DEWI FITRI DESWATI, S.Pi & PABRISAL</span>
																					</div>
                                                                                        </td>
                                                                                        <td> PARIAMAN (SUMATERA BARAT)</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">DR. GENIUS UMAR, S.Sos, M.Si & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>
																					<div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>
																					</div>
                                                                                        </td>
                                                                                        <td> WILAYAH</td>			
																					</tr>
                                                                                    <tr>
                                                                                        <td>	
                                                                                            <div class="user-with-avatar">
                                                                                                <img alt="" src="img/icon_calon.jpg">
                                                                                                <span class="d-none d-xl-inline-block">A & B</span>			
                                                                                            </div>
                                                                                        </td>
                                                                                        <td> WILAYAH </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
																
																
																<div class="tab-pane" id="tab_jadwal">
                                                                    <div class="element-box">
                                                                   <!--      <div class="table-responsive">
                                                                            <table class="table table-lightborder"> -->
                                    <!-- <div class="element-box"> -->
                                        <h6 class="element-header">Jadwal Pilkada</h6>
                                        <div style="text-align: center;"><img alt="" src="img/jadwal.jpg" width="600" height="3300"></div>                                                                                                                                                                 
                                                                           <!--      </tbody>
                                                                            </table> -->
                                                                        <!-- </div> -->
                                                                    </div>
                                                                </div>																																
																<div class="tab-pane" id="tab_video">
                                                                    							<!-- <div class="top-big-grids"> -->
											<iframe  width="100%" height="315"
											src="https://www.youtube.com/embed/121t0yAbmLc">
											</iframe>
											<!-- </div> -->
									
										<!-- <div class="col-md-4 top-grid-left-info"></div> -->
										<iframe width="100%" height="315"
											src="https://www.youtube.com/embed/fILiTzEyOF4">
											</iframe>
										
											<!-- <div class="col-md-4 top-grid-left-info"></div> -->
										<iframe width="100%" height="315"
											src="https://www.youtube.com/embed/gij0TiYmFZo">
											</iframe>
											<!-- </div> -->
										<!-- </div> -->
								<!-- 		<div class="col-md-4 top-grid-left-info">
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div> -->
                                                                    <!-- </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>

            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal1" role="dialog" tabindex="-1">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="<?=base_url('img/jabar.jpg')?>">
                        </div>
                        <!-- PROFILE CALON 1 -->
                        <div class="col-sm-6">
                            <div class="element-box">
                                NAMA
                            </div>
                        </div>
                        <!-- PROFILE CALON 2 -->
                        <div class="col-sm-6">
                            <div class="element-box">
                                NAMA
                            </div>
                        </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal2" role="dialog" tabindex="-1">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="<?=base_url('img/jabar.jpg')?>">
                        </div>
                        <!-- PROFILE CALON 1 -->
                        <div class="col-sm-6">
                            <div class="element-box">
                                NAMA
                            </div>
                        </div>
                        <!-- PROFILE CALON 2 -->
                        <div class="col-sm-6">
                            <div class="element-box">
                                NAMA
                            </div>
                        </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" id="base" value="<?=base_url()?>">
        </div>
        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>bower_components/switchery/switchery.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>
        <script type="text/javascript">
            var base = $("#base").val();
        function load_modal(id){
            $.ajax({
                url: base+'home/get_paslon_detail/'+id,
                cache: false,
                type:"POST",
                dataType : 'json',
                success: function(respond){
                    console.log(respond);
                    $("#exampleModal1").modal('show');
                }
            })
        }
    </script>
    </body>
</html>
