
<!DOCTYPE html>
<html>
  <head>
    <title>SIPP Dashboard</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="css/main.css?version=4.2.0" rel="stylesheet">
    <!-- <link href="assets2/css/dafters.css" rel="stylesheet"> -->
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
    <style type="text/css">
      body:before{
            background: -webkit-gradient(linear, left top, right bottom, from(#c40000), to(#c40000));
      }

    .auth-box-w .logo-w {
        text-align: center;
        padding-top:  30px;
        padding-bottom:  30px;
        padding-left:  20%;
        padding-right:  20%;
    }

    </style>
  </head>
  <body class="auth-wrapper" style="background-color: #c40000 !important;">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">
          <a href="#"><img alt="" src="img/logo-teknopol.png" width="200" height="110"></a>
        </div>
        <h4 class="auth-header">
          Login
        </h4>
        <form id="login">
          <div class="form-group">
            <label for="">Username</label><input class="form-control" name="username" placeholder="Masukkan Username" type="text">
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
          </div>
          <div class="form-group">
            <label for="">Password</label><input class="form-control" name="password" placeholder="Masukkan Password" type="password" autocomplete="off">
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
          </div>
          <!-- <div class="g-recaptcha" data-sitekey="6LcHrVQUAAAAAIgstNZyPxWlcbmijqh0BJO5414S"></div> -->
          <div class="buttons-w">
            <button class="btn btn-primary">Login</button>
          </div>
        </form>
      </div>
    </div>
  </body>
</html>
<script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $('#login').submit(function(event){
        event.preventDefault();
                    
        $.ajax({
          url: "<?= site_url().'auth/login'?>",
          type : 'post',
          data : $( "#login" ).serialize(),
          dataType: "json",
          success : function(data){
              console.log(data);
              if(data.status == 1){
                location.reload();
              }else{
                location.reload();
              }
          },
          error: function(data){
            console.log(data);
              alert('ERROR');
          }
                        
        });
        return false;
    });
</script>