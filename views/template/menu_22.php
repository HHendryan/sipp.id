<div class="top-menu-secondary with-overflow color-scheme-dark" style="height: 52px;">
                <!--START - Messages Link in secondary top menu-->
                <div class="logo-w menu-size">
                    <a class="logo" href="<?=base_url()?>"><img src="<?=base_url()?>img/logo-teknopol.png"></a>
                </div>
                <ul class="main-menu">
                      <li>
                        <a href="<?=site_url()?>home">
                          <div class="icon-w">
                            <div class="os-icon os-icon-grid-squares-22"></div>
                          </div>
                          <span>Home</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>dashboard">
                          <div class="icon-w">
                            <div class="os-icon os-icon-bar-chart-stats-up"></div>
                          </div>
                          <span>Dashboard</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>dashboard/second">
                          <div class="icon-w">
                            <div class="os-icon os-icon-bar-chart-stats-up"></div>
                          </div>
                          <span>Dashboard 2</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>maps">
                          <div class="icon-w">
                            <div class="os-icon os-icon-mail-18"></div>
                          </div>
                          <span>Peta Politik</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>maps/navigation">
                          <div class="icon-w">
                            <div class="os-icon os-icon-mail-18"></div>
                          </div>
                          <span>Navigasi</span>
                        </a>
                      </li>
                      <li>
                        <a href="peta_politik.html">
                          <div class="icon-w">
                            <div class="os-icon os-icon-delivery-box-2"></div>
                          </div>
                          <span>Perolehan Suara</span>
                        </a>
                      </li>
                      <?php 
                        if($role == 'client'){
                          $disp = 'none';
                        }else{
                          $disp = '';
                        }
                      ?>
                      <li style="display: <?php echo $disp ?>">
                        <a href="<?=site_url()?>about">
                          <div class="icon-w">
                            <div class="os-icon os-icon-user-male-circle"></div>
                          </div>
                          <span>Tentang IPOL</span>
                        </a>
                      </li>
                    </ul>
                <!--START - Top Menu Controls-->
                <div class="top-menu-controls">
                    <!--START - User avatar and menu in secondary top menu-->
                    <div class="logged-user-w">
                        <div class="logged-user-i">
                            <div class="avatar-w">
                                <img alt="" src="<?=base_url()?>img/avatar1.jpg">
                            </div>
                            <div class="logged-user-menu">
                                <div class="logged-user-avatar-info">
                                    <div class="avatar-w">
                                        <img alt="" src="<?=base_url()?>img/avatar1.jpg">
                                    </div>
                                    <div class="logged-user-info-w">
                                        <div class="logged-user-name"><?php echo $username ?></div>
                                        <div class="logged-user-role"><?php echo $role ?></div>
                                    </div>
                                </div>
                                <div class="bg-icon">
                                    <i class="os-icon os-icon-wallet-loaded"></i>
                                </div>
                                <ul>
                                    <li>
                                        <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile</span></a>
                                    </li>
                                    <li>
                                        <a href="<?=site_url('auth/logout')?>"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--END - User avatar and menu in secondary top menu-->
                </div>
                <!--END - Top Menu Controls-->
            </div>