<!--START - Menu top image -->
                <div class=" desktop-menu menu-top-w menu-activated-on-hover color-scheme-dark">
                  <div class="menu-top-i">
                    <div class="logo-w">
                        <a class="logo" href="<?=base_url()?>">
                            <img src="<?=base_url()?>img/logo-teknopol.png">
                        </a>
                    </div>
                    <ul class="main-menu">
                      <li>
                        <a href="<?=site_url()?>home">
                          <div class="icon-w">
                            <div class="os-icon os-icon-grid-squares-22"></div>
                          </div>
                          <span>Home</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>dashboard">
                          <div class="icon-w">
                            <div class="os-icon os-icon-bar-chart-stats-up"></div>
                          </div>
                          <span>Dashboard</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>maps">
                          <div class="icon-w">
                            <div class="os-icon os-icon-mail-18"></div>
                          </div>
                          <span>Peta Politik</span>
                        </a>
                      </li>
                      <li>
                        <a href="peta_politik.html">
                          <div class="icon-w">
                            <div class="os-icon os-icon-delivery-box-2"></div>
                          </div>
                          <span>Perolehan Suara</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?=site_url()?>about">
                          <div class="icon-w">
                            <div class="os-icon os-icon-user-male-circle"></div>
                          </div>
                          <span>Tentang IPOL</span>
                        </a>
                      </li>
                    </ul>
                    <div class="logged-user-w">
                        <div class="avatar-w">
                            <img alt="" src="<?=base_url()?>img/avatar1.jpg">
                        </div>
                    </div>
                  </div>
                </div>
                <!--END - Menu top image -->