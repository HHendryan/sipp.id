<div class="row">
	<div class="col-md-12" style="padding-top: 10px">
    <h6 class="element-header">SIMULASI KEMENANGAN KANDIDAT</h6></div>
	<div class="col-md-4">	
		<div class="element-box el-tablo">
			
        <div class=" centered ">
            <div class="label">DPT 17 PROVINSI</div>
            <div class="value">120.000.000</div>
        </div>

		</div>
	</div>
        <div class="col-md-4">
        <div class="element-box el-tablo">
            
            <div class=" centered ">
                <div class="label">TARGET SUARA</div>
                <div class="value">70.708.000</div>
            </div>

        </div>  
    </div>
    <div class="col-md-4">
        <div class="element-box el-tablo">
            <div class=" centered ">
                <div class="label">POTENSI SUARA GURAKLIH</div>
                <div class="value">65.000.000</div>
            </div>
        </div>  
    </div>

	<div class="col-md-12">
		<div class="">
				<div class="table-responsive">
    <table class="table table-bordered table-lg table-v2 table-striped">
        <thead>
            <tr>
                <th colspan="2">Calon</th>
                <th>Wilayah</th>
                <th>Jumlah TPS</th>
                <th>Jumlah DPT</th>
                <th>Asumsi Suara Sah (75%)</th>
                <th>Target Menang (%)</th>
                <th>Target Perolehan Suara</th>
              
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>
            <tr>
                <td class="nowrap">
                	<a href="">
                		<div class="cell-image-list">
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <div class="cell-img" style="background-image: url(<?=base_url();?>img/avatar1.jpg)"></div>
                        <!-- <div class="cell-img-more">+ 5 more</div> -->
                    </div>
                	</a>
                </td>
                <td>
                    Kodim - Ojan
                </td>
                <td class="text-center">
                    DKI Jakarta
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
                <td class="text-center">
                	139019
                </td>
            </tr>

  
        </tbody>
    </table>
</div>	
		</div>
	</div>
    <div class="col-md-6">
        <div class="element-box el-tablo">
            
            <div class=" centered ">
                <div class="label">KEKURANGAN PEMILIH TETAP NASIONAL</div>
                <div class="value">7.708.000</div>
            </div>

        </div>

    
    </div>
    <div class="col-md-6">
        <div class="element-box el-tablo">
            
            <div class=" centered ">
                <div class="label">DATA INFLUENCER NASIONAL</div>
                <div class="value">1.000</div>
            </div>

        </div>

    </div>

</div>





<div class="row">
	<div class="col-md-12"><h6 class="element-header">SIMULASI KEMENANGAN KANDIDAT</h6></div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DPT JAWA BARAT</div>
			    <div class="value">31.708.000</div>
			</div>

		</div>



	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">TARGET SUARA</div>
			    <div class="value">7.708.000</div>
			</div>

		</div>	
	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			<div class=" centered ">
			    <div class="label">POTENSI SUARA GURAKLIH</div>
			    <div class="value">7.000.000</div>
			</div>
		</div>	
	</div>

	<div class="col-md-12">
		<br>
	
		
				<div class="table-responsive">
    <table class="table table-bordered table-lg table-v2 table-striped">
        <thead>
            <tr>
                <th>KABUPATEN</th>
                <th>TIM GURAKLIH</th>
                <th>TARGET SUARA KABUPATEN</th>
                <th>TARGET SUARA KABUPATEN</th>
                <th>POOTENSI SUARA</th>
              
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>

           
        </tbody>
    </table>
</div>			
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">KEKURANGAN PEMILIH TETAP JAWA BARAT</div>
			    <div class="value">7.708.000</div>
			</div>

		</div>

	
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DATA INFLUENCER JAWA BARAT</div>
			    <div class="value">1.000</div>
			</div>

		</div>

	</div>
</div>




<div class="row">
	<div class="col-md-12"><h6 class="element-header">SIMULASI KEMENANGAN KANDIDAT</h6></div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DPT KAB. BOGOR</div>
			    <div class="value">31.708.000</div>
			</div>

		</div>



	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">TARGET SUARA</div>
			    <div class="value">7.708.000</div>
			</div>

		</div>	
	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			<div class=" centered ">
			    <div class="label">POTENSI SUARA GURAKLIH</div>
			    <div class="value">7.000.000</div>
			</div>
		</div>	
	</div>

	<div class="col-md-12">
		<br>
	
		
				<div class="table-responsive">
    <table class="table table-bordered table-lg table-v2 table-striped">
        <thead>
            <tr>
                <th>KABUPATEN</th>
                <th>TIM GURAKLIH</th>
                <th>TARGET SUARA KABUPATEN</th>
                <th>TARGET SUARA KABUPATEN</th>
                <th>POOTENSI SUARA</th>
              
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>

           
        </tbody>
    </table>
</div>			
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">KEKURANGAN PEMILIH TETAP KAB. BOGOR</div>
			    <div class="value">908.000</div>
			</div>

		</div>

	
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DATA INFLUENCER KAB. BOGOR</div>
			    <div class="value">300</div>
			</div>

		</div>

	</div>
</div>


<div class="row">
	<div class="col-md-12"><h6 class="element-header">SIMULASI KEMENANGAN KANDIDAT</h6></div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DPT MALASARI</div>
			    <div class="value">4000</div>
			</div>

		</div>



	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">TARGET SUARA</div>
			    <div class="value">1200</div>
			</div>

		</div>	
	</div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
			<div class=" centered ">
			    <div class="label">POTENSI SUARA GURAKLIH</div>
			    <div class="value">250</div>
			</div>
		</div>	
	</div>

	<div class="col-md-12">
		<br>
	
		
				<div class="table-responsive">
    <table class="table table-bordered table-lg table-v2 table-striped">
        <thead>
            <tr>
                <th>DATA TPS</th>
                <th>TIM GURAKLIH</th>
                <th>TARGET SUARA DESA</th>
                <th>POTENSI SUARA</th>
              
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
             <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
             <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
             <tr>
                <td class="text-left">
                    DKI Jakarta
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
                <td class="text-right">
                	139019
                </td>
            </tr>
  
           
        </tbody>
    </table>
</div>			
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">KEKURANGAN PEMILIH TETAP MALASARI</div>
			    <div class="value">800</div>
			</div>

		</div>

	
	</div>
	<div class="col-md-6">
		<div class="element-box el-tablo">
			
			<div class=" centered ">
			    <div class="label">DATA INFLUENCER MALASARI</div>
			    <div class="value">12</div>
			</div>

		</div>

	</div>
</div>
<script type="text/javascript">
    <?php 
    if($nama == 'Nasional'){
        $tipe = '';
    }
?>
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=$tipe?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";
</script>


