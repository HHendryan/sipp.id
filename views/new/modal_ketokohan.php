<div class="row">

<?php 
$score1=0;
$score2=0;
$tottangin=$jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible'];
if ($tottangin) {
	$munculin1='';
} else {
	$munculin1='style="display:none;"';
	$score1=1;
	// echo'ngga ada';
}


if($jum_ketokohan_partai){
	$munculin2='';
} else {
	$munculin2='style="display:none;"';
	$score2=1;
}

$scoreall=$score1+$score2;
// echo $scoreall;

if($scoreall>1){
echo '<div style="width:100%;padding-bottom: 70px; padding-top: 70px;font-size: 25px; color: #c9c9c9; font-weight: bold;">
              <div align="center">DATA TIDAK TERSEDIA</div>
          </div>';
}


?>



   	<div class="col-md-6" <?=$munculin1;?> >

   		<div id="graph_ketokohan" style="width: 100%; height: 700px;" ></div>
		<!-- <br>
		<br>
        <div align="left">
            <div class="el-legend">
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #000;"></div>
                    <div class="legend-value">Total : <?= number_format($jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible']) ?></div>
                </div>
               <div style="padding-left: 90px;">
               	 <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #F69200;"></div>
                    <div class="legend-value">Jumlah Formal : <?=number_format($jum_ketokohan_group['jum_tangible']);?></div>
                </div>
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #A6A6A6;"></div>
                    <div class="legend-value">Jumlah Tidak Formal : <?=number_format($jum_ketokohan_group['jum_intangible']);?></div>
                </div>
               </div>

            </div>            
        </div> -->
   	</div>
  	<div class="col-md-6" <?=$munculin2;?> >
  		<div id="pie_netral" style="width: 100%; height: 300px;" ></div>
  		<div id="chartdiv" style="width: 100%; height: 300px;" ></div>
	</div>
</div>
<?php $tottangin=$jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible']?>

<!-- <script type="text/javascript">
AmCharts.makeChart("ketokohan_pie",
		{
			"type": "pie",
			// "angle": 7.2,
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			// "depth3D": 20,
			// "innerRadius": 0,
			// "labelRadius": 0,
			// "outlineThickness": 0,
			// "titleField": "category",
			// "valueField": "column-1",
			"fontSize": 12,
			// "handDrawScatter": 1,
			"theme": "light",
			// "allLabels": [],
			// "balloon": {},
			// "legend": {
			// 	"enabled": false,
			// 	"align": "center",
			// 	"labelWidth": 1,
			// 	"markerType": "circle",
			// 	"maxColumns": 8,
			// 	"tabIndex": 0,
			// 	"valueWidth": 20
			// },
			  "allLabels": [{
			    "text": "Total",
			    "align": "center",
			    "bold": true,
			    "y": 145
			  }, {
			    "text": '<?= number_format($tottangin) ?>',
			    "align": "center",
			    "bold": false,
			    "y": 170
			  }],	

			"titles": [{
				"id": "Influencer",
				"text": "Influencer"
			}],
			// "colors": [
			// 	"#73d2de",
			// 	"#ffbc42"
			// ],
			"dataProvider": [
				{
					"category": "Formal",
					"column-1": "<?=$jum_ketokohan_group['jum_tangible']?>",
					"color": "#F69200",
				},
				{
					"category": "Informal",
					"column-1": "<?=$jum_ketokohan_group['jum_intangible']?>",
					"color": "#A6A6A6",
				}
			],
			"listeners": [{
	        	"event": "clickSlice",
	            "method": function(event) {
	                console.log();
	                nama = event.dataItem.dataContext.category;
	                if(nama == 'Informal'){
	                	tipe = 'intangible';
	                }else{
	                	tipe = 'tangible';
	                }
	             	load_detail(tipe);   
	            }
	        }],
				  // "titleField": "title",
			  "valueField": "column-1",
			  "colorField": "color",
			  "labelRadius": 10,
			  "radius": "42%",
			  "innerRadius": "60%",
			  "labelText": "",	
		}
	);


</script> -->
<script type="text/javascript">
	$( document ).ready(function() {
	    load_detail('all');
	});
</script>
<script type="text/javascript">
	var chart = AmCharts.makeChart("graph_ketokohan", {
      	"type": "serial",
	  	"theme": "light",
	  	"colorField": "color",
	  	"marginRight": 70,
	  	"dataProvider": [
	  		<?php foreach ($jum_ketokohan_id as $key => $value) {?>
	  		{
            	"name": "<?=$value->nama?>",
            	"points": <?=$value->percentage?>,
        	},
	  		<?php } ?>
        ],
	  	"startDuration": 1,
	  	"graphs": [{
			"balloonText": "<b>[[category]]: [[value]] %</b>",
			"fillColorsField": "color",
			"labelText": "[[value]]%",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "points"
	  	}],
	  	"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
	  	},
	  	"rotate": true,
	  	"categoryField": "name",
	  	"categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  	"valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
		"listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
				nama = event.item.category;
				// alert(nama);
				load_detail(nama);
            }
        }],
		"export": {
	    	"enabled": true,
			"divId":"exportdivGraphP1"
	  	}
	});
</script>
<script type="text/javascript">
	function load_detail(tipe_tokoh){
		id = '<?=$id?>';
		tipe = '<?=$tipe?>';
		tipe_tokoh = encodeURI(tipe_tokoh);
		$.ajax({url: "<?=site_url('dashboard/detail_influencer_modal/')?>"+id+'/'+tipe+'/'+tipe_tokoh, success: function(result){
			data = JSON.parse(result);
			console.log(data);
			var chart = AmCharts.makeChart("pie_netral", {
		      	"type": "pie",
		      	"pullOutRadius" : "5%",
				"radius": 100,
		      	"theme": "light",
		      	"pulledField": 'pulled',
		      	"colorField": "color",
		       	"labelRadius": 10,
		        "labelText": "[[country]] : [[percents]]%",
		      	"dataProvider": [
		      	{
			    	"country": "Netral",
			        "litres": data.netral,
		          	"pulled":true,
			    },
			    {
			    	"country": "Afiliasi Politik",
			        "litres": data.pilih,
		          	"pulled":true,
			    },
		  		],
		      	"valueField": "litres",
		      	"titleField": "country",
		      	"listeners": [{
		        	"event": "clickSlice",
		            "method": function(event) {
		                console.log(event.dataItem.title);
						nama_survey = event.dataItem.title;
		            }
		        }],
		      	"export": {
			    	"enabled": true,
					"divId": "exportdivPendidikan"
			  	}
		    });
		    var chart = AmCharts.makeChart("chartdiv", {
		        "type": "serial",
		        "theme": "light",
		        
		        "dataProvider": data.partai,
		        "titles": [{
		        "text": "10 BESAR PERSENTASE AFILIASI POLITIK TOKOH BERPENGARUH"
		         }],
		        "startDuration": 2,
		        "graphs": [{
		            "balloonText": "[[category]]: <b>[[value]]</b>%",
		            "bulletOffset": 10,
		            "labelText": "[[value]]%",
		            "bulletSize": 42,
		            "colorField": "color",
		            // "cornerRadiusTop": 8,
		            // "customBulletField": "bullet",
		            "fillAlphas": 0.8,
		            "lineAlpha": 0,
		            "type": "column",
		            "valueField": "points",
		        }],
		        "marginTop": 0,
		        "marginRight": 0,
		        "marginLeft": 0,
		        "marginBottom": 0,
		        // "autoMargins": false,
		        "categoryField": "name",
		        "categoryAxis": {
		            "gridColor": "#FFFFFF",
		            "gridPosition": "start",
		            "axisAlpha": 0,
		            "fillAlpha": 0.00,
		            "position": "left",
		            "labelRotation": 90
		        },
		        "valueAxes": [{
		            "axisAlpha": 0,
		            "labelsEnabled": false,
		            "gridColor": "#FFFFFF",
		            "gridAlpha": 0,
		            "dashLength": 0
		        }],
		    });
	        $("#table_ketokohan").html(result);
	    }});
	}
</script>