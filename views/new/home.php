<style>
#opsiInf {
	position: absolute;
	top: 20px;
	left: 20px;
	padding: 10px;
	font-size: 15px;
}
#opsi {
	position: absolute;
	top: 20px;
	right: 20px;
	padding: 10px;
	font-size: 15px;
	visibility: hidden;
}
#maps {
	width: 100%;
	height: 500px;
	margin: 10px auto;
	position: relative;
}
.mapdiv {
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
}
</style>
<div class="content-w">

<!--     <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="index.html">Products</a></li>
        <li class="breadcrumb-item"><span>Laptop with retina screen</span></li>
    </ul>
 -->
    <div class="content-i">
        <div class="content-box">
            <div class="row">
                <div class="col-md-12">
                  <div class="element-box">
                    <!--<div id="map" style="width: 100%; height: 300px;"></div>-->
					
					<div id="maps">
						<div id="kabupatenmap" class="mapdiv" style="visibility:hidden"></div>
						<div id="provinsimap" class="mapdiv" style="visibility:visible"></div>
					</div>
					
					<div id="opsiInf" style="max-width: 30%">
						<select class="form-control opsiInf text-secondary"id="sel1" onchange="get_pilihan(this.value);" style="font-weight:bold;">
							<option value="modal_simulasi" class="text-secondary" style="font-weight:bold;">PILGUB 2018</option>
							<option value="modal_parpol" class="text-secondary" style="font-weight:bold;">PARPOL 2019</option>
							<option value="modal_ketokohan" class="text-secondary" style="font-weight:bold;">TOKOH BERPENGARUH</option>
						</select>
					</div>
					<div id="opsi" style="max-width: 30%">
						<button id="home" onclick="reload_home();" class="btn btn-danger">KEMBALI</button>
					</div>
					<div id="modal_profile" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
							  <div class="modal-header">
								<h5 class="modal-title" id="modal_profile_title">

								</h5>
								<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
							  </div>
							  <div class="modal-body" id="modal_profile_body">
								<div class="row">
									<div class="col-md-6">ini pie jens kelamin</div>
									<div class="col-md-6"> ini pie usia</div>
								</div>
							  </div>
							  <div class="modal-footer">
								<button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
							  </div>
							</div>
						</div>
					</div>
					<!--
					<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">UNDER MAINTENANCE</h4>
						  </div>
						  <div class="modal-body" style="max-height: 400px; overflow: scroll; overflow-x: hidden;">
							<div id="content">
								<table id="content_data" class="table">
									<thead>
										<tr>
											<th>ID Daerah</th>
											<th>Nama Daerah</th>
											<th>Alias</th>
											<th>Latitude</th>
											<th>Longitude</th>
										</tr>
									</thead>
								</table>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-8">
										<div id="chart_hasil" style="width: 640px; height: 400px;"></div>
									</div>

								</div>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>
					  </div>
					</div>
					-->
                  </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12" align="center" style="padding-bottom: 30px">
                <img alt="" src="<?=base_url();?>img/logo_pilkada.png" height="60" width="270">
              </div>
              <div class="col-md-12">
        <ul class="medium-block-grid-6">
          <?php foreach ($paslon as $key => $value) {
    if ($value->kepala_alias == 'GUS IPUL') {
        $tes = 'H. SAIFULLAH YUSUF';
    } else if ($value->kepala_alias == 'HERMAN') {
        $tes = 'HERMAN HASANUSI';
    } else if ($value->kepala_alias == 'KAROLIN') {
        $tes = 'KAROLIN MARGRET NATASA';
    } else {
        $tes = $value->kepala_alias;
    }
    ?>
          <li>
            <div class="element-box" style="min-height: 370px">
              <a href="javascript:void(0)" class="" style="text-align:center;">
                <img onclick="show_detail('<?=$value->id_paslon?>')" class="rounded-circle img-fluid" src="<?=base_url();?>/img/paslon/<?=$value->image_paslon?>" height="75" width="400">
              </a>
              <br>
              <br>
              <div class="text-center" style="margin-left:-30px; margin-right:-30px;">
                <div style="min-height: 80px;" >
                  <b><?=$tes?></b><br><b><?=$value->wakil_alias?></b>
                </div>
                <div style="min-height: 50px;width:100%;">
                  <a href="<?=base_url("home/test/")?><?=$value->id_area?>" class="btn btn-outline-danger" style="font-weight:bold;font-size:0.7rem;margin-top:10px;"><?=$value->area?></a>
                </div>
              </div>
            </div>
          </li>
        <?php }?>
              </ul>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-6" >
                <div class="element-wrapper" >
                    <h6 class="element-header">PERSENTASE HASIL PILIHAN PRESIDEN 2014</h6>
                    <div class="element-box" style="min-height: 430px">
                      <div class="row">
                        <div class="col-md-6">
                          <img src="<?=base_url();?>img/Capres2014/jokowi_jk.jpg" class='img-fluid'>
                          <br>
                          <br>
                          <table width="100%">
                            <tr style="font-weight:bold;">
                              <td>Nama Capres</td>
                              <td>:</td>
                              <td>Joko Widodo</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Pendamping</td>
                              <td>:</td>
                              <td>Jusuf Kalla</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Partai</td>
                              <td>:</td>
                              <td>PDI Perjuangan</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Suara Rakyat</td>
                              <td>:</td>
                              <td><span class="btn btn-danger btn-sm"><b>70.997.883</b></span></td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Persentase</td>
                              <td>:</td>
                              <td><span class="btn btn-danger btn-sm"><b>53,15%</b></span></td>
                            </tr>
                          </table>
                        </div>
                        <div class="col-md-6">
                          <img src="<?=base_url();?>img/Capres2014/prabowo_hatta.jpg"  class='img-fluid'>
                           <br>
                          <br>
                          <table width="100%">
                            <tr style="font-weight:bold;">
                              <td>Nama Capres</td>
                              <td>:</td>
                              <td>Prabowo Subianto</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Pendamping</td>
                              <td>:</td>
                              <td>Hatta Rajasa</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Partai</td>
                              <td>:</td>
                              <td>Gerindra</td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Suara Rakyat</td>
                              <td>:</td>
                              <td><span class="btn btn-danger btn-sm"><b>62.576.444</b></span></td>
                            </tr>
                            <tr style="font-weight:bold;">
                              <td>Persentase</td>
                              <td>:</td>
                              <td><span class="btn btn-danger btn-sm"><b>46,85%</b></span></td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
           <!--          <div class="element-box-tp">
                        <?php foreach ($survey_pilpres_14 as $key => $value) {?>
                        <div class="profile-tile">
                          <a class="profile-tile-box" href="users_profile_small.html">
                              <div class="pt-avatar-w"><img alt="" src="img/avatar1.jpg"></div>
                              <div class="pt-user-name"><?=$value->nama?></div>
                          </a>
                          <div class="profile-tile-meta">
                              <ul>
                                <li>Persentase:<strong><a href="#"><?=$value->jumlah?> %</a></strong></li>

                              </ul>

                          </div>
                        </div>
                        <?php }?>
                    </div> -->
                  </div>
                </div>
                <div class="col-sm-6" >
                <div class="element-wrapper" >
                  <h6 class="element-header">PERSENTASE HASIL SURVEI CALON PRESIDEN 2019 (10 BESAR)</h6>
                  <div class="element-box" style="min-height: 430px">
                    <div class="row">
                      <div class="col-md-6">
                        <table style="width: 100%">
                          <?php $total = 0;
                          foreach ($survey_pilpres_19 as $key => $value) {
                              $total = $total + $value->total;
                          }
                          $total_pilpres19 = $total;
                          ?>
                          <?php $i = 0;
$pilres2 = '';foreach ($survey_pilpres_19 as $key => $value) {if ($i < 5) {?>
							  <tr>
								<td>
								  <div class="fotocalon" style="text-align:center">
									  <img class="rounded-circle img-fluid" src="<?=base_url();?>/img/paslon/<?=$value->img_paslon?>" height='40' width="40">
								  </div>
								</td>
								<td>
								  <div class="os-progress-bar danger">
									<div class="bar-labels">
									  <div class="bar-label-left"><span class="bigger" ><?=ucwords(strtolower($value->nama))?></span></div>
									  <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;"><?=$value->jumlah?> %</span></div>
									</div>
									<div class="bar-level-1" style="width: 100%">
									  <div class="bar-level-2" style="width: <?=$value->jumlah?>%">
										<div class="bar-level-3" style="width: 100%"></div>
									  </div>
									</div>
								  </div>

								</td>
							  </tr>
                          <?php $i++;} else {

    $pilres2 .= '<tr>
													<td>
													  <div class="fotocalon" style="text-align:center">
														  <img class="rounded-circle img-fluid" src=img/paslon/' . $value->img_paslon . ' height="40" width="40">
													  </div>
													</td>
													<td>
													  <div class="os-progress-bar danger">
														<div class="bar-labels">
														  <div class="bar-label-left"><span class="bigger">' . ucwords(strtolower($value->nama)) . '</span></div>
														  <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;">' . $value->jumlah . '%</span></div>
														</div>
														<div class="bar-level-1" style="width: 100%">
														  <div class="bar-level-2" style="width:' . $value->jumlah . '%">
															<div class="bar-level-3" style="width: 100%"></div>
														  </div>
														</div>
													  </div>
													  </td>
												  </tr>';
}
}?>
                        </table>
                      </div>
                      <div class="col-md-6">
                         <table style="width: 100%">
                          <?=$pilres2;?>
                        </table>
                      </div>
                      <div class="col-md-12">
                      <div class="text-center btn-helper">
                      <span class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_pilpres19);?></span>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                <div class="element-wrapper" >
                  <h6 class="element-header">PERSENTASE PEROLEHAN SUARA PARTAI POLITIK  2014</h6>
                  <div class="element-box" style="min-height: 720px;">

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
      <img src="<?=base_url();?>img/logo_parpol/pdi-p.png" height="20" width="20">&nbsp;PDI-P</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">18.95 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 18.95%;background-color: #da251c;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/Golkar.png" height="20" width="20">&nbsp;GOLKAR</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">14.75 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 14.75%;background-color: #ffed00;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/gerindra.png" height="20" width="20">&nbsp;GERINDRA</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">11.81 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 11.81%;background-color: #d43c32;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/demokrat.png" height="20" width="20">&nbsp;DEMOKRAT</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">10.9 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 10.9%;background-color: #155ca8;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/PKB.png" height="20" width="20">&nbsp;PKB</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">9.04 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 9.04%;background-color: #058b3e;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/pan.png" height="20" width="20">&nbsp;PAN</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">7.59 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 7.59%;background-color: #000080;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/pks.png" height="20" width="20">&nbsp;PKS</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">6.79 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 6.79%;background-color: #a79e9e;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/nasdem.png" height="20" width="20">&nbsp;NASDEM</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">6.72 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 6.72%;background-color: #214aa0;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/PPP.png" height="20" width="20">&nbsp;PPP</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">6.53 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 6.53%;background-color: #00923f;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/hanura.png" height="20" width="20">&nbsp;HANURA</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">5.26 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 5.26%;background-color:#f08519;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
    <img src="<?=base_url();?>img/logo_parpol/PBB.png" height="20" width="20">&nbsp;PBB</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">1.46 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 1.46%;background-color: #00923f;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

<div class="os-progress-bar">
  <div class="bar-labels">
    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">
      <img src="<?=base_url();?>img/logo_parpol/pkpi.png" height="20" width="20">&nbsp;PKPI</span></div>
    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">0.91 %</span></div>
  </div>
  <div class="bar-level-1" style="width: 100%">
    <div class="bar-level-2" style="width: 0.91%;background-color: #d61f26;">
      <div class="bar-level-3" style="width: 100%"></div>
    </div>
  </div>
</div>

    <div class="text-center btn-helper">
                    <span class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: </span>
                    </div>






                   <!--  <?php foreach ($survey_parpol_14 as $key => $value) {?>
                    <div class="os-progress-bar">
                      <div class="bar-labels">
                        <div class="bar-label-left"><span class="bigger"><?=$value->nama?></span></div>
                        <div class="bar-label-right"><span class="info bigger"><?=$value->jumlah?> %</span></div>
                      </div>
                      <div class="bar-level-1" style="width: 100%">
                        <div class="bar-level-2" style="width: <?=$value->jumlah?>%;background-color: <?=$value->color?>;">
                          <div class="bar-level-3" style="width: 100%"></div>
                        </div>
                      </div>
                    </div>
                    <?php }?> -->
                  </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="element-wrapper">
                  <h6 class="element-header">HASIL SURVEI PILIHAN PARTAI POLITIK 2019</h6>
                  <div class="element-box" style="min-height: 720px;">
                    <?php $total = 0; foreach ($survey_parpol_19 as $key => $value) {
                      $total = $total + $value->total;
                    ?>

                    <div class="os-progress-bar">
                      <div class="bar-labels">
                        <div class="bar-label-left"><span class="bigger" style="font-size:1rem;"><img height="20" width="20" src="<?=base_url('img/logo_parpol/')?><?=$value->picture?>"> <?=$value->nama?></span></div>
                        <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;"><?=$value->jumlah?> %</span></div>
                      </div>
                      <div class="bar-level-1" style="width: 100%;">
                        <div class="bar-level-2" style="width: <?=$value->jumlah?>%;background-color: <?=$value->color?>;">
                          <div class="bar-level-3" style="width: 100%"></div>
                        </div>
                      </div>
                    </div>
                    <?php }?>
                    <?php
                      $total_survei_partai = $total;
                    ?>
                    <div class="text-center btn-helper">
                    <span class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_survei_partai);?></span>
                    </div>
                  </div>
                </div>
                </div>
            </div>

        </div>
    </div>
</div>
 
<div class="modal fade bd-example-modal-lg" id="modal_paslon" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <div class="daft-modal-head" style="margin-bottom: -10px;">
                       Profile Pasangan Calon
                   </div>
                   <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" align="center"><img id="foto_kepala" class="img-fluid" src="<?=base_url();?>img/paslon/SUMUT.jpg" style="max-height:285px;min-width:189px;max-width:189;">
                    <h5 id="nama_kepala">DJAROT SAIFUL H</h5>
                    </div>
                <div class="col-md-3">
                    <span class="daft-red">Tanggal Lahir</span><br>
                    <p id="tanggal_lahir_kepala">06-07-1962</p><br>
                    <span class="daft-red">Jenis Kelamin</span><br>
                    <p id="jenis_kelamin_kepala">Laki-laki</p>
                    <br>
                    <span class="daft-red">Pekerjaan</span><br>
                    <p id="pekerjaan_kepala">Swasta</p>
                    <br>
                    <span class="daft-red">Alamat</span><br>
                    <p id="alamat_kepala">Jl Jelambar Raya</p>
                    <br>
                    <!-- <span class="daft-red">Riwayat</span><br> -->

                </div>
                <div class="col-md-3"  align="center"><img id="foto_wakil" class="img-fluid" src="<?=base_url();?>img/paslon/SUMUT.jpg" style="max-height:285px;min-width:189px;max-width:189;">
                    <h5 id="nama_wakil">Sihar</h5>
                </div>
                <div class="col-md-3">
                    <span class="daft-red">Tanggal Lahir</span><br>
                    <p id="tanggal_lahir_wakil">06-07-1962</p><br>
                    <span class="daft-red">Jenis Kelamin</span><br>
                    <p id="jenis_kelamin_wakil">Laki-laki</p>
                    <br>
                    <span class="daft-red">Pekerjaan</span><br>
                    <p id="pekerjaan_wakil">Swasta</p>
                    <br>
                    <span class="daft-red">Alamat</span><br>
                    <p id="alamat_wakil">Jl Jelambar Raya</p>
                    <br>
                    <!-- <span class="daft-red">Riwayat</span><br> -->

                </div>
                <div class="col-md-12">
                    <hr>
                    <h4>Partai Pengusung</h4>
                    <!-- <span style="padding-right: 30px;"><img height="20" width="20" src="<?=base_url();?>img/logo_parpol/pdi-p.png"> PDI-P</span> -->
                    <div id="pengusung">
                      <span><img height="20" width="20" src="<?=base_url();?>img/logo_parpol/pdi-p.png"> PDI-P</span>  
                    </div>
                    

                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" style="display:none;">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Styles -->


<!-- Resources -->

		<script src="<?=base_url()?>bower_components/amcharts/amcharts.js"></script>
		<script src="<?=base_url()?>bower_components/amcharts/serial.js"></script>
		<script src="<?=base_url()?>bower_components/amcharts/pie.js"></script>
		<script src="<?=base_url()?>bower_components/amcharts/themes/light.js"></script>
		<script src="<?=base_url()?>bower_components/amcharts/plugins/export/export.min.js"></script>

		<script src="<?=base_url()?>bower_components/ammap/ammap_amcharts_extension.js"></script>

		<!-- map file should be included after ammap.js -->
		<script src="<?=base_url()?>bower_components/ammap/maps/js/indonesiaLow.js" type="text/javascript"></script>
		<!-- Custom JS -->

		<script src="<?=base_url()?>bower_components/js/config.js" type="text/javascript"></script>
    <script src="<?=base_url()?>bower_components/js/provinsi.js" type="text/javascript"></script>
    <script src="<?=base_url()?>bower_components/js/kabupaten.js" type="text/javascript"></script>
		
    <script type="text/javascript">
      function show_detail(id_paslon){
        $.get( "<?=site_url('dashboard/detail_paslon/')?>"+id_paslon, function( data ) {
          obj = JSON.parse(data);
          console.log(obj[0]);
          var url = '<?=base_url('img/paslon/')?>';
          $("#nama_kepala").text(obj.paslon.nama_kepala);
          $("#tanggal_lahir_kepala").text(obj.paslon.tgl_lahir_ketua);
          $("#jenis_kelamin_kepala").text(obj.paslon.jk_kepala);
          $("#pekerjaan_kepala").text(obj.paslon.pekerjaan_kepala);
          $("#alamat_kepala").text(obj.paslon.alamat_ketua);
          $("#foto_kepala").attr("src",url+obj.paslon.image_ketua);

          $("#nama_wakil").text(obj.paslon.nama_wakil);
          $("#tanggal_lahir_wakil").text(obj.paslon.tgl_lahir_wakil);
          $("#jenis_kelamin_wakil").text(obj.paslon.jk_kepala);
          $("#pekerjaan_wakil").text(obj.paslon.pekerjaan_kepala);
          $("#alamat_wakil").text(obj.paslon.alamat_wakil);
          // $("foto_wakil").val(obj[0].image_wakil);
          $("#foto_wakil").attr("src",url+obj.paslon.image_wakil);
          $("#pengusung").html(obj.pengusung);
          $("#modal_paslon").modal('show');
          // alert( "Load was performed." );
        });
      }
    </script>

<!-- Chart code -->
<script>
/**
 * Create a map
 */
 /*
AmCharts.makeChart("map",{
					"type": "map",
					"pathToImages": "http://www.amcharts.com/lib/3/images/",
					"addClassNames": true,
					"fontSize": 15,
					"color": "#000000",
					"projection": "mercator",
					"backgroundAlpha": 1,
					"backgroundColor": "rgba(255,255,255,1)",
					"dataProvider": {
						"map": "indonesiaLow",
						"getAreasFromMap": true,
						"images": [
							{
								"top": 40,
								"left": 60,
								"width": 80,
								"height": 40,
                "pixelMapperLogo": true,
                "pie": {
                    "type": "pie",
                    "pullOutRadius": 0,
                    "labelRadius": 0,
                    "radius": "10%",
                    "dataProvider": [{
                      "category": "Category #1",
                      "value": 200
                    }, {
                      "category": "Category #2",
                      "value": 600
                    }, {
                      "category": "Category #3",
                      "value": 350
                    }],
                    "labelText": "",
                    "valueField": "value",
                    "titleField": "category"
                  }
							}
						]
					},
					"balloon": {
						"horizontalPadding": 15,
						"borderAlpha": 0,
						"borderThickness": 1,
						"verticalPadding": 15
					},
					"areasSettings": {
						"color": "rgba(232,54,54,1)",
						"outlineColor": "rgba(255,255,255,1)",
						"rollOverOutlineColor": "rgba(255,255,255,1)",
						"rollOverBrightness": 20,
						"selectedBrightness": 20,
						"selectable": true,
						"unlistedAreasAlpha": 0,
						"unlistedAreasOutlineAlpha": 0
					},
					"imagesSettings": {
						"alpha": 1,
						"color": "rgba(232,54,54,1)",
						"outlineAlpha": 0,
						"rollOverOutlineAlpha": 0,
						"outlineColor": "rgba(255,255,255,1)",
						"rollOverBrightness": 20,
						"selectedBrightness": 20,
						"selectable": true
					},
					"linesSettings": {
						"color": "rgba(232,54,54,1)",
						"selectable": true,
						"rollOverBrightness": 20,
						"selectedBrightness": 20
					},
					"zoomControl": {
						"zoomControlEnabled": true,
						"homeButtonEnabled": false,
						"panControlEnabled": false,
						"right": 38,
						"bottom": 30,
						"minZoomLevel": 0.25,
						"gridHeight": 100,
						"gridAlpha": 0.1,
						"gridBackgroundAlpha": 0,
						"gridColor": "#FFFFFF",
						"draggerAlpha": 1,
						"buttonCornerRadius": 2
					}
				});

*/
</script>




