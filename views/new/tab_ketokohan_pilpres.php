<script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
<script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- <h6>Filter</h6>
<div class="row">
    <div class="col-md-3" style="padding-right:0px,width:80%;">
        <div class="middle">
            <label for="switch-0-1">Informal</label>
            <input id="check" type="checkbox" class="js-switch" checked />
            <label for="switch-0-1">Formal</label>
        </div>
    </div>
    <div class="col-md-9" style="padding-left:0px">
        <select id="ketokohan" class="form-control form-control-sm rounded" style="width:30%;"></select>
            <input type="hidden" id="base" value="<?=base_url()?>">
            <input type="hidden" id="type_form" value="<?=base_url()?>">
            <input type="hidden" id="ketokohan_form" value="<?=base_url()?>">
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-4">
           <div id="ketokohan_pie" style="width: 100%; height: 300px;"></div>
    </div>
    <div class="col-md-4">
            <div id="chartdiv2" style="width: 100%;height: 300px;"></div>
    </div>
    <div class="col-md-4">
        <div id="chartdiv" style="width: 100%;height: 300px;"></div>
    </div>
</div> -->
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">DATA PEMILIH TETAP | DPT</h6>
                <a href="#" class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class=" centered ">
                        <div class="value"><?= number_format($jum_dpt) ?></div>
                    </div>

                </a>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">ASUMSI SUARA SAH</h6>
                <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($asumsi_suara_sah)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TARGET SUARA</h6>
                 <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($target_suara)?></div>
                </div>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">POTENSI SUARA</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=$jml_perolehan?></div>
                </div>
            </div>
        </div>
    </div>
</div>

            <div class="row">
                <div class="col-md-6">
                    <?php
$formal = $jum_ketokohan_group['jum_tangible'];
$informal = $jum_ketokohan_group['jum_intangible'];

$total_ketokohan = $formal + $informal;
if ($formal != 0) {
    $persen_formal = (($formal / $total_ketokohan) * 100);
} else {
    $persen_formal = 0;
}

if ($informal != 0) {
    $persen_informal = (($informal / $total_ketokohan) * 100);
} else {
    $persen_informal = 0;
}

?>
                    <a class="element-box el-tablo" data-target="" data-toggle="modal" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                        <div class="centered ">
                            <div class="label">Informal</div>
                            <div class="value"><?=number_format($jum_ketokohan_group['jum_intangible'])?><br>
                                <span style="font-size: 20px">(<?=number_format($persen_informal, 2)?>%)</span>
                            </div>

                    </div>
                    </a>

                </div>
                <div class="col-md-6">
                     <a class="element-box el-tablo" data-target="" data-toggle="modal" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                        <div class="centered ">
                            <div class="label">Formal</div>
                            <div class="value"><?=number_format($jum_ketokohan_group['jum_tangible'])?><br>
                                 <span style="font-size: 20px; margin-top: -30px">(<?=number_format($persen_formal, 2)?>%)</span>
                                 </div>

                    </div>
                    </a>
                </div>
            </div>

<div class="row">
    <div class="col-md-12 projects-list">
        <div class="element-box">
            <div class="row">
                <div class="col-md-12">
                    <h6 class="text-center">AFILIASI POLITIK TOKOH BERPENGARUH</h6>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:35px;margin-bottom:40px;">
                <div class="col-md-6">
                    <select id="ketokohan" class="mr-auto form-control form-control-sm rounded" style="width:50%;"></select>
                    <!-- <div class="mx-auto" style="width:50%;">
                        <label for="switch-0-1" style="font-weight:bold;">Informal</label>
                        <input id="check" type="checkbox" class="js-switch" checked  style="width:25%;"/>
                        <label for="switch-0-1" style="font-weight:bold;">Formal</label>
                    </div> -->
                </div>
                <div class="col-md-6">
                    <div class="ml-auto" style="max-width:170px;">
                        <label for="switch-0-1" style="font-weight:bold;">Informal</label>
                        <input id="check" type="checkbox" class="js-switch" checked  style="width:25%;"/>
                        <label for="switch-0-1" style="font-weight:bold;">Formal</label>
                    </div>
                    <!-- <select id="ketokohan" class="mx-auto form-control form-control-sm rounded" style="width:50%;"></select> -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div id="chartdiv2_pilpres" style="width: 100%;height: 300px;"></div>
                </div>
                <div class="col-md-6">
                        <div id="chartdiv_pilpres" style="width: 100%;height: 300px;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align:center;margin-top:15px;">
                    <div class="btn-helper">
                        <h3 class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000" id="jumlah_pilpres"
                    onclick="alltable('ALL');">N : </h3>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA PRABOWO 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?= number_format($jokowi) ?></div>
                </div>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA JOKOWI 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($prabowo)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">JUMLAH RELAWAN</h6>
                <a href="#" class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                    <div class="centered ">
                        <div class="value" ><?=$jum_guraklih?></div>

                </div>
                </a>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TOKOH BERPENGARUH</h6>
                <a href="#" class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                        <div class="centered ">
                            <div class="value"><?=$jum_ketokohan?></div>
                        </div>

                </a>
            </div>
        </div>
    </div>
</div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="base" value="<?=base_url()?>">
    <input type="hidden" id="type_form" value="<?=base_url()?>">
    <input type="hidden" id="ketokohan_form" value="<?=base_url()?>">
</div>
</div>
</div>
<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_ketokohan" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" style="margin: auto;max-width: 1100px;">
        <div class="modal-content " style=" width: 1100px !important;">
            <div class="modal-header">
                <input type="hidden" id="vpartai">
                <input type="hidden" id="vketokohan">
                <input type="hidden" id="vtipe_ketokohan">
                <input type="hidden" id="vtipe" value="<?=$tipe?>">
                <input type="hidden" id="vid" value="<?=$id?>">
                <h5 class="modal-title" id="exampleModalLabel">&nbsp;Daftar Tokoh Berpengaruh</h5>&nbsp;&nbsp;
                <?php if($user['role'] == 'admin'){ ?>
                    <img  onclick="save_excel_fauzan()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;">
                <?php } ?>
                <!-- <img  onclick="save_excel_fauzan()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;"> -->
                <!-- <img  onclick="save_excel_fauzan()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;"> -->
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body" style="padding-left: 0; padding-right: 0;">
                <div class="table-responsive">
                    <table id="table_detail_ketokohan_pilpres" width="120%" class="table table-striped table-lightfont"></table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_all" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="vpartai_all">
                <input type="hidden" id="vketokohan_all">
                <input type="hidden" id="vtipe_ketokohan_all">
                <input type="hidden" id="vtipe_all" value="<?=$tipe?>">
                <input type="hidden" id="vid_all" value="<?=$id?>">
                <h5 class="modal-title" id="exampleModalLabel">
                    &nbsp;Daftar Tokoh Berpengaruh
                </h5>
                <div class="col-md-8" >
                        <select id="list_ketokohan" class="mr-auto form-control form-control-sm rounded" style="width:50%;">
                            <option value="ALL">ALL</option>
                            <?php foreach ($list_ketokohan as $key => $value) { ?>
                                <option value="<?=$value->id_pertokohan?>"><?=$value->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body" style="padding-left: 0; padding-right: 0;">
                <div class="row">
                    
                </div>
                <br>
                <div class="table-responsive">
                    <table id="table_detail_all_pilpres" width="120%" class="table table-striped table-lightfont"></table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table_detail_ketokohan_pilpres").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_detail_ketokohan_filter1 input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Pilpres/detail_influencer/all/0/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'name', "title": "Nama"},
                    { "data": 'phone', "title": "Telephone"},
                    { "data": 'pertokohan', "title": "Pertokohan" },
                    { "data": 'prov', "title": "Provinsi" },
                    { "data": 'kab', "title": "Kab"},
                    { "data": 'kec', "title": "Kec" },
                    { "data": 'kel', "title": "Kel" },
                    { "data": 'partai', "title": "Afiliasi Politik" }
                ],
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });

        var t = $("#table_detail_all_pilpres").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_detail_all_filter1 input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Pilpres/detail_influencer/all/0/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'name', "title": "Nama"},
                    { "data": 'phone', "title": "Telephone"},
                    { "data": 'pertokohan', "title": "Pertokohan" },
                    { "data": 'prov', "title": "Provinsi" },
                    { "data": 'kab', "title": "Kab"},
                    { "data": 'kec', "title": "Kec" },
                    { "data": 'kel', "title": "Kel" },
                    { "data": 'partai', "title": "Afiliasi Politik" }
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });

function load_modal_profile(modal,judul){
    console.log(modal);
    $("#modal_profile_body").empty();
        $.ajax({
           type:'POST',
           url:"<?php echo base_url(); ?>pilpres/load_modal_profile/",
           //data: "modal_name="+modal,
           data: {modal_name:modal,id:vid,nama:vnama,tipe:vtipe,logo:vlogo},
           success:function(msg){
            console.log(msg);
            $("#modal_profile_body").html(msg);
            $("#modal_profile_title").text(judul);
            // $("#modal_profile_title").addClass("btn-lg btn-danger");
           },
           error: function(result)
           {
             $("#modal_profile_body").html("Error");
           },
           fail:(function(status) {
             $("#modal_profile_body").html("Fail");
           }),
           beforeSend:function(d){
            //$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img height='25' width='120' src='<?php echo base_url(); ?>img/ajax-loader.gif' /></strong></center>");
            $("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets2/spinner.png'></strong></center>");

           }

        });
}
</script>
<script type="text/javascript">
    AmCharts.makeChart("ketokohan_pie", {
        "type": "pie",
        // "angle": 7.2,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        // "innerRadius": 0,
        // "labelRadius": 0,
        // "radius": 80,
        "autoMargins" : false,
        // "outlineThickness": 0,
        "titleField": "category",
        "valueField": "column-1",
        "fontSize": 12,
        // "handDrawScatter": 1,
        "theme": "light",
        "allLabels": [],
        "balloon": {},
        "titles": [{
            "id": "Influencer",
            "text": "Influencer",
            "size": 20,
        }],
        "colors": [
            "#73d2de",
            "#ffbc42"
        ],
        "dataProvider": [{
            "category": "Formal",
            "column-1": "<?=$jum_ketokohan_group['jum_tangible']?>",
            "color":"#F69200",
        }, {
            "category": "Informal",
            "column-1": "<?=$jum_ketokohan_group['jum_intangible']?>",
            "color":"#A6A6A6",
        }]
    });

    $(document).ready(function() {
        var elem = document.querySelector('.js-switch');
        var init = new Switchery(elem, {
            color: '#fffff',
            secondaryColor: '#fffff',
            jackColor: '#c72a00',
            jackSecondaryColor: '#c72a00'
        });
        $("#ketokohan_form").val('ALL');
        load_pertokohan();
        generate_chart('ALL', 1);
    });

    function load_pertokohan() {
        if ($("#check").is(':checked')) {
            $("#type_form").val('1');
        } else {
            $("#type_form").val('0');
        }

        $.ajax({
            url: "<?php echo site_url('pilpres/get_pertokohan/') ?>" + $("#type_form").val(),
            cache: false,
            type: "GET",
            success: function(respond) {
                $("#ketokohan").html(respond);
            }
        });
    }

    $("#ketokohan").change(function() {
        console.log($(this).val());
        id = $(this).val();
        tipe =  $("#type_form").val();
        generate_chart(id, tipe);
        // alert($(this).val());
        // id = $("#ketokohan").val();
        // $("#ketokohan_form").val(id);
        // generate_chart($("#ketokohan_form").val(),$("#type_form").val());
    });

    $("#list_ketokohan").change(function() {
        console.log($(this).val());
        id = $(this).val();
        alltable2(id);
        // alert($(this).val());
        // id = $("#ketokohan").val();
        // $("#ketokohan_form").val(id);
        // generate_chart($("#ketokohan_form").val(),$("#type_form").val());
    });

    $("#check").change(function() {
        load_pertokohan();
        generate_chart('ALL', $("#type_form").val());
    });
    var partai;
    var ketokohan;
    var tipe_ketokohan;
    var tipe;
    var id;

    function generate_chart(id, tipe) {
        $.ajax({
            url: "<?php echo site_url('pilpres/get_chart/') ?>",
            cache: false,
            data: {id_influencer:id,tipe_influencer:tipe,id_wilayah:<?=$id?>,tipe_wilayah:'<?=$tipe?>'},
            type: "POST",
            dataType : "json",
            success: function(respond) {
                console.log(respond);
                // total = (respond.total).toLocaleString('en')
                $("#jumlah_pilpres").text("N: "+respond.total.toLocaleString());
                var chart = AmCharts.makeChart("chartdiv_pilpres", {
                    "type": "serial",
                    "theme": "light",
                    
                    "dataProvider": respond.chart,
                    "listeners": [{
                        "event": "clickGraphItem",
                        "method": function(event) {
                            Pace.track(function() {
                                var table = $('#table_detail_ketokohan_pilpres').DataTable();
                                 partai = event.item.category;
                                 ketokohan =  $("#ketokohan").val();
                                 tipe_ketokohan =  $("#type_form").val();
                                 tipe = '<?=$tipe?>';
                                 id = <?=$id?>;
                                 partai = event.item.category;
                                table.ajax.url("<?=site_url('pilpres/detail_influencer_new/')?>"+partai+'/'+ketokohan+'/'+tipe_ketokohan+'/'+tipe+'/'+id).load();
                                $("#modal_detail_ketokohan").modal('show');
                                vpartai.value = partai;
                                vketokohan.value = ketokohan;
                                vtipe_ketokohan.value = tipe_ketokohan;
                            })
                        }
                    }],
                    "titles": [{
                    "text": "10 BESAR PERSENTASE AFILIASI POLITIK TOKOH BERPENGARUH"
                     }],
                    "startDuration": 2,
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>%",
                        "bulletOffset": 10,
                        "labelText": "[[value]]%",
                        "bulletSize": 42,
                        "colorField": "color",
                        // "cornerRadiusTop": 8,
                        // "customBulletField": "bullet",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0,
                        "type": "column",
                        "valueField": "points",
                    }],
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    // "autoMargins": false,
                    "categoryField": "name",
                    "categoryAxis": {
                        "gridColor": "#FFFFFF",
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "fillAlpha": 0.00,
                        "position": "left",
                        "labelRotation": 90
                    },
                    "valueAxes": [{
                        "axisAlpha": 0,
                        "labelsEnabled": false,
                        "gridColor": "#FFFFFF",
                        "gridAlpha": 0,
                        "dashLength": 0,
                        "export": {
                            "enabled": true,
                        }
                    }],
                });
                var chart = AmCharts.makeChart("chartdiv2_pilpres", {
                    "type": "pie",
                    "theme": "light",
                    "labelRadius": 10,
                    "radius":75,
                    "colorField": "color",
                    "labelText": "[[country]] : [[percents]]%",
                    // "colors": [
                    //     "#73d2de",
                    //     "#ffbc42"
                    // ],
                    // "legend": {
                    //     "position": "bottom",
                    //     // "valueWidth": 0,
                    //     "valueText":"[[value]]"
                    // },
                    "dataProvider": [{
                        "country": "Netral",
                        "litres": respond.jum_netral,
                        "color" : '#A6A6A6',
                    }, {
                        "country": "Afiliasi Politik",
                        "litres": respond.jum_pilih,
                        "color" : '#F69200',
                    },],
                    "listeners": [{
                        "event": "clickSlice",
                        "method": function(event) {
                            Pace.track(function() {
                                var table = $('#table_detail_ketokohan_pilpres').DataTable();
                                 partai = event.dataItem.title;
                                 ketokohan =  $("#ketokohan").val();
                                 tipe_ketokohan =  $("#type_form").val();
                                 tipe = '<?=$tipe?>';
                                 id = <?=$id?>;
                                 partai = event.dataItem.title;
                                table.ajax.url("<?=site_url('pilpres/detail_influencer_new/')?>"+partai+'/'+ketokohan+'/'+tipe_ketokohan+'/'+tipe+'/'+id).load();
                                $("#modal_detail_ketokohan").modal('show');
                                vpartai.value = partai;
                                vketokohan.value = ketokohan;
                                vtipe_ketokohan.value = tipe_ketokohan;
                                
                            })
                        }
                    }],
                    "valueField": "litres",
                    "titleField": "country",
                    "export": {
                        "enabled": true,
                    }
                });
            }
        })
    }

    function alltable(ketokohan){

        var table = $('#table_detail_all_pilpres').DataTable();
        var tipe_ketokohan = $("#type_form").val();
        //console.log(tipe_ketokohan);
        var ketokohan =  $("#ketokohan").val();
        var tipe = '<?=$tipe?>';
        var id = <?=$id?>;
        
        if(id == 0){
            alert('PILIH AREA PROVINSI DULU');
        }else{
            table.ajax.url("<?=site_url('pilpres/detail_influencer_all/')?>"+tipe+'/'+id+'/'+ketokohan+'/'+tipe_ketokohan).load();
            $("#modal_detail_all").modal('show');  
            $("vtipe_all").val(tipe);
            $("vid_all").val(id);
            $("vtipe_ketokohan_all").val(ketokohan);
            // vtipe_all.value = tipe;
            // vid_all.value = id;
            // vtipe_ketokohan_all.value = ketokohan;           
        }   
        
        //$("#table_detail").html("");
        
    }

    function alltable2(ketokohan){
        var table = $('#table_detail_all_pilpres').DataTable();
        var tipe_ketokohan = $("#type_form").val();
        var ketokohan =  $("#list_ketokohan").val();
        var tipe = '<?=$tipe?>';
        var id = <?=$id?>;
        table.ajax.url("<?=site_url('pilpres/detail_influencer_all/')?>"+tipe+'/'+id+'/'+ketokohan+'/ALL').load();
        $("vtipe_all").val(tipe);
            $("vid_all").val(id);
            $("vtipe_ketokohan_all").val(ketokohan);
            // vtipe_all.value = tipe;
            // vid_all.value = id;
            // vtipe_ketokohan_all.value = ketokohan;
    }
    function save_excel_fauzan(){
        tipe = '<?=$tipe?>';
        id = <?=$id?>;
        console.log(partai);
        console.log(ketokohan);
        console.log(tipe_ketokohan);
        console.log(tipe);
        console.log(id);
        if(<?=$id?> == 0){
            alert('Data terlalu banyak, pilih provinsi terlebih dahulu');
        }else{
            window.location = "<?=site_url('excel/detail_influencer_new/')?>"+partai+'/'+ketokohan+'/'+tipe_ketokohan+'/'+tipe+'/'+id;
        }
        
    }
    function save_excell(){ 
        alert('Maintenance');
        // vtipe = $("#vtipe").val();
        // vid = $("#vid").val();
    
        // //ajax.url("<?=site_url('dashboard/save_to_excel_influencer/')?>"+vpartai.value+'/'+vketokohan.value+'/'+vtipe_ketokohan.value+'/'+vtipe.value+'/'+vid.value).load();
        // window.location = '<?php echo base_url(); ?>dashboard/save_to_excel_influencer/'+vpartai.value+'/'+vketokohan.value+'/'+vtipe_ketokohan.value+'/'+vtipe+'/'+vid;
    }
</script>
<script type="text/javascript">
    <?php
if ($nama == 'NASIONAL') {
    $tipe = '';
}
?>
var url = "<?=base_url('img/logo_provinsi/')?>";
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "PETA POLITIK TOKOH BERPENGARUH (POLITICAL INFLUENCER) DI";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";
</script>

