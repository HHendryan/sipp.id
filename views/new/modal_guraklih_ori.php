<div class="row">
					
			 <div class="col-md-4">
				<div class="el-tablo centered ">
			<div class="label">Jumlah TPS</div>
			<div class="value"><?= $jum_tps; ?></div>
		</div>
			</div>
			<div class="col-md-4">
				<div class="el-tablo centered ">
			<div class="label">Jml Petugas</div>
			<div class="value"><?= $jum_petugas; ?></div>
		</div>
			</div>
			<div class="col-md-4">
				<div class="el-tablo centered ">
			<div class="label">Target Prospek</div>
			<div class="value"><?= $jml_targetprospek;  ?></div>
		</div>
	</div>
</div>
<div style="padding-top: 50px"></div>
	<div class="row" >
		
			<div class="col-md-4">
				<div class="el-tablo centered ">
			<div class="label">Perolehan</div>
			<div class="value"><?= $jml_perolehan; ?></div>
		</div>
			</div>
			<div class="col-md-4"><div class="el-tablo centered ">
			<div class="label">Sisa</div>
			<div class="value"><?= $jml_targetprospek-$jml_perolehan;  ?></div>
		</div>
	</div>
			<div class="col-md-4" valign='center'>
				<br>
			
				<div class="os-progress-bar danger">
					<div class="bar-labels">
						<div class="bar-label-left"><span class="bigger">Progress</span></div>
						<div class="bar-label-right"><span class="danger"><?= number_format($persen_perolehan,2) ?>%</span></div>
					</div>
					<div class="bar-level-1" style="width: 100%">
						<div class="bar-level-3" style="width: <?= $persen_perolehan ?>%">
							<div class="bar-level-3" style="width: <?= $persen_prospek ?>%"></div>
						</div>
					</div>
				</div>
			</div>                                                     
	</div>
</div>