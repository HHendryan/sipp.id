<link href="<?=base_url()?>assets2/bower_components/summernote/summernote-bs4.css" type="text/css" media="all" rel="stylesheet"/>

<style type="text/css">
    .dt-center{
        text-align: center;
    }
</style>
<div class="row">
	<div class="col-md-12" style="padding-top: 10px">
    <h6 class="element-header">SIMULASI KEMENANGAN KANDIDAT</h6></div>
	<div class="col-md-4">
		<div class="element-box el-tablo">
		<div class=" centered ">
            <div class="label" style="font-size: 10.5px">Jumlah DPT</div>
            <div class="value">
			<?php echo number_format($jumlah_dpt);?>
			</div>
        </div>

		</div>
	</div>
    <div class="col-md-4">
        <div class="element-box el-tablo">
        <div class=" centered ">
            <div class="label" style="font-size: 10.5px">Asumsi Suara Sah</div>
            <div class="value" id="asumsi_suara_sah">
            <?php echo number_format($asumsi_suara);?>
            </div>
        </div>

        </div>
    </div>
    <div class="col-md-4">
        <div class="element-box el-tablo">
            <div class=" centered ">
                <div class="label" style="font-size: 10.5px">TARGET SUARA</div>
                <div class="value" id="target_suara">
					<?php
                        echo number_format($target_suara);
                    ?>
				</div>
            </div>

        </div>
    </div>
    <div class="col-md-4">
        <div class="element-box el-tablo">
            <div class=" centered ">
                <div class="label" style="font-size: 10.5px">Presentase Target</div>
                <div class="value">
                    <?php
                        if($asumsi_suara == 0){
                            echo number_format(0);
                        }else{
                            echo $presentase_target;
                        }
                        
                    ?>%
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-4">
        <?php if ($tipe == 'kelurahan'){ ?>
            <a href="javascript:void(0)" onclick="sisa_potensi_suara_checked()" class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">POTENSI SUARA GURAKLIH</div>
                    <div class="value"><?=number_format($suara_guraklih)?></div>
                </div>
            </a>
        <?php }else{ ?>
            <div class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">POTENSI SUARA GURAKLIH</div>
                    <div class="value"><?=number_format($suara_guraklih)?></div>
                </div>
            </div>
        <?php }?>
    </div>
    <div class="col-md-4">
        <?php if ($tipe == 'kabupaten'|| $tipe == 'kecamatan'||$tipe == 'kelurahan'){ ?>
            <a href="javascript:void(0)" onclick="guraklih_detail('0')" class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">PETUGAS GURAKLIH</div>
                    <div class="value"><?=number_format($jum_petugas_guraklih)?></div>
                </div>
            </a>
        <?php }else{ ?>
            <div class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">PETUGAS GURAKLIH</div>
                    <div class="value"><?=number_format($jum_petugas_guraklih)?></div>
                </div>
            </div>
        <?php }?>
    </div>

	<div class="col-md-12">
        <div class="element-box">
            <div class="table-responsive">
                <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <?php 
                            if($id == 0){
                                $wil = 'Provinsi';
                            }else{
                                if($tipe == 'kecamatan'){
                                    $wil = 'Kelurahan';
                                }else if($tipe == 'kelurahan'){
                                    $wil = 'TPS';
                                }else if($tipe == 'kabupaten'){
                                    $wil = 'Kecamatan';
                                }else if($tipe == 'provinsi'){
                                    $wil = 'Kabupaten';
                                }
                            }
                            ?>
                            <th><?=$wil?></th>
                            <th>Jumlah TPS</th>
                            <th>Jumlah DPT</th>
                            <th>Asumsi Suara Sah (75%)</th>
                            <th>Target Menang (%)</th>
                            <th>Target Perolehan Suara</th>
                            <?php if($tipe == 'provinsi'){ ?>
                            <th>Action</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($tabel as $key => $value) {
                            if($value['jum_dpt'] != 0){
                                $asumsi_suara_sah = round($value['jum_dpt']*0.75);
                                $target_perolehan_suara = round($value['target']/100*$asumsi_suara_sah);
                                // $target_perolehan_suara = round($asumsi_suara_sah/$jumlah_paslon+($asumsi_suara_sah*0.05));
                                // $target_menang = round($target_perolehan_suara/$asumsi_suara_sah*100,2);
                            }else{
                                $asumsi_suara_sah = 0;
                                $target_perolehan_suara = 0;
                                // $target_menang = 0;
                            }
                            
                        ?>
                        <tr>
                            <td style="text-align: left"><?= $value['id'] ?></td>

                            <?php if($tipe == 'kelurahan'){ ?>
                            <td style="text-align: center"><a class="wilayah" data-id="<?= $value['wilayah'] ?>" href="javascript:void(0)"><?= $value['wilayah'] ?></a></td>
                            <?php }else{ ?>
                            <td style="text-align: left"><a class="wilayah" data-nama="<?= $value['wilayah']?>" data-id="<?= $value['id_wilayah'] ?>" data-logo="<?= $value['logo_wilayah'] ?>" href="javascript:void(0)"><?= $value['wilayah'] ?></td>
                            <?php } ?>
                            
                            <td style="text-align: right"><?= number_format($value['jum_tps']) ?></td>
                            <td style="text-align: right"><?= number_format($value['jum_dpt']) ?></td>
                            <td style="text-align: right"><?= number_format($asumsi_suara_sah) ?></td>
                            <td style="text-align: center"><?= $value['target'] ?> %</td>
                            <td style="text-align: right"><?= number_format($target_perolehan_suara) ?></td>
                            <?php if($tipe == 'provinsi'){ ?>
                            <td style="text-align: center"><?= $value['button'] ?></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Wilayah</th>
                            <th style="text-align: right">Jumlah TPS</th>
                            <th style="text-align: right"><?php echo number_format($jumlah_dpt);?></th>
                            <th style="text-align: right">Asumsi Suara Sah (75%)</th>
                            <th>Target Menang (%)</th>
                            <th style="text-align: right">Target Perolehan Suara</th>
                            <?php if($tipe == 'provinsi'){ ?>
                            <th>Action</th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
	</div>
    <div class="col-md-3">
        <?php if ($tipe == 'kelurahan'){ ?>
            <a href="javascript:void(0)" onclick="sisa_potensi_suara()" class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">SISA POTENSI SUARA (DPT)</div>
                    <div class="value" id="kekurangan_suara_dpt"><?=number_format($kekurangan_suara)?></div>
                </div>
            </a>
        <?php }else{ ?>
            <div class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">SISA POTENSI SUARA (DPT)</div>
                    <div class="value" id="kekurangan_suara_dpt"><?=number_format($kekurangan_suara)?></div>
                </div>
            </div>
        <?php }?>
        
    </div>
    <div class="col-md-3">
        <div class="element-box el-tablo">
            <div class=" centered ">
                <div class="label" style="font-size: 10.5px">KEKURANGAN POTENSI SUARA</div>
                <div class="value" id="kekurangan_suara"><?=number_format($kekurangan_potensi_suara)?></div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <?php if ($tipe == 'kabupaten'|| $tipe == 'kecamatan'||$tipe == 'kelurahan'){ ?>
            <a href="javascript:void(0)" onclick="guraklih_detail('1')" class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">JUMLAH KOORDINATOR</div>
                <div class="value" ><?=number_format($jum_koordinator_guraklih)?></div>
                </div>
            </a>
        <?php }else{ ?>
            <div class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">JUMLAH KOORDINATOR</div>
                <div class="value" ><?=number_format($jum_koordinator_guraklih)?></div>
                </div>
            </div>
        <?php }?>
    </div>
    <div class="col-md-3">
        <?php if ($tipe == 'kelurahan'){ ?>
            <a href="javascript:void(0)" onclick="alltable('ALL');" class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">DATA INFLUENCER</div>
                <div class="value"><?=number_format($jum_ketokohan_group['jum_tangible'] + $jum_ketokohan_group['jum_intangible'])?></div>
                </div>
            </a>
        <?php }else{ ?>
            <div class="element-box el-tablo">
                <div class=" centered ">
                    <div class="label" style="font-size: 10.5px">DATA INFLUENCER</div>
                    <div class="value"><?=number_format($jum_ketokohan_group['jum_tangible'] + $jum_ketokohan_group['jum_intangible'])?></div>
                </div>
            </div>
        <?php }?>
    </div>

</div>
<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_dpt" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display:inline-block;margin-right:20px;">
                    Detail
          <!-- <button onclick="export_excel()" class="mr-2 mb-2 btn btn-success btn-sm btn-excel" type="button">Excel</button> -->
        </h5>
        <?php if($user['role'] == 'admin'){ ?>
            <img  onclick="save_excel()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;">
        <?php } ?>
        
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table id="table_detail_dpt" width="100%" class="table table-striped"></table>
      </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade large-modal"  role="dialog" aria-labelledby="myLargeModalLabel" id="modal_detail" aria-hidden="true" width="100%">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Guraklih</h5> 

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <br>
            <!--h5 class="modal-title" id="prov_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sulawesi Tenggara</h5-->
            <form id="edit" width="100%">
            <div class="modal-body" width="100%">
                <div class="container-fluid">
                    <div class="table-responsive">
                                <table id="table_detail_guraklih" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>No. Telp</th>
                                        <th>NIK</th>
                                        <th>Alamat</th>
                                        <th>Kelurahan</th>
                                        <th>Kecamatan</th>
                                        <th>Kabupaten</th>
                                        <th>Tipe</th>
                                        <th>TPS</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                </div>
            </div>
            <div class="modal-footer" width="100%">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade large-modal"  role="dialog" aria-labelledby="myLargeModalLabel" id="modal_rekomendasi" aria-hidden="true" width="100%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add/Edit Rekomendasi</h5> 

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <br>
            <!--h5 class="modal-title" id="prov_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sulawesi Tenggara</h5-->
            <form id="rekomendasi" width="100%">
            <div class="modal-body row" width="100%">
                <div class="form-group col-md-6">
                    <label for="">ID Wilayah</label>
                    <input class="form-control" type="text" name="id_area" required readonly>
                </div>
                <div class="form-group col-md-6">
                    <label for="">Nama Wilayah</label>
                    <input class="form-control" type="text" name="nama_area" required readonly>
                </div>
                <div class="form-group col-md-12">
                    <label for="">Rekomendasi</label>
                    <textarea id="summernote" class="form-control" rows="10" name="rekomendasi"></textarea>
                </div>
            </div>
            <div class="modal-footer" width="100%">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary-outline ks-light" >Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_all" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="vpartai_all">
                <input type="hidden" id="vketokohan_all">
                <input type="hidden" id="vtipe_ketokohan_all">
                <input type="hidden" id="vtipe_all" value="<?=$tipe?>">
                <input type="hidden" id="vid_all" value="<?=$id?>">
                <h5 class="modal-title" id="exampleModalLabel">
                    &nbsp;Daftar Tokoh Berpengaruh
                </h5>
                <div class="col-md-8" >
                        <select id="list_ketokohan" class="mr-auto form-control form-control-sm rounded" style="width:50%;">
                            <option value="ALL">ALL</option>
                            <?php foreach ($list_ketokohan as $key => $value) { ?>
                                <option value="<?=$value->id_pertokohan?>"><?=$value->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body" style="padding-left: 0; padding-right: 0;">
                <div class="row">
                    
                </div>
                <br>
                <div class="table-responsive">
                    <table id="table_detail_all_hari" width="120%" class="table table-striped table-lightfont"></table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="type_form" value="<?=base_url()?>">
<script src="<?=base_url();?>assets2/bower_components/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
    // $('#modal_rekomendasi').on('shown.bs.modal', function() {
      
    // })
    $(document).ready(function() {
        $('#summernote').summernote({
            toolbar: [],
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table_detail_dpt").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets2/spinner.png'></strong></center>"
                },
                processing: true,
                serverSide: true,
                "columnDefs": [
                    {"className": "dt-center", "targets": [4]}
                  ],
                ajax: {"url": "<?php echo site_url('Dashboard/get_detail_dpt/0/0')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'nik', "title": "NIK"},
                    { "data": 'nama', "title": "Nama"},
                    { "data": 'alamat', "title": "Alamat"},
                    { "data": 'tanggal_lahir', "title": "Tanggal Lahir" },
                    { "data": 'jns_kelamin', "title": "Jenis Kelamin"},
                    { "data": 'tps', "title": "TPS"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });    
    $("#table_detail_dpt_length").css("display", "none");


    var tInfulencer = $("#table_detail_all_hari").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_detail_all_hari_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Dashboard/detail_influencer/all/0/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'name', "title": "Nama"},
                    { "data": 'phone', "title": "Telephone"},
                    { "data": 'pertokohan', "title": "Pertokohan" },
                    { "data": 'prov', "title": "Provinsi" },
                    { "data": 'kab', "title": "Kab"},
                    { "data": 'kec', "title": "Kec" },
                    { "data": 'kel', "title": "Kel" },
                    { "data": 'partai', "title": "Afiliasi Politik" }
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>
<script type="text/javascript">
    <?php
if ($nama == 'NASIONAL') {
    $tipe = '';
}
?>
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=$tipe?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";
// $("#table_detail_dpt_length").css("display":"none");

var tps;
$("#dataTable1").on("click", ".wilayah",function(event){
    tps = $(this).data('id');
    id_kel = '<?= $id?>';
    var table = $("#table_detail_dpt").dataTable();
    var tipe = '<?= $tipe?>';
    if(tipe != 'kelurahan'){
        if(id_kel == 0){
            vtipe = 'provinsi';
            vid = $(this).data('id');
            vnama =$(this).data('nama');
            vlogo = $(this).data('logo');
            load_tab('tab_simulasi');
            children = $( "span:contains('"+vnama+"')" ).children();
            children[0].click();
            
        }
        if(tipe == 'provinsi'){
            vtipe = 'kabupaten';
            vid = $(this).data('id');
            vnama = $(this).data('nama');
            vlogo = $(this).data('logo');
            load_tab('tab_simulasi');
            children = $( "span:contains('"+vnama+"')" ).children();
            children[0].click();
        }
        if(tipe == 'kabupaten'){
            vtipe = 'kecamatan';
            vid = $(this).data('id');
            vnama = $(this).data('nama');
            vlogo = $(this).data('logo');
            load_tab('tab_simulasi');
            children = $( "span:contains('"+vnama+"')" ).children();
            children[0].click();
        }
        if(tipe == 'kecamatan'){
            vtipe = 'kelurahan';
            vid = $(this).data('id');
            vnama = $(this).data('nama');
            vlogo = $(this).data('logo');
            load_tab('tab_simulasi');
            children = $( "span:contains('"+vnama+"')" ).children();
            children[0].click();
        }
    }else{
        table.api().ajax.url("<?= site_url('Dashboard/get_detail_dpt/0/0')?>").load();
        table.api().ajax.url("<?= site_url('dashboard/get_detail_dpt2/')?>"+id_kel+'/'+tps).load();
        $("#modal_detail_dpt").modal("show");
    }
    // table.fnClearTable();
    // $("#table_detail").empty();
    
});

$("#dataTable1").on("click", ".btn-rekomendasi",function(event){
    id = $(this).data('id');
    nama = $(this).data('nama');
    $.ajax({
        url: "<?= site_url().'dashboard/get_rekomendasi/'?>"+id,
        type : 'post',
        dataType: "json",
        success : function(data){
            $("[name=id_area]").val(id);
            $("[name=nama_area]").val(nama);
            $('#summernote').summernote('reset');
            $("#summernote").summernote("code", data.rekomendasi);
            $("#modal_rekomendasi").modal('show');
        },
        error: function(xhr, status, error) {
            if(xhr.status == 422){
                alert(xhr.responseJSON);
            }else{
                message = xhr.status+' : '+error;
                alert(message);
            }
        }                
    });
});

$( "#rekomendasi" ).submit(function( event ) {
    $.ajax({
        url: "<?= site_url().'dashboard/edit_rekomendasi'?>",
        type : 'post',
        data : $( "#rekomendasi" ).serialize(),
        dataType: "json",
        success : function(data){
            $("#modal_rekomendasi").modal('hide');
        },
        error: function(xhr, status, error) {
            if(xhr.status == 422){
                alert(xhr.responseJSON);
            }else{
                message = xhr.status+' : '+error;
                alert(message);
            }
        }                
    });
    event.preventDefault();
});
var jumlah;
var asumsi_suara_sah;
var kekurangan_guraklih;
var id__ = <?=$id?>;
$("#dataTable1").dataTable({
    dom: 'Brt',
    "lengthMenu": [[-1], ["All"]],
    buttons: [
        'excel'
    ],
    "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,.]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var total_tps = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totaldpt = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalsah = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalsuara = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('-');
                    $( api.column( 1 ).footer() ).html('TOTAL');
                    $( api.column( 2 ).footer() ).html(total_tps);
                    // $( api.column( 3 ).footer() ).html('<?php echo ($jumlah_dpt);?>');
                    $( api.column( 4 ).footer() ).html(totalsah);
                    $( api.column( 5 ).footer() ).html('-');
                    $( api.column( 6 ).footer() ).html(totalsuara);
                    jumlah = totalsuara;
                    asumsi_suara_sah = totalsah;
                },
});
$("#target_suara").html(jumlah);
$("#asumsi_suara_sah").html(asumsi_suara_sah);
$("#asumsi_suara_sah").html(asumsi_suara_sah);
jumlah = jumlah.replace(',','');
jumlah = jumlah.replace(',','');
<?php if($suara_guraklih){?>
var suara_guraklih = <?=$suara_guraklih?>;
<?php }else{ ?>
var suara_guraklih = 0;
<?php } ?>

kekurangan_guraklih = parseInt(jumlah) - suara_guraklih;
console.log(kekurangan_guraklih);
$("#kekurangan_suara").html((kekurangan_guraklih).toLocaleString('en')    );
console.log(id__);

// $('.buttons-excel').remove( ":contains('Excel')" );
var element = $('.buttons-excel');//convert string to JQuery element
element.find("span").remove();
$('.buttons-excel').prepend('<img alt="" src="img/flags-icons/xl.png" width="25px">');
$('.buttons-excel').css('background-color','Transparent');
$('.buttons-excel').css('background-repeat','no-repeat');
$('.buttons-excel').css('border','none');
$('.buttons-excel').css('cursor','pointer');
$('.buttons-excel').css('overflow','hidden');
$('.buttons-excel').css('outline','none');
var checked;
function save_excel(){
    kelurahan = '<?= $id ?>';
    if(checked == 0){
        window.location = '<?php echo base_url(); ?>dashboard/save_excel_dpt_checked/'+kelurahan+'/'+checked;
    }else{
        window.location = '<?php echo base_url(); ?>dashboard/save_excel_dpt/'+kelurahan+'/'+tps;
    }
    
    
}

function sisa_potensi_suara(){
    checked = 0;
    var id_kel = '<?= $id?>';
    var table = $("#table_detail_dpt").dataTable();
    table.api().ajax.url("<?= site_url('Dashboard/get_detail_dpt/0/0')?>").load();
    table.api().ajax.url("<?= site_url('dashboard/get_detail_dpt_is_checked/')?>"+id_kel+'/'+checked).load();
    $("#modal_detail_dpt").modal("show");
}

function sisa_potensi_suara_checked(){
    checked = 1;
    var id_kel = '<?= $id?>';
    var table = $("#table_detail_dpt").dataTable();
    //table.api().ajax.url("<?= site_url('Dashboard/get_detail_dpt/0/0')?>").load();
    table.api().ajax.url("<?= site_url('dashboard/get_detail_dpt_is_checked/')?>"+id_kel+'/'+checked).load();
    $("#modal_detail_dpt").modal("show");
}

function guraklih_detail(type){
    var id = '<?= $id?>';
    var tipe = '<?= $tipe?>';
    var table = $("#table_detail_guraklih").dataTable();
    table.api().ajax.url("<?= site_url('dashboard/get_detail_guraklih/')?>"+id+'/'+tipe+'/'+type).load();
    $("#modal_detail").modal("show");
}

function alltable(ketokohan){
        var table = $('#table_detail_all_hari').DataTable();
        var tipe_ketokohan = 'ALL';
        var ketokohan = 'ALL';
        var tipe = '<?=$tipe?>';
        var id = <?=$id?>;

        //alert("<?=site_url('dashboard/detail_influencer_all/')?>"+tipe+'/'+id+'/'+ketokohan+'/'+tipe_ketokohan);
        if(id == 0){
            alert('PILIH AREA PROVINSI DULU');
        }else{
            table.ajax.url("<?=site_url('dashboard/detail_influencer_all/')?>"+tipe+'/'+id+'/'+ketokohan+'/'+tipe_ketokohan).load();
            $("#modal_detail_all").modal('show');  
            $("vtipe_all").val(tipe);
            $("vid_all").val(id);
            $("vtipe_ketokohan_all").val(ketokohan);        
        }
}

function alltable2(ketokohan){
        var table = $('#table_detail_all_hari').DataTable();
        var tipe_ketokohan = $("#type_form").val();
        var ketokohan =  $("#list_ketokohan").val();
        var tipe = '<?=$tipe?>';
        var id = <?=$id?>;
        table.ajax.url("<?=site_url('dashboard/detail_influencer_all/')?>"+tipe+'/'+id+'/'+ketokohan+'/ALL').load();
        $("vtipe_all").val(tipe);
            $("vid_all").val(id);
            $("vtipe_ketokohan_all").val(ketokohan);
}

$("#list_ketokohan").change(function() {
        console.log($(this).val());
        id = $(this).val();
        alltable2(id);
        // alert($(this).val());
        // id = $("#ketokohan").val();
        // $("#ketokohan_form").val(id);
        // generate_chart($("#ketokohan_form").val(),$("#type_form").val());
    });
</script>

<script type="text/javascript">
    $('#table_detail_guraklih').DataTable({
        columns: [
                    { data: 'cp' },
                    { data: 'phone' },
                    { data: 'nik' },
                    { data: 'address' },
                    { data: 'kel' },
                    { data: 'kec' },
                    { data: 'kab' },
                    { data: 'type' },
                    { data: 'tps' },
                ],
        "autoWidth": false
    });
</script>


