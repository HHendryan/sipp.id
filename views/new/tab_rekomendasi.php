<style type="text/css">
    .dt-center{
        text-align: center;
    }
</style>
<?php if($id == 0){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="element-box">
            <div class="table-responsive">
                <table id="dataTable3" width="100%" class="table table-striped table-lightfont">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <?php 
                            if($id == 0){
                                $wil = 'Provinsi';
                            }else{
                                if($tipe == 'kecamatan'){
                                    $wil = 'Kelurahan';
                                }else if($tipe == 'kelurahan'){
                                    $wil = 'TPS';
                                }else if($tipe == 'kabupaten'){
                                    $wil = 'Kecamatan';
                                }else if($tipe == 'provinsi'){
                                    $wil = 'Kabupaten';
                                }
                            }
                            ?>
                            <th><?=$wil?></th>
                            <th>Rekomendasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($tabel as $key => $value) {?>
                        <tr>
                            <td style="text-align: left"><?= $value->id ?></td>
                            <td style="text-align: left"><?= $value->name ?></td>
                            <td style="text-align: left"><?= $value->rekomendasi ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="row">
    <div class="col-md-12">
        <div class="element-box">
            <?php if($tabel){
                echo $tabel->rekomendasi;
            }else{
                echo 'NO DATA';
            } ?>
        </div>
    </div>
</div>
<?php } ?>


<script type="text/javascript">
<?php
if ($nama == 'NASIONAL') {
    $tipe = '';
}
?>
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=$tipe?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";
var jumlah;
var asumsi_suara_sah;
var kekurangan_guraklih;
var id__ = <?=$id?>;
$("#dataTable3").dataTable({
    dom: 'Brt',
    "lengthMenu": [[-1], ["All"]],
    buttons: [
        'excel'
    ],
});

// $('.buttons-excel').remove( ":contains('Excel')" );
var element = $('.buttons-excel');//convert string to JQuery element
element.find("span").remove();
$('.buttons-excel').prepend('<img alt="" src="img/flags-icons/xl.png" width="25px">');
$('.buttons-excel').css('background-color','Transparent');
$('.buttons-excel').css('background-repeat','no-repeat');
$('.buttons-excel').css('border','none');
$('.buttons-excel').css('cursor','pointer');
$('.buttons-excel').css('overflow','hidden');
$('.buttons-excel').css('outline','none');
var checked;
function save_excel(){
    kelurahan = '<?= $id ?>';
    if(checked == 0){
        window.location = '<?php echo base_url(); ?>dashboard/save_excel_dpt_checked/'+kelurahan+'/'+checked;
    }else{
        window.location = '<?php echo base_url(); ?>dashboard/save_excel_dpt/'+kelurahan+'/'+tps;
    }
    
    
}
</script>


