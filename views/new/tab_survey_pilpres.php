<script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
<script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">DATA PEMILIH TETAP | DPT</h6>
                <a href="#" class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class=" centered ">
                        <div class="value"><?= number_format($jum_dpt) ?></div>
                    </div>

                </a>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">ASUMSI SUARA SAH</h6>
                <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($asumsi_suara_sah)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TARGET SUARA</h6>
                 <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($target_suara)?></div>
                </div>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">POTENSI SUARA</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=$jml_perolehan?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-8" >
		<div class="element-box" style="min-height:190px">
			<h6>Metodologi Survei</h6>
			<p style="font-weight: 300;">
				<ul style="list-style-type:disc; margin-top: 0px"> 
				  <li><em>Key opinion leader</em> adalah tokoh berpengaruh sekaligus tokoh kunci yang berpotensi menjadi <em>electoral vote getter</em> (pendulang suara pemilih) di setiap daerah pemilihan.</li> 
				  <li>Survei ini menggunakan metode wawancara melalui telepon terhadap <em>key opinion leader</em>, tokoh berpengaruh<em> (influencer)</em> di tingkat desa.</li>
				  <li>Wawancara dilakukan terhadap semua populasi yang tercatat pada 17 provinsi sampai pada jumlah yang terjangkau/ berhasil diwawancarai.</li>
				</ul>  

			</p>
		</div>
		
	</div>
	<div class="col-md-4" >
		<div class="element-box" style="min-height:190px">
		    <div class="centered el-tablo " style="padding-top: 30px">
		   		        <div class="centered-header"><h6>Jumlah Responden</h6></div>
		        <div class="value" id="value_ketokohan"><?= number_format($jum_survei) ?></div>
		    </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding-top: 10px"><h6 class="element-header">HASIL SURVEY TOKOH BERPENGARUH</h6></div>
	<?php 
	
	$total_pilihan_politik_2014 = 0;
	$total_pilihan_politik_2019 = 0;
	if ($id != 0) { ?>
	
	<?php }else{ ?>
	
	<?php } ?>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 450px;">
			<div class="project-head head-helper"><h6>PILIHAN POLITIK TOKOH BERPENGARUH PADA PEMILU PRESIDEN 2014</h6>
			<?php
			$total=0;
			foreach ($pres_14_pilih as $key => $value) {
				
				$total = $total+$value->total;
			} 
			$total2=0;
			foreach ($pres_14_tdk_pilih as $key => $value) {
				$total2 = $total2+$value->total;
				
			} 
				$total_pilihan_politik_2014 = $total + $total2;
			?>
			</div>
			<div id="graph_2014" style="width: 100%; height: 500px;"></div>   
			<div class="text-center btn-helper">
								 <button onclick="modal_profile('PILIHAN POLITIK TOKOH BERPENGARUH PADA PEMILU PRESIDEN 2014','biasa','p5','detail_survey');"  class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_pilihan_politik_2014);?></button>
			</div>
		</div>
	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 450px;">
			<div class="project-head head-helper"><h6>PILIHAN POLITIK TOKOH BERPENGARUH TERHADAP CALON PRESIDEN 2019</h6>
				<?php
				$total=0;
				foreach ($pres_19_pilih as $key => $value) {
					
					$total = $total+$value->total;
				} 
				$total2='';
				foreach ($pres_19_tdk_pilih as $key => $value) {
					$total2 = $total2+$value->total;
					
				} 
					$total_pilihan_politik_2019 = $total + $total2;
				?>
				</div>
	<div id="graph_2019" style="width: 100%; height: 500px;"></div>
	<div class="text-center btn-helper">
								 <button onclick="modal_profile('PILIHAN POLITIK TOKOH BERPENGARUH TERHADAP CALON PRESIDEN 2019','biasa','p6','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_pilihan_politik_2019);?></button>
			</div>	
</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 projects-list">
		<div class="project-box">
			<div class="project-head head-helper">
				<h6>PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2014</h6>
			<?php $total = ''; foreach ($partai_14 as $key => $value) { 
					$total = $total+$value->total;
				}
				$total_partai_pilihan = (float) $total;
			?>
			</div>
			<div id="div_partai_14" class="element-box" style="min-height:710px !important;">
				<?php foreach ($partai_14 as $key => $value) { 
				if($value->color == NULL){
					$color = '#A6A6A6';
				}else{
					$color = $value->color;
				}
				?>
	            <div class="os-progress-bar" style="margin-bottom: 0px;">
	              <div class="bar-labels">
	                <div class="bar-label-left"><span class="bigger">
	                 <?php 
	                  $picturecount=strlen($value->picture);
	                  if ($picturecount>3) { ?>
	                    <a href="javascript:void(0)" class="partai_14" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/logo_parpol/')?><?=$value->picture?>"></a>
	                    
	                  <?php } else { ?>
	                  	<a href="javascript:void(0)" class="partai_14" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/')?>others.png"></a>
	                    
	                  <?php } ?>

	                 &nbsp;<?= $value->nama ?></span></div>
	                <div class="bar-label-right partai_14" data-parpol="<?= $value->nama ?>"><span class="info bigger"><?= $value->jumlah ?> %</span></div>
	              </div>
	              <div class="bar-level-1" style="width: 100%">
	                <div class="bar-level-2" style="width: <?= $value->jumlah ?>%;background-color: <?= $color ?>;">
	                  <div class="bar-level-3" style="width: 100%"></div>
	                </div>
	              </div>
	            </div>
            	<?php } ?>
				<!-- <h5 class	="text-center" style="color:#000; font-family: Verdana;font-weight:bold;margin-top:30px;">N: <?=$total_partai_pilihan; ?></h5> -->
				<div class="text-center btn-helper" style="padding-top:98px;">
					<button onclick="modal_profile('PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2014','biasa','p3','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N : <?=number_format($total_partai_pilihan);?></button>
				</div>	
			</div>
			
		</div>

	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box">
			<div class="project-head head-helper"><h6>PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2019</h6>
				<?php $total = ''; foreach ($partai_19 as $key => $value) { 
					$total = $total+$value->total;
					}
					$total_partai_pilihan_2019=(float) $total;
				?>
			</div>
			<!-- <div id="partai_2019" style="width: 100%; height: 300px;"></div> -->
			<div id="div_partai_19"  class="element-box" style="min-height:710px !important;">
				<?php foreach ($partai_19 as $key => $value) { 
				if($value->color == NULL){
					$color = '#A6A6A6';
				}else{
					$color = $value->color;
				}
				?>
	            <div class="os-progress-bar" style="margin-bottom: 0px;">
	              <div class="bar-labels">
	                <div class="bar-label-left"><span class="bigger">
	                  <?php 
	                  $picturecount=strlen($value->picture);
	                  if ($picturecount>3) { ?>
	                    <a href="javascript:void(0)" class="partai_19" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/logo_parpol/')?><?=$value->picture?>"></a>
	                    
	                  <?php } else { ?>
	                  	<a href="javascript:void(0)" class="partai_19" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/')?>others.png"></a>
	                    
	                  <?php } ?>

	             
	                  

	                  &nbsp;<?= $value->nama ?></span>
	              	</div>
	                <div class="bar-label-right partai_19" data-parpol="<?= $value->nama ?>"><span class="info bigger"><?= $value->jumlah ?> %</span></div>
	              </div>
	              <div class="bar-level-1" style="width: 100%">
	                <div class="bar-level-2" style="width: <?= $value->jumlah ?>%;background-color: <?= $color ?>;">
	                  <div class="bar-level-3" style="width: 100%"></div>
	                </div>
	              </div>
	            </div>
				<?php } ?>
				<div class="text-center btn-helper">
					<button onclick="modal_profile('PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2019','biasa','p4','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_partai_pilihan_2019);?></button>
				</div>	
			</div>
		</div>
	</div>
	<div class="col-md-12 projects-list">
		<div class="project-box">
			<div class="project-head head-helper">
				<h6>PERUBAHAN PILIHAN PARTAI POLITIK TOKOH BERPENGARUH TERHADAP PDI PERJUANGAN</h6>
				<?php $total = ''; foreach ($change as $key => $value) { 
						$total = $total+$value->total;
					}
					$total_change = (float) $total;
				?>
			</div>
			<div id="div_partai_change" class="element-box" >
				<?php foreach ($change as $key => $value) { 
				if($value->color == NULL){
					$color = '#A6A6A6';
				}else{
					$color = $value->color;
				}
				?>
	            <div class="os-progress-bar" style="margin-bottom: 0px;">
	              <div class="bar-labels">
	                <div class="bar-label-left"><span class="bigger">
	                 <?php 
	                  $picturecount=strlen($value->picture);
	                  if ($picturecount>3) { ?>
	                    <a href="javascript:void(0)" class="change" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/logo_parpol/')?><?=$value->picture?>"></a>
	                    
	                  <?php } else { ?>
	                  	<a href="javascript:void(0)" class="change" data-parpol="<?= $value->nama ?>"><img height="20" width="20" src="<?=base_url('img/')?>others.png"></a>
	                    
	                  <?php } ?>

	                 &nbsp;<?= $value->nama ?></span></div>
	                <div class="bar-label-right change" data-parpol="<?= $value->nama ?>"><span class="info bigger"><?= $value->percentage ?> %</span></div>
	              </div>
	              <div class="bar-level-1" style="width: 100%">
	                <div class="bar-level-2" style="width: <?= $value->percentage ?>%;background-color: <?= $color ?>;">
	                  <div class="bar-level-3" style="width: 100%"></div>
	                </div>
	              </div>
	            </div>
            	<?php } ?>
				<!-- <h5 class	="text-center" style="color:#000; font-family: Verdana;font-weight:bold;margin-top:30px;">N: <?=$total_partai_pilihan; ?></h5> -->
				<div class="text-center btn-helper">
					<button onclick="modal_profile('PILIHAN PARTAI POLITIK YANG BERPINDAH DARI PDIP','change','p4','detail_survey_change');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N : <?=number_format($total_change);?></button>
				</div>	
			</div>
			
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-md-6 projects-list">
		<div class="project-box"  style="min-height: 600px !important;">
			<div class="project-head head-helper"><h6>PENDAPAT TOKOH BERPENGARUH TERHADAP PERMASALAHAN POKOK MASYARAKAT</h6>
				<?php $total=0;
				  foreach ($pengeluaran as $key => $value) { 
					 $total = $total+$value->total;
				  } 
					$total_masalah_pokok = $total;
				  ?>
		</div>
			<div id="graph_p1" style="width: 100%; height: 400px;"></div>
						<div class="text-center btn-helper">
								 <button onclick="modal_profile('Pendapat Tokoh Berpengaruh Terhadap Permasalahan Pokok Masyarakat','alias','p1','detail_survey_alias');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_masalah_pokok);?></button>
					</div>		
		</div>
	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 600px !important;">
			<div class="project-head head-helper"><h6>PENDAPAT TOKOH BERPENGARUH TERHADAP PERSOALAN YANG PERLU DISELESAIKAN CEPAT OLEH PEMERINTAH</h6>
				<?php $total=0;
				  foreach ($p2 as $key => $value) { 
					 $total = $total+$value->total;
				  } 
					$total_persoalan = $total;
					?>
					</div>
			<div id="graph_p2" style="width: 100%; height: 400px;"></div>
			<div class="text-center btn-helper">
				<button onclick="modal_profile('Pendapat Tokoh Berpengaruh Terhadap Persoalan Yang Perlu Diselesaikan Cepat Oleh Pemerintah','alias','p2','detail_survey_alias');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_persoalan);?></button>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="padding-top: 10px"><h6 class="element-header">DEMOGRAFI TOKOH BERPENGARUH</h6></div>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 680px !important;">
			<div class="project-head head-helper" style="padding-bottom:24px;">				
							<h6 id="textPendidikan"> TINGKATAN PENDIDIKAN TOKOH BERPENGARUH</h6>					
							<div  id="exportdivPendidikan"></div>
									<?php $total = 0;
									foreach ($pendidikan as $key => $value) {
											$total = $total + $value->total;
									}
									$total_pendidikan = $total;
									?>
			</div>
			<div id="pie_pendidikan" style="width: 100%; height: 500px;padding: 5px;"></div>
				<div class="text-center btn-helper">
				<button onclick="modal_profile('Tingkatan Pendidikan Tokoh Berpengaruh','alias','p10','detail_survey_alias');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_pendidikan);?></button>
				</div>	
		</div>

	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 700px !important;">
			<div class="project-head head-helper" style="padding-bottom:24px;">
					<h6> TOKOH BERPENGARUH BERDASARKAN AGAMA</h6>					
					<div  id="exportdivAgama"></div>
					<?php $total = 0;
					foreach ($agama as $key => $value) {
							$total = $total + $value->total;
					}
					$total_agama = $total;
					?>
			 </div>
			<div id="pie_agama" style="width: 100%; height: 500px;padding: 5px;"></div>
			<div class="text-center  btn-helper">
								 <button onclick="modal_profile('TOKOH BERPENGARUH BERDASARKAN AGAMA','biasa','p14','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_agama);?></button>
			</div>		
			<!-- <h5 role class	="btn btn-lg btn-outline-danger text-dark" style="font-family: Verdana;font-weight:bold;position:absolute;bottom:30px;right:180px;">
									N: <?=$total_agama; ?>
							</h5>		 -->
		</div>
	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box" style="min-height: 700px !important;">
			<div class="project-head head-helper" style="padding-bottom:24px;">
				<h6> TOKOH BERPENGARUH BERDASARKAN SUKU</h6>					
				<div  id="exportdivSuku"></div>
				<br><br>
						<?php $total=0;
							foreach ($suku as $key => $value) { 
							$total = $total+$value->total;
							} 
							$total_suku = $total;
							?>
			</div>
			<div id="pie_suku" style="width: 100%; height: 500px;padding: 5px;"></div>
			<div class="text-center  btn-helper">
				<button onclick="modal_profile('TOKOH BERPENGARUH BERDASARKAN SUKU','biasa','p15','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_suku);?></button>
			</div>
			<!-- <h5 role class	="btn btn-lg btn-outline-danger text-dark" style="font-family: Verdana;font-weight:bold;position:absolute;bottom:30px;right:180px;">
													N: <?=$total_suku; ?>
							</h5>		 -->
		</div>
	</div>
	<div class="col-md-6 projects-list">
		<div class="project-box"  style="min-height: 680px !important;">
			<div class="project-head head-helper">

				<h6> DATA PENGELUARAN TOKOH BERPENGARUH PER BULAN</h6>					
				<div  id="exportdivPengeluaran"></div>

			<!-- <div class="row justify-content-between">
							<div class="col-md-10">
												<h6>DATA PENGELUARAN TOKOH BERPENGARUH PER BULAN</h6>
							</div>
							<div class="col-md-2" id="exportdivPengeluaran">
							</div>
											</div> -->	

				<?php $total=0;
				  foreach ($pengeluaran as $key => $value) { 
					 $total = $total+$value->total;
				  } 
					$total_pengeluaran = $total;
				  ?>
			</div>
			<div id="pie_pengeluaran" style="width: 100%; height: 500px;padding: 5px;"></div>
			<div class="text-center btn-helper" >
				<button onclick="modal_profile('Data Pengeluaran Tokoh Berpengaruh Per Bulan','pengeluaran','p13','detail_survey_pengeluaran');" class="btn btn-outline-danger btnnyoh" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_pengeluaran);?></button>
			</div>
			<!-- <h5 role class	="btn btn-lg btn-outline-danger text-dark" style="font-family: Verdana;font-weight:bold;position:absolute;bottom:30px;right:180px;">
													N: <?=$total_pengeluaran; ?>
							</h5>		 -->
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding-top: 10px"><h6 class="element-header">ORGANISASI TOKOH BERPENGARUH
	</h6></div>
	<div class="col-md-12">
		<div class="element-box">
			<div class="project-head head-helper">
				<div class="row">
					<div class="col-md-12"><h6>ORGANISASI YANG DIANUT TOKOH BERPENGARUH</h6>
						<?php $total = ''; foreach ($organisasi as $key => $value) { 
							$total = $total+$value->total;
							}
							$total_organisasi = (float) $total;
						?>
					</div>
					<!-- <div class="col-md-4"><h6>Organisasi Yang Dipilih</h6></div> -->
				</div>
			</div>
			<!-- <div id="pie_organisasi" style="width: 100%; height: 300px;"></div> -->
			<div id="graph_organisasi" style="width: 100%; height: 300px;"></div>
			<!-- <h5 class	="text-center" style="color:#000; font-family: Verdana;font-weight:bold;">N: <?=$total_organisasi; ?></h5> -->
				<div class="text-center btn-helper">
					<button onclick="modal_profile('ORGANISASI YANG DIANUT TOKOH BERPENGARUH','organisasi','p16','detail_survey_organisasi');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_organisasi);?></button>
				</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding-top: 10px"><h6 class="element-header">DATA PENGGUNA MEDIA SOSIAL TOKOH BERPENGARUH</h6>
		<?php $total = ''; foreach ($sosial_media as $key => $value) { 
								$total = $total+$value->total;
								}
								$total_media_sosial = (float) $total;
							?>
</div>
	<div class="col-md-12">
			<div class="element-box">
				<div class="project-head head-helper">
					<div class="row">
						<div class="col-md-6"><h6>TINGKAT PENGGUNAAN MEDIA SOSIAL OLEH TOKOH BERPENGARUH
							
						</h6></div>
						<div class="col-md-6"><h6>JENIS SOSIAL MEDIA YANG DIGUNAKAN
							
						</h6></div>
					</div>
				<div class="row" id="divtotal_media_sosial">
					<div class="col-md-6">
						<div id="pie_sosmed" style="width: 100%; height: 300px;"></div>
						<div class="text-center btn-helper">
							<button onclick="modal_profile('detail_survey_organisasi','biasa','p17','detail_survey');" class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($total_media_sosial);?></button>
						</div>
					</div>
					<div class="col-md-6">
						<div id="pie_sosmed_detail" style="width: 100%; height: 300px;"></div>
						<div class="text-center btn-helper">
							<button class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N: <?=number_format($facebook+$email+$twitter+$instagram+$lainnya);?></button>
						</div>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA PRABOWO 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?= number_format($jokowi) ?></div>
                </div>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA JOKOWI 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($prabowo)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">JUMLAH RELAWAN</h6>
                <a href="#" class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                    <div class="centered ">
                        <div class="value" ><?=$jum_guraklih?></div>

                </div>
                </a>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TOKOH BERPENGARUH</h6>
                <a href="#" class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                        <div class="centered ">
                            <div class="value"><?=$jum_ketokohan?></div>
                        </div>

                </a>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display:inline-block;margin-right:20px;">
					Detail
          <!-- <button onclick="export_excel()" class="mr-2 mb-2 btn btn-success btn-sm btn-excel" type="button">Excel</button> -->
				</h5>
				<?php if($user['role'] == 'admin'){ ?>
                    <img  onclick="export_excel()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;">
                <?php } ?>
				
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table id="table_detail" width="100%" class="table table-striped"></table>
      </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_pilpres" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display:inline-block;margin-right:20px;">
					Detail
          <!-- <button onclick="export_excel()" class="mr-2 mb-2 btn btn-success btn-sm btn-excel" type="button">Excel</button> -->
				</h5>
				<?php if($user['role'] == 'admin'){ ?>
                    <img  onclick="export_excel()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;">
                <?php } ?>
				
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table id="table_detail_pilpres" width="100%" class="table table-striped"></table>
      </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail_pilpres1" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display:inline-block;margin-right:20px;">
					Detail
          <!-- <button onclick="export_excel()" class="mr-2 mb-2 btn btn-success btn-sm btn-excel" type="button">Excel</button> -->
				</h5>
				<?php if($user['role'] == 'admin'){ ?>
                    <img  onclick="export_excel()" alt="export to excel" src="img/flags-icons/xl.png" width="25px" style="margin-top:3px;">
                <?php } ?>
				
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table id="table_detail_pilpres1" width="100%" class="table table-striped"></table>
      </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
      </div>
    </div>
  </div>
</div>

<link href="<?=base_url()?>summernote-master/dist/summernote.css" rel="stylesheet"> 
<script src="<?=base_url()?>summernote-master/dist/summernote.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table_detail").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Pilpres/detail_survey_alias/all/p1/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'nama', "title": "Nama"},
					{ "data": 'hp', "title": "HP"},
					{ "data": 'jabatan', "title": "Jabatan"},
                    { "data": 'provinsi', "title": "Provinsi" },
                    { "data": 'kotaKabupaten', "title": "Kab"},
                    { "data": 'kecamatan', "title": "Kec" },
                    { "data": 'desaKelurahan', "title": "Kel" },
                    { "data": 'informasi', "title": "Informasi" },
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });

		var t = $("#table_detail_pilpres").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Pilpres/detail_survey_alias/all/p1/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'nama', "title": "Nama"},
					{ "data": 'hp', "title": "HP"},
					{ "data": 'jabatan', "title": "Jabatan"},
                    { "data": 'provinsi', "title": "Provinsi" },
                    { "data": 'kotaKabupaten', "title": "Kab"},
                    { "data": 'kecamatan', "title": "Kec" },
                    { "data": 'desaKelurahan', "title": "Kel" },
                    { "data": 'parpol', "title": "Parpol" },
                    { "data": 'informasi', "title": "Pilihan" },
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });

		var t = $("#table_detail_pilpres1").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Pilpres/detail_survey_alias/all/p1/all/all/all')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'nama', "title": "Nama"},
					{ "data": 'hp', "title": "HP"},
					{ "data": 'jabatan', "title": "Jabatan"},
                    { "data": 'provinsi', "title": "Provinsi" },
                    { "data": 'kotaKabupaten', "title": "Kab"},
                    { "data": 'kecamatan', "title": "Kec" },
                    { "data": 'desaKelurahan', "title": "Kel" },
                    { "data": 'informasi', "title": "Parpol" },
                    { "data": 'parpol', "title": "Pilihan" },
                    
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
		
    });
</script>
<script type="text/javascript">
  	var nama_survey;
  	var nama_surveyURI;
  	var tipe_surveyURI;
  	var tipe_wilayahURI;
  	var nama_wilayahURI;
  	var id_wilayahURI;
  	var tipe;
  	var judul;
  	var endPoinURI = 'detail_survey';
  	
  	function export_excel(){
	  	judulURI = encodeURI(judul);
	  	console.log(judulURI);
	  	console.log(tipe);
	  	console.log(nama_surveyURI);
	  	console.log(tipe_surveyURI);
	  	console.log(tipe_wilayahURI);
	  	console.log(nama_wilayahURI);
	  	console.log(id_wilayahURI);
	  	console.log(id_wilayahURI);
	    if(tipe == 'alias'){
	      window.location = '<?php echo base_url(); ?>Excel/export_alias/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;  
	    }else if(tipe == 'organisasi'){
	    	window.location = '<?php echo base_url(); ?>Excel/export_organisasi/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }else if(tipe == 'pengeluaran'){
	    	window.location = '<?php echo base_url(); ?>Excel/export_pengeluaran/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }else if(tipe == 'gubernur'){
	    	window.location = '<?php echo base_url(); ?>Excel/export_gubernur/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }else if(tipe == 'sosmed'){
	    	window.location = '<?php echo base_url(); ?>Excel/export_sosmed/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }else if(tipe == 'change'){
	    	window.location = '<?php echo base_url(); ?>Excel/export_change/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }else if(tipe == 'partai'){

	    }else{
	    	window.location = '<?php echo base_url(); ?>Excel/export/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI+'/'+judulURI;
	    }
  	}
  	
  	$(".change").click(function() {
		parpol = $(this).data('parpol');
		nama_surveyURI = encodeURI(parpol);
        tipe_surveyURI = encodeURI('p4');
        tipe_wilayahURI = encodeURI('<?= $tipe ?>');
        nama_wilayahURI = encodeURI('<?= $nama ?>');
        id_wilayahURI = encodeURI('<?= $id ?>');
        tipe = 'change';
        judul = 'PERUBAHAN PILIHAN PARTAI POLITIK TOKOH BERPENGARUH TERHADAP PDI PERJUANGAN';
        var table = $('#table_detail').DataTable();
        table.ajax.url("<?= site_url('pilpres/detail_survey_change/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
		$("#exampleModalLabel").text(judul);	
    	$("#modal_detail").modal('show');
		
	});

	$(".partai_14").click(function() {
		parpol = $(this).data('parpol');
		nama_surveyURI = encodeURI(parpol);
        tipe_surveyURI = encodeURI('p3');
        tipe_wilayahURI = encodeURI('<?= $tipe ?>');
        nama_wilayahURI = encodeURI('<?= $nama ?>');
        id_wilayahURI = encodeURI('<?= $id ?>');
        tipe = 'biasa';
        judul = 'PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2014';
        var table = $('#table_detail_pilpres1').DataTable();
        table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
		$("#exampleModalLabel").text(judul);	
    	$("#modal_detail_pilpres1").modal('show');
	});

	$(".partai_19").click(function() {
		parpol = $(this).data('parpol');
		nama_surveyURI = encodeURI(parpol);
        tipe_surveyURI = encodeURI('p4');
        tipe_wilayahURI = encodeURI('<?= $tipe ?>');
        nama_wilayahURI = encodeURI('<?= $nama ?>');
        id_wilayahURI = encodeURI('<?= $id ?>');
        tipe = 'biasa';
        judul = 'PILIHAN PARTAI POLITIK TOKOH BERPENGARUH PADA PEMILU 2019';
        var table = $('#table_detail_pilpres1').DataTable();
        table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
		$("#exampleModalLabel").text(judul);	
    	$("#modal_detail_pilpres1").modal('show');
	});
  
	function modal_profile(vjudul,vtipe,vtipe_survey,vendPoin){     
		var base_url = <?php echo json_encode(base_url()); ?>;
		
		console.log(base_url);
		console.log(vjudul);
		console.log(vtipe);
		console.log(vtipe_survey);
		console.log(vendPoin);

		
		nama_surveyURI = 'ALL';
		tipe_surveyURI = vtipe_survey;
		tipe_wilayahURI = encodeURI('<?= $tipe ?>');
		nama_wilayahURI = encodeURI('<?= $nama ?>');
		id_wilayahURI = encodeURI('<?= $id ?>');
		judul = vjudul;
		tipe = vtipe;
		endPoinURI = vendPoin;
		var table = $('#table_detail').DataTable();
		table.ajax.url(base_url+'/pilpres/'+endPoinURI+'/'+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
		$("#exampleModalLabel").text(judul);
		$("#modal_detail").modal('show');
		
	}
  
	var chart = AmCharts.makeChart("pie_pendidikan", {
      	"type": "pie",
      	"pullOutRadius" : "5%",
		"radius": 150,
      	"theme": "light",
      	"pulledField": 'pulled',
      	"colorField": "color",
       	"labelRadius": 10,
        "labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
      	<?php 
			foreach ($pendidikan as $key => $value) {

			$valuenya=$value->nama_alias;
			if($valuenya=='Tamat SD dan di bawahnya') {
				$color='#C12823';
			}elseif($valuenya=='Tamat SLTP') {
				$color='#26166E';
			}elseif($valuenya=='Tamat SLTA') {
				$color='#8EA0B4';
			}elseif($valuenya=='Di atas SLTA') {
				$color='#FEC746';
			}


      	 ?>
      	{
	    	"country": "<?=$value->nama_alias?>",
	        "litres": <?=$value->jumlah?>,
          	"pulled":true,
          	"color": "<?=$color?>",
	    },
      	<?php } ?>
  		],
      	"valueField": "litres",
      	"titleField": "country",
      	"listeners": [{
        	"event": "clickSlice",
            "method": function(event) {
                console.log(event.dataItem.title);
				nama_survey = event.dataItem.title;
                if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p10');
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>');
                tipe = 'alias';
                judul = 'Tingkatan Pendidikan Tokoh Berpengaruh';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey_alias/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Tingkatan Pendidikan Tokoh Berpengaruh");	
            	$("#modal_detail").modal('show');
            }
        }],
      	"export": {
	    	"enabled": true,
			"divId": "exportdivPendidikan"
	  	}
    });
	
	var chart = AmCharts.makeChart("pie_agama", {
      	"type": "pie",
      	"pullOutRadius" : "5%",
      	"theme": "light",
		"radius": 150,
      	"pulledField": 'pulled',
      	"colorField": "color",
       	"labelRadius": 10,
      	"labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
      	<?php foreach ($agama as $key => $value) { 

$valuenya=$value->nama;
if($valuenya=='Islam') {
	$color='#0B8E0E';
}elseif($valuenya=='Protestan') {
	$color='#0001AB';
}elseif($valuenya=='Hindu') {
	$color='#FECE02';
}elseif($valuenya=='Budha') {
	$color='#F5B201';
}elseif($valuenya=='Katolik') {
	$color='#0081DE';
}elseif($valuenya=='Kong Hu Cu') {
	$color='#000000';
}elseif($valuenya=='Tidak Mengisi') {
	$color='#A6A6A6';
}elseif($valuenya=='Lainnya') {
	$color='#FFDCBE';
}

      		?>
      	{

      		

	    	"country": "<?=$value->nama?>",
	        "litres": <?=$value->jumlah?>,
          	"pulled":true,
          	"color": "<?=$color?>",
	    },
      	<?php } ?>
  		],
      	"valueField": "litres",
      	"titleField": "country",
      	"listeners": [{
            "event": "clickSlice",
            "method": function(event) {
                console.log(event.dataItem.title);
                nama_survey = event.dataItem.title;
                if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p14');
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>');
                tipe = 'biasa';
                judul = 'Tokoh Berpengaruh Berdasarkan Agama';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Tokoh Berpengaruh Berdasarkan Agama");
            	$("#modal_detail").modal('show');
            }
        }],
      	"export": {
	    	"enabled": true,
			"divId": "exportdivAgama"
	  	}
    });

    var chart = AmCharts.makeChart("pie_suku", {
      	"type": "pie",
      	"pullOutRadius" : "5%",
      	"theme": "light",
		"radius": 150,
      	"pulledField": 'pulled',
       	"labelRadius": 10,
		"labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
      	<?php foreach ($suku as $key => $value) { ?>
      	{
	    	"country": "<?=$value->nama?>",
	        "litres": <?=$value->jumlah?>,
          	"pulled":true
	    },
      	<?php } ?>
  		],
      	"valueField": "litres",
      	"titleField": "country",
      	"listeners": [{
            "event": "clickSlice",
            "method": function(event) {
            	nama_survey = event.dataItem.title;
            	if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p15'); 
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>');
                tipe = 'biasa';
                judul = 'Tokoh Berpengaruh Berdasarkan Suku';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
            	$("#exampleModalLabel").text("Tokoh Berpengaruh Berdasarkan Suku");
				$("#modal_detail").modal('show');
            }
        }],
      	"export": {
	    	"enabled": true,
			"divId": "exportdivSuku"
	  	}
    });

    var chart = AmCharts.makeChart("pie_pengeluaran", {
      	"type": "pie", 
      	"pullOutRadius" : "5%",
      	"theme": "light",
		"radius": 150,
      	"pulledField": 'pulled',
       	"labelRadius": 10,
      	"labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
      	<?php foreach ($pengeluaran as $key => $value) { ?>
      	{
	   		"country": "<?=$value->nama_alias?>",
	        "litres": <?=$value->jumlah?>,
          	"pulled":true
	    },
      	<?php } ?>

  		],
      	"valueField": "litres",
      	"titleField": "country",
      	"listeners": [{
            "event": "clickSlice",
            "method": function(event) {
             	//    console.log(event.dataItem.title);
                nama_survey = event.dataItem.title;
                if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p13');
                
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>')
                tipe = 'pengeluaran';
                judul = 'Data Pengeluaran Tokoh Berpengaruh Per Bulan';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey_pengeluaran/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Data Pengeluaran Tokoh Berpengaruh Per Bulan");
            	$("#modal_detail").modal('show');
            }
        }],
      "export": {
	    "enabled": true,
			"divId": "exportdivPengeluaran"
	  }
    });

    var chart = AmCharts.makeChart("graph_p1", {
      	"type": "serial",
	  	"theme": "light",
	  	"colorField": "color",
	  	"marginRight": 70,
	  	"dataProvider": [
	  		<?php 
	  		$i=0;
	  		foreach ($p1 as $key => $value) {
		    	$valuenya=$value->nama_alias;
		    	if ($valuenya=='Lainnya' or $valuenya=='Tidak Tahu atau Tidak Jawab' ) {
		        	$color='#A6A6A6';
		    	} else {
		      		$color='#F69200';
		    }
	  		?>
	  		
	  		{
            	"name": "<?=$value->nama_alias?>",
            	"points": <?=$value->jumlah?>,
            	"color": "<?=$color?>",
        	},
	  		<?php } ?>
        ],
	  	"startDuration": 1,
	  	"graphs": [{
			"balloonText": "<b>[[category]]: [[value]] %</b>",
			"fillColorsField": "color",
			"labelText": "[[value]]%",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "points"
	  	}],
	  	"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
	  	},
	  	"rotate": true,
	  	"categoryField": "name",
	  	"categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  	"valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
		"listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
				nama_survey = event.item.category;
				if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
            	nama_surveyURI = encodeURI(nama_survey);
            	tipe_surveyURI = encodeURI('p1');
            	tipe_wilayahURI = encodeURI('<?= $tipe ?>');
            	nama_wilayahURI = encodeURI('<?= $nama ?>');
            	id_wilayahURI = encodeURI('<?= $id ?>');
				judul = 'Pendapat Tokoh Berpengaruh Terhadap Permasalahan Pokok Masyarakat';
				tipe = 'alias';
            	var table = $('#table_detail').DataTable();
            	table.ajax.url("<?= site_url('pilpres/detail_survey_alias/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Pendapat Tokoh Berpengaruh Terhadap Permasalahan Pokok Masyarakat");
				$("#modal_detail").modal('show');
            }
        }],
		"export": {
	    	"enabled": true,
			"divId":"exportdivGraphP1"
	  	}

	});
	var chart = AmCharts.makeChart("graph_p2", {
      	"type": "serial",
	  	"theme": "light",
	   	"colorField": "color",
	  	"marginRight": 70,
	 	"dataProvider": [
	  		<?php 
	  		$i=0;
	  		foreach ($p2 as $key => $value) {
 				$valuenya=$value->nama_alias;
    			if ($valuenya=='Lainnya' or $valuenya=='Tidak Tahu atau Tidak Jawab' ) {
        			$color='#A6A6A6';
    			} else {
      				$color='#F69200';
    			}
	  	 	?>
	  		{
            "name": "<?=$value->nama_alias?>",
            "points": <?=$value->jumlah?>,
            "color": "<?=$color?>",
        },
	  	<?php } ?>
        ],
	  	"startDuration": 1,
	  	"graphs": [{
			"balloonText": "<b>[[category]]: [[value]] %</b>",
			"fillColorsField": "color",
			"labelText": "[[value]]%",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "points"
	  	}],
	  	"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
	  	},
	  	"rotate": true,
	  	"categoryField": "name",
	  	"categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  	"valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
		"listeners": [{
        	"event": "clickGraphItem",
        	"method": function(event) {
				nama_survey = event.item.category;
				if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p2');
                
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>')
				tipe = 'alias';
				judul = 'Pendapat Tokoh Berpengaruh Terhadap Persoalan Yang Perlu Diselesaikan Cepat Oleh Pemerintah';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey_alias/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Pendapat Tokoh Berpengaruh Terhadap Persoalan Yang Perlu Diselesaikan Cepat Oleh Pemerintah");
            	$("#modal_detail").modal('show');
                }
       	}],
		"export": {
	    	"enabled": true,
			"divId":"exportdivGraphP2"
	  	}
	});
	var chart = AmCharts.makeChart("gubernur",
    {
        "type": "serial",
        "theme": "light",
        "dataProvider": [
	  	<?php foreach ($gubernur as $key => $value) { 					
			if ($value->nama_kepala == "BELUM MENENTUKAN PILIHAN" || $value->nama_kepala == "RAHASIA" || $value->nama_kepala == "TIDAK AKAN MEMILIH") {
    			$color = '#A6A6A6';
			} else {
				//$color = '#F69200';
				if ($value->warna == null) {
			 	$color = '#F69200';
				} else {
			 	$color = '#C40000';
				}
				
				
			}
			// if ($value->warna == null) {
			// 	$color = '#F69200';
			// } else {
			// 	$color = '#F69200';
			// }
	  		?>
	  		{
            "name": "<?=$value->kepala_alias?>",
            "points": <?=$value->jumlah?>,
            "color": '<?=$color?>',
        	},
	  	<?php } ?>
        ],
        "startDuration": 2,
        "rotate": true,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]] %</b></span>",
            "bulletOffset": 10,
            "labelText": "[[value]]%",
            "bulletSize": 42,
            "colorField": "color",
            // "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        // "autoMargins": false,
        "categoryField": "name",
        "categoryAxis": {
            "gridColor": "#FFFFFF",
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillAlpha": 0.00,
            "position": "left",
        },
        "valueAxes": [ {
        	"axisAlpha": 0,
        	"maximum" : 100,
        	"labelsEnabled" : false,
		    "gridColor": "#FFFFFF",
		    "gridAlpha": 0,
		    "dashLength": 0
		} ],
		"listeners": [{
        	"event": "clickGraphItem",
        	"method": function(event) {
				nama_survey = event.item.category;
				if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p7');
                
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>'); 
                id_wilayahURI = encodeURI('<?= $id ?>')
				tipe = 'gubernur';
                judul = 'PILIHAN POLITIK TOKOH BERPENGARUH TERHADAP CALON GUBERNUR 2018';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey_gubernur/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text(judul);
            	$("#modal_detail").modal('show');
                }
       	}],
		"export": {
	    "enabled": true,
			"divId":"exportdivGubernur"
	  }
    });

    var chart = AmCharts.makeChart("graph_2014", {
      "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "dataProvider": [
	  	<?php foreach ($pres_14_pilih as $key => $value) { 
			 $namanya1 = strtolower($value->nama);
			 $namanya2 = ucwords($namanya1); 
			  ?>
	  		{
            "name": "<?=$namanya2?>",
            "points": <?=$value->jumlah?>,
            "color": "#F69200",
        },
	  	<?php } ?>
		<?php foreach ($pres_14_tdk_pilih as $key => $value) { 
			 $namanya1 = strtolower($value->nama);
			 $namanya2 = ucwords($namanya1);
			?>
	  		{
            "name": "<?=$namanya2?>",
            "points": <?=$value->jumlah?>,
            "color": "#A6A6A6",
        },
	  	<?php } ?>
        ],
	  "startDuration": 1,
	  "graphs": [{
		"balloonText": "<b>[[category]]: [[value]] %</b>",
		"fillColorsField": "color",
		"labelText": "[[value]]%",
		"fillAlphas": 0.9,
		"lineAlpha": 0.2,
		"type": "column",
		"valueField": "points"
	  }],
	  "chartCursor": {
		"categoryBalloonEnabled": false,
		"cursorAlpha": 0,
		"zoomable": false
	  },
	  "rotate": true,
	  "categoryField": "name",
	  "categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  "valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
		"listeners": [{
                        "event": "clickGraphItem",
                        "method": function(event) {
						console.log("Test: ",event.item.category);
						nama_survey = event.item.category;
						if (nama_survey == 'Tidak Mengisi'){
		                	nama_survey = 'NULL';
		                }
                        nama_surveyURI = encodeURI(nama_survey);
                        tipe_surveyURI = encodeURI('p5');
                        
                        tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                        nama_wilayahURI = encodeURI(event.item.category); 
												// encodeURI('<?= $nama ?>');
                        id_wilayahURI = encodeURI('<?= $id ?>')
												console.log(nama_wilayahURI);
						tipe = 'biasa';
						judul = 'Pilihan Politik Tokoh Berpengaruh Pada Pemilu Presiden 2014';
                        var table = $('#table_detail_pilpres').DataTable();
                        table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
												$("#exampleModalLabel").text("Pilihan Politik Tokoh Berpengaruh Pada Pemilu Presiden 2014");
                    	$("#modal_detail_pilpres").modal('show');
                        }
                    }],
		"export": {
	    "enabled": true,
			"divId":"exportdivGraph2014"
	  }

	});

	var chart = AmCharts.makeChart("graph_2019", {
      "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "dataProvider": [
	  	<?php foreach ($pres_19_pilih as $key => $value) { 
			 $namanya1 = strtolower($value->nama);
			 $namanya2 = ucwords($namanya1);
			  ?>
	  		{
            "name": "<?=$namanya2?>",
            "points": <?=$value->jumlah;?>,
            "color": "#F69200",
        },
	  	<?php } ?>
	  	<?php foreach ($pres_19_tdk_pilih as $key => $value) { 
			 $namanya1 = strtolower($value->nama);
			 $namanya2 = ucwords($namanya1);
			  ?>
	  		{
            "name": "<?=$namanya2?>",
            "points": <?=$value->jumlah?>,
            "color": "#A6A6A6",
        },
	  	<?php } ?>
        ],
	  "startDuration": 1,
	  "graphs": [{
		"balloonText": "<b>[[category]]: [[value]] %</b>",
		"fillColorsField": "color",
		"labelText": "[[value]]%",
		"fillAlphas": 0.9,
		"lineAlpha": 0.2,
		"type": "column",
		"valueField": "points"
	  }],
	  "chartCursor": {
		"categoryBalloonEnabled": false,
		"cursorAlpha": 0,
		"zoomable": false
	  },
	  "rotate": true,
	  "categoryField": "name",
	  "categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  "valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
				"listeners": [{
                        "event": "clickGraphItem",
                        "method": function(event) {
						console.log("Test: ",event.item.category);
						nama_survey = event.item.category;
						if (nama_survey == 'Tidak Mengisi'){
		                	nama_survey = 'NULL';
		                }
                        nama_surveyURI = encodeURI(nama_survey);
                        tipe_surveyURI = encodeURI('p6');
                        
                        tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                        nama_wilayahURI = encodeURI(event.item.category); 
												// encodeURI('<?= $nama ?>');
                        id_wilayahURI = encodeURI('<?= $id ?>')
												console.log(nama_wilayahURI);
												tipe = 'biasa';
						judul = 'Pilihan Politik Tokoh Berpengaruh Terhadap Calon Presiden 2019';
                        var table = $('#table_detail_pilpres').DataTable();
                        table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
											$("#exampleModalLabel").text("Pilihan Politik Tokoh Berpengaruh Terhadap Calon Presiden 2019");
                    	$("#modal_detail_pilpres").modal('show');
                        }
                    }],
		"export": {
	    "enabled": true,
			"divId":"exportdivGraph2019"
	  }

	});

	/* var chart = AmCharts.makeChart("partai_2014", {
      "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "dataProvider": [
	  	<?php foreach ($partai_14 as $key => $value) { ?>
	  		{
            "name": "<?=$value->nama?>",
            "points": <?=$value->jumlah?>
        },
	  	<?php } ?>
        ],
	  "startDuration": 1,
	  "graphs": [{
		"balloonText": "<b>[[category]]: [[value]] %</b>",
		"fillColorsField": "color",
		"labelText": "[[value]]%",
		"fillAlphas": 0.9,
		"lineAlpha": 0.2,
		"type": "column",
		"valueField": "points"
	  }],
	  "chartCursor": {
		"categoryBalloonEnabled": false,
		"cursorAlpha": 0,
		"zoomable": false
	  },
	  "rotate": true,
	  "categoryField": "name",
	  "categoryAxis": {
			"axisAlpha": 0,
			"gridAlpha": 0,
			// "inside": true,
			"tickLength": 0
		},
	  "valueAxes": [ {
			"axisAlpha": 0,
			"labelsEnabled" : false,
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		} ],
		"export": {
	    "enabled": true,
			"divId":"exportdivPartai2014"
	  }

	}); */

  	var chart = AmCharts.makeChart("graph_organisasi", {
    	"type": "serial",
    	"theme": "light",
     	"colorField": "color",
    	"marginRight": 70,
    	"dataProvider": [
      	<?php 
			$i=0;
      		foreach ($organisasi as $key => $value) {
        		if($value->nama == 'Bukan bagian dari organisasi islam manapun'){
					$color = '#A6A6A6';
					$namax = 'Bukan bagian dari organisasi manapun';
       		 	}
				if($value->nama == 'Lainnya' or $value->nama == 'Tidak tahu/Tidak Menjawab' or $value->nama == 'Bukan bagian dari organisasi islam manapun'){
					$color = '#A6A6A6';
				}else{
				 	$color = '#F69200';
				}
      		?>
        	{
            	"name": "<?=$value->nama?>",
            	"points": <?=$value->jumlah?>,
				"color": "<?=$color?>",
        	},
      	<?php } ?>
        ],
    	"startDuration": 1,
    	"graphs": [{
    		"balloonText": "<b>[[category]]: [[value]] %</b>",
    		"fillColorsField": "color",
    		"labelText": "[[value]]%",
    		"fillAlphas": 0.9,
    		"lineAlpha": 0.2,
    		"type": "column",
    		"valueField": "points"
    	}],
    	"chartCursor": {
    		"categoryBalloonEnabled": false,
    		"cursorAlpha": 0,
    		"zoomable": false
    	},
    	"rotate": true,
    	"categoryField": "name",
    	"categoryAxis": {
      		"axisAlpha": 0,
      		"gridAlpha": 0,
      		// "inside": true,
      		"tickLength": 0
    	},
    	"valueAxes": [ {
      		"axisAlpha": 0,
      		"labelsEnabled" : false,
      		"gridColor": "#FFFFFF",
      		"gridAlpha": 0.2,
      		"dashLength": 0
    	} ],
		"listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
				nama_survey = event.item.category;
				if(nama_survey == 'Tidak tahu/Tidak Menjawab'){
					nama_survey = 'Tidak Tahu Tidak Menjawab';
				}
				if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
				console.log(nama_survey);
            	nama_surveyURI = encodeURI(nama_survey);
            	tipe_surveyURI = encodeURI('p16');
            	tipe_wilayahURI = encodeURI('<?= $tipe ?>');
            	nama_wilayahURI = encodeURI('<?= $nama ?>');
            	id_wilayahURI = encodeURI('<?= $id ?>');
						
				tipe = 'organisasi';
				judul = 'Organisasi Yang Dianut Tokoh Berpengaruh';
            	var table = $('#table_detail').DataTable();
            	table.ajax.url("<?= site_url('pilpres/detail_survey_organisasi/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("Organisasi Yang Dianut Tokoh Berpengaruh");
				$("#modal_detail").modal('show');
            }
        }],
    	"export": {
      		"enabled": true,
			"divId":"exportdivPartai2014"
   	 	}

  	});
  	var chart = AmCharts.makeChart("pie_sosmed", {
      	"type": "pie",
      	// "pullOutRadius" : "5%",
      	"theme": "light",
      	"colorField": "color",
      	"pulledField": 'pulled',
       	"labelRadius": 10,
    	"labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
      		<?php 
      		$i=0;
      		foreach ($sosial_media as $key => $value) {
      		$i++;
		 	if ($i % 2 == 0) {
		  		$color='#F69200';
			} else if($value->nama == 'Tidak') {
				$color='#FFD85D';
			} else {
				$color = '#ffb64c';
			} ?>
      		{
	    		"country": "<?=$value->nama?>",
	        	"litres": <?=$value->jumlah?>,
	        	"color": "<?=$color?>",
          		"pulled":true
	    	},
      		<?php } ?>
  		],
      	"valueField": "litres",
      	"titleField": "country",
      	"listeners": [{
        	"event": "clickSlice",
            "method": function(event) {
                console.log(event.dataItem.title);
				nama_survey = event.dataItem.title;
                if (nama_survey == 'Tidak Mengisi'){
                	nama_survey = 'NULL';
                }
                nama_surveyURI = encodeURI(nama_survey);
                tipe_surveyURI = encodeURI('p17');
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>');
                tipe = 'biasa';
                judul = 'DATA PENGGUNA MEDIA SOSIAL TOKOH BERPENGARUH';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("DATA PENGGUNA MEDIA SOSIAL TOKOH BERPENGARUH");	
            	$("#modal_detail").modal('show');
            }
        }],
      	"export": {
	    	"enabled": true,
			"divId":"exportdivSosmed"
	  	}
    });

    var chart = AmCharts.makeChart("pie_sosmed_detail", {
    	"type": "pie",
      	// "pullOutRadius" : "5%",
      	"theme": "light",	
		"pulledField": 'pulled',
 		"labelRadius": 10,
    	"labelText": "[[country]] : [[percents]]%",
      	"dataProvider": [
	      	{
		    	"country": "Email",
		        "litres": <?=$email?>,
		        "color": "#E84F4B",
	          	"pulled":true
		    },
		    {
		        "country": "Facebook",
		        "litres": <?=$facebook?>,
		        "color": "#3B5998",
	          	"pulled":true,
		    },
		    {
		        "country": "Twitter",
		        "litres": <?=$twitter?>,
		        "color": "#1EA1F3",
	          	"pulled":true
		    },
		    {
		        "country": "Instagram",
		        "litres": <?=$instagram?>,
		        "color": "#CB2662",
	          	"pulled":true
		    },
		    {
		        "country": "Lainnya",
		        "litres": <?=$lainnya?>,
		        "color": "#98999B",
	          	"pulled":true
		    },
	  	],
	  	"listeners": [{
        	"event": "clickSlice",
            "method": function(event) {
                console.log(event.dataItem.title);
				nama_survey = event.dataItem.title;
                if(nama_survey == 'Email'){
                	tipe_surveyURI = encodeURI('p17a');	
                }else if(nama_survey == 'Facebook'){
                	tipe_surveyURI = encodeURI('p17b');	
                }else if(nama_survey == 'Twitter'){
                	tipe_surveyURI = encodeURI('p17c');	
                }else if(nama_survey == 'Instagram'){
                	tipe_surveyURI = encodeURI('p17d');	
                }else if(nama_survey == 'Lainnya'){
                	tipe_surveyURI = encodeURI('p17e');	
                }
                nama_surveyURI = encodeURI(nama_survey);
                // tipe_surveyURI = encodeURI('p17');
                tipe_wilayahURI = encodeURI('<?= $tipe ?>');
                nama_wilayahURI = encodeURI('<?= $nama ?>');
                id_wilayahURI = encodeURI('<?= $id ?>');
                tipe = 'sosmed';
                judul = 'DATA PENGGUNA MEDIA SOSIAL TOKOH BERPENGARUH';
                var table = $('#table_detail').DataTable();
                table.ajax.url("<?= site_url('pilpres/detail_survey_sosmed/')?>"+nama_surveyURI+'/'+tipe_surveyURI+'/'+tipe_wilayahURI+'/'+nama_wilayahURI+'/'+id_wilayahURI).load();
				$("#exampleModalLabel").text("DATA PENGGUNA MEDIA SOSIAL TOKOH BERPENGARUH");	
            	$("#modal_detail").modal('show');
            }
        }],
      	"valueField": "litres",
      	"colorField": "color",
      	"titleField": "country",
      	"export": {
	    	"enabled": true,
			"divId":"exportdivSosmedDetail"
	  	}
    });
	
	var vid = '<?= $id ?>';
	var vnama = '<?= $nama ?>'; 
	var vtype = '<?= $tipe ?>';
	
	var edit = function() {
	  	$('.click2edit').summernote({focus: true,airMode: true,placeholder: 'write note here...'});
		$("#edit").hide();
		$("#save").show();
	};
  	$('.click2edit').summernote('destroy');
	var save = function() {
	 	// var markup = $('.click2edit').summernote('code');
	  	var markup = $($('.click2edit').summernote('code')).text();
		

	  	$('.click2edit').summernote('destroy');
		$("#edit").show();
		$("#save").hide();
		$(".click2edit").summernote("insertText", " ");
		var vtext = markup;
		notes(vid,vnama,vtype,vtext,'save');
	};
	
	notes(vid,vnama,vtype,'','get');  
	
	function notes(vid,vnama,vtype,vtext,vmethod){
		
		$.ajax({
    	   type:'POST',
    	   url:"<?php echo base_url(); ?>pilpres/save_note/",
		   dataType:'json',
           data: {id:vid,nama:vnama,tipe:vtipe,vtext:vtext,method:vmethod},
    	   success:function(msg){
			console.log(msg);
				if(msg){
					
					if(vmethod == "get"){
						$(".click2edit").summernote("insertText", msg);
					}
					$('.click2edit').summernote('destroy');
				}
		   },
    	   error: function(result)
    	   {
    		// $("#"+tab).html("Error");
				alert('Error : '+result);
    	   },
    	   fail:(function(status) {
				alert('Fail : '+status);
    	   }),
    	   beforeSend:function(d){
		   
		   }

    	});
		
	}
</script>

<script type="text/javascript">
	<?php 
	    if($nama == 'NASIONAL'){
	        $tipe = '';
	    }
		
		
		
	?> 
	var sedia = '<div style="width:100%;padding-bottom: 70px; padding-top: 70px;font-size: 25px; color: #c9c9c9; font-weight: bold;">\
					  <div align="center">DATA TIDAK TERSEDIA</div>\
				  </div>';
	if(<?=$total_pendidikan?> == 0){
		$("#pie_pendidikan").html(sedia);
	}
	if(<?=$total_agama?> == 0){
		$("#pie_agama").html(sedia);
	}
	if(<?=$total_suku?> == 0){
		$("#pie_suku").html(sedia);
	}
	if(<?=$total_pengeluaran?> == 0){
		$("#pie_pengeluaran").html(sedia);
	}
	if(<?=$total_masalah_pokok?> == 0){
		$("#graph_p1").html(sedia);
	}
	if(<?=$total_persoalan?> == 0){
		$("#graph_p2").html(sedia);
	}
	if(<?=$total_gubernur?> == 0){
		$("#gubernur").html(sedia);
	}
	if(<?=$total_organisasi?> == 0){
		$("#graph_organisasi").html(sedia);
	}
	if(<?=$total_media_sosial?> == 0){
		$("#divtotal_media_sosial").html(sedia);
	}
	if(<?=$total_partai_pilihan?> == 0){
		$("#div_partai_14").html(sedia);
	}
	if(<?=$total_partai_pilihan_2019?> == 0){
		$("#div_partai_19").html(sedia);
	}
	
	
	 
	
	
	
	
	document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
	document.getElementById("tipe_wilayah").innerHTML = "<?=strtoupper($tipe)?>";
	document.getElementById("logo_wilayah").src = url+"<?=$logo?>";
</script>