<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">DATA PEMILIH TETAP | DPT</h6>
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_dpt','Data Pemilih Tetap');" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class=" centered ">
                        <div class="value"><?= number_format($jum_dpt) ?></div>
                    </div>

                </a>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">ASUMSI SUARA SAH</h6>
                <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($asumsi_suara_sah)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TARGET SUARA</h6>
                 <div class="element-box el-tablo"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($target_suara)?></div>
                </div>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">POTENSI SUARA</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=$jml_perolehan?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row" id="list_paslon">
            <? if($paslon){
                echo $paslon;
            }else{
                echo '<div style="width:100%;padding-bottom: 70px; padding-top: 70px;font-size: 25px; color: #c9c9c9; font-weight: bold;">
                          <div align="center">DATA TIDAK TERSEDIA</div>
                      </div>';
            }
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA PRABOWO 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?= number_format($prabowo) ?></div>
                </div>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">SUARA JOKOWI 2014</h6>
                <div class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">
                    <div class="value"><?=number_format($jokowi)?></div>
                </div>
            </div>
            <!-- Survei -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">JUMLAH RELAWAN</h6>
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_guraklih','Detail Guraklih');" style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                    <div class="centered ">
                        <div class="value" ><?=$jum_guraklih?></div>

                </div>
                </a>
            </div>
            <!-- Guraklih -->
            <div class="col-md-3">
                <h6 style="margin-bottom: 5px;">TOKOH BERPENGARUH</h6>
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_ketokohan','Data Influencer');"  style="padding-left: 3px;padding-right: 3px;padding-bottom: 10px;padding-top: 10px;text-align:center;">

                        <div class="centered ">
                            <div class="value"><?=$jum_ketokohan?></div>
                        </div>

                </a>
            </div>
        </div>
    </div>
</div>

<div id="modal_profile" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal_profile_title">

            </h5>
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
          </div>
          <div class="modal-body" id="modal_profile_body">
            <div class="row">
                <div class="col-md-6">ini pie jens kelamin</div>
                <div class="col-md-6"> ini pie usia</div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
          </div>
        </div>
    </div>
</div>

<script>
<?php
if ($id == 0) {
    $tipe = '';
}
?>
var url = "<?=base_url('img/logo_provinsi/')?>";
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=strtoupper($tipe)?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";

function load_modal_profile(modal,judul){
	console.log(modal);
	$("#modal_profile_body").empty();
    	$.ajax({
    	   type:'POST',
    	   url:"<?php echo base_url(); ?>pilpres/load_modal_profile/",
    	   //data: "modal_name="+modal,
		   data: {modal_name:modal,id:vid,nama:vnama,tipe:vtipe,logo:vlogo},
    	   success:function(msg){
			console.log(msg);
    		$("#modal_profile_body").html(msg);
			$("#modal_profile_title").text(judul);
            // $("#modal_profile_title").addClass("btn-lg btn-danger");
    	   },
    	   error: function(result)
    	   {
    		 $("#modal_profile_body").html("Error");
    	   },
    	   fail:(function(status) {
    		 $("#modal_profile_body").html("Fail");
    	   }),
    	   beforeSend:function(d){
    		//$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img height='25' width='120' src='<?php echo base_url(); ?>img/ajax-loader.gif' /></strong></center>");
    		$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets2/spinner.png'></strong></center>");

    	   }

    	});
}
</script>