<div class="row">
	<div class="col-md-6">
		<div id="jenis_kelamin" style="width: 100%; height: 400px;" ></div>
	</div>
	<div class="col-md-6">
		<div id="usia" style="width: 100%; height: 400px;" ></div>
	</div>
</div>

<script type="text/javascript">
AmCharts.makeChart("jenis_kelamin",
		{
			"type": "pie",
			// "pullOutRadius" : "2%",
			// "pulledField": 'pulled',
			// "angle": 7.2,
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"labelRadius": 10,
			// "outlineThickness": 0,
			"titleField": "category",
			"valueField": "column-1",
			"fontSize": 12,
			// "handDrawScatter": 1,
			"theme": "light",
			// "colors": [
   //          "#73d2de",
   //          "#ffbc42"
   //      ],
			"allLabels": [],
			"balloon": {},
			"titles": [{
				"id": "Jenis Kelamin",
				"text": "Jenis Kelamin"
			}],
			"colors": [
				"#A6A6A6",
				"#F69200"
			],
			"dataProvider": [
				{
					"category": "Pria",
					// "pulled":false,
					"column-1": <?=$jum_jeniskelamin['laki'];?>
				},
				{
					"category": "Wanita",
					// "pulled":false,
					"column-1": <?=$jum_jeniskelamin['wanita'];?>
				}
			]
		}
	);

AmCharts.makeChart("usia",
				{
					"type": "pie",
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"labelRadius": 0,
					"titleField": "country",
					"valueField": "litres",
					"fontSize": 12,
					"theme": "light",
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": [
						{
							"country": "17-25",
							"litres":  <?=$jum_usia['jml_1'];?>
						},
						{
							"country": "26-35",
							"litres": <?=$jum_usia['jml_2'];?>
						},
						{
							"country": "36-45",
							"litres": <?=$jum_usia['jml_3'];?>
						},
						{
							"country": "46-55",
							"litres": <?=$jum_usia['jml_4'];?>
						},
						{
							"country": ">56",
							"litres": <?=$jum_usia['jml_5'];?>
						}
					],
					"titles": [{
						"id": "Kategori Usia",
						"text": "Kategori Usia"
					}]
				}
			);
</script>