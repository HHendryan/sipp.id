<div class="row">
   <div class="col-md-6">

   		<div id="ketokohan_pie" style="width: 100%; height: 300px;" ></div>
        <div align="center">
            <div class="el-legend">
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #73d2de;"></div>
                    <div class="legend-value">Formal : <?= number_format($jum_ketokohan_group['jum_tangible']); ?></div>
                </div>
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #ffbc42;"></div>
                    <div class="legend-value">Informal : <?= number_format($jum_ketokohan_group['jum_intangible']); ?></div>
                </div>
				 <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #000;"></div>
                    <div class="legend-value">Jumlah : <?= number_format($jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible']); ?></div>
                </div>
               
            </div>            
        </div>
   </div>
  <div class="col-md-4">
<h5></h5>

<table width="100%">
<?

foreach ($jum_ketokohan_partai as $key => $value) {

	echo '<tr>
		<td width="5%">
			<div class="fotocalon">
                <img class=" img-fluid " style="width: 45px; height: 45px"  src="img/logo_parpol/'.$value->picture.'">
            </div>
		</td>
		<td width="10%">
		    <div class="os-progress-bar primary" >
		        <div class="bar-labels">
		            <div class="bar-label-left"><span class="bigger">'.$value->alias.'</span></div>
		            <div class="bar-label-right"><span class="info">'.$value->jumlah.'%</span></div>
		        </div>
		        <div class="progress">
		            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="'.$value->jumlah.'" class="progress-bar" role="progressbar" style="width:'.$value->jumlah.'%;background-color:'.$value->color.';"><span class="bigger">'.$value->jumlah.'%</span></div>
		        </div>
		    </div>
		</td>
	</tr>';
}

?>	

</table>
</div>
</div>


<script type="text/javascript">
AmCharts.makeChart("ketokohan_pie",
		{
			"type": "pie",
			// "angle": 7.2,
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			// "depth3D": 20,
			// "innerRadius": 0,
			// "labelRadius": 0,
			// "outlineThickness": 0,
			"titleField": "category",
			"valueField": "column-1",
			"fontSize": 12,
			// "handDrawScatter": 1,
			"theme": "light",
			// "allLabels": [],
			// "balloon": {},
			// "legend": {
			// 	"enabled": false,
			// 	"align": "center",
			// 	"labelWidth": 1,
			// 	"markerType": "circle",
			// 	"maxColumns": 8,
			// 	"tabIndex": 0,
			// 	"valueWidth": 20
			// },
			"titles": [{
				"id": "Influencer",
				"text": "Influencer"
			}],
			"colors": [
				"#73d2de",
				"#ffbc42"
			],
			"dataProvider": [
				{
					"category": "Formal",
					"column-1": "<?=$jum_ketokohan_group['jum_tangible']?>"
				},
				{
					"category": "Informal",
					"column-1": "<?=$jum_ketokohan_group['jum_intangible']?>"
				}
			]
		}
	);
	

</script>