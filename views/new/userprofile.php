<div class="container" style="padding-top: 30px">
    <div class="row">
      <div class="col-md-3 projects-list">
        <div class="project-box">
          <div class="project-head">
            <img src="<?=base_url();?>assets2/spinner.png" style='width: 100%' class='rounded-circle'>
          </div>
        </div>
      </div>
        <div class="col-md-6 projects-list">
            <div class="project-box">
                <div class="project-head">
                    USER PROFILE
                </div>
                <div style="padding: 40px">

                  <form>
                    <div class="form-group">
                      <label for="username">Username</label>
                      <input type="text" name="username" class="form-control" id="username" aria-describedby="username" >
                      <small id="emailHelp" class="form-text text-muted">Masukan username untuk Login</small>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      <small id="emailHelp" class="form-text text-muted">Masukan password untuk login</small>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Lengkap</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap anda.</small>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      <small id="emailHelp" class="form-text text-muted">Masukan alamat Email anda.</small>
                    </div>


                    <div class="form-group">
                      <label for="exampleInputEmail1">No Telpon</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      <small id="emailHelp" class="form-text text-muted">Masukan No Telpon anda</small>
                    </div>

                    <div class="form-group">
                      <label for="exampleFormControlFile1">Photo</label>
                      <input type="file" class="form-control-file" id="exampleFormControlFile1">
                    </div>

                    <button class="btn btn-outline-danger btn-block" type="submit">Submit</button>      
                  </form>           

                </div>
            </div>
        </div>
    </div>
</div>

