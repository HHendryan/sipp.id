<div class="row">
    <div class="col-lg-4">
      <!--START - MESSAGE ALERT-->
      <div class="alert alert-warning borderless">
        <h5 class="alert-heading">
          INFORMASI WILAYAH
        </h5>
        <?php if ($tipe=='kabupaten' or $tipe == 'kecamatan' or $tipe == 'kelurahan') { ?>
            DATA TIDAK TERSEDIA
        <?php }else{ ?>
            <table width="100%" class="infoprovinsi">
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Luas Wilayah</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->luas_wilayah?> km<sup>2</sup></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Jumlah Kabupaten</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->jumlah_kabupaten?></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Jumlah Kota</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->jumlah_kota?></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Jumlah Kecamatan</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->jumlah_kecamatan?></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Jumlah Desa</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->jumlah_desa?></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Jumlah Penduduk</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->jumlah_penduduk?></span></td>
                </tr>
                <tr style="text-align: left;">
                    <td><span class="daft-propisi">Persentase Kepadatan</span></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><span class="daft-propjudul"><?=$informasi->persentase_kepadatan?></span></td>
                </tr>
            </table>
            <br>
        <?php } ?>
      </div>
      <!--END - MESSAGE ALERT-->
    </div>
    <!-- <div class="col-md-4">
        <a class="element-box el-tablo" style="padding-left: 3px;padding-right: 3px;text-align:center;">
            <div class="label">INFORMASI WILAYAH</div><br>
            <table width="100%" class="infoprovinsi">
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Luas Wilayah</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->luas_wilayah?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Jumlah Kabupaten</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->jumlah_kabupaten?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Jumlah Kota</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->jumlah_kota?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Jumlah Kecamatan</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->jumlah_kecamatan?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Jumlah Desa</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->jumlah_desa?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Jumlah Penduduk</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->jumlah_penduduk?></span></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td><span class="daft-propisi">&nbsp;&nbsp;Persentase Kepadatan</span></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td><span class="daft-propjudul"><?=$informasi->persentase_kepadatan?></span></td>
                    </tr>
                </table>
        </a>
    </div> -->
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_dpt','Data Pemilih Tetap');" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                    <div class=" centered ">
                        <div class="label">Data Pemilih Tetap | DPT</div>
                        <div class="value" id="value_dpt"><?= number_format($jum_dpt) ?></div>
                    </div>

                </a>
            </div>
            <!-- Ketokohan -->
            <div class="col-md-6">
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_ketokohan','Data Influencer');" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                        <div class="centered ">
                            <div class="label">Influencer | Tokoh Berpengaruh</div>
                            <div class="value" id="value_ketokohan"><?=$jum_ketokohan?></div>
                        </div>

                </a>
            </div>
            <!-- Survei -->
            <div class="col-md-6">
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_survei','Survei Key Opinion Leader');" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                    <div class="centered ">
                        <div class="label">Survei</div>
                        <div class="value" id="value_survei"><?=number_format($jum_survei)?></div>

                </div>
                </a>
            </div>
            <!-- Guraklih -->
            <div class="col-md-6">
                <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_guraklih','Detail Guraklih');" style="padding-left: 3px;padding-right: 3px;text-align:center;">

                        <div class="centered ">
                            <div class="label">Guraklih</div>
                            <div class="value" id="value_guraklih"><?=$jum_guraklih?></div>
                        </div>

                </a>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <?php if ($id == 0) {?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-12" >
                <div class="element-wrapper" >
                    <h6 class="element-header">SURVEI PILPRES 2019</h6>
                    <div class="element-box" style="min-height: 430px">
                        <div class="row">
                            <div class="col-md-6">
                                <table style="width: 100%">
                                    <?php 
                                    $total = 0;
                                      foreach ($survey_pilpres_19 as $key => $value) {
                                          $total = $total + $value->total;
                                      }
                                      $total_pilpres19 = $total;
                                      ?>
                                <?php $i = 0;$pilres2 = '';foreach ($survey_pilpres_19 as $key => $value) {if ($i < 5) {?>
                                    <tr>
                                        <td>
                                            <div class="fotocalon" style="text-align:center">
                                                <img class="rounded-circle img-fluid" src="<?=base_url();?>/img/paslon/<?=$value->img_paslon?>" height='40' width="40">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="os-progress-bar danger">
                                                <div class="bar-labels">
                                                    <div class="bar-label-left"><span class="bigger" ><?php echo strtoupper($value->nama); ?></span></div>
                                                    <div class="bar-label-right"><span class="info bigger" style="font-weight: bold;"><?=$value->jumlah?> %</span></div>
                                                </div>  
                                                <div class="bar-level-1" style="width: 100%">
                                                    <div class="bar-level-2" style="width: <?=$value->jumlah?>%">
                                                        <div class="bar-level-3" style="width: 100%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php $i++;} else {
                                $pilres2 .= '<tr>
                                                    <td>
                                                      <div class="fotocalon" style="text-align:center">
                                                          <img class="rounded-circle img-fluid" src=img/paslon/' . $value->img_paslon . ' height="40" width="40">
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="os-progress-bar danger">
                                                        <div class="bar-labels">
                                                          <div class="bar-label-left"><span class="bigger" >' . strtoupper($value->nama)  . '</span></div>
                                                          <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;">' . $value->jumlah . '%</span></div>
                                                        </div>
                                                        <div class="bar-level-1" style="width: 100%">
                                                          <div class="bar-level-2" style="width:' . $value->jumlah . '%">
                                                            <div class="bar-level-3" style="width: 100%"></div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      </td>
                                                  </tr>';
                                }
                                }?>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table style="width: 100%">
                                    <?=$pilres2;?>
                                </table>
                            </div>
                            <div class="col-md-12">
                            <div class="text-center btn-helper">
                                <span class="btn btn-outline-danger" style="font-weight:bold; color: #000;border-color: #000">N:  <?=number_format($total_pilpres19);?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else {?>
    <div class="col-md-12">
        <div class="row" id="list_paslon">
            <? if($paslon){
				echo $paslon;
			}else{
				echo '<div style="width:100%;padding-bottom: 70px; padding-top: 70px;font-size: 25px; color: #c9c9c9; font-weight: bold;">
						  <div align="center">DATA TIDAK TERSEDIA</div>
					  </div>';
			}
			?>
        </div>
    </div>
    <?php }?>
</div>
<div id="modal_profile" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal_profile_title">

            </h5>
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
          </div>
          <div class="modal-body" id="modal_profile_body">
            <div class="row">
                <div class="col-md-6">ini pie jens kelamin</div>
                <div class="col-md-6"> ini pie usia</div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
          </div>
        </div>
    </div>
</div>

<script>
<?php
if ($id == 0) {
    $tipe = '';
}
?>
var url = "<?=base_url('img/logo_provinsi/')?>";
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=strtoupper($tipe)?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";

function load_modal_profile(modal,judul){
	console.log(modal);
	$("#modal_profile_body").empty();
    	$.ajax({
    	   type:'POST',
    	   url:"<?php echo base_url(); ?>dashboard/load_modal_profile/",
    	   //data: "modal_name="+modal,
		   data: {modal_name:modal,id:vid,nama:vnama,tipe:vtipe,logo:vlogo},
    	   success:function(msg){
			console.log(msg);
    		$("#modal_profile_body").html(msg);
			$("#modal_profile_title").text(judul);
            // $("#modal_profile_title").addClass("btn-lg btn-danger");
    	   },
    	   error: function(result)
    	   {
    		 $("#modal_profile_body").html("Error");
    	   },
    	   fail:(function(status) {
    		 $("#modal_profile_body").html("Fail");
    	   }),
    	   beforeSend:function(d){
    		//$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img height='25' width='120' src='<?php echo base_url(); ?>img/ajax-loader.gif' /></strong></center>");
    		$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets2/spinner.png'></strong></center>");

    	   }

    	});
}
</script>