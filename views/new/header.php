<!DOCTYPE html>
<html>

<head>
    <title>SIPP</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="<?=base_url();?>assets2/favicon.png" rel="shortcut icon">
    <link href="<?=base_url();?>assets2/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- <link href="<?=base_url();?>assets2/../fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet" type="text/css">-->
    <link href="<?=base_url();?>assets2/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <!-- <link href="<?=base_url();?>assets2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?=base_url();?>assets2/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/css/main4a76.css?version=4.3.0" rel="stylesheet">
    <link href="<?=base_url();?>assets2/css/dafters.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>bower_components/ammap/ammap.css" type="text/css">
	<link href="<?=base_url()?>bower_components/amcharts/plugins/export/export.css" type="text/css" media="all" rel="stylesheet"/>


</head>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-dark color-style-bright sub-menu-color-bright menu-position-top menu-layout-compact sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="#">
       <!--      <div class="logo-element"></div>
            <div class="logo-label">Clean Admin</div> -->

<img  style="width: 100px; margin-top: -30px; margin-bottom: -20px;" src="<?=base_url()?>img/logo-teknopol-w.png">

        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w"><img alt="" src="<?=base_url();?>img/<?=$profile_pic?>"></div>
            <div class="logged-user-info-w">
                <div class="logged-user-name"><?=$username?></div>
                <div class="logged-user-role"><?=$userlevel?></div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w"><img alt="" src="<?=base_url();?>img/<?=$profile_pic?>"></div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name"><?=$username?></div>
                        <div class="logged-user-role"><?=$userlevel?></div>
                    </div>
                </div>
                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                <ul>
                    <li><a href="<?=base_url();?>modal/profile"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile</span></a></li>
                </ul>
				<ul>
                    <li><a href="<?=base_url();?>auth/logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="main-menu">
        <li class="sub-header"><span>Layouts</span></li>

        <li class="has-sub-menu">
            <a href="<?=base_url();?>home">
                <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares-22"></div>
                </div>
                <span>Home</span>
            </a>
        </li>
        <li class="has-sub-menu">
            <a href="<?=base_url();?>dashboard">
                <div class="icon-w">
                    <div class="os-icon os-icon-bar-chart-stats-up"></div>
                </div>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="has-sub-menu">
            <a href="<?=base_url();?>about">
                <div class="">
                    <img class="rounded-circle img-fluid" src="<?=base_url();?>img/avatar1.jpg" height="25" width="25">
                </div>
                <span>Tentang PDI Perjuangan</span>
            </a>
        </li>
    </ul>
</div>


