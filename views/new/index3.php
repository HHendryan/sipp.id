<!DOCTYPE html>
<html>

<head>
    <title>SIPP</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="<?=base_url();?>assets2/favicon.png" rel="shortcut icon">
    <link href="<?=base_url();?>assets2/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?=base_url();?>assets2/../fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets2/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?=base_url();?>assets2/css/main4a76.css?version=4.3.0" rel="stylesheet">
    <link href="<?=base_url();?>assets2/css/dafters.css" rel="stylesheet">
</head>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-dark color-style-bright sub-menu-color-bright menu-position-top menu-layout-compact sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="#">
       <!--      <div class="logo-element"></div>
            <div class="logo-label">Clean Admin</div> -->

<img  style="width: 100px; margin-top: -30px; margin-bottom: -20px;" src="<?=base_url()?>img/logo-teknopol-w.png">

        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w"><img alt="" src="<?=base_url();?>img/avatar1.jpg"></div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">Maria Gomez</div>
                <div class="logged-user-role">Administrator</div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w"><img alt="" src="<?=base_url();?>img/avatar1.jpg"></div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">Maria Gomez</div>
                        <div class="logged-user-role">Administrator</div>
                    </div>
                </div>
                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                <ul>
                    <li><a href="#"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="main-menu">
        <li class="sub-header"><span>Layouts</span></li>
   
        <li class="has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares-22"></div>
                </div>
                <span>Home</span>
            </a>
        </li>
        <li class="has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-bar-chart-stats-up"></div>
                </div>
                <span>Influencer</span>
            </a>
        </li>
        <li class="has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-mail-18"></div>
                </div>
                <span>Navigation</span>
            </a>
        </li>
        <li class="has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle"></div>
                </div>
                <span>Tentang PDI - P</span>
            </a>
        </li>

   

    </ul>
  
</div>




<div class="content-w">


    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="index.html">Products</a></li>
        <li class="breadcrumb-item"><span>Laptop with retina screen</span></li>
    </ul>

    <div class="content-i">
        <div class="content-box">
            <div class="row">
                <div class="col-sm-12">
                    <div class="element-wrapper">
                        <h6 class="element-header">Dashboard Box</h6>
                        <div class="element-box">
                            <div class="element-info">
                                <div class="row align-items-center">
                                    <div class="col-sm-8">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon">
                                                <div class="os-icon os-icon-wallet-loaded"></div>
                                            </div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">Sales Statistics</h5>
                                                <div class="element-inner-desc">Discharge best employed your phase each the of shine. Be met even.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="element-search">
                                            <input placeholder="Type to search for products..." type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-sm-6 b-r b-b">
                                            <div class="el-tablo centered padded">
                                                <div class="value">3814</div>
                                                <div class="label">Products Sold</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 b-b">
                                            <div class="el-tablo centered padded">
                                                <div class="value">47.5K</div>
                                                <div class="label">Followers</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 b-r">
                                            <div class="el-tablo centered padded">
                                                <div class="value">$95</div>
                                                <div class="label">Daily Earnings</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="el-tablo centered padded">
                                                <div class="value">12</div>
                                                <div class="label">Products</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="padded b-l b-r">
                                        <div class="element-info-with-icon smaller">
                                            <div class="element-info-icon">
                                                <div class="os-icon os-icon-bar-chart-stats-up"></div>
                                            </div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">Monthly Revenue</h5>
                                                <div class="element-inner-desc">Calculated every month</div>
                                            </div>
                                        </div>
                                        <div class="os-progress-bar primary">
                                            <div class="bar-labels">
                                                <div class="bar-label-left"><span>Accessories</span><span class="positive">+10</span></div>
                                                <div class="bar-label-right"><span class="info">72/100</span></div>
                                            </div>
                                            <div class="bar-level-1" style="width: 100%">
                                                <div class="bar-level-2" style="width: 60%">
                                                    <div class="bar-level-3" style="width: 20%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="os-progress-bar primary">
                                            <div class="bar-labels">
                                                <div class="bar-label-left"><span>Shoe Sales</span><span class="negative">-5</span></div>
                                                <div class="bar-label-right"><span class="info">62/100</span></div>
                                            </div>
                                            <div class="bar-level-1" style="width: 100%">
                                                <div class="bar-level-2" style="width: 40%">
                                                    <div class="bar-level-3" style="width: 10%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="os-progress-bar primary">
                                            <div class="bar-labels">
                                                <div class="bar-label-left"><span>New Customers</span><span class="positive">+12</span></div>
                                                <div class="bar-label-right"><span class="info">78/100</span></div>
                                            </div>
                                            <div class="bar-level-1" style="width: 100%">
                                                <div class="bar-level-2" style="width: 80%">
                                                    <div class="bar-level-3" style="width: 50%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="padded">
                                        <div class="el-tablo bigger">
                                            <div class="value">245</div>
                                            <div class="trending trending-up"><span>12%</span><i class="os-icon os-icon-arrow-up2"></i></div>
                                            <div class="label">Products Sold</div>
                                        </div>
                                        <div class="el-chart-w">
                                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                                </div>
                                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                                </div>
                                            </div>
                                            <canvas class="chartjs-render-monitor" style="display: block; width: 323px; height: 107px;" height="107" id="liteLineChart" width="323"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="element-wrapper">
                        <h6 class="element-header">New Orders</h6>
                        <div class="element-box">
                            <div class="table-responsive">
                                <table class="table table-lightborder">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Products</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="nowrap">John Mayers</td>
                                            <td>
                                                <div class="cell-image-list">
                                                    <div class="cell-img" style="background-image: url(img/portfolio9.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio2.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio12.jpg)"></div>
                                                    <div class="cell-img-more">+ 5 more</div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="text-right">$354</td>
                                        </tr>
                                        <tr>
                                            <td class="nowrap">Kelly Brans</td>
                                            <td>
                                                <div class="cell-image-list">
                                                    <div class="cell-img" style="background-image: url(img/portfolio14.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio8.jpg)"></div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="text-right">$94</td>
                                        </tr>
                                        <tr>
                                            <td class="nowrap">Tim Howard</td>
                                            <td>
                                                <div class="cell-image-list">
                                                    <div class="cell-img" style="background-image: url(img/portfolio16.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio14.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio5.jpg)"></div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="text-right">$156</td>
                                        </tr>
                                        <tr>
                                            <td class="nowrap">Joe Trulli</td>
                                            <td>
                                                <div class="cell-image-list">
                                                    <div class="cell-img" style="background-image: url(img/portfolio1.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio5.jpg)"></div>
                                                    <div class="cell-img" style="background-image: url(img/portfolio6.jpg)"></div>
                                                    <div class="cell-img-more">+ 2 more</div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="text-right">$1,120</td>
                                        </tr>
                                        <tr>
                                            <td class="nowrap">Jerry Lingard</td>
                                            <td>
                                                <div class="cell-image-list">
                                                    <div class="cell-img" style="background-image: url(img/portfolio9.jpg)"></div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="text-right">$856</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="element-wrapper">
                        <h6 class="element-header">Unique Visitors Graph</h6>
                        <div class="element-box">
                            <div class="os-tabs-w">
                                <div class="os-tabs-controls">
                                    <ul class="nav nav-tabs smaller">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview">Overview</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_sales">Sales</a></li>
                                    </ul>
                                    <ul class="nav nav-pills smaller d-none d-md-flex">
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Today</a></li>
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">7 Days</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">14 Days</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Last Month</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_overview">
                                        <div class="el-tablo bigger">
                                            <div class="label">Unique Visitors</div>
                                            <div class="value">12,537</div>
                                        </div>
                                        <div class="el-chart-w">
                                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                                </div>
                                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                                </div>
                                            </div>
                                            <canvas class="chartjs-render-monitor" style="display: block; width: 563px; height: 187px;" height="187" id="lineChart" width="563"></canvas>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_sales"></div>
                                    <div class="tab-pane" id="tab_conversion"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="element-wrapper">
                        <h6 class="element-header">Top Selling Today</h6>
                        <div class="element-box">
                            <div class="el-chart-w">
                                <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                    </div>
                                </div>
                                <canvas class="chartjs-render-monitor" style="display: block; width: 234px; height: 234px;" height="234" id="donutChart" width="234"></canvas>
                                <div class="inside-donut-chart-label"><strong>142</strong><span>Total Orders</span></div>
                            </div>
                            <div class="el-legend">
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: #6896f9;"></div>
                                    <div class="legend-value">Processed</div>
                                </div>
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: #85c751;"></div>
                                    <div class="legend-value">Cancelled</div>
                                </div>
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: #d97b70;"></div>
                                    <div class="legend-value">Pending</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="element-wrapper">
                        <h6 class="element-header">Recent Orders</h6>
                        <div class="element-box-tp">
                            <!--------------------
START - Controls Above Table
-------------------->
                            <div class="controls-above-table">
                                <div class="row">
                                    <div class="col-sm-6"><a class="btn btn-sm btn-secondary" href="#">Download CSV</a><a class="btn btn-sm btn-secondary" href="#">Archive</a><a class="btn btn-sm btn-danger" href="#">Delete</a></div>
                                    <div class="col-sm-6">
                                        <form class="form-inline justify-content-sm-end">
                                            <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                            <select class="form-control form-control-sm rounded bright">
                                                <option selected="selected" value="">Select Status</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Active">Active</option>
                                                <option value="Cancelled">Cancelled</option>
                                            </select>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
END - Controls Above Table
------------------          -->
                            <!--------------------
START - Table with actions
------------------  -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-lg table-v2 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </th>
                                            <th>Customer Name</th>
                                            <th>Country</th>
                                            <th>Order Total</th>
                                            <th>Referral</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </td>
                                            <td>John Mayers</td>
                                            <td><img alt="" src="img/flags-icons/us.png" width="25px"></td>
                                            <td class="text-right">$245</td>
                                            <td>Organic</td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="row-actions"><a href="#"><i class="os-icon os-icon-ui-49"></i></a><a href="#"><i class="os-icon os-icon-grid-10"></i></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </td>
                                            <td>Mike Astone</td>
                                            <td><img alt="" src="img/flags-icons/fr.png" width="25px"></td>
                                            <td class="text-right">$154</td>
                                            <td>Organic</td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="row-actions"><a href="#"><i class="os-icon os-icon-ui-49"></i></a><a href="#"><i class="os-icon os-icon-grid-10"></i></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </td>
                                            <td>Kira Knight</td>
                                            <td><img alt="" src="img/flags-icons/us.png" width="25px"></td>
                                            <td class="text-right">$23</td>
                                            <td>Adwords</td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="row-actions"><a href="#"><i class="os-icon os-icon-ui-49"></i></a><a href="#"><i class="os-icon os-icon-grid-10"></i></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </td>
                                            <td>Jessica Bloom</td>
                                            <td><img alt="" src="img/flags-icons/ca.png" width="25px"></td>
                                            <td class="text-right">$112</td>
                                            <td>Organic</td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="row-actions"><a href="#"><i class="os-icon os-icon-ui-49"></i></a><a href="#"><i class="os-icon os-icon-grid-10"></i></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input class="form-control" type="checkbox">
                                            </td>
                                            <td>Gary Lineker</td>
                                            <td><img alt="" src="img/flags-icons/ca.png" width="25px"></td>
                                            <td class="text-right">$64</td>
                                            <td>Organic</td>
                                            <td class="text-center">
                                                <div title="" data-original-title="" class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                            </td>
                                            <td class="row-actions"><a href="#"><i class="os-icon os-icon-ui-49"></i></a><a href="#"><i class="os-icon os-icon-grid-10"></i></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--------------------
END - Table with actions
------------------            -->
                            <!--------------------
START - Controls below table
------------------  -->
                            <div class="controls-below-table">
                                <div class="table-records-info">Showing records 1 - 5</div>
                                <div class="table-records-pages">
                                    <ul>
                                        <li><a href="#">Previous</a></li>
                                        <li><a class="current" href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--------------------
END - Controls below table
-------------------->
                        </div>
                    </div>
                </div>
            </div>
            <!--------------------
START - Color Scheme Toggler
-------------------->
     <!--        <div class="floated-colors-btn second-floated-btn">
                <div class="os-toggler-w">
                    <div class="os-toggler-i">
                        <div class="os-toggler-pill"></div>
                    </div>
                </div><span>Dark </span><span>Colors</span></div> -->
            <!--------------------
END - Color Scheme Toggler
-------------------->
            <!--------------------
START - Demo Customizer
-------------------->
        <!--     <div class="floated-customizer-btn third-floated-btn">
                <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div><span>Customizer</span></div>
            <div class="floated-customizer-panel active">
                <div class="fcp-content">
                    <div class="close-customizer-btn"><i class="os-icon os-icon-x"></i></div>
                    <div class="fcp-group">
                        <div class="fcp-group-header">Menu Settings</div>
                        <div class="fcp-group-contents">
                            <div class="fcp-field">
                                <label for="">Menu Position</label>
                                <select class="menu-position-selector">
                                    <option value="left">Left</option>
                                    <option value="right">Right</option>
                                    <option value="top">Top</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Menu Style</label>
                                <select class="menu-layout-selector">
                                    <option value="compact">Compact</option>
                                    <option value="full">Full</option>
                                    <option value="mini">Mini</option>
                                </select>
                            </div>
                            <div style="" class="fcp-field with-image-selector-w">
                                <label for="">With Image</label>
                                <select class="with-image-selector">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Menu Color</label>
                                <div class="fcp-colors menu-color-selector">
                                    <div class="color-selector menu-color-selector color-bright selected"></div>
                                    <div class="color-selector menu-color-selector color-dark"></div>
                                    <div class="color-selector menu-color-selector color-light"></div>
                                    <div class="color-selector menu-color-selector color-transparent"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fcp-group">
                        <div class="fcp-group-header">Sub Menu</div>
                        <div class="fcp-group-contents">
                            <div class="fcp-field">
                                <label for="">Sub Menu Style</label>
                                <select class="sub-menu-style-selector">
                                    <option value="flyout">Flyout</option>
                                    <option value="inside">Inside/Click</option>
                                    <option value="over">Over</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Sub Menu Color</label>
                                <div class="fcp-colors">
                                    <div class="color-selector sub-menu-color-selector color-bright selected"></div>
                                    <div class="color-selector sub-menu-color-selector color-dark"></div>
                                    <div class="color-selector sub-menu-color-selector color-light"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fcp-group">
                        <div class="fcp-group-header">Other Settings</div>
                        <div class="fcp-group-contents">
                            <div class="fcp-field">
                                <label for="">Full Screen?</label>
                                <select class="full-screen-selector">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Show Top Bar</label>
                                <select class="top-bar-visibility-selector">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Above Menu?</label>
                                <select class="top-bar-above-menu-selector">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="fcp-field">
                                <label for="">Top Bar Color</label>
                                <div class="fcp-colors">
                                    <div class="color-selector top-bar-color-selector color-bright"></div>
                                    <div class="color-selector top-bar-color-selector color-dark"></div>
                                    <div class="color-selector top-bar-color-selector color-light selected"></div>
                                    <div class="color-selector top-bar-color-selector color-transparent"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--------------------
END - Demo Customizer
-------------------->
            <!--------------------
START - Chat Popup Box
-------------------->
          <!--   <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
            <div class="floated-chat-w">
                <div class="floated-chat-i">
                    <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                    <div class="chat-head">
                        <div class="user-w with-status status-green">
                            <div class="user-avatar-w">
                                <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                            </div>
                            <div class="user-name">
                                <h6 class="user-title">John Mayers</h6>
                                <div class="user-role">Account Manager</div>
                            </div>
                        </div>
                    </div>
                    <div data-ps-id="b7cae198-de2c-ac80-3d11-664ad11a47bc" class="chat-messages ps ps--theme_default">
                        <div class="message">
                            <div class="message-content">Hi, how can I help you?</div>
                        </div>
                        <div class="date-break">Mon 10:20am</div>
                        <div class="message">
                            <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                        </div>
                        <div class="message self">
                            <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                        </div>
                        <div style="left: 0px; bottom: 0px;" class="ps__scrollbar-x-rail">
                            <div style="left: 0px; width: 0px;" tabindex="0" class="ps__scrollbar-x"></div>
                        </div>
                        <div style="top: 0px; right: 0px;" class="ps__scrollbar-y-rail">
                            <div style="top: 0px; height: 0px;" tabindex="0" class="ps__scrollbar-y"></div>
                        </div>
                    </div>
                    <div class="chat-controls">
                        <input class="message-input" placeholder="Type your message here..." type="text">
                        <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                    </div>
                </div>
            </div> -->
            <!--------------------
END - Chat Popup Box
-------------------->
        </div>
    </div>
</div>



 
    <script src="<?=base_url();?>assets2/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/moment/moment.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?=base_url();?>assets2/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?=base_url();?>assets2/js/demo_customizer4a76.js?version=4.3.0"></script>
    <script src="<?=base_url();?>assets2/js/main4a76.js?version=4.3.0"></script>

</body>

</html>