
<!DOCTYPE html>
<html>
    <head>
        <title>Teknopol Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/switchery/switchery.min.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
        
        <style type="text/css">
            .middle{
                margin: auto;
            }
        </style>
    </head>
    <body class="full-screen">
  


<div class="content-w">
    <div class="top-menu-secondary">
        <ul>
            <li ><a href="#" class="mainbrand"><img  style="width: 100px; margin-top: -20px; margin-bottom: -20px;" src="<?=base_url()?>img/logo-teknopol-w.png"></a></li>
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Projects</a></li>
            <li><a href="#">Customers</a></li>
            <li><a href="#">Reports</a></li>
        </ul>



        <div class="top-menu-controls">
           

         
            <div class="top-icon top-settings os-dropdown-trigger os-dropdown-center">
      <div class="logged-user-w">
                        <div class="avatar-w">
                            <img alt="" src="http://36.67.51.253/teknopol_dashboard_new/img/avatar1.jpg">
                        </div>
                    </div>
                <div class="os-dropdown">
                    <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>
                    <ul>
                        <li><a href="#"><i class="os-icon os-icon-ui-49"></i><span>Logout</span></a></li>
                    </ul>
                </div>
            </div>

            <div class="logged-user-w">
                <div class="logged-user-i">
                    <div class="avatar-w"><img alt="" src="img/avatar1.jpg"></div>
                    <div class="logged-user-menu">
                        <div class="logged-user-avatar-info">
                            <div class="avatar-w"><img alt="" src="img/avatar1.jpg"></div>
                            <div class="logged-user-info-w">
                                <div class="logged-user-name">Maria Gomez</div>
                                <div class="logged-user-role">Administrator</div>
                            </div>
                        </div>
                        <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                        <ul>
                            <li><a href="apps_email.html"><i class="os-icon os-icon-mail-01"></i><span>Incoming Mail</span></a></li>
                            <li><a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a></li>
                            <li><a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a></li>
                            <li><a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a></li>
                            <li><a href="#"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--------------------
END - User avatar and menu in secondary top menu
-------------------->
        </div>
    </div>
    <div class="content-i">
        <div class="content-box">
           
            <div class="os-tabs-w mx-4">
                <div class="os-tabs-controls">
                    <ul class="nav nav-tabs upper">
                        <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_overview"> Active</a></li>
                        <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_sales"> Overview</a></li>
                        <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_sales"> Closed</a></li>
                        <li class="nav-item"><a aria-expanded="true" class="nav-link active show" data-toggle="tab" href="#tab_sales"> Required</a></li>
                    </ul>
                    <ul class="nav nav-pills smaller hidden-md-down">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> Today</a></li>
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#"> 7 Days</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> 14 Days</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> Last Month</a></li>
                    </ul>
                </div>
            </div>
        
            <div class="row">
                <div class="col-lg-7">
                    <div class="padded-lg">
                
                        <div class="projects-list">
                            <div class="project-box">
                                <div class="project-head">
                                    <div class="project-title">
                                        <h5>Website Redesign</h5></div>
                                    <div class="project-users">
                                        <div class="avatar"><img alt="" src="img/avatar3.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar1.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar5.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar2.jpg"></div>
                                        <div class="more">+ 5 More</div>
                                    </div>
                                </div>
                                <div class="project-info">
                                    <div class="row align-items-center">
                                        <div class="col-sm-5">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Open tasks</div>
                                                        <div class="value">15</div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Contributors</div>
                                                        <div class="value">24</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 offset-sm-2">
                                            <div class="os-progress-bar primary">
                                                <div class="bar-labels">
                                                    <div class="bar-label-left"><span>Progress</span><span class="positive">+10</span></div>
                                                    <div class="bar-label-right"><span class="info">72/100</span></div>
                                                </div>
                                                <div class="bar-level-1" style="width: 100%">
                                                    <div class="bar-level-2" style="width: 72%">
                                                        <div class="bar-level-3" style="width: 36%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-box">
                                <div class="project-head">
                                    <div class="project-title">
                                        <h5>Marketing Text</h5></div>
                                    <div class="project-users">
                                        <div class="avatar"><img alt="" src="img/avatar7.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar6.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar4.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar1.jpg"></div>
                                        <div class="more">+ 5 More</div>
                                    </div>
                                </div>
                                <div class="project-info">
                                    <div class="row align-items-center">
                                        <div class="col-sm-5">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Open tasks</div>
                                                        <div class="value">27</div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Contributors</div>
                                                        <div class="value">12</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 offset-sm-2">
                                            <div class="os-progress-bar primary">
                                                <div class="bar-labels">
                                                    <div class="bar-label-left"><span>Progress</span><span class="positive">+10</span></div>
                                                    <div class="bar-label-right"><span class="info">56/100</span></div>
                                                </div>
                                                <div class="bar-level-1" style="width: 100%">
                                                    <div class="bar-level-2" style="width: 56%">
                                                        <div class="bar-level-3" style="width: 28%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-box">
                                <div class="project-head">
                                    <div class="project-title">
                                        <h5>Sales Directions</h5></div>
                                    <div class="project-users">
                                        <div class="avatar"><img alt="" src="img/avatar6.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar4.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar3.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar7.jpg"></div>
                                        <div class="more">+ 5 More</div>
                                    </div>
                                </div>
                                <div class="project-info">
                                    <div class="row align-items-center">
                                        <div class="col-sm-5">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Open tasks</div>
                                                        <div class="value">45</div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Contributors</div>
                                                        <div class="value">37</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 offset-sm-2">
                                            <div class="os-progress-bar primary">
                                                <div class="bar-labels">
                                                    <div class="bar-label-left"><span>Progress</span><span class="positive">+10</span></div>
                                                    <div class="bar-label-right"><span class="info">32/100</span></div>
                                                </div>
                                                <div class="bar-level-1" style="width: 100%">
                                                    <div class="bar-level-2" style="width: 32%">
                                                        <div class="bar-level-3" style="width: 16%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-box">
                                <div class="project-head">
                                    <div class="project-title">
                                        <h5>New Hiring</h5></div>
                                    <div class="project-users">
                                        <div class="avatar"><img alt="" src="img/avatar2.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar3.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar1.jpg"></div>
                                        <div class="avatar"><img alt="" src="img/avatar5.jpg"></div>
                                        <div class="more">+ 5 More</div>
                                    </div>
                                </div>
                                <div class="project-info">
                                    <div class="row align-items-center">
                                        <div class="col-sm-5">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Open tasks</div>
                                                        <div class="value">32</div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="el-tablo highlight">
                                                        <div class="label">Contributors</div>
                                                        <div class="value">17</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 offset-sm-2">
                                            <div class="os-progress-bar primary">
                                                <div class="bar-labels">
                                                    <div class="bar-label-left"><span>Progress</span><span class="positive">+10</span></div>
                                                    <div class="bar-label-right"><span class="info">85/100</span></div>
                                                </div>
                                                <div class="bar-level-1" style="width: 100%">
                                                    <div class="bar-level-2" style="width: 85%">
                                                        <div class="bar-level-3" style="width: 42%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
               
                    </div>
                </div>
                <div class="col-lg-5 b-l-lg">
                    <div class="padded-lg">
         
                        <div class="element-wrapper">
                            <div class="element-actions">
                                <form class="form-inline justify-content-sm-end">
                                    <select class="form-control form-control-sm rounded">
                                        <option value="Pending">Today</option>
                                        <option value="Active">Last Week </option>
                                        <option value="Cancelled">Last 30 Days</option>
                                    </select>
                                </form>
                            </div>
                            <h6 class="element-header">Project Statistics</h6>
                            <div class="element-box">
                                <div class="padded m-b">
                                    <div class="centered-header">
                                        <h6>Period Statistics</h6></div>
                                    <div class="row">
                                        <div class="col-6 b-r b-b">
                                            <div class="el-tablo centered padded-v-big highlight bigger">
                                                <div class="label">Users</div>
                                                <div class="value">24</div>
                                            </div>
                                        </div>
                                        <div class="col-6 b-b">
                                            <div class="el-tablo centered padded-v-big highlight bigger">
                                                <div class="label">Tasks</div>
                                                <div class="value">251</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="padded m-b">
                                    <div class="centered-header">
                                        <h6>Completions</h6></div>
                                    <div class="os-progress-bar primary">
                                        <div class="bar-labels">
                                            <div class="bar-label-left"><span>Progress</span><span class="positive">+12</span></div>
                                            <div class="bar-label-right"><span class="info">72/100</span></div>
                                        </div>
                                        <div class="bar-level-1" style="width: 100%">
                                            <div class="bar-level-2" style="width: 72%">
                                                <div class="bar-level-3" style="width: 25%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="os-progress-bar primary">
                                        <div class="bar-labels">
                                            <div class="bar-label-left"><span>Progress</span><span class="negative">-5</span></div>
                                            <div class="bar-label-right"><span class="info">54/100</span></div>
                                        </div>
                                        <div class="bar-level-1" style="width: 100%">
                                            <div class="bar-level-2" style="width: 54%">
                                                <div class="bar-level-3" style="width: 25%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="os-progress-bar primary">
                                        <div class="bar-labels">
                                            <div class="bar-label-left"><span>Progress</span><span class="positive">+5</span></div>
                                            <div class="bar-label-right"><span class="info">86/100</span></div>
                                        </div>
                                        <div class="bar-level-1" style="width: 100%">
                                            <div class="bar-level-2" style="width: 86%">
                                                <div class="bar-level-3" style="width: 25%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="padded">
                                    <div class="centered-header">
                                        <h6>Tasks Closed</h6></div>
                                    <div class="el-chart-w">
                                        <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                            <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                            </div>
                                        </div>
                                        <canvas class="chartjs-render-monitor" style="display: block; width: 271px; height: 117px;" height="117" id="liteLineChart" width="271"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
            <!--------------------
START - Chat Popup Box
-------------------->
          <!--   <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
            <div class="floated-chat-w">
                <div class="floated-chat-i">
                    <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                    <div class="chat-head">
                        <div class="user-w with-status status-green">
                            <div class="user-avatar-w">
                                <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                            </div>
                            <div class="user-name">
                                <h6 class="user-title">John Mayers</h6>
                                <div class="user-role">Account Manager</div>
                            </div>
                        </div>
                    </div>
                    <div data-ps-id="d90b72b1-3872-f582-81a2-618cf6f3043a" class="chat-messages ps ps--theme_default">
                        <div class="message">
                            <div class="message-content">Hi, how can I help you?</div>
                        </div>
                        <div class="date-break">Mon 10:20am</div>
                        <div class="message">
                            <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                        </div>
                        <div class="message self">
                            <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                        </div>
                        <div style="left: 0px; bottom: 0px;" class="ps__scrollbar-x-rail">
                            <div style="left: 0px; width: 0px;" tabindex="0" class="ps__scrollbar-x"></div>
                        </div>
                        <div style="top: 0px; right: 0px;" class="ps__scrollbar-y-rail">
                            <div style="top: 0px; height: 0px;" tabindex="0" class="ps__scrollbar-y"></div>
                        </div>
                    </div>
                    <div class="chat-controls">
                        <input class="message-input" placeholder="Type your message here..." type="text">
                        <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                    </div>
                </div>
            </div> -->
            <!--------------------
END - Chat Popup Box
-------------------->
        </div>
    </div>
</div>





        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>bower_components/switchery/switchery.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>
        <script src="<?=base_url()?>bower_components/amcharts/amcharts.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/serial.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/pie.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/themes/light.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    </body>
</html>
