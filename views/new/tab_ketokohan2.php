<div class="row">
   <div class="col-md-6">

      <div id="ketokohan_pie" style="width: 100%; height: 300px;" ></div>
        <div align="center">
            <div class="el-legend">
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #73d2de;"></div>
                    <div class="legend-value">Formal : <?= number_format($jum_ketokohan_group['jum_tangible']); ?></div>
                </div>
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #ffbc42;"></div>
                    <div class="legend-value">Informal : <?= number_format($jum_ketokohan_group['jum_intangible']); ?></div>
                </div>
         <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #000;"></div>
                    <div class="legend-value">Jumlah : <?= number_format($jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible']); ?></div>
                </div>
               
            </div>            
        </div>
   </div>
   <div class="col-md-6" >
    <div style="font-weight: bold;text-align: center;padding-top: 10px;padding-bottom: 40px;">Kecondongan Terhadap Partai</div>
    <br>
    <div style="padding-right: 20px">
    <?= $jum_ketokohan_partai; ?>
   </div>
   </div>
</div>


<script type="text/javascript">
AmCharts.makeChart("ketokohan_pie",
    {
      "type": "pie",
      "angle": 7.2,
      "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "depth3D": 20,
      "innerRadius": 0,
      "labelRadius": 0,
      "outlineThickness": 0,
      "titleField": "category",
      "valueField": "column-1",
      "fontSize": 12,
      "handDrawScatter": 1,
      "theme": "light",
      "allLabels": [],
      "balloon": {},
      // "legend": {
      //  "enabled": false,
      //  "align": "center",
      //  "labelWidth": 1,
      //  "markerType": "circle",
      //  "maxColumns": 8,
      //  "tabIndex": 0,
      //  "valueWidth": 20
      // },
      "titles": [{
        "id": "Influencer",
        "text": "Influencer"
      }],
      "colors": [
        "#73d2de",
        "#ffbc42"
      ],
      "dataProvider": [
        {
          "category": "Formal",
          "column-1": "58"
        },
        {
          "category": "Informal",
          "column-1": "41"
        }
      ]
    }
  );
  

</script>