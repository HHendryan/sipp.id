<div class="row">

<?php

$score1=0;
$score2=0;
$score3=0;
// $tottangin=$jum_ketokohan_group['jum_tangible']+$jum_ketokohan_group['jum_intangible'];

$pemilihannya=$total_pilih + $total_tidak_pilih;
if ($pemilihannya>1) {
  $munculin1='';
} else {
  $munculin1='style="display:none;"';
  $score1=1;
  // echo'ngga ada';
}


if($jml_pemilih){
  $munculin2='';
} else {
  $munculin2='style="display:none;"';
  $score2=1;
}

if($jml_tidak_memilih){
  $munculin3='';
} else {
  $munculin3='style="display:none;"';
  $score3=1;
}

$scoreall=$score1+$score2+$score3;
// echo $scoreall;

if($scoreall>2){
echo '<div style="width:100%;padding-bottom: 70px; padding-top: 70px;font-size: 25px; color: #c9c9c9; font-weight: bold;">
              <div align="center">DATA TIDAK TERSEDIA</div>
          </div>';
}


?>


<div class="col-md-4" <?=$munculin1?>>
<style>
#pemilih_donat {
	width		: 100%;
	height		: 300px;
	font-size	: 11px;
}
</style>

<script>
var chart =  AmCharts.makeChart("pemilih_donat", {
  "type": "pie",
  "theme": "light",
   "allLabels": [{
    "text": "Total",
    "align": "center",
    "bold": true,
    "y": 130
  }, {
    "text": '<?=number_format($total_pilih + $total_tidak_pilih)?>',
    "align": "center",
    "bold": false,
    "y": 150
  }],
  "dataProvider": [{
    "title": "Memilih",
    "value": <?=$total_pilih?>,
	"color": "#F69200"
  }, {
    "title": "Tidak Memilih",
    "value": <?=$total_tidak_pilih?>,
	"color": "#A6A6A6"
  }],
  "titleField": "title",
  "valueField": "value",
  "colorField": "color",
  "labelRadius": 10,
  "radius": "42%",
  "innerRadius": "60%",
  "labelText": ""
});

</script>

<!-- HTML -->
<div id="pemilih_donat"></div>
        <div align="center">
            <div class="el-legend">
          <!--       <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #000;"></div>
                    <div class="legend-value">Total : <?=number_format($total_pilih + $total_tidak_pilih)?></div>
                </div> -->
                <div style="padding-left: 30px;">
                  <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #F69200;"></div>
                    <div class="legend-value">Jumlah Memilih :  <?=number_format($total_pilih)?></div>
                </div>
                <div class="legend-value-w">
                    <div class="legend-pin" style="background-color: #A6A6A6;"></div>
                    <div class="legend-value">Jumlah Tidak Memilih : <?=number_format($total_tidak_pilih)?></div>
                </div>
                </div>
            </div>
        </div>
</div>
<div class="col-md-4" <?=$munculin2?>>
<h5></h5>

<table width="100%">
<?

foreach ($jml_pemilih as $key => $value) {

	echo '<tr>
		<td width="5%">
			<div class="fotocalon">
                <img class=" img-fluid " style="width: 45px; height: 45px"  src="img/logo_parpol/'.$value->picture.'">
            </div>
		</td>
		<td width="10%">
		    <div class="os-progress-bar primary" >
		        <div class="bar-labels">
		            <div class="bar-label-left"><span class="bigger">'.$value->nama.'</span></div>
		            <div class="bar-label-right"><span class="info">'.$value->jumlah.'%</span></div>
		        </div>
		        <div class="progress">
		            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="'.$value->jumlah.'" class="progress-bar" role="progressbar" style="width:'.$value->jumlah.'%;background-color:'.$value->color.';"><span class="bigger">'.$value->jumlah.'%</span></div>
		        </div>
		    </div>
		</td>
	</tr>';
}

?>

</table>
</div>
<div class="col-md-4" <?=$munculin3?> >
<h5> </h5>
<?
	foreach ($jml_tidak_memilih as $key => $value) {

		echo '<div class="os-progress-bar primary" >
		        <div class="bar-labels">
		            <div class="bar-label-left"><span class="bigger">'.$value->nama.'</span></div>
		            <div class="bar-label-right"><span class="info">'.$value->jumlah.'%</span></div>
		        </div>
		        <div class="progress">
		            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="'.$value->jumlah.'" class="progress-bar" role="progressbar" style="width:'.$value->jumlah.'%;	"><span class="bigger">'.$value->jumlah.'%</span></div>
		        </div>
		    </div>';
	}
?>

</div>
</div>