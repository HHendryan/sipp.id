<div class="">                  
    <form class="form-inline justify-content-sm-left"><br><br><br><br>
        <select class="form-control form-control-sm rounded">
            <option value="Pending">Legislator 2014</option>
            <option value="Active">Daftar Caleg 2014 </option>
            <option value="Cancelled">Daftar Caleg Sementara 2019</option>
			<option value="Cancelled">Daftar Caleg Tetap 2019</option>
        </select>
    </form>
    <div class="row">
        <div class="col-md-8" >
            <div class="element-box daft-legislator">
            <table width='100%'>
                <tbody>
                    <tr>
                        <td rowspan="5">
                        <img class="rounded-circle img-fluid" src="https://www.sipp.biz.id//img/paslon/legislator.png" height="150" width="150">
                        </td>
                        <td>Nama</td>
                        <td>:&nbsp;&nbsp;</td>
                        <td>Asep Dedi Sukmawan</td> 
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>:</td>
                        <td>DPRD Kab. Bandung</td>
                    </tr>
                    <tr>
                        <td>Wilayah</td>
                        <td>:</td>
                        <td>Dapil 1</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>Jl Raya Mekarjati No. 12 Kab. Bandung </td>
                    </tr>
                    <tr>
                        <td>No. HP</td>
                        <td>:</td>
                        <td>08126123123</td>
                    </tr>
                  
                </tbody>
            </table>
            </div>
            <div class="element-box  daft-legislator">
            <table width='100%'>
                <tbody>
                    <tr>
                        <td rowspan="5">
                        <img class="rounded-circle img-fluid" src="https://www.sipp.biz.id//img/paslon/legislator.png" height="150" width="150">
                        </td>
                        <td>Nama</td>
                        <td>:&nbsp;&nbsp;</td>
                        <td>R Yunandar Eka Prawira</td> 
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>:</td>
                        <td>DPRD Provinsi Jawa Barat</td>
                    </tr>
                    <tr>
                        <td>Wilayah</td>
                        <td>:</td>
                        <td>Dapil 1</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>Jl Raya Cicahem Cikutra Kab. Bandung </td>
                    </tr>
                    <tr>
                        <td>No. HP</td>
                        <td>:</td>
                        <td>08123765181</td>
                    </tr>
                  
                </tbody>
            </table>
            </div>
            <div class="element-box  daft-legislator">
            <table width='100%'>
                <tbody>
                    <tr>
                        <td rowspan="5">
                        <img class="rounded-circle img-fluid" src="https://www.sipp.biz.id//img/caleg_dprri_2014/jabar_1/aaaaamdn1ketutsu_pdip.jpg" height="150" width="150">
                        </td>
                        <td>Nama</td>
                        <td>:&nbsp;&nbsp;</td>
                        <td>Ketut Sustiawan</td> 
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>:</td>
                        <td>DPRD RI Jawa Barat</td>
                    </tr>
                    <tr>
                        <td>Wilayah</td>
                        <td>:</td>
                        <td>Dapil 1</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>Jl Cilengka Buah Batu Kab. Bandung </td>
                    </tr>
                    <tr>
                        <td>No. HP</td>
                        <td>:</td>
                        <td>081267827632</td>
                    </tr>
                  
                </tbody>
            </table>
            </div>
            
        </div>
 
    </div>
    <div class="row">
        
        <div class="col-md-12  daft-legislator-caption">
             <div class="daft-space30"></div>
            Berikut adalah perolehan suara Legislator PDI PERJUANGAN di Kec Arjasari
            <div class="daft-space30"></div>
        </div>
        <div class="col-md-12  daft-legislator-table">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <td></td>
                        <td>ASEP DEDI</td>
                        <td>YUNANDAR EKA</td>
                        <td>KETUT</td>
                    </tr>
                    <tr>
                        <td>DESA BATU KARUT</td>
                        <td>1123</td>
                        <td>1323</td>
                        <td>1232</td>
                    </tr>
                    <tr>
                        <td>DESA MANGUNJAYA</td>
                        <td>2311</td>
                        <td>1231</td>
                        <td>1212</td>
                    </tr>
                    <tr>
                        <td>DESA MEKARJAYA</td>
                        <td>1111</td>
                        <td>1231</td>
                        <td>1112</td>
                    </tr>
                    <tr>
                        <td>DESA BAROS</td>
                        <td>1222</td>
                        <td>1222</td>
                        <td>1111</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>