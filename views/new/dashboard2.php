<div class="content-w">

<!--     <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="index.html">Products</a></li>
        <li class="breadcrumb-item"><span>Laptop with retina screen</span></li>
    </ul>
 -->
    <div class="content-i">
        <div class="content-box">
            <div class="row">
                <div class="col-md-3">
                    <div class="element-wrapper" style="padding-bottom: 0px;">
                        <h6 class="element-header">Navigasi Wilayah</h6>
                        <div class="element-box" style="padding-left: 2px;padding-right:2px;overflow: auto;height: 580px;">
                            <div id="tree2">
                                <ul id="treeData" style="display: none;">
                                    <li  data-tipe='provinsi' data-nama="" data-id="0">NASIONAL</li>
                                    <?php foreach ($provinsi as $key => $value) { ?>
                                    <li class="lazy" data-tipe='provinsi' data-nama="<?php echo $value->name?>" data-id="<?php echo $value->id?>"><?php echo $value->name?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="element-wrapper" style="padding-bottom: 0px;">
                        <!-- <h6 class="element-header">Infomasi</h6> -->
                                    <!-- <div class="os-tabs-w mx-4"> -->
                            <div class="os-tabs-controls" style="margin-top: -9px">
                                <ul class="nav nav-tabs upper">
                                    <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_profile"> PROFILE</a></li>
                                    <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_survei"> SURVEI</a></li>
                                    <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_ketokohan"> KETOKOHAN</a></li>
                                    <li class="nav-item"><a aria-expanded="false" class="nav-link" data-toggle="tab" href="#tab_simulasi"> SIMULASI</a></li>
                                </ul>
                              <!--   <ul class="nav nav-pills smaller d-none d-lg-flex">
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> Today</a></li>
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#"> 7 Days</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> 14 Days</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#"> Last Month</a></li>
                                </ul> -->
                                
                            </div>

                 
                            <div class="element-box-tp">
                                <div class="profile-tile">
                                    <a class="" href="users_profile_small.html">
                                        <div class="pt-avatar-w"><img alt="" src="<?=base_url();?>img/avatar1.jpg"></div>
                                        <!-- <div class="pt-user-name">Mark Parson</div> -->
                                    </a>
                                    <div class="profile-tile-meta">
                                        <ul>
                                            <li><span class='daft-title-wil'>Wilayah</span></li>
                                            <li><span class='daft-title-wil'>Jakarta Barat</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>     
                            <div class="tab-content">
                                <div id="tab_profile" class="tab-pane active show"> 
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href="#" data-target=".bd-example-modal-lg" data-toggle="modal">
                                                <div class="element-box">
                                                <div class="el-tablo centered ">
                                                    <div class="label">DPT</div>
                                                    <div class="value">300M</div>
                                                </div>
                                            </div>
                                            </a>

                                            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                      DPT
                                                    </h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">ini pie jens kelamin</div>
                                                        <div class="col-md-6"> ini pie usia</div>
                                                    </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                                                    <!-- <button class="btn btn-primary" type="button"> Save changes</button> -->
                                                  </div>
                                                </div>
                                              </div>
                                            </div>                                            
                                        </div>
                                        <div class="col-md-3">
                                            <a href="" data-target=".bd-example-modal-lgketokohan" data-toggle="modal">
                                                <div class="element-box">
                                                <div class="el-tablo centered ">
                                                    <div class="label">Ketokohan</div>
                                                    <div class="value">3814</div>
                                                </div>
                                            </div>
                                            </a>
                                            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lgketokohan" role="dialog" tabindex="-1">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                      Ketokohan
                                                    </h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                  </div>
                                                  <div class="modal-body">
                                                   <div class="row">
                                                       <div class="col-md-6">Ketokohan</div>
                                                       <div class="col-md-6">Kecondongan Terhadap Partai</div>
                                                   </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                                                    <!-- <button class="btn btn-primary" type="button"> Save changes</button> -->
                                                  </div>
                                                </div>
                                              </div>
                                            </div>                                              
                                        </div>
                                        <div class="col-md-3">
                                            <a href=""  data-target=".bd-example-modal-lgSurvei" data-toggle="modal">
                                                <div class="element-box">
                                                <div class="el-tablo centered ">
                                                    <div class="label">Survei</div>
                                                    <div class="value">3814</div>
                                                </div>
                                            </div>
                                            </a>
                                            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lgSurvei" role="dialog" tabindex="-1">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                      Survei
                                                    </h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-4">Kiri</div>
                                                    <div class="col-md-4">Tengah</div>
                                                    <div class="col-md-4">kanan</div>
                                                    </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button"> Save changes</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <a  data-target=".bd-example-modal-lgGuraklih" data-toggle="modal">
                                                <div class="element-box">
                                                <div class="el-tablo centered ">
                                                    <div class="label">Guraklih</div>
                                                    <div class="value">3814</div>
                                                </div>
                                            </div>
                                            </a>
                                            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lgGuraklih" role="dialog" tabindex="-1">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                     Guraklih
                                                    </h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                  </div>
                                                  <div class="modal-body">
                                                        <div class="row">
                                                            
                                                     <div class="col-md-4">
                                                        <div class="el-tablo centered ">
                                                    <div class="label">Jumlah TPS</div>
                                                    <div class="value">3814</div>
                                                </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="el-tablo centered ">
                                                    <div class="label">Jml Petugas</div>
                                                    <div class="value">3814</div>
                                                </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="el-tablo centered ">
                                                    <div class="label">Target Prospek</div>
                                                    <div class="value">3814</div>
                                                </div>
                                            </div>
                                        </div>
                                            <div style="padding-top: 50px"></div>
                                            <div class="row" >
                                                
                                                    <div class="col-md-4">
                                                        <div class="el-tablo centered ">
                                                    <div class="label">Perolehan</div>
                                                    <div class="value">3814</div>
                                                </div>
                                                    </div>
                                                    <div class="col-md-4"><div class="el-tablo centered ">
                                                    <div class="label">Sisa</div>
                                                    <div class="value">3814</div>
                                                </div>
                                            </div>
                                                    <div class="col-md-4" valign='center'>
                                                        <br>
                                                    
                                                        <div class="os-progress-bar danger">
                                                            <div class="bar-labels">
                                                                <div class="bar-label-left"><span class="bigger">Progress</span></div>
                                                                <div class="bar-label-right"><span class="danger">1% Pending</span></div>
                                                            </div>
                                                            <div class="bar-level-1" style="width: 100%">
                                                                <div class="bar-level-2" style="width: 70%">
                                                                    <div class="bar-level-3" style="width: 40%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                     
                                                      </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button"> Save changes</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="element-box">Peta Disini</div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="element-box pad10">
                                                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
                                                <br>
                                                <div class="" align="center">
                                                    Bambang<br>
                                                    <br>
                                                    PDI, Golkar, Hanura<br>
                                                    <br>
                                                    <label class="badge badge-danger">Rawan</label>
                                                    <label class="badge badge-warning">Hati-hati</label>
                                                    <label class="badge badge-success">Diprediksi Menang</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="element-box pad10">
                                                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
                                                <br>
                                                <div class="" align="center">
                                                    Bambang<br>
                                                    <br>
                                                    PDI, Golkar, Hanura<br>
                                                    <br>
                                                    <label class="badge badge-danger">Rawan</label>
                                                    <label class="badge badge-warning">Hati-hati</label>
                                                    <label class="badge badge-success">Diprediksi Menang</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="element-box pad10">
                                                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
                                                <br>
                                                <div class="" align="center">
                                                    Bambang<br>
                                                    <br>
                                                    PDI, Golkar, Hanura<br>
                                                    <br>
                                                    <label class="badge badge-danger">Rawan</label>
                                                    <label class="badge badge-warning">Hati-hati</label>
                                                    <label class="badge badge-success">Diprediksi Menang</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="element-box pad10">
                                                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
                                                <br>
                                                <div class="" align="center">
                                                    Bambang<br>
                                                    <br>
                                                    PDI, Golkar, Hanura<br>
                                                    <br>
                                                    <label class="badge badge-danger">Rawan</label>
                                                    <label class="badge badge-warning">Hati-hati</label>
                                                    <label class="badge badge-success">Diprediksi Menang</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <div id="tab_survei" class="tab-pane"> 
                                    servei
                                </div>
                                <div id="tab_ketokohan" class="tab-pane"> 
                                    ketokohan
                                </div>
                                <div id="tab_simulasi" class="tab-pane"> 
                                    simulasi
                                </div>
                            </div> 
                     

                    </div>

                    
         
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="element-wrapper">
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



 