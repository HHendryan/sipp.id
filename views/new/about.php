<div style="padding: 30px">
    <div class="row">
        <div class="col-md-6">
<div class="user-profile compact">
    <div class="up-head-w" style="background-image:url(<?=base_url();?>img/profile_bg1.jpg)">
        <div class="up-social">
        </div>
        <div class="up-main-info">
            <h2 class="up-header">PDI PERJUANGAN</h2>
            <h6 class="up-sub-header">Sistem Informasi Pemenangan Pemilu</h6>
        </div>
        <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
            </g>
        </svg>
    </div>
    <div style="text-align: center;"><img alt="" src="<?=base_url();?>img/ttd_mega.png" height="60" width="270"></div>
    <br>
    <div class="up-contents">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <h6 class="element-header" style="margin-bottom: 0px;">
                                                      Visi dan Misi
                                                    </h6>
                    <div class="element-box">
                        <div class="os-tabs-w">
                            <div class="os-tabs-controls">
                                <ul class="nav nav-tabs smaller">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#tab_visi">Visi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab_misi">Misi</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active show" id="tab_visi">
                                    <b>Visi Partai adalah keadaan pada masa depan yang diidamkan oleh Partai, dan oleh karena itu menjadi arah bagi perjuangan Partai.<p>
                                                                Berdasarkan amanat pasal 6 Anggaran Dasar Partai PDI Perjuangan adalah :
                                                                Partai adalah:</p></b>
                                    <p><b><br></b> a. alat perjuangan guna membentuk dan membangun karakter bangsa berdasarkan Pancasila 1 Juni 1945;</p>
                                    <p>
                                        b. alat perjuangan untuk melahirkan kehidupan berbangsa dan bernegara yang ber-Ketuhanan, memiliki semangat sosio nasionalisme, dan sosio demokrasi (Tri Sila);</p>
                                    <p>
                                        c. alat perjuangan untuk menentang segala bentuk individualisme dan untuk menghidupkan jiwa dan semangat gotong royong dalam kehidupan bermasyarakat, berbangsa dan bernegara (Eka Sila);</p>
                                    <p>
                                        d. wadah komunikasi politik, mengembangkan dan memperkuat partisipasi politik warga negara; dan</p>
                                    <p>
                                        e. wadah untuk membentuk kader bangsa yang berjiwa pelopor, dan memiliki pemahaman, kemampuan menjabarkan dan melaksanakan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;
                                    </p>
                                </div>
                                <div class="tab-pane" id="tab_misi">
                                    <b>Misi Partai adalah muatan hidup yang diemban oleh partai, sekaligus menjadi dasar pemikiran atas keberlangsungan eksistensi Partai, sebagaimana diamanatkan dalam pasal 7,8, 9 dan 10 Anggaran Dasar Partai, yaitu :</b>
                                    <p><b><br></b> Pasal 7 Partai mempunyai tujuan umum:</p>
                                    <p>
                                        a. mewujudkan cita-cita Proklamasi Kemerdekaan 17 Agustus 1945 sebagaimana dimaksud dalam Pembukaan Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 dalam bentuk mewujudkan masyarakat adil dan makmur dalam bingkai Negara Kesatuan Republik Indonesia yang bersemboyan Bhinneka Tunggal Ika; dan</p>
                                    <p>
                                        b. berjuang mewujudkan Indonesia sejahtera berkeadilan sosial yang berdaulat di bidang politik, berdiri di atas kaki sendiri di bidang ekonomi, dan Indonesia yang berkepribadian dalam kebudayaan.</p>
                                    <p>
                                        Pasal 8 Partai mempunyai tujuan khusus: a. membangun gerakan politik yang bersumber pada kekuatan rakyat untuk mewujudkan kesejahteraan berkeadilan sosial;</p>
                                    <p>
                                        b. membangun semangat, mengkonsolidasi kemauan, mengorganisir tindakan dan kekuatan rakyat, mendidik dan menuntun rakyat untuk membangun kesadaran politik dan mengolah semua tenaga rakyat dalam satu gerakan politik untuk mencapai kemerdekaan politik dan ekonomi;</p>
                                    <p>
                                        c. memperjuangkan hak rakyat atas politik, ekonomi, sosial dan budaya, terutama demi pemenuhan kebutuhan absolut rakyat, yaitu kebutuhan material berupa sandang, pangan, papan dan kebutuhan spiritual berupa kebudayaan, pendidikan dan kesehatan;</p>
                                    <p>
                                        d. berjuang mendapatkan kekuasaan politik secara konstitusional sebagai alat untuk mewujudkan amanat Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 yaitu mewujudkan pemerintahan yang melindungi segenap bangsa Indonesia dan seluruh tumpah darah Indonesia, memajukan kesejahteraan umum, mencerdaskan kehidupan bangsa, serta ikut melaksanakan ketertiban dunia yang berdasarkan kemerdekaan, perdamaian abadi dan keadilan sosial; dan</p>
                                    <p>
                                        e. menggalang solidaritas dan membangun kerjasama internasional berdasarkan spirit Dasa Sila Bandung dalam upaya mewujudkan cita-cita Pembukaan Undang-Undang Dasar Negara Republik Tahun 1945</p>
                                    <p>
                                        Pasal 9 Partai mempunyai fungsi:</p>
                                    <p>
                                        a. mendidik dan mencerdaskan rakyat agar bertanggung jawab menggunakan hak dan kewajibannya sebagai warga negara;</p>
                                    <p>
                                        b. melakukan rekrutmen anggota dan kader Partai untuk ditugaskan dalam struktural Partai, LembagaLembaga Politik dan Lembaga-Lembaga Publik;</p>
                                    <p>
                                        c. membentuk kader Partai yang berjiwa pelopor, dan memiliki pemahaman, kemampuan menjabarkan dan melaksanakan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;</p>
                                    <p>
                                        d. menghimpun, merumuskan, dan memperjuangkan aspirasi rakyat menjadi kebijakan pemerintahan negara;</p>
                                    <p>
                                        e. menghimpun, membangun dan menggerakkan kekuatan rakyat guna membangun dan mencapai cita-cita masyarakat Pancasila; dan</p>
                                    <p>
                                        f. membangun komunikasi politik berlandaskan hakekat dasar kehidupan berpolitik, serta membangun partisipasi politik warga negara.</p>
                                    <p>
                                        Pasal 10 Partai mempunyai tugas:</p>
                                    <p>
                                        a. mempertahankan dan mewujudkan cita-cita negara Proklamasi 17 Agustus 1945 di dalam Negara Kesatuan Republik Indonesia;</p>
                                    <p>
                                        b. mempertahankan, menyebarluaskan dan melaksanakan Pancasila sebagai dasar, pandangan hidup, tujuan berbangsa dan bernegara;</p>
                                    <p>
                                        c. menjabarkan, menyebarluaskan dan membumikan ajaran Bung Karno dalam kehidupan bermasyarakat, berbangsa, dan bernegara;</p>
                                    <p>
                                        d. menghimpun dan memperjuangkan aspirasi rakyat berdasarkan ideologi Pancasila 1 Juni 1945 dan Undang Undang Dasar Negara Republik Indonesia 1945, serta jalan TRISAKTI sebagai pedoman strategi dan tujuan kebijakan politik Partai;</p>
                                    <p>
                                        e. memperjuangkan kebijakan politik Partai menjadi kebijakan politik penyelenggaraan Negara;</p>
                                    <p>
                                        f. mempersiapkan kader Partai sebagai petugas Partai dalam jabatan politik dan jabatan publik;</p>
                                    <p>
                                        g. mempengaruhi dan mengawasi jalannya penyelenggaraan negara agar senantiasa berdasarkan pada ideologi Pancasila 1 Juni 1945 dan Undang Undang Dasar Negara Republik Indonesia 1945, serta jalan TRISAKTI sebagai pedoman strategi dan tujuan kebijakan politik Partai demi terwujudnya pemerintahan yang kuat, efektif, bersih dan berwibawa;</p>
                                    <p>
                                        h. sebagai poros kekuatan politik nasional wajib berperan aktif dalam menghidupkan spirit Dasa Sila Bandung untuk membangun konsolidasi dan solidaritas antar bangsa sebagai bentuk perlawanan terhadap liberalisme dan individualisme.</p>
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>            
        </div>
        <div class="col-md-6">
<div class="element-box">
    <div class="os-tabs-w">
        <div class="os-tabs-controls">
            <ul class="nav nav-tabs smaller">
            
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab_jadwal">Jadwal Pilkada</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab_video">Video</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            
         

            <div class="tab-pane active show" id="tab_jadwal">
                <div class="element-box">
                    <!--      <div class="table-responsive">
                                                                            <table class="table table-lightborder"> -->
                    <!-- <div class="element-box"> -->
                    <!-- <h6 class="element-header">Jadwal Pilkada</h6> -->
                    <div style="text-align: center;"><img alt="" src="<?=base_url();?>img/jadwal.jpg" class='img-fluid' height="3300" width="100%"></div>
                    <!--      </tbody>
                                                                            </table> -->
                    <!-- </div> -->
                </div>
            </div>
            <div class="tab-pane" id="tab_video">
                <!-- <div class="top-big-grids"> -->
                <iframe src="https://www.youtube.com/embed/121t0yAbmLc" height="315" width="100%">
                </iframe>
                <!-- </div> -->
				
				 <!-- <div class="col-md-4 top-grid-left-info"></div> -->
                <iframe src="https://www.youtube.com/embed/pBW5IcHYuKE" height="315" width="100%">
                </iframe>
				
                <!-- <div class="col-md-4 top-grid-left-info"></div> -->
                <iframe src="https://www.youtube.com/embed/fILiTzEyOF4" height="315" width="100%">
                </iframe>

                <!-- <div class="col-md-4 top-grid-left-info"></div> -->
                <iframe src="https://www.youtube.com/embed/gij0TiYmFZo" height="315" width="100%">
                </iframe>
                <!-- </div> -->
                <!-- </div> -->
                <!--        <div class="col-md-4 top-grid-left-info">
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>            
        </div>
    </div>
</div>