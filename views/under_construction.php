
<!DOCTYPE html>
<html>
  <head>
    <title>SIPP Dashboard</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="css/main.css?version=4.2.0" rel="stylesheet">
    <!-- <link href="assets2/css/dafters.css" rel="stylesheet"> -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style type="text/css">
      body:before{
            background: -webkit-gradient(linear, left top, right bottom, from(#c40000), to(#c40000));
      }

    .auth-box-w .logo-w {
        text-align: center;
        padding-top:  30px;
        padding-bottom:  30px;
        padding-left:  20%;
        padding-right:  20%;
    }

    </style>
  </head>
  <body class="auth-wrapper" style="background-color: #c40000 !important;">
    <div class="big-error-w">
                <h5>
                  UNDER MAINTENANCE
                </h5>
                <h4>
                  Regards IT TEAM
                </h4>
                <form>
                  <div class="input-group">
                    
                  </div>
                </form>
              </div>
  </body>
</html>