
<!DOCTYPE html>
<html>
    <head>
        <title>SIPP Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/switchery/switchery.min.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
        <style type="text/css">
            .middle{
                margin: auto;
            }
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <?php include 'template/menu.php' ?>
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box" style="padding-top: 30px;padding-left: 30px;padding-right: 30px;padding-bottom: 30px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a class="element-box el-tablo" href="#">
                                                        <div class="value">0</div>
                                                        <hr>
                                                        <div class="label" style="font-size: 15px">DATA PEMILIH TETAP</div>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a class="element-box el-tablo" href="#">
                                                        <div class="value"><?php echo number_format($jum_influencer) ?></div>
                                                        <hr>
                                                        <div class="label" style="font-size: 15px">DATA INFLUENCER</div>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a class="element-box el-tablo" href="#">
                                                        <div class="value">0</div>
                                                        <hr>
                                                        <div class="label" style="font-size: 15px">DATA SURVEY</div>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a class="element-box el-tablo" href="#">
                                                        <div class="value">0</div>
                                                        <hr>
                                                        <div class="label" style="font-size: 15px">DATA POTENSI DESA</div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <h6 class="element-header">Data Pemilih Tetap</h6>
                                        <div class="element-content">
                                            <div class="element-box">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div id="pie_jenis_kelamin" style="width: 100%;height: 300px;"></div> 
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div id="pie_usia" style="width: 100%;height: 300px;"></div> 
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div id="data_penduduk" style="width: 100%;height: 300px;"></div> 
                                                    </div>
<!--                                                     <div class="col-sm-4">
                                                        <div id="pie_statuskawin" style="width: 100%;height: 300px;"></div> 
                                                    </div> -->
                                                    <!-- <div class="col-sm-3">
                                                        <div id="pie_agama" style="width: 100%;height: 300px;"></div> 
                                                    </div> -->
                                                    <!-- <div class="col-sm-3">
                                                        <div id="pie_pendidikan" style="width: 100%;height: 300px;"></div> 
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div id="pie_agama" style="width: 100%;height: 300px;"></div> 
                                                    </div> -->
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <h6 class="element-header">Data Influencer</h6>
                                        <div class="element-content">
                                            
                                                <div class="row">

                                                    <div class="col-sm-3">
                                                        <div class="element-box">
                                                        <div id="pie_influencer" style="width: 100%;height: 300px;"></div> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="element-box">
                                                        <div id="graph_kecondongan_umum" style="width: 100%;height: 300px;"></div> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="element-box">
                                                        <div id="graph_kecondongan_detail" style="width: 100%;height: 300px;"></div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <?php for ($i=0; $i < $jum_province; $i++) { 
                                                        $graph_id = 'graph_'.$i;
                                                    ?>

                                                        <div class="col-sm-4">
                                                            <div class="element-box">
                                                            <div id="<?php echo $graph_id?>" style="width: 100%;height: 300px;"></div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    
                                                </div>  
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <h6 class="element-header">Summary Kecondongan Partai</h6>
                                        <div class="element-content">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="element-box">
                                                            <div class="table-responsive">
                                                            <table class="table table-lightborder">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Logo</th>
                                                                        <th>Partai</th>
                                                                        <th>Provinsi</th>
                                                                        <th>Kab/Kota</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/pdi-p.png"  height="42" width="42"> </td>
                                                                        <td>PDI</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/demokrat.png"  height="42" width="42"> </td>
                                                                        <td>Demokrat</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/Golkar.png"  height="42" width="42"> </td>
                                                                        <td>Golkar</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/gerindra.png"  height="42" width="42"> </td>
                                                                        <td>Gerindra</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/pks.png"  height="42" width="42"> </td>
                                                                        <td>PKS</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><img src="<?=base_url()?>/img/logo_parpol/nasdem.png"  height="42" width="42"> </td>
                                                                        <td>Nasdem</td>
                                                                        <td>4</td>
                                                                        <td>16</td>
                                                                        <td>20</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="element-box">
                                                        <h6 class="element-header">Wilayah : Provinsi &nbsp;&nbsp;|&nbsp;&nbsp; Partai : PDI-Perjuangan</h6>
                                                        <div class="table-responsive">
                                                            <table class="table table-lightborder">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Provinsi</th>
                                                                        <th>Pasangan Calon</th>
                                                                        <th>Foto</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>SULAWESI SELATAN</td>
                                                                        <td>Nurdin Abdullah - </td>
                                                                        <td><img src="<?=base_url()?>/img/sulsel.jpg"  height="42" width="42"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>SULAWESI SELATAN</td>
                                                                        <td>Nurdin Abdullah - </td>
                                                                        <td><img src="<?=base_url()?>/img/sulsel.jpg"  height="42" width="42"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>SULAWESI SELATAN</td>
                                                                        <td>Nurdin Abdullah - </td>
                                                                        <td><img src="<?=base_url()?>/img/sulsel.jpg"  height="42" width="42"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>SULAWESI SELATAN</td>
                                                                        <td>Nurdin Abdullah - </td>
                                                                        <td><img src="<?=base_url()?>/img/sulsel.jpg"  height="42" width="42"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>    
                                                        </div>
                                                        
                                                    </div>
                                                </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
            <input type="hidden" id="base" value="<?=base_url()?>">
            <input type="hidden" id="type_form" value="<?=base_url()?>">
            <input type="hidden" id="ketokohan_form" value="<?=base_url()?>">
        </div>
        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>bower_components/switchery/switchery.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>
        <script src="<?=base_url()?>bower_components/amcharts/amcharts.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/serial.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/pie.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/themes/light.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['Tangible', <?php echo $jum_tangible ?>],
              ['Intangible', <?php echo $jum_intangible ?>]
            ]);
            var options = {
              title: 'Tangible & Intangible',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie'));

            chart.draw(data, options);
          }
        </script>

        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(jenis_kelamin);
          google.charts.setOnLoadCallback(usia);
          google.charts.setOnLoadCallback(pendidikan);
          google.charts.setOnLoadCallback(agama);
          google.charts.setOnLoadCallback(influencer);
          google.charts.setOnLoadCallback(status_kawin);

          function status_kawin() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['Kawin', <?php echo $jum_status->jml_kawin ?>],
              ['Belum Kawin', <?php echo $jum_status->jml_belumkawin ?>]
            ]);
            var options = {
              title: 'Status Kawin',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_statuskawin'));

            chart.draw(data, options);
          }

          function jenis_kelamin() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['Laki-Laki', <?php echo $jum_jeniskelamin->laki ?>],
              ['Perempuan', <?php echo $jum_jeniskelamin->wanita ?>]
            ]);
            var options = {
              title: 'Jenis Kelamin',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_jenis_kelamin'));

            chart.draw(data, options);
          }

          function usia() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['17-25 (Milenial)', <?php echo $jum_usia->jml_1 ?>],
              ['26-35', <?php echo $jum_usia->jml_2 ?>],
              ['36-45', <?php echo $jum_usia->jml_3 ?>],
              ['46-55', <?php echo $jum_usia->jml_4 ?>],
              ['>56', <?php echo $jum_usia->jml_5 ?>],
            ]);
            var options = {
              title: 'Usia',
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_usia'));

            chart.draw(data, options);
          }

          function pendidikan() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['SD-SMP', 5],
              ['SMA', 5],
              ['Sarjana', 5],
            ]);
            var options = {
              title: 'Pendidikan',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_pendidikan'));

            chart.draw(data, options);
          }

          function agama() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['Islam', 5],
              ['Katolik', 5],
              ['Protestan', 5],
              ['Hindu', 5],
              ['Budha', 5],
              ['Konghucu', 5],
            ]);
            var options = {
              title: 'Agama',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_agama'));

            chart.draw(data, options);
          }

          function influencer() {

            var data = google.visualization.arrayToDataTable([
              ['Kategori', 'Jumlah'],
              ['Tangible', <?php echo $jum_tangible ?>],
              ['Intangible', <?php echo $jum_intangible ?>]
            ]);
            var options = {
              title: 'Tangible & Intangible',
              legend: {position: 'bottom'},
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie_influencer'));

            chart.draw(data, options);
          }
        </script>

        <script type="text/javascript">
                        var chart4 = AmCharts.makeChart("graph_kecondongan_umum", {
                            "theme": "light",
                            "type": "serial",
                            "dataProvider": [
                            {
                                "name": "Netral",
                                "points": <?php echo $jum_netral ?>,
                                "color" : "#ffcc29",
                            },
                            {
                                "name": "Memilih",
                                "points": <?php echo $jum_pilih ?>,
                                "color" : "#4ecc48",
                            },
                            ],
                            "graphs": [{
                                "balloonText": "[[category]]:[[value]] %",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "labelText": "[[value]]%",
                                "title": "Tangible",
                                "type": "column",
                                "colorField": "color",
                                "valueField": "points"
                            }],
                            // "depth3D": 20,
                            "angle": 30,
                            "rotate": true,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "gridAlpha": 0,
                                "fillAlpha": 0,
                                "position": "left"
                            },
                            "valueAxes": [ {
                                "axisAlpha": 0,
                                "labelsEnabled" : false,
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0
                            } ],
                        });

                        var chart5 = AmCharts.makeChart("graph_kecondongan_detail", {
                            "theme": "light",
                            "type": "serial",
                            "dataProvider": [
                            <?php foreach ($influence_detail as $key => $value) { ?>
                                {
                                    "name": "<?php echo $value->alias ?>",
                                    "points": <?php echo $value->jml_ ?>,
                                    "color" : "<?php echo $value->color ?>",
                                },
                            <?php } ?>
                            ],
                            "graphs": [{
                                "balloonText": "[[category]]:[[value]] %",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "labelText": "[[value]]%",
                                "title": "Tangible",
                                "type": "column",
                                "colorField": "color",
                                "valueField": "points"
                            }],
                            // "depth3D": 20,
                            "angle": 30,
                            "rotate": true,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0,
                                "axisAlpha": 0,
                                "fillAlpha": 0.05,
                                "position": "left"
                            },
                            "valueAxes": [ {
                                "axisAlpha": 0,
                                "labelsEnabled" : false,
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0
                            } ],
                        });
        </script>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "data_penduduk", {
              "type": "serial",
              "titles": [
                    {
                        "text": "Top 5 Penduduk Terbesar",
                        "size": 15
                    }
                ],
              "theme": "light",
              "dataProvider": [ {
                "country": "Jawa Barat",
                "visits": 47379400
              }, {
                "country": "Jawa Timur",
                "visits": 39075300
              }, {
                "country": "Jawa Tengah",
                "visits": 34019100
              }, {
                "country": "Sumatera Utara",
                "visits": 14102900
              }, {
                "country": "Banten",
                "visits": 12203100
              }],
              "valueAxes": [ {
                "axisAlpha": 0,
                "labelsEnabled" : false,
                "gridColor": "#FFFFFF",
                "gridAlpha": 0,
                "dashLength": 0
              } ],
              "gridAboveGraphs": true,
              "startDuration": 1,
              "graphs": [ {
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
              } ],
              "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
              },
              "categoryField": "country",
              "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "axisAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
              }

            } );
        </script>
        <script type="text/javascript">

            <?php $i=0; foreach ($new_graph as $key => $value) { 
                 $graph_id = 'graph_'.$i;
            ?>
                var chart = AmCharts.makeChart("<?php echo $graph_id ?>",
                        {
                            "titles": [
                                {
                                    "text": "<?php echo $key ?>",
                                    "size": 15
                                }
                            ],
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                            <?php foreach ($value as $cek) { ?>
                            {

                                "name": "<?php echo $cek['alias'] ?>",
                                "points": <?php echo $cek['jumlah'] ?>,
                                "color" : "<?php echo $cek['color'] ?>",
                            },
                            <?php } ?>
                            ],
                            "startDuration": 1,
                            "graphs": [{
                                "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]] %</b></span>",
                                "bulletOffset": 10,
                                "bulletSize": 52,
                                "colorField": "color",
                                "labelText": "[[value]]%",
                                "cornerRadiusTop": 8,
                                "fillAlphas": 0.8,
                                "lineAlpha": 0,
                                "topRadius":1,
                                "type": "column",
                                "valueField": "points"
                            }],
                            "depth3D": 40,
                            "angle": 30,
                            "marginTop": 0,
                            "marginRight": 0,
                            "marginLeft": 0,
                            "marginBottom": 0,
                            // "autoMargins": false,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridColor": "#FFFFFF",
                                "axisAlpha": 0,
                                "gridPosition": "start",
                                "fillAlpha": 0.00,
                                "position": "left",
                                "labelRotation": 90
                            },
                            "valueAxes": [ {
                                "axisAlpha": 0,
                                "gridColor": "#FFFFFF",
                                "labelsEnabled" : false,
                                "gridAlpha": 0.2,
                                "dashLength": 0
                            } ],
                        });
            <?php $i++; } ?>
        </script>

    </body>
</html>
