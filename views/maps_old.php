
<!DOCTYPE html>
<html>
    <head>
        <title>Teknopol Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>
                <?php include 'template/menu.php' ?>

                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box" style="padding-top: 10px;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <h6 class="element-header">Peta Ketokohan</h6>
                                        <div class="element-box">
                                            <div class="element-info">
                                                <div id="map" style="width: 100%;height: 400px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-5">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                          <h6 class="element-header">Tabel Ketokohan</h6>
                                          <div class="element-box">
                                              <table id="table_influencer" width="100%" class="table table-striped table-lightfont"></table>
                                          </div>
                                      </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      Detail
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="table-responsive">
                    <table id="table_detail" width="100%" class="table table-striped table-lightfont"></table>
                  </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button"> Save changes</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>

        <!-- MAPS -->
        <script type="text/javascript">
            function initMap() {
                var uluru = {lat: -2.554743, lng: 118.120507};
                $.ajax({
                    url: "<?php echo site_url('maps/marker')?>",
                    cache: false,
                    type:"GET",
                    success: function(respond){
                        obj = JSON.parse(respond);
                        var map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 5,
                          center: uluru
                        });

                        $.each(obj,function(index, val){
                            var icon = "<?=site_url('img/logo_provinsi/') ?>"+val.logo;
                            var mark = Object.create(null);
                            mark.lat = parseFloat(val.latitude);
                            mark.lng = parseFloat(val.longitude);
                            var marker = new google.maps.Marker({
                                position: mark,
                                icon: icon,
                                optimized: false,
                                id:val.id,
                                title: val.name+" Tangible : "+val.jum_tangible+" Intangible : "+val.jum_intangible,
                                map: map
                            });

                            marker.addListener('click', function() {
                                Pace.track(function(){
                                  // alert(marker.id);
                                  $("#modal_detail").modal('show');  
                                  var table = $('#table_detail').DataTable();
                                  table.ajax.url("<?= site_url('maps/get_detail_influencer/')?>"+marker.id).load();
                                })
                            });
                        })
                    }
                })
                
                var map = new google.maps.Map(document.getElementById('map'), {
                    
                    center: uluru
                });
                var marker = new google.maps.Marker({
                        position: {lat: 4.454114, lng: 97.084948},
                        map: map
                    });
            }
            
        </script>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwDdoUa7ozoEQub5oXnmBTeD9AT74gMJA&callback=initMap">
        </script>

        <!-- TABLE DETAIL -->
        <script type="text/javascript">
          $('#table_detail').DataTable({
                columns: [
                            { data: 'name', title: "Nama"},
                            { data: 'phone', title: "Telephone"},
                            { data: 'pertokohan', title: "Pertokohan" },
                            { data: 'kecondongan', title: "Kecondongan" },
                            { data: 'gender', title: "Gender" },
                            { data: 'kab', title: "Kab"},
                            { data: 'kec', title: "Kec" },
                            { data: 'kel', title: "Kel" },
                        ],
                "autoWidth": false,
            });
        </script>

        <!-- TABLE INFLUENCER -->
        <script type="text/javascript">
          (function ($) {
            $(document).ready(function() {
                var table = $('#table_influencer').DataTable({
                    ajax: '<?=site_url('maps/datatable_influence/all/all/all')?>',
                    columns: [
                                { data: 'provinsi', title : 'Provinsi'},
                                { data: 'jum_tangible', title : 'Jumlah'},
                                { data: 'button', title : 'Action' },
                            ],
                    "autoWidth": false,
                    "searching": false,
                    "paging":false,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;
             
                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,.]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        var totalkab = api
                            .column( 1 )
                            .data()
                            .reduce( function (a, b) {
                                return (intVal(a) + intVal(b)).toLocaleString();
                            }, 0 );

                        $( api.column( 0 ).footer() ).html('Total');
                        $( api.column( 1 ).footer() ).html(totalkab);
                        
                    },
                    initComplete: function (settings, json) {
                        $('.dataTables_wrapper select').select2({
                            minimumResultsForSearch: Infinity
                        });
                    }
                });
            });
        })(jQuery);
        </script>

        <script type="text/javascript">
          function change_date(date){
            date = date.split('-');
            datebaru = date[2]+'-'+date[1]+'-'+date[0];
            return datebaru
          }
        </script>
    </body>
</html>
