
<!DOCTYPE html>
<html>
    <head>
        <title>SIPP Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
        <link href="<?=base_url()?>bower_components/pace/pace.css" rel="stylesheet">
        <link href="<?=base_url()?>css/main.css?version=4.2.0" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/dafters.css" rel="stylesheet">
    </head>
    <body>
        <div class="all-wrapper menu-top">
            <?php include 'template/menu.php' ?>
            <div class="layout-w">
                <?php include 'template/mobile.php' ?>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box" style="padding-top: 10px;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper" style="padding-bottom: 0px;">
                                        <h6 class="element-header">Peta Ketokohan</h6>
                                        <div class="element-box">
                                            <div class="element-info">
                                                <div id="map" style="width: 100%;height: 400px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
            <div aria-hidden="true" aria-labelledby="myLargeModalLabel" id="modal_detail" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="os-tabs-w">
                                <div class="os-tabs-controls">
                                    <ul class="nav nav-tabs smaller">
                                        <li class="nav-item">
                                            <a class="nav-link active show" data-toggle="tab" href="#tab_tangible">Tangible</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#tab_intangible">Intangible</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#tab_kecondongan">Kecondongan</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#tab_detail">Detail</a>
                                        </li> -->
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active show" id="tab_tangible">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <table class="table table-lightborder" >
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Ketokohan</th>
                                                            <th class="text-left">Jumlah</th>
                                                            <th class="text-left">Persentase</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tablebody_tangible">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-8">
                                                <div id="chart_tangible" style="width: 100%;height: 500px;"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="tab_intangible">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Ketokohan</th>
                                                            <th class="text-left">Jumlah</th>
                                                            <th class="text-left">Persentase</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tablebody_intangible">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-8">
                                                <div id="chart_intangible" style="width: 100%;height: 500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_kecondongan">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Kategori</th>
                                                            <th class="text-left">Jumlah</th>
                                                            <th class="text-left">Persentase</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tablebody_kecondongan_umum">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-8">
                                                <div id="chart_kecondongan_umum" style="width: 100%;height: 300px;"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Kategori</th>
                                                            <th class="text-left">Jumlah</th>
                                                            <th class="text-left">Persentase</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tablebody_kecondongan_detail">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-8">
                                                <div id="chart_kecondongan_detail" style="width: 100%;height: 500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_detail"></div>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?=base_url()?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=base_url()?>bower_components/moment/moment.js"></script>
        <script src="<?=base_url()?>bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="<?=base_url()?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?=base_url()?>bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="<?=base_url()?>bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
        <script src="<?=base_url()?>bower_components/dropzone/dist/dropzone.js"></script>
        <script src="<?=base_url()?>bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?=base_url()?>bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=base_url()?>bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?=base_url()?>bower_components/slick-carousel/slick/slick.min.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/util.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/button.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="<?=base_url()?>bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="<?=base_url()?>bower_components/pace/pace.min.js"></script>
        <script src="<?=base_url()?>js/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>js/main.js?version=4.2.0"></script>
        <script src="<?=base_url()?>bower_components/amcharts/amcharts.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/serial.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/pie.js"></script>
        <script src="<?=base_url()?>bower_components/amcharts/themes/light.js"></script>
        <script type="text/javascript">
            function myFunction() {
                return "Write something clever here...";
            }
            $(document).ready(function() {
                // generate_chart(73);
            });
        </script>
        <!-- MAPS -->
        <script type="text/javascript">
            function initMap() {
                var uluru = {lat: -2.554743, lng: 118.120507};
                $.ajax({
                    url: "<?php echo site_url('maps/marker')?>",
                    cache: false,
                    type:"GET",
                    success: function(respond){
                        obj = JSON.parse(respond);
                        var map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 5,
                          center: uluru
                        });

                        $.each(obj,function(index, val){
                            var icon = "<?=site_url('img/logo_provinsi/') ?>"+val.logo;
                            var mark = Object.create(null);
                            mark.lat = parseFloat(val.latitude);
                            mark.lng = parseFloat(val.longitude);
                            var marker = new google.maps.Marker({
                                position: mark,
                                icon: icon,
                                optimized: false,
                                id:val.id,
                                title: val.name+" Tangible : "+val.jum_tangible+" Intangible : "+val.jum_intangible,
                                map: map
                            });

                            marker.addListener('click', function() {
                                Pace.track(function(){
                                  // alert(marker.id);
                                  generate_chart(marker.id);
                                  $("#modal_detail").modal('show');  
                                })
                            });
                        })
                    }
                })
                
                var map = new google.maps.Map(document.getElementById('map'), {
                    
                    center: uluru
                });
                var marker = new google.maps.Marker({
                        position: {lat: 4.454114, lng: 97.084948},
                        map: map
                    });
            }
        </script>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwDdoUa7ozoEQub5oXnmBTeD9AT74gMJA&language=id&callback=initMap">
        </script>

        <!-- GRAPH -->
        <script type="text/javascript">
            function generate_chart(id){
                $.ajax({
                    url: "<?php echo site_url('maps/graph/')?>"+id,
                    cache: false,
                    type:"GET",
                    success: function(respond){
                        data = JSON.parse(respond);
                        $("#tablebody_tangible").html(data.tangible);
                        $("#tablebody_intangible").html(data.intangible);
                        $("#tablebody_kecondongan_umum").html(data.table_condong_umum);
                        $("#tablebody_kecondongan_detail").html(data.partai);
                        console.log(data);
                        var chart = AmCharts.makeChart("chart_tangible", {
                            "theme": "light",
                            "type": "serial",
                            "dataProvider": data.tangible_chart,
                            "graphs": [{
                                "balloonText": "[[category]]:[[value]] %",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "labelText": "[[value]]%",
                                "title": "Tangible",
                                "type": "column",
                                "colorField": "color",
                                "valueField": "points"
                            }],
                            "depth3D": 20,
                            "angle": 30,
                            "rotate": true,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "fillAlpha": 0.05,
                                "position": "left"
                            },
                        });
                        var chart2 = AmCharts.makeChart("chart_intangible", {
                            "theme": "light",
                            "type": "serial",
                            "dataProvider": data.intangible_chart,
                            "graphs": [{
                                "balloonText": "[[category]]:[[value]] %",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "labelText": "[[value]]%",
                                "title": "Tangible",
                                "type": "column",
                                "colorField": "color",
                                "valueField": "points"
                            }],
                            "depth3D": 20,
                            "angle": 30,
                            "rotate": true,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "fillAlpha": 0.05,
                                "position": "left"
                            },
                        });
                        var chart3 = AmCharts.makeChart( "chart_kecondongan_umum", {
                          "type": "pie",
                          "theme": "light",
                          "dataProvider": [ {
                            "name": "Netral",
                            "value": data.persen_jum_netral
                          }, {
                            "name": "Condong",
                            "value": data.persen_jum_pilih
                          }],
                          "valueField": "value",
                          "titleField": "name",
                          "outlineAlpha": 0.4,
                          "depth3D": 15,
                          "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                          "angle": 30,
                          "export": {
                            "enabled": true
                          }
                        } );
                        var chart4 = AmCharts.makeChart("chart_kecondongan_detail", {
                            "theme": "light",
                            "type": "serial",
                            "dataProvider": data.partai_chart,
                            "graphs": [{
                                "balloonText": "[[category]]:[[value]] %",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "labelText": "[[value]]%",
                                "title": "Tangible",
                                "type": "column",
                                "colorField": "color",
                                "valueField": "points"
                            }],
                            "depth3D": 20,
                            "angle": 30,
                            "rotate": true,
                            "categoryField": "name",
                            "categoryAxis": {
                                "gridColor": "#FFFFFF",
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "fillAlpha": 0.00,
                                "position": "left",
                                "labelRotation": 90
                            },
                            "valueAxes": [ {
                                "axisAlpha": 0,
                                "labelsEnabled" : false,
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0
                            } ],
                        });
                    }
                })
            }
        </script>
    </body>
</html>
