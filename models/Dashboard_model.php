<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Dashboard_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Survei_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }
	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function thousandsCurrencyFormat($num) {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('K', 'M', 'B', 'T');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_data_dashboard(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		return $data;
	}

	function detail_area($data){
		extract($data,EXTR_OVERWRITE);
		$table= $this->get_table($tipe);
		
		// $data = $this->db->query("SELECT * from $table where id=$id")->row();
		$data = $this->db->get_where($table, array('id' => $id))->row();
		return $data;
	}

	function get_tree($tipe,$id){
		$table= $this->get_table($tipe);
		$parent_table = $this->get_parent_table($tipe);
		$result = $this->db->get_where($table, array($parent_table => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['nama'] = "$value->name";
			$data[$i]['lazy'] = TRUE;
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = $tipe;
			$data[$i]['logo'] = '$value->logo';
			$i++;
		}
		return $data;
	}

	function get_tree_dapil($tipe,$id){

		if ($tipe=='dapil') {
			$logo = $this->db->get_where('m_provinces', array('id' => $id))->row('logo');

			$result = $this->db->query("SELECT a.id_dapil, a.nama_dapil, b.`id_provinces` FROM m_dapil a 
				INNER JOIN dapil_has_area b ON a.id_dapil=b.id_dapil 
				where a.periode='2019' and a.type='1' and b.id_provinces=".$id."  
				GROUP BY a.id_dapil")->result();

			$i=0;
			foreach ($result as $key => $value) {
				$data[$i]['title'] = "$value->nama_dapil";
				$data[$i]['nama'] = "$value->nama_dapil";
				$data[$i]['lazy'] = TRUE;
				$data[$i]['id'] = "$value->id_dapil";
				$data[$i]['tipe'] = $tipe;
				$data[$i]['logo'] = $logo;
				$i++;
			}
			
		} else if ($tipe == 'kabupaten') {
			$result = $this->db->query("SELECT a.`id_regency`, b.name, b.id, b.logo FROM dapil_has_area a 
				INNER JOIN m_regencies b ON a.`id_regency` = b.id WHERE a.id_dapil=".$id." 
				GROUP BY a.id_regency")->result();

			$i=0;
			foreach ($result as $key => $value) {
				$data[$i]['title'] = "$value->name";
				$data[$i]['nama'] = "$value->name";
				$data[$i]['lazy'] = TRUE;
				$data[$i]['id'] = "$value->id";
				$data[$i]['tipe'] = $tipe;
				$data[$i]['logo'] = "$value->logo";
				$i++;
			}
		}

		return $data;
	}

	//TAB
	function get_data_tab_profile($id,$nama,$tipe,$logo){
		// $data['link'] = $this->get_link_map($id,$tipe);
		if($tipe == 'provinsi'){
			$data['informasi'] = $this->db->get_where('m_info_provinsi', array('id_provinsi' => $id))->row();	
		}
		
		//$data['jum_survey'] = $this->get_jum_survey($nama,$tipe);
		$data['jum_survei'] = $this->get_jum_survey($id,$tipe);
		$data['paslon'] = $this->get_paslon($id,$nama,$tipe);
		$data['jum_dpt'] = $this->get_jmldpt($id,$tipe);
		$data['jum_ketokohan'] = $this->get_jmlketokohan($id,$tipe);
		//$data['jum_survei'] = $this->get_jmlsurvey($id,$tipe);
		$data['jum_guraklih'] = $this->get_jmlguraklih($id,$tipe);
		$data['survey_pilpres_19'] = $this->db->query("select count(1) as total, IFNULL(b.image_paslon,'avatarblank.jpg') img_paslon, round(( count(1)/(SELECT count(1) from trans_survey_clean where p6 <> 225 and p6 <> 218 and p6 <> 206 and p6 <> 226 and p6 <> 205 and p6 <> 227) * 100 ),2) as jumlah, b.nama  
			from trans_survey_clean a
			inner join m_pilih b on a.p6 = b.id
			LEFT JOIN m_paslon c ON a.`id_paslon` = c.`id_paslon`
			where b.id <> 225 and b.id <> 218 and b.id <> 206 and b.id <> 226 and b.id <> 205 and b.id <> 227
			group by p6 order by jumlah desc limit 10")->result();
		return $data;
	}

	//TAB
	function get_data_tab_profile_pilpres($id,$nama,$tipe,$logo){
		$asumsi_suara = round($this->get_jmldpt($id,$tipe)*0.75);
		//$data['jum_survey'] = $this->get_jum_survey($nama,$tipe);
		//$data['jum_survei'] = $this->get_jum_survey($id,$tipe);
		$data['paslon'] = $this->get_paslon_pilpres($id,$nama,$tipe);
		$data['jum_dpt'] = $this->get_jmldpt($id,$tipe);
		$data['asumsi_suara_sah'] = $asumsi_suara ;
		$data['target_suara'] = round(($asumsi_suara/2)+($asumsi_suara *0.05));
		$data['jum_ketokohan'] = $this->get_jmlketokohan($id,$tipe);
		$data['jml_perolehan'] = $this->get_jmlperolehan($id,$tipe);
		$data['jokowi'] = $this->get_suara_pilpres($id,$tipe,'jokowi','2014');
		$data['prabowo'] = $this->get_suara_pilpres($id,$tipe,'prabowo','2014');
		//$data['jum_survei'] = $this->get_jmlsurvey($id,$tipe);
		$data['jum_guraklih'] = $this->get_jmlguraklih($id,$tipe);
		return $data;
	}
	
	function get_data_tab_simulasi($id,$nama,$tipe){
		
		$data['simulasi'] = $this->get_data_simulasi($id,$nama,$tipe);
		$data['tot_dpt'] = $this->get_tot_dpt($id,$nama,$tipe);
		$data['jml_perolehan'] = $this->get_jmlperolehan($id,$tipe);
		$data['jum_ketokohan_group'] = $this->get_jmlketokohan_group($id,$tipe);
		return $data;
	}
	
	function get_data_simulasi($id,$nama,$tipe){
		
		
		$where_wilayah ="";
		$where_tps=" WHERE provinsi = d.id GROUP BY provinsi ";
		$where_dpt="";
		if($id > 0){
		
			if($tipe == "provinsi"){
					$where_wilayah = " AND d.id = '".$id."'";
					$where_tps = " WHERE provinsi = d.id GROUP BY provinsi ";
			}
			
			if($tipe == "kabupaten"){
					$where_wilayah = " AND r.id = '".$id."'";
					$where_tps = " WHERE kabupaten = r.id GROUP BY kabupaten ";
					$where_dpt = " WHERE kabupaten = '".$id."'";
			}

			if($tipe == "kecamatan"){
					$where_wilayah = " AND s.id = '".$id."'";
					$where_tps = " WHERE kecamatan = s.id GROUP BY kecamatan ";
					$where_dpt = " WHERE kecamatan = '".$id."'";
			}
			
			if($tipe == "kelurahan"){
					$where_wilayah = " AND g.id = '".$id."'";
					$where_tps = " WHERE kelurahan = g.id GROUP BY kelurahan ";
					$where_dpt = " WHERE kelurahan = '".$id."'";
			}
			
		}
		
		$data = $this->db->query("SELECT * FROM 
								(
								SELECT a.pasangan_alias,a.kepala_alias,a.wakil_alias,a.`id_area`,a.area,a.image_paslon,d.table_dpt,d.exist_table,
								(
								SELECT COUNT(1) FROM 
								m_paslon
								WHERE a.`id_area` = id_area
								GROUP BY id_area
								) jml_paslon,
								IFNULL((
								SELECT COUNT(1) FROM m_tps ".$where_tps."
								),0)  jml_tps
								FROM m_paslon a ,m_parpol b, m_pengusung c, m_provinces d,  m_regencies r, m_districts s, m_villages g
								WHERE a.`id_paslon` = c.`id_paslon`
								AND b.`id_parpol` = c.`id_parpol`
								AND b.`id_parpol` = 3
								AND a.`id_area` = d.`id`
								AND d.id = r.`province_id`
								and r.id = s.regency_id
								and s.id = g.district_id
								".$where_wilayah."
								GROUP BY a.`id_paslon`
								)v")->result();
		 $data3[] = array( 
							'kepala_alias' => '',
							'pasangan_alias' => '',
							'wakil_alias' => '',
							'jml_paslon' => '',
							'image_paslon' => '',
							'exist_table' => '',
							'id_area' => '',
							'area' => '',
							'table_dpt' => '',
							'jml_dpt' => '0',
							'jml_tps' => '0',
							'asumsi_suara_75' => '0',
							'asumsi_suara_5' => '0',
							'target_perolehan_suara' => '0',
							'target_menang' => '0'
							);  
		if(count($data) > 0){
			unset($data3);
		}
		foreach ($data as $key => $value) {
			
			if($value->exist_table == 1){
				
				$data2 = $this->db->query("SELECT '".$value->kepala_alias."' as kepala_alias,
										'".$value->pasangan_alias."' as pasangan_alias,
										'".$value->wakil_alias."' as wakil_alias,
										'".$value->jml_paslon."' as jml_paslon,
										'".$value->id_area."' as id_area,
										'".$value->area."' as area,
										'".$value->image_paslon."' as image_paslon,
										'".$value->exist_table."' as exist_table,
										'".$value->table_dpt."' as table_dpt,
										'".$value->jml_tps."' as jml_tps,
										COUNT(1) AS jml_dpt,
										ROUND((COUNT(1)*75)/100) AS asumsi_suara_75,
										ROUND((ROUND((COUNT(1)*75)/100)*5)/100) AS asumsi_suara_5,
										ROUND((ROUND((COUNT(1)*75)/100)/".$value->jml_paslon.")+ROUND((ROUND((COUNT(1)*75)/100)*5)/100)) target_perolehan_suara,
										IFNULL(ROUND(((ROUND((ROUND((COUNT(1)*75)/100)/".$value->jml_paslon.")+ROUND((ROUND((COUNT(1)*75)/100)*5)/100))/ROUND((COUNT(1)*75)/100))*100),2),0) target_menang
										FROM ".$value->table_dpt.$where_dpt)->result();
										
				foreach ($data2 as $key2 => $value2) {
					
					$data3[] = array( 
							'kepala_alias' => $value2->kepala_alias,
							'pasangan_alias' => $value2->pasangan_alias,
							'wakil_alias' => $value2->wakil_alias,
							'jml_paslon' => $value2->jml_paslon,
							'image_paslon' => $value2->image_paslon,
							'exist_table' => $value2->exist_table,
							'id_area' => $value2->id_area,
							'area' => $value2->area,
							'table_dpt' => $value2->table_dpt,
							'jml_tps' => $value2->jml_tps,
							'jml_dpt' => $value2->jml_dpt, 
							'asumsi_suara_75' => $value2->asumsi_suara_75,
							'asumsi_suara_5' => $value2->asumsi_suara_5,
							'target_perolehan_suara' => $value2->target_perolehan_suara,
							'target_menang' => $value2->target_menang,
							'id' => $id,
							'nama' => $nama,
							'tipe' => $tipe
							);  
				}
				
			}else{
				
				  $data3[] = array( 
							'kepala_alias' => $value->kepala_alias,
							'pasangan_alias' => $value->pasangan_alias,
							'wakil_alias' => $value->wakil_alias,
							'jml_paslon' => $value->jml_paslon,
							'image_paslon' => $value->image_paslon,
							'exist_table' => $value->exist_table,
							'id_area' => $value->id_area,
							'area' => $value->area,
							'table_dpt' => $value->table_dpt,
							'jml_dpt' => '0',
							'jml_tps' => '0',
							'asumsi_suara_75' => '0',
							'asumsi_suara_5' => '0',
							'target_perolehan_suara' => '0',
							'target_menang' => '0',
							'id' => $id,
							'nama' => $nama,
							'tipe' => $tipe
							);  
			}
			
			
		}
			//echo json_encode($data3); 	
			
			return $data3;
		 
		
	}
	
	function get_data_simulasixx($id,$nama,$tipe){ 
		/* 
		m_provinces provinsi
		m_regencies kabupaten
		m_district kecamatan
		m_villages kelurahan */
		
		$where_wilayah ="";
		if($id > 0){
		
			if($tipe == "provinsi"){
				
			}
			$where_wilayah = " AND a.id = '".$id."'";
		}
		
		$data = $this->db->query("SELECT *,
								ROUND((jml_dpt*75)/100) AS asumsi_suara_75,
								ROUND((ROUND((jml_dpt*75)/100)*5)/100) AS asumsi_suara_5,
								ROUND((ROUND((jml_dpt*75)/100)/jml_paslon)+ROUND((ROUND((jml_dpt*75)/100)*5)/100)) target_perolehan_suara,
								-- round(((ROUND((ROUND((jml_dpt*75)/100)/jml_paslon)+ROUND((ROUND((jml_dpt*75)/100)*5)/100))*ROUND((jml_dpt*75)/100))/100)) target_menang_persen
								IFNULL(ROUND(((ROUND((ROUND((jml_dpt*75)/100)/jml_paslon)+ROUND((ROUND((jml_dpt*75)/100)*5)/100))/ROUND((jml_dpt*75)/100))*100),2),0) target_menang
								FROM 
								(
								SELECT a.pasangan_alias,a.kepala_alias,a.wakil_alias,a.`id_area`,a.area,a.image_paslon
								,IFNULL((
								SELECT COUNT(DISTINCT(a.id))
								FROM tps a, m_regencies b, m_provinces c
								WHERE a.id_kelurahan = b.id
								AND b.province_id = c.id
								AND c.id = d.`id`
								),0) AS jml_tps,
								(
								CASE WHEN d.`id` = 16 THEN
								(SELECT COUNT(1) FROM dpt_sumatera_selatan)
								ELSE 0 END 
								) jml_dpt,
								(
								SELECT COUNT(1) FROM 
								m_paslon
								WHERE a.`id_area` = id_area
								GROUP BY id_area
								) jml_paslon
								FROM m_paslon a ,m_parpol b, m_pengusung c, `m_provinces` d
								WHERE a.`id_paslon` = c.`id_paslon`
								AND b.`id_parpol` = c.`id_parpol`
								AND b.`id_parpol` = 3
								AND a.`id_area` = d.`id`
								".$where_wilayah."
								GROUP BY a.`id_paslon`
								)v")->result(); 
								
		
								
		return $data;
	}
	
	function get_tot_dpt($id,$nama,$tipe){ 
		
		if($id == 16)
		{ 
			$data = $this->db->query("SELECT COUNT(1) as tot_dpt FROM dpt_sumatera_selatan")->row("tot_dpt");
		}else{
			$data = 0;
		}
		return $data;
		
	}
	
	function get_paslon($id,$nama,$tipe){
		$where = '';
		if($tipe == 'provinsi'){
			$where .= "and id_provinsi='$id'"; 		
		}else if($tipe == 'kabupaten'){
			$where .= "and id_kabupaten='$id'";
			$id = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
		}else if($tipe == 'kecamatan'){
			$where .= "and id_kecamatan='$id'";
		}else if($tipe == 'kelurahan'){
			$where .= "and id_kelurahan='$id'";
		}

		if($id != 0){
			$data = $this->db->query("SELECT a.id_paslon, a.image_paslon, a.nama_kepala,a.kepala_alias,a.nama_wakil,a.wakil_alias,a.warna,b.p7_id,IFNULL(b.total,0) as total,IFNULL(b.jumlah,0) as jumlah 
			FROM m_paslon a 
			LEFT JOIN (SELECT count(*) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 $where) * 100 ),2) AS jumlah, p7_id, p7 from trans_survey_clean where 1=1 $where group by p7_id) b on a.id_paslon=b.p7_id
			where a.id_area='$id'
			order by jumlah desc")->result();

			$data2 = $this->db->query("SELECT a.id_paslon, a.image_paslon, a.nama_kepala,a.kepala_alias,a.nama_wakil,a.wakil_alias,a.warna,b.p7_id,IFNULL(b.total,0) as total,IFNULL(b.jumlah,0) as jumlah 
			FROM m_paslon a 
			LEFT JOIN (SELECT count(*) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 $where) * 100 ),2) AS jumlah, p7_id, p7 from trans_survey_clean where 1=1 $where group by p7_id) b on a.id_paslon=b.p7_id
			where a.id_area='1'
			order by jumlah desc")->result();
		}else{
			$data = [];
			$data2 = [];
		}
		$string = '';
		$j=0;
		foreach ($data as $key => $value) { 
			$pengusung = $this->db->query("select b.alias,b.picture from m_pengusung a
				inner join m_parpol b on a.id_parpol=b.id_parpol
				where id_paslon = $value->id_paslon")->result();
			$string .= '<div class="col-md-3">
					        <div class="element-box pad10">
					            <div class="fotocalon" style="text-align:center">
					                <img height="150" width="150" class="rounded-circle img-fluid" src="'.base_url().'/img/paslon/'.$value->image_paslon.'">
					            </div>
					            <br>
					            <div class="" align="center">
					                <b>'.$value->kepala_alias.'</b><br>
					                <b>'.$value->wakil_alias.'</b><br>
					                <br>';
			$i=0;
			foreach ($pengusung as $nilai) {
				if($i == count($pengusung)-1){
					$string .= $nilai->alias;	
				}else{
					$string .= $nilai->alias.' | ';
				}
				
			$i++;
			}

			// $nilai = $data[0]->jumlah;
			// // $nilai = $value->jumlah;
			// if(count($data)>1){
			// 	if($j==0){
			// 		$selisih = $nilai-$data[1]->jumlah;
			// 		if($selisih >= 5){
			// 			$status = '<label class="badge badge-success">Diprediksi Menang</label>';
			// 		}else if($selisih < 5){
			// 			$status = '<label class="badge badge-warning">Hati-hati</label>';
			// 		}
			// 	}else{
			// 		$selisih = $nilai - $value->jumlah ;
			// 		if($selisih < 5){
			// 			$status = '<label class="badge badge-warning">Hati-hati</label>';
			// 		}else if($selisih > 5){
			// 			$status = '<label class="badge badge-danger">Rawan</label>';
			// 		}
			// 	}
			// }else{
			// 	$status = '<label class="badge badge-success">Diprediksi Menang</label>';
			// }
			
			$jumlah = 0;
			if($value->jumlah){
				$jumlah = $value->jumlah;
			}
			
			$string .= '<br>
					                <span class="persentasecalon">
					                    '.$jumlah.'%
					                </span>
					            </div>
					        </div>
					    </div>';
			// $string .= '<br>
			// 		                <br>
			// 		                '.$status.'
			// 		                <br>
			// 		                <span class="persentasecalon">
			// 		                    '.$value->jumlah.'%
			// 		                </span>
			// 		            </div>
			// 		        </div>
			// 		    </div>';
		$j++;
		}
		$string .= '<div class="col-md-6"><div class="element-box pad10"><div class="col-md-12">';
		foreach ($data2 as $key => $value) {
			$string .= '<div class="os-progress-bar">
					  <div class="bar-labels">
					    <div class="bar-label-left"><span class="bigger" style="font-size:1rem;">'.$value->kepala_alias.'</div>
					    <div class="bar-label-right"><span class="info bigger" style="font-weight:bold;font-size:1rem;">'.$value->jumlah.' %</span></div>
					  </div>
					  <div class="bar-level-1" style="width: 100%">
					    <div class="bar-level-2" style="width: '.$value->jumlah.'%;background-color: #da251c;">
					      <div class="bar-level-3" style="width: 100%"></div>
					    </div>
					  </div>
					</div>';
		}
		$string .= '</div></div></div.';
		return $string;
	}

	function get_paslon_pilpres($id,$nama,$tipe){
		$data = $this->db->query("SELECT * FROM m_paslon a  where a.id_area='010'")->result();
		$string = '';
		$j=0;
		foreach ($data as $key => $value) { 
			$pengusung = $this->db->query("select b.alias,b.picture from m_pengusung a
				inner join m_parpol b on a.id_parpol=b.id_parpol
				where id_paslon = $value->id_paslon")->result();
			$string .= '<div class="col-md-6">
					        <div class="element-box pad10">
					            <div class="fotocalon" style="text-align:center">
					                <img height="350" width="350" class="rounded-circle img-fluid" src="'.base_url().'/img/paslon/'.$value->image_paslon.'">
					            </div>
					            <br>
					            <div class="" align="center">
					                <!--b>'.$value->kepala_alias.'</b><br>
					                <b>'.$value->wakil_alias.'</b><br>
					                <br-->';
			$i=0;
			foreach ($pengusung as $nilai) {
				if($i == count($pengusung)-1){
					$string .= '<font size="4">'.$nilai->alias.'</font>';	
				}else{
					$string .= '<font size="4">'.$nilai->alias.' | '.'</font>';
				}
				
			$i++;
			}
			
			$string .= '<br>
					            </div>
					        </div>
					    </div>';
		$j++;
		}
		
		return $string;
	}

	function get_link_map($id,$tipe){
		if($tipe == 'provinsi'){
			if($id == 0){
				$data = 'INDONESIA';
			}else{
				$result = $this->db->get_where('m_provinces', array('id' => $id))->row();
				if($result->name == 'BALI'){
					$result->name = 'BALI, INDONESIA';
				}
				$data = $result->name;
			}

		}else if($tipe == 'kabupaten'){
			$result = $this->db->query("SELECT a.id, a.name as kab, b.name as prov from m_regencies a inner join m_provinces b on b.id=a.province_id where a.id = $id")->row();
			$data = $result->kab.', '.$result->prov;
		}else if($tipe == 'kecamatan'){
			$result = $this->db->query("SELECT a.id, a.name as kec, b.name as kab, c.name as prov from m_districts a inner join m_regencies b on b.id=a.regency_id inner join m_provinces c on c.id=b.province_id where a.id=$id")->row();
			$data = $result->kec.', '.$result->kab.', '.$result->prov;
		}else if($tipe == 'kelurahan'){
			$result = $this->db->query("SELECT a.id, a.name as kel, b.name as kec, c.name as kab, d.name as prov from m_villages a inner join m_districts b on b.id=a.district_id inner join m_regencies c on c.id=b.regency_id inner join m_provinces d on d.id=c.province_id where a.id=$id")->row();
			$data = $result->kel.', '.$result->kec.', '.$result->kab.', '.$result->prov;
		}

		return $data;	
	}

	function get_jum_survey($id,$tipe){
		if($tipe == 'provinsi'){
			if($id == 0){
				$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean")->row('jml');
			}else{
				$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean where id_provinsi = '$id'")->row('jml');
			}
		}else if($tipe == 'kabupaten'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean where id_kabupaten = '$id'")->row('jml');
		}else if($tipe == 'kecamatan'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean where id_kecamatan = '$id'")->row('jml');
		}else if($tipe == 'kelurahan'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean where id_kelurahan = '$id'")->row('jml');
		} else if ($tipe == 'dapil') {
			$idProv ='';
			$idKab="";
			$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
			$i=0;
			foreach ($result as $key => $value) {
				$idProv = $value->id_provinces;
				if ($i==0) {
					$idKab = "'".$value->id_regency."'";
				} else {
					$idKab = $idKab.",'".$value->id_regency."'";
				}
				$i++;
			}

			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey_clean where id_provinsi = '$idProv' and id_kabupaten in (".$idKab.")")->row('jml');
			//$where .= "and id_provinsi= '$idProv' and id_kabupaten in (".$idKab.")";
		}
		return $data;
	}
	

	//MODAL
	function get_modalprofile($modal_name,$id,$nama,$tipe){
		
		if($modal_name == "modal_dpt"){
			$data['jum_jeniskelamin'] = $this->get_jeniskelamin($id,$tipe);
			$data['jum_usia'] = $this->get_usia($id,$tipe);
		}
		
		if($modal_name =="modal_ketokohan"){
			$data['list_ketokohan'] = $this->db->query("SELECT * from m_pertokohan where is_deleted = '0'")->result();
			$data['jum_ketokohan_group'] = $this->get_jmlketokohan_group($id,$tipe);
			$data['jum_ketokohan_partai'] = $this->get_jmlketokohan_partai($id,$tipe);
			$data['jum_ketokohan_id'] = $this->get_jmlketokohan_id($id,$tipe);
			$data['id'] = $id;
			$data['tipe'] = $tipe;
		}
		
		if($modal_name =="modal_survei"){
			
			
			$data['jml_pemilih'] = $this->Survei_model->get_data_survey_pemilih($id,$nama,$tipe);
			$data['jml_tidak_memilih'] = $this->Survei_model->get_data_survey_tidak_memilih($id,$nama,$tipe);
			$data['total_pilih'] =$this->Survei_model->get_jum_pilih($id,$nama,$tipe);
			$data['total_tidak_pilih'] =$this->Survei_model->get_jum_tidak_pilih($id,$nama,$tipe);
			
		}
		
		
		if($modal_name =="modal_parpol"){
			
			
			$data['jml_pemilih'] = $this->Survei_model->get_data_survey_pemilih($id,$nama,$tipe);
			$data['jml_tidak_memilih'] = $this->Survei_model->get_data_survey_tidak_memilih($id,$nama,$tipe);
			$data['total_pilih'] =$this->Survei_model->get_jum_pilih($id,$nama,$tipe);
			$data['total_tidak_pilih'] =$this->Survei_model->get_jum_tidak_pilih($id,$nama,$tipe);
			
		}
		
		if($modal_name =="modal_guraklih"){
			
			$data['jum_tps'] = $this->get_jmltps($id,$tipe);
			$data['jum_petugas'] = $this->get_jmlpetugas($id,$tipe);
			$data['jml_targetprospek'] = $this->get_jmltargetprospek($id,$tipe);
			$data['jml_perolehan'] = $this->get_jmlperolehan($id,$tipe);
			$total = ($data['jml_targetprospek']+$data['jml_perolehan']);
			
			$data['persen_prospek'] =0;
			$data['persen_perolehan'] =0;
			if($data['jum_petugas'] > 0)
			{
				if($total == 0){
					$data['persen_prospek'] = 0;
					$data['persen_perolehan'] = 0;
				}else{
					$data['persen_prospek'] = (($data['jml_targetprospek']/$total)*100);
					$data['persen_perolehan'] = (($data['jml_perolehan']/$total)*100);
				}
				
			}
		}
		
		if($modal_name =="modal_simulasi"){
			$data['paslon'] = $this->get_paslon($id,$nama,$tipe);
		}

		$asumsi_suara = round($this->get_jmldpt($id,$tipe)*0.75);
		$data['jum_dpt'] = $this->get_jmldpt($id,$tipe);
		$data['asumsi_suara_sah'] = $asumsi_suara ;
		$data['target_suara'] = round(($asumsi_suara/2)+($asumsi_suara *0.05));
		$data['jum_ketokohan'] = $this->get_jmlketokohan($id,$tipe);
		$data['jml_perolehan'] = $this->get_jmlperolehan($id,$tipe);
		$data['jokowi'] = $this->get_suara_pilpres($id,$tipe,'jokowi','2014');
		$data['prabowo'] = $this->get_suara_pilpres($id,$tipe,'prabowo','2014');
		$data['jum_guraklih'] = $this->get_jmlguraklih($id,$tipe);
		
		return $data;
	}
	
	function get_jeniskelamin($id,$tipe){
		if($id != 0){
			$where = '';
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');	
			}else if($tipe == 'kabupaten'){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKab = $id";
			}else if($tipe == 'kecamatan'){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKec = $id";
			}else if($tipe == 'kelurahan'){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKel = $id";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row('tabel_demo');
				$where .= "and idKab in ($idKab)";
			}
			if($tabel){
				$data = $this->db->query("SELECT IFNULL(SUM(jns_laki),0) AS laki, IFNULL(SUM(jns_perempuan),0) wanita from $tabel where 1=1 ".$where)->row_array();	
			}else{
				$data['laki'] = 0;
				$data['wanita'] = 0;	
			}
			
		}else{
			$master = $this->db->get('m_dpt')->result();
			$data['laki'] = 0;
			$data['wanita'] = 0;
			foreach ($master as $key => $value) {
				$tabel = $value->tabel_demo;
				if($tabel != NULL){
					$hasil = $this->db->query("SELECT IFNULL(SUM(jns_laki),0) AS laki, IFNULL(SUM(jns_perempuan),0) wanita from $tabel")->row_array();
					$data['laki'] += $hasil['laki'];
					$data['wanita'] += $hasil['wanita'];
				}
			}
		}
		return $data;
	}
	
	function get_usia($id,$tipe){
		if($id != 0){
			$where = '';
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');	
			}else if($tipe == 'kabupaten'){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKab = $id";
			}else if($tipe == 'kecamatan'){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKec = $id";
			}else if($tipe == 'kelurahan'){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_demo');
				$where = "and idKel = $id";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row('tabel_demo');
				$where .= "and idKab in ($idKab)";
			}

			if($tabel){
				$data = $this->db->query("SELECT IFNULL(SUM(usia1),0) as jml_1,IFNULL(SUM(usia2),0) as jml_2,IFNULL(SUM(usia3),0) as jml_3,IFNULL(SUM(usia4),0) as jml_4,IFNULL(SUM(usia5),0) as jml_5 from $tabel where 1=1 ".$where)->row_array();
			}else{
				$data['jml_1'] = 0;
				$data['jml_2'] = 0;
				$data['jml_3'] = 0;
				$data['jml_4'] = 0;
				$data['jml_5'] = 0;
			}
		}else{
			$master = $this->db->get('m_dpt')->result();
			$data['jml_1'] = 0;
			$data['jml_2'] = 0;
			$data['jml_3'] = 0;
			$data['jml_4'] = 0;
			$data['jml_5'] = 0;
			foreach ($master as $key => $value) {
				$tabel = $value->tabel_demo;
				if($tabel != NULL){
					$hasil = $this->db->query("SELECT IFNULL(SUM(usia1),0) as jml_1,IFNULL(SUM(usia2),0) as jml_2,IFNULL(SUM(usia3),0) as jml_3,IFNULL(SUM(usia4),0) as jml_4,IFNULL(SUM(usia5),0) as jml_5 from $tabel")->row_array();
					$data['jml_1'] += $hasil['jml_1'];
					$data['jml_2'] += $hasil['jml_2'];
					$data['jml_3'] += $hasil['jml_3'];
					$data['jml_4'] += $hasil['jml_4'];
					$data['jml_5'] += $hasil['jml_5'];
				}
			}
		}
		return $data;
		
		// $data = $this->db->query("SELECT IFNULL(SUM(jml_1),0) as jml_1,IFNULL(SUM(jml_2),0) as jml_2,IFNULL(SUM(jml_3),0) as jml_3,IFNULL(SUM(jml_4),0) as jml_4,IFNULL(SUM(jml_5),0) as jml_5 FROM sum_umur where 1=1 ".$where_wilayah)->row();
		// return $data;
	}
	
	
	function get_jmldpt($id,$tipe){
		$jum_dpt = 0;
		$where = '';
		if($id!=0){
			if($tipe=="provinsi"){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
			}else if($tipe =="kabupaten"){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
			}else if($tipe =="kecamatan"){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
			}else if($tipe =="kelurahan"){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
			} else if ($tipe=='dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					
					$i++;
				}
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row('tabel_sum');
				$where .= "and idKab in ($idKab)";
				//echo $tabel;die();

			}
			if($tabel != NULL){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');
			}else{
				$jum_dpt = 0;
			}
			
		}else{
			$data = $this->db->get('m_dpt')->result();
			foreach ($data as $key => $value) {
				if ($value->tabel_sum != NULL) {
					$table = $value->tabel_sum;
					$jum= $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $table")->row('jumlah');	
					$jum_dpt = $jum_dpt+$jum;
				}
			}
		}
		
		return $jum_dpt;
		
	}
	
	function get_jmlketokohan($id,$tipe){
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_city=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_districts=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			}else if ($tipe =="dapil") {
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}

				$where_wilayah = " and id_city in(".$idKab.")";
			}
		}
		
		
		$data = $this->db->query("SELECT IFNULL(COUNT(1),0) as jml FROM intangible WHERE 1=1 and is_deleted  = '0'  ".$where_wilayah)->row('jml');
		$data = number_format($data);
		return $data;
		
	}
	
	function jum_ketokohan_partai($id,$tipe){
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_city=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_districts=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			}
		}
		
		
		$data = $this->db->query("SELECT IFNULL(COUNT(1),0) as jml,`type` FROM intangible WHERE 1=1 and is_deleted  = '0' GROUP BY `type` ".$where_wilayah)->row('jml');
		$data = number_format($data);
		return $data;
		
	}
	
	function get_jmlsurvey($id,$tipe){
		
		$where_wilayah = "";
		if($id > 0){ 
			$where_wilayah = " and id_area = ".$id;
		}
		
		$data = $this->db->query("SELECT IFNULL(COUNT(1),0) as jml FROM trans_survey WHERE id_area<>0 ".$where_wilayah)->row('jml');
		$data = number_format($data);
		return $data;
	}
	
	function get_jmlguraklih($id,$tipe){
		
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_regency=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_district=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			} else if ($tipe =="dapil") {
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}
				$where_wilayah = " and id_regency in (".$idKab.")";
			}
		}
		
		
		$data = $this->db->query("SELECT IFNULL(COUNT(1),0) AS jml FROM m_team WHERE `type` = 'kortps' and is_deleted = '0' ".$where_wilayah)->row('jml');
		$data = number_format($data);
		return $data;
	}
	
	function get_jmlketokohan_group($id,$tipe){
		
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_city=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_districts=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			} else if ($tipe =="dapil") {
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}
				$where_wilayah = " and id_city in (".$idKab.")";
			}
		}
		
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'".$where_wilayah)->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'".$where_wilayah)->row('jml');
		return $data;
	}
	function get_jmlketokohan_id($id,$tipe){
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_city=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_districts=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			} else if ($tipe =="dapil") {
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}
				$where_wilayah = " and id_city in (".$idKab.")";
			}
		}

		$data = $this->db->query("SELECT ROUND(( count(1)/(SELECT COUNT(1) FROM intangible WHERE is_deleted='0' $where_wilayah) * 100 ),2) AS  percentage , count(1) as jumlah, a.id_pertokohan, b.nama from intangible a
			left join m_pertokohan b on a.id_pertokohan = b.id_pertokohan
			where a.is_deleted='0' $where_wilayah
			group by a.id_pertokohan order by jumlah desc")->result();
		return $data;
	}
	function get_jmlketokohan_partai($id,$tipe){ 
		
		$where_wilayah="";
		if($id!=0){
			$where_wilayah=" and area = '$id' ";
		}
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_city=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_districts=".$id;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " and id_village=".$id;
			} else if ($tipe =="dapil") {
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}
				$where_wilayah = " and id_city in (".$idKab.")";
			}
		}

		$data['partai'] = $this->db->query("SELECT ROUND(( count(1)/(SELECT count(1) FROM intangible where is_deleted='0' and inclination != 0 $where_wilayah ) * 100 ),2) AS percentage, count(1) as jumlah,a.inclination,b.alias,b.color from intangible a
				left join m_parpol b on a.inclination = b.id_parpol
				where a.is_deleted='0' and inclination != 0 $where_wilayah
				group by inclination order by jumlah desc limit 10")->result();
		$data['netral'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination=0 $where_wilayah")->row('jml');
		$data['pilih'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted='0' and inclination!=0 $where_wilayah")->row('jml');
				
		return $data;
	}
	
	function get_jmltps($id,$tipe){
		
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_regency=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_district=".$id;
			}else if($tipe == 'kelurahan'){
				$where_wilayah = " and id_village=".$id;
			}
		}
		
		$data = $this->db->query("SELECT COUNT(tps) AS jml_tps FROM m_team_has_tps a, m_team b WHERE a.id_team = b.id ".$where_wilayah)->row('jml_tps');
		return $data;
	}
	
	function get_jmlpetugas($id,$tipe){
		
		
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_regency=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_district=".$id;
			}else if($tipe == 'kelurahan'){
				$where_wilayah = " and id_village=".$id;
			}
		}
		
		$data = $this->db->query("SELECT COUNT(1) AS jml_petugas FROM  m_team WHERE `type` = 'kortps' and is_deleted = '0' ".$where_wilayah)->row('jml_petugas');
		return $data;
	}
	
	function get_jmltargetprospek($id,$tipe){
		
		$where_wilayah="";
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " and id_provinces=".$id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " and id_regency=".$id;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " and id_district=".$id;
			}else if($tipe == 'kelurahan'){
				$where_wilayah = " and id_village=".$id;
			}
		}
		
		$data = $this->db->query("SELECT IFNULL(SUM(target),0) AS jml_targetprospek
									FROM target_pemilih
									WHERE is_deleted = '0' ".$where_wilayah)->row('jml_targetprospek');
		return $data;
	}
	
	
	function get_jmlperolehan($id,$tipe){
		
		$where_wilayah="";
		$idarea = "";
		$total = 0;
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = "";
				$idarea = $id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " WHERE idKab=".$id;
				$idarea = substr($id, 0,2) ;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " WHERE idKec=".$id;
				$idarea = substr($id, 0,2) ;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " WHERE idKel=".$id;
				$idarea = substr($id, 0,2) ;
			} else if ($tipe =="dapil") {
				$idProv ='';
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					$i++;
				}

				$idarea = $idProv;
				$where_wilayah = " WHERE idKab in (".$idKab.")";
			}

			$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $idarea))->row('tabel_sum');

			if ($tabelSum!=NULL) {
				$data = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah)->row('jml_pemilih');
				return $data;
			}
		} else {
			$data = $this->db->query("SELECT sum(jml_prospek) as  jml_pemilih from v_prospek")->row('jml_pemilih');
			return $data;
		}
	}

	function get_suara_pilpres($id,$tipe,$column,$years){
		
		$where_wilayah="";
		$idarea = "";
		$total = 0;
		if($id!=0){
			if($tipe=="provinsi"){
				$where_wilayah = " WHERE idProv=".$id." AND periode='".$years."'";
				$idarea = $id;
			}else if($tipe =="kabupaten"){
				$where_wilayah = " WHERE idKab=".$id." AND periode='".$years."'";
				$idarea = substr($id, 0,2) ;
			}else if($tipe =="kecamatan"){
				$where_wilayah = " WHERE idKec=".$id." AND periode='".$years."'";
				$idarea = substr($id, 0,2) ;
			}else if($tipe =="kelurahan"){
				$where_wilayah = " WHERE idKel=".$id." AND periode='".$years."'";
				$idarea = substr($id, 0,2) ;
			} else if ($tipe =="tps") {
				$data = explode("_" , $id);

				$where_wilayah = " WHERE idKel=".$data[0]." AND tps=".$data[1]." AND periode='".$years."'";
				$idarea = substr($id, 0,2) ;
			} else if ($tipe =="dapil") {
				$idProv ='';
				$idKab='';
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = $value->id_regency;
					} else {
						$idKab = $idKab.','.$value->id_regency;
					}
					
					$i++;
				}

				$where_wilayah = " WHERE idKab in (".$idKab.") AND periode='".$years."'";
				$idarea = $idProv;
			}

			$data = $this->db->query("SELECT sum(".$column.") as ".$column." from suara_pilpres ".$where_wilayah)->row($column);
			return $data;
		} else {
			$data = $this->db->query("SELECT sum(".$column.") as  ".$column." from suara_pilpres WHERE periode='".$years."'")->row($column);
			return $data;
		}
	}
	
	function get_data_tab_ketokohan(){
		
		$data['jum_dpt'] = $this->db->query("SELECT COUNT(*) as jml FROM dpt_sumatera_selatan")->row('jml');
		$data['jum_influencer'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0'")->row('jml');
		$data['jum_tangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
		$data['jum_intangible'] = $this->db->query("SELECT COUNT(*) as jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
		$data['provinsi']=$this->db->get('m_provinces')->result();
		return $data;
		//$this->load->view('dashboard',$data);
	}
	
	
	


}