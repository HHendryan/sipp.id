<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Dashboard_model extends CI_Model {
	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_data_dashboard(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		return $data;
	}

	function detail_area($data){
		extract($data,EXTR_OVERWRITE);
		$table= $this->get_table($tipe);
		
		// $data = $this->db->query("SELECT * from $table where id=$id")->row();
		$data = $this->db->get_where($table, array('id' => $id))->row();
		return $data;
	}

	function get_tree($tipe,$id){
		$table= $this->get_table($tipe);
		$parent_table = $this->get_parent_table($tipe);
		$result = $this->db->get_where($table, array($parent_table => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['lazy'] = TRUE;
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = $tipe;
			$data[$i]['logo'] = '$value->logo';
			$i++;
		}
		return $data;
	}

	function get_data_tab_profile($id,$nama,$tipe,$logo){
		$data['link'] = $this->get_link_map($id,$tipe);
		$data['jum_survey'] = $this->get_jum_survey($nama,$tipe);
		return $data;
	}

	function get_link_map($id,$tipe){
		if($tipe == 'provinsi'){
			if($id == 0){
				$data = 'INDONESIA';
			}else{
				$result = $this->db->get_where('m_provinces', array('id' => $id))->row();
				if($result->name == 'BALI'){
					$result->name = 'BALI, INDONESIA';
				}
				$data = $result->name;
			}

		}else if($tipe == 'kabupaten'){
			$result = $this->db->query("SELECT a.id, a.name as kab, b.name as prov from m_regencies a inner join m_provinces b on b.id=a.province_id where a.id = $id")->row();
			$data = $result->kab.', '.$result->prov;
		}else if($tipe == 'kecamatan'){
			$result = $this->db->query("SELECT a.id, a.name as kec, b.name as kab, c.name as prov from m_districts a inner join m_regencies b on b.id=a.regency_id inner join m_provinces c on c.id=b.province_id where a.id=$id")->row();
			$data = $result->kec.', '.$result->kab.', '.$result->prov;
		}else if($tipe == 'kelurahan'){
			$result = $this->db->query("SELECT a.id, a.name as kel, b.name as kec, c.name as kab, d.name as prov from m_villages a inner join m_districts b on b.id=a.district_id inner join m_regencies c on c.id=b.regency_id inner join m_provinces d on d.id=c.province_id where a.id=$id")->row();
			$data = $result->kel.', '.$result->kec.', '.$result->kab.', '.$result->prov;
		}

		return $data;	
	}

	function get_jum_survey($nama,$tipe){
		if($tipe == 'provinsi'){
			if($nama == 'Nasional'){
				$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey")->row('jml');
			}else{
				$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey where provinsi = '$nama'")->row('jml');
			}
		}else if($tipe == 'kabupaten'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey where kotaKabupaten = '$nama'")->row('jml');
		}else if($tipe == 'kecamatan'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey where kecamatan = '$nama'")->row('jml');
		}else if($tipe == 'kelurahan'){
			$data = $this->db->query("SELECT COUNT(1) as jml from trans_survey where desaKelurahan = '$nama'")->row('jml');
		}
		return $data;
	}
	
	function get_modalprofile($modal_name,$id,$nama,$tipe){
		
		if($modal_name == "modal_dpt"){
			$data['jum_jeniskelamin'] = $this->get_jeniskelamin($id,$tipe);
			$data['jum_usia'] = $this->get_usia($id,$tipe);
		}
		
		return $data;
	}
	
	
	function get_jeniskelamin(){
		
		$data = $this->db->query("SELECT sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin")->row();
		return $data;
	}
	
	function get_usia(){
		
		$data = $this->db->query("select SUM(jml_1) as jml_1,SUM(jml_2) as jml_2,SUM(jml_3) as jml_3,SUM(jml_4) as jml_4,SUM(jml_5) as jml_5 FROM sum_umur")->row();
		return $data;
	}
}