<?php
class Map_model extends CI_Model {
	function __construct(){
		parent::__construct(); 
		$this->load->library('table');
	}

    public function getProv() {
        $query = $this->db->get('province'); // db location table
        return $query->result();
    }
	
	public function getKab(){
		$query = $this->db->get( 'kabupaten' ); // db location table
		return $query->result();
	}
	
	public function getOneKab($id){
		$this->db->select('*');
		$this->db->from('kabupaten');
		$this->db->join('province', 'id.id=province_id.id', 'inner');
		$this->db->where('id.id', $id);  // Also mention table name here
		$query = $this->db->get();
		if($query->num_rows() > 0)
        return $data->result();
	}
	
	public function getOneProv($id){
		$query = $this->db->get_where('province', array('id' => $id));
		return $query->result();
	}
}
?>