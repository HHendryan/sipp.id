<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Survei_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Dashboard_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }

	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}

		return $table;
	}
	
	function save_note($id,$nama,$tipe,$text,$method){  
		
		if($method=="save"){
			
			$data2 = $this->db->query("INSERT INTO notes (id_area,tipe,nama,notes )VALUES('$id','$tipe','$nama','$text')  ON DUPLICATE KEY UPDATE notes = '$text' ");
			$data = $this->db->query("select notes from notes where id_area = '$id' ")->row("notes");
		}else{
			
			$data = $this->db->query("select notes from notes where id_area = '$id' ")->row("notes");
			
		}
		echo json_encode($data);  
	}

	function thousandsCurrencyFormat($num) {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('K', 'M', 'B', 'T');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_data($id,$nama,$tipe,$logo){
		$data['pendidikan'] = $this->get_data_survey('p10',$id,$nama,$tipe);
		$data['agama'] = $this->get_data_survey('p14',$id,$nama,$tipe);
		$data['suku'] = $this->get_data_survey('p15',$id,$nama,$tipe);
		$data['organisasi'] = $this->get_data_survey_2('p16',$id,$nama,$tipe);
		$data['sosial_media'] = $this->get_data_survey('p17',$id,$nama,$tipe);
		$data['pengeluaran'] = $this->get_data_survey('p13',$id,$nama,$tipe);
		$data['p1'] = $this->get_data_survey_2('p1',$id,$nama,$tipe);
		$data['p2'] = $this->get_data_survey_2('p2',$id,$nama,$tipe);
		$data['jum_survei'] = $this->Dashboard_model->get_jum_survey($id,$tipe);
		$data['pres_14_pilih'] = $this->get_data_survey_5('p5',$id,$nama,$tipe);
		$data['pres_14_tdk_pilih'] = $this->get_data_survey_6('p5',$id,$nama,$tipe);
		$data['pres_19_pilih'] = $this->get_data_survey_7('p6',$id,$nama,$tipe);
		$data['pres_19_tdk_pilih'] = $this->get_data_survey_8('p6',$id,$nama,$tipe);
		$data['partai_14'] = $this->get_data_survey_4('p3',$id,$nama,$tipe);
		$data['partai_19'] = $this->get_data_survey_4('p4',$id,$nama,$tipe);
		$data['email'] = $this->get_data_survey_3('p17a',$id,$nama,$tipe);
		$data['facebook'] = $this->get_data_survey_3('p17b',$id,$nama,$tipe);
		$data['twitter'] = $this->get_data_survey_3('p17c',$id,$nama,$tipe);
		$data['instagram'] = $this->get_data_survey_3('p17d',$id,$nama,$tipe);
		$data['lainnya'] = $this->get_data_survey_3('p17e',$id,$nama,$tipe);
		$data['gubernur'] = $this->get_data_gubernur($id,$nama,$tipe);
		$data['change'] = $this->get_change_from_pdip($id,$nama,$tipe);

		$asumsi_suara = round($this->Dashboard_model->get_jmldpt($id,$tipe)*0.75);
		$data['jum_dpt'] = $this->Dashboard_model->get_jmldpt($id,$tipe);
		$data['asumsi_suara_sah'] = $asumsi_suara ;
		$data['target_suara'] = round(($asumsi_suara/2)+($asumsi_suara *0.05));
		$data['jum_ketokohan'] = $this->Dashboard_model->get_jmlketokohan($id,$tipe);
		$data['jml_perolehan'] = $this->Dashboard_model->get_jmlperolehan($id,$tipe);
		$data['jokowi'] = $this->Dashboard_model->get_suara_pilpres($id,$tipe,'jokowi','2014');
		$data['prabowo'] = $this->Dashboard_model->get_suara_pilpres($id,$tipe,'prabowo','2014');
		$data['jum_guraklih'] = $this->Dashboard_model->get_jmlguraklih($id,$tipe);
		// $data['gubernur_tidak'] = $this->get_data_gubernur_tdk_pilih($nama);
		return $data;
	}

	function get_change_from_pdip($id,$nama,$tipe){
		$where ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}

				$where .= "and id_provinsi= '$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$data = $this->db->query("SELECT count(1) as total,round(( count(1)/(SELECT count(1) from trans_survey_clean where p3 = 31 and p4 <> 48 $where) * 100 ),2) as percentage, p3,p4, b.nama, c.color,c.picture from trans_survey_clean a
			left join m_pilih b on a.p4 = b.id
			left join m_parpol c on b.nama = c.alias
			where p3 = 31 and p4 <> 48 $where
			group by p4
			order by FIELD(b.nama, 'Belum Memilih','Tidak Akan Memilih','Rahasia','Tidak Menjawab','Tidak Tahu','Belum Menentukan Pilihan','Lupa'), total desc")->result();
		return $data;
	}

	function get_data_gubernur($id,$nama,$tipe){
		$where = '';
		if($tipe == 'provinsi'){
			$where .= "and id_provinsi='$id'"; 		
		}else if($tipe == 'kabupaten'){
			$where .= "and id_kabupaten='$id'";
			$id = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
		}else if($tipe == 'kecamatan'){
			$where .= "and id_kecamatan='$id'";
		}else if($tipe == 'kelurahan'){
			$where .= "and id_kelurahan='$id'";
		} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
		}
		$data = $this->db->query("SELECT a.nama_kepala,a.kepala_alias,a.nama_wakil,a.wakil_alias,a.warna,b.p7_id,IFNULL(b.total,0) as total,IFNULL(b.jumlah,0) as jumlah 
			FROM m_paslon a 
			LEFT JOIN (SELECT count(*) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 $where) * 100 ),2) AS jumlah, p7_id, p7 from trans_survey_clean where 1=1 $where group by p7_id) b on a.id_paslon=b.p7_id
			where a.id_area='$id' OR a.id_area='1'
			order by FIELD(kepala_alias, 'BELUM MENENTUKAN PILIHAN','RAHASIA','TIDAK AKAN MEMILIH'),jumlah desc")->result();
		return $data;
	}

	//TOTAL DATA
	function get_data_survey($jenis,$id,$nama,$tipe){
		$where ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}

		}
		$text = 'a.'.$jenis;
		if($jenis == 'p13'){
			$jenis = 'b.nama_alias';
		}
		$data = $this->db->query("SELECT count(1) as total, COUNT(*) as jumlah, IFNULL(b.nama,'Tidak Mengisi') as nama,IFNULL(b.nama_alias,'Tidak Mengisi') as nama_alias from trans_survey_clean a
			left join m_pilih b on $text=b.id where 1=1
			".$where."
			group by $jenis")->result();
		return $data;
	}

	function get_data_survey_2($jenis,$id,$nama,$tipe){
		$where ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,b.nama,b.nama_alias from trans_survey_clean a
									left join m_pilih b on $text=b.id
									where 1=1 ".$where."
									group by $jenis 
									order by FIELD(b.nama_alias, 'lainnya','Tidak Tahu atau Tidak Jawab'),FIELD(b.nama, 'Tidak Tahu/Tidak Menjawab','Bukan bagian dari organisasi islam manapun','Lainnya'),jumlah desc")->result();
		return $data;
	}

	// FIELD(b.nama, 'Lainnya','Tidak Tahu/Tidak Menjawab','Bukan bagian dari organisasi islam manapun')


	function get_data_survey_3($jenis,$id,$nama,$tipe){
		$where ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as jumlah from trans_survey_clean where $jenis!= '' ".$where."")->row('jumlah');
		if($data == NULL){
			$data = 0;
		}
		return $data;
	}

	function get_data_survey_4($jenis,$id,$nama,$tipe){
		$where ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total,round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,IFNULL(b.nama,'Tidak Mengisi') as nama,IFNULL(b.nama_alias,'Tidak Mengisi') as nama_alias, c.color,c.picture from trans_survey_clean a
									left join m_pilih b on $text=b.id
									left join m_parpol c on c.alias=b.nama
									where 1=1 ".$where."
									group by $jenis order by FIELD(b.nama, 'Belum Memilih','Tidak Akan Memilih','Tidak Memilih','Rahasia','Tidak Menjawab','Tidak Tahu','Belum Menentukan Pilihan','Lupa'), jumlah desc")->result();
		return $data;
	}

	function get_data_survey_5($jenis,$id,$nama,$tipe){
		$where ='';
		$where2 ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,b.nama,b.nama_alias from trans_survey_clean a
									left join m_pilih b on $text=b.id
									where b.id <> 162 and b.id <> 163 and b.id <> 236 ".$where."
									group by $jenis order by jumlah desc")->result();
		return $data;
	}

	function get_data_survey_6($jenis,$id,$nama,$tipe){
		$where ='';
		$where2 ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			}  else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total,round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,b.nama,b.nama_alias from trans_survey_clean a
									left join m_pilih b on $text=b.id
									where (b.id = 162 or b.id = 163 or b.id = 236) ".$where."
									group by $jenis order by jumlah desc")->result();
		return $data;
	}

	function get_data_survey_7($jenis,$id,$nama,$tipe){
		$where ='';
		$where2 ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			}  else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,b.nama,b.nama_alias from trans_survey_clean a
									left join m_pilih b on $text=b.id
									where b.id <> 225 and b.id <> 218 and b.id <> 206 and b.id <> 226 and b.id <> 205 and b.id <> 227 ".$where."
									group by $jenis order by jumlah desc")->result();
		return $data;
	}

	function get_data_survey_8($jenis,$id,$nama,$tipe){
		$where ='';
		$where2 ='';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$text = 'a.'.$jenis;
		$data = $this->db->query("SELECT count(1) as total,round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah,b.nama,b.nama_alias from trans_survey_clean a
									left join m_pilih b on $text=b.id
									where (b.id = 225 or b.id = 218 or b.id = 206 or b.id = 226 or b.id = 205 or b.id = 227) ".$where."
									group by $jenis order by jumlah desc")->result();
		return $data;
	}

	function get_data_survey_pemilih($id,$nama,$tipe){
		$where = '';
		$where2 = '';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$data = $this->db->query("select count(1) as total, c.color,c.picture, round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah, b.nama  
			from trans_survey_clean a
			inner join m_pilih b on a.p4 = b.id
			left join m_parpol c on c.alias = b.nama
			where b.id <> 60 and b.id <> 61 and b.id <> 62 and b.id <> 63 and b.id <> 64 and b.id <> 65 ".$where."
			group by p4 order by jumlah desc")->result();
		return $data;
	} 
	function get_data_survey_tidak_memilih($id,$nama,$tipe){
		$where = '';
		$where2 = '';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			}  else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$data = $this->db->query("select round(( count(1)/(SELECT count(1) from trans_survey_clean where 1=1 ".$where.") * 100 ),2) as jumlah, b.nama  
			from trans_survey_clean a
			inner join m_pilih b on a.p4 = b.id
			where (b.id = 60 OR b.id = 61 OR b.id = 62 OR b.id = 63 OR b.id = 64 OR b.id = 65) ".$where."
			group by p4 order by jumlah desc")->result();
		return $data;
	} 
	function get_jum_pilih($id,$nama,$tipe){
		$where = '';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}
		$data = $this->db->query("SELECT count(1) as total, count(1) as jumlah from trans_survey_clean where p4 <> 60 and p4 <> 61 and p4 <> 62 and p4 <> 63 and p4 <> 64 and p4 <> 65 ".$where."")->row('jumlah');
		return $data;
	}
	function get_jum_tidak_pilih($id,$nama,$tipe){
		$where = '';
		if($id != 0){
			if($tipe == 'provinsi'){
				$where .= "and id_provinsi='$id'"; 		
			}else if($tipe == 'kabupaten'){
				$where .= "and id_kabupaten='$id'";
			}else if($tipe == 'kecamatan'){
				$where .= "and id_kecamatan='$id'";
			}else if($tipe == 'kelurahan'){
				$where .= "and id_kelurahan='$id'";
			} else if ($tipe == 'dapil') {
				$idProv ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$idProv = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}
				
				$where .= "and id_provinsi='$idProv' and id_kabupaten in (".$idKab.")";
			}
		}	
		$data = $this->db->query("SELECT count(1) as total, count(1) as jumlah from trans_survey_clean where (p4 = 60 OR p4 = 61 OR p4 = 62 OR p4 = 63 OR p4 = 64 OR p4 = 65) ".$where."")->row('jumlah');
		return $data;
	}

}