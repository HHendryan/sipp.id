 <div class="row">
    <div class="col-md-6">
        <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_dpt','Data Pemilih Tetap');" style="padding-left: 3px;padding-right: 3px;text-align:center;">
            
            <div class=" centered ">
                <div class="label">Data Pemilih Tetap</div>
                <div class="value" id="value_dpt"><?= $jum_dpt ?></div>
            </div>
      
        </a>                                           
    </div>
    <!-- Ketokohan -->
	<div class="col-md-6">
        <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_ketokohan','Data Influencer');" style="padding-left: 3px;padding-right: 3px;text-align:center;">
          
                <div class="centered ">
                    <div class="label">Influencer</div>
                    <div class="value" id="value_ketokohan"><?= $jum_ketokohan ?></div>
                </div>
           
        </a>                                       
    </div>
    <!-- Survei -->
    <div class="col-md-6">
        <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_survei','Survei Key Opinion Leader');" style="padding-left: 3px;padding-right: 3px;text-align:center;">
        
            <div class="centered ">
                <div class="label">Survei</div>
                <div class="value" id="value_survei"><?= number_format($jum_survei) ?></div>
       
        </div>
        </a>
    </div>
    <!-- Guraklih -->
    <div class="col-md-6">
        <a href="#" class="element-box el-tablo" data-target="#modal_profile" data-toggle="modal" onclick="load_modal_profile('modal_guraklih','Detail Guraklih');" style="padding-left: 3px;padding-right: 3px;text-align:center;">
           
                <div class="centered ">
                    <div class="label">Guraklih</div>
                    <div class="value" id="value_guraklih"><?= $jum_guraklih ?></div>
                </div>
         
        </a>
    </div>
</div>
<div class="row">
    <!--
	<div class="col-md-4">
        <div class="element-box">
			<iframe id="frame" height="260" frameborder="0" style="border:0; width: 100%" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyARSxVnQPIC6yvDqFNwpy0Ym5sMpRk-dSs&amp;q=INDONESIA" allowfullscreen="">
            </iframe>
		</div>
    </div>
	-->
    <?php 
        if($id !=0){ ?>
            <div class="col-sm-12" >
                <div class="element-wrapper" >
                  <h6 class="element-header">SURVEI PILPRES 2019</h6>
                  <div class="element-box" style="min-height: 430px">
                    <div class="row">
                      <div class="col-md-6">
                        <table style="width: 100%">
                          <?php $i=0;$pilres2=''; foreach ($survey_pilpres_19 as $key => $value) { if($i<5){?>
                              <tr>
                                <td>
                                  <div class="fotocalon" style="text-align:center">
                                      <img class="rounded-circle img-fluid" src="<?=base_url();?>/img/paslon/<?=$value->img_paslon?>" height='40' width="40">
                                  </div>                              
                                </td>
                                <td>
                                  <div class="os-progress-bar danger">
                                    <div class="bar-labels">
                                      <div class="bar-label-left"><span class="bigger"><?= $value->nama ?></span></div>
                                      <div class="bar-label-right"><span class="info bigger"><?= $value->jumlah ?> %</span></div>
                                    </div>
                                    <div class="bar-level-1" style="width: 100%">
                                      <div class="bar-level-2" style="width: <?= $value->jumlah ?>%">
                                        <div class="bar-level-3" style="width: 100%"></div>
                                      </div>
                                    </div>
                                  </div>

                                </td>
                              </tr>
                          <?php $i++; }else{
                              
                                      $pilres2.='<tr>
                                                    <td>
                                                      <div class="fotocalon" style="text-align:center">
                                                          <img class="rounded-circle img-fluid" src=img/paslon/'.$value->img_paslon.' height="40" width="40">
                                                      </div>                              
                                                    </td>
                                                    <td>
                                                      <div class="os-progress-bar danger">
                                                        <div class="bar-labels">
                                                          <div class="bar-label-left"><span class="bigger">'.$value->nama.'</span></div>
                                                          <div class="bar-label-right"><span class="info bigger">'.$value->jumlah.'%</span></div>
                                                        </div>
                                                        <div class="bar-level-1" style="width: 100%">
                                                          <div class="bar-level-2" style="width:'.$value->jumlah.'%">
                                                            <div class="bar-level-3" style="width: 100%"></div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      </td>
                                                  </tr>';
                                          }
                            } ?>   
                        </table>               
                      </div>
                      <div class="col-md-6">
                         <table style="width: 100%">
                          <?=$pilres2;?>  
                        </table>                             
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        <?php}else{ ?>
            <div class="col-md-12">
                <div class="row" id="list_paslon">
                    <?= $paslon ?>
                </div>
            </div>
        <?php}
    ?>
    <!-- <div class="col-md-2">
        <div class="element-box pad10">
            <div class="fotocalon">
                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
            </div>
            <br>
            <div class="" align="center">
                Bambang<br>
                <br>
                PDI, Golkar, Hanura<br>
                <br>
                <label class="badge badge-danger">Rawan</label>
                <label class="badge badge-warning">Hati-hati</label>
                <label class="badge badge-success">Diprediksi Menang</label>
                <br>
                <span class="persentasecalon">
                    70%
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="element-box pad10">
            <div class="fotocalon">
                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
            </div>
            <br>
            <div class="" align="center">
                Bambang<br>
                <br>
                PDI, Golkar, Hanura<br>
                <br>
                <label class="badge badge-danger">Rawan</label>
                <label class="badge badge-warning">Hati-hati</label>
                <label class="badge badge-success">Diprediksi Menang</label>
                <br>
                <span class="persentasecalon">
                    70%
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="element-box pad10">
            <div class="fotocalon">
                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
            </div>
            <br>
            <div class="" align="center">
                Bambang<br>
                <br>
                PDI, Golkar, Hanura<br>
                <br>
                <label class="badge badge-danger">Rawan</label>
                <label class="badge badge-warning">Hati-hati</label>
                <label class="badge badge-success">Diprediksi Menang</label>
                <br>
                <span class="persentasecalon">
                    70%
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="element-box pad10">
            <div class="fotocalon">
                <img class="rounded-circle img-fluid" src="<?=base_url();?>img/calon.png">
            </div>
            <br>
            <div class="" align="center">
                Bambang<br>
                <br>
                PDI, Golkar, Hanura<br>
                <br>
                <label class="badge badge-danger">Rawan</label>
                <label class="badge badge-warning">Hati-hati</label>
                <label class="badge badge-success">Diprediksi Menang</label>
                <br>
                <span class="persentasecalon">
                    70%
                </span>
            </div>
        </div>
    </div> -->
</div>
<div id="modal_profile" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal_profile_title">
                
            </h5>
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
          </div>
          <div class="modal-body" id="modal_profile_body">
            <div class="row">
                <div class="col-md-6">ini pie jens kelamin</div>
                <div class="col-md-6"> ini pie usia</div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
          </div>
        </div>
    </div>
</div> 
									
<script>
// load_informasi(0,'provinsi');
// function load_informasi(id,tipe,nama){
// 	var vid = id;
// 		$.ajax({
// 			url: "<?= site_url('maps/get_information_/') ?>"+id+'/'+tipe,
// 			cache: false,
// 			type:"POST",
// 			success: function(respond){
// 				console.log(respond);
// 				obj = JSON.parse(respond);
// 				$("#table_paslon").html(obj.paslon);
// 				$("#table_paslon_depan").html(obj.paslon);
// 				link = "https://www.google.com/maps/embed/v1/place?key=AIzaSyARSxVnQPIC6yvDqFNwpy0Ym5sMpRk-dSs&q="+obj.link;
// 				$("#frame").attr("src", link);
// 				vtipe = tipe;
// 				vidarea = id; 
// 				get_data('provinsi',vid,nama);
// 			}
// 		})
	
// }

<?php 
    if($nama == 'Nasional'){
        $tipe = '';
    }
?>
var url = "<?=base_url('img/logo_provinsi/')?>";
document.getElementById("nama_wilayah").innerHTML = "<?=$nama?>";
document.getElementById("tipe_wilayah").innerHTML = "<?=strtoupper($tipe)?>";
document.getElementById("logo_wilayah").src = url+"<?=$logo?>";

function load_modal_profile(modal,judul){ 
	console.log(modal);
	$("#modal_profile_body").empty();
    	$.ajax({
    	   type:'POST',
    	   url:"<?php echo base_url(); ?>dashboard/load_modal_profile/",
    	   //data: "modal_name="+modal,
		   data: {modal_name:modal,id:vid,nama:vnama,tipe:vtipe,logo:vlogo},
    	   success:function(msg){
			console.log(msg);
    		$("#modal_profile_body").html(msg);
			$("#modal_profile_title").text(judul);
    	   },
    	   error: function(result)
    	   {
    		 $("#modal_profile_body").html("Error"); 
    	   },
    	   fail:(function(status) {
    		 $("#modal_profile_body").html("Fail");
    	   }),
    	   beforeSend:function(d){
    		$("#modal_profile_body").html("<center><strong style='color:red'>Please Wait...<br><img height='25' width='120' src='<?php echo base_url();?>img/ajax-loader.gif' /></strong></center>");
    	   }

    	}); 
	
}
</script>