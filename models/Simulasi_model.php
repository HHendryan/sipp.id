<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Simulasi_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Survei_model');
        $this->load->model('Dashboard_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }
	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function thousandsCurrencyFormat($num) {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('K', 'M', 'B', 'T');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}
	
	function get_data($id,$nama,$tipe){
		$where = '';
		$where_guraklih = '';
		$query_lanjutan = '';
		if($id == 0){
			$tabel = $this->db->get('m_dpt')->result();	
			$tabel_target = 'am_prov_target';
			$id_target = 'idProv';
			$jum_dpt = 0;
			$jml_jokowi =0;
			$jml_prabowo =0;
			$i = 0;
			$target_nasional = 0;
			foreach ($tabel as $key => $value) {
				if($value->tabel_sum != NULL){
					$id_prov = $value->idProv;
					$sum_tabel = $value->tabel_sum;
					$hasil = $this->db->query("SELECT SUM(dpt) as jumlah from $sum_tabel")->row('jumlah');
					$jum_dpt=$jum_dpt+$hasil;
					$detail_prov = $this->db->get_where('m_provinces', array('id' => $value->idProv))->row();
					$new_table = $value->tabel_sum;
					$jokowi = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','jokowi','2014');
					$prabowo = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','prabowo','2014');
					$jml_jokowi=$jml_jokowi+$jokowi;
					$jml_prabowo=$jml_prabowo+$prabowo;

					$data['tabel'][$i]['id'] = $i+1;
					$data['tabel'][$i]['id_wilayah'] = $value->idProv;
					$data['tabel'][$i]['logo_wilayah'] = $detail_prov->logo;
					$data['tabel'][$i]['wilayah'] = $value->provinsi;
					$data['tabel'][$i]['jokowi'] = $jokowi;
					$data['tabel'][$i]['prabowo'] = $prabowo;
					$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','sah','2014');
					$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','tidak_sah','2014');

					$data['tabel'][$i]['button'] = '<a class="btn btn-sm btn-danger btn-rekomendasi" data-id="'.$value->idProv.'" data-nama="'.$value->provinsi.'" href="javascript:void(0)">Rekomendasi</a>';
					$data['tabel'][$i]['target'] = $this->db->query("SELECT target from $tabel_target where $id_target = '$id_prov'")->row('target');
					if($new_table != NULL){
						$data['tabel'][$i]['jum_dpt'] = $this->db->query("SELECT sum(dpt) as jml from $new_table")->row('jml');
						$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT count(1) as jml from $new_table")->row('jml');
					}else{
						$data['tabel'][$i]['jum_dpt'] = 0;
						$data['tabel'][$i]['jum_tps'] = 0;
					}
					$target_nasional += $data['tabel'][$i]['target']/100*$data['tabel'][$i]['jum_dpt'];
					$i++;	
				}	

			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon")->row('jml');
		}else{
			$tipe_pilpres = '';
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel_target = 'am_kab_target';
				$id_target = 'idKab';
				$tabel_target_parent = 'am_prov_target';
				$id_target_parent = 'idProv';
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where_guraklih .= "and id_provinces = $id";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id WHERE tps!='999' group by idKab";
				$jenis = 'idKab';
				$tipe_pilpres ='kabupaten';
			}else if($tipe == 'kabupaten'){
				$tabel_target = 'am_kec_target';
				$id_target = 'idKec';
				$tabel_target_parent = 'am_kab_target';
				$id_target_parent = 'idKab';
				$id_prov = substr($id, 0,2) ;//$this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
				$where_guraklih .= "and id_regency = $id";
				$query_lanjutan .= "inner join m_districts b on a.idKec=b.id where 1=1 AND tps!='999' $where group by idKec";
				$jenis = 'idKec';
				$tipe_pilpres ='kecamatan';
			}else if($tipe == 'kecamatan'){
				$tabel_target = 'am_kel_target';
				$id_target = 'idKel';
				$tabel_target_parent = 'am_kec_target';
				$id_target_parent = 'idKec';
				$id_prov = substr($id, 0,2) ;//$this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
				$where_guraklih .= "and id_district = $id";
				$query_lanjutan .= "inner join m_villages b on a.idKel=b.id where 1=1 AND tps!='999' $where group by idKel";
				$jenis = 'idKel';
				$tipe_pilpres ='kelurahan';
			}else if($tipe == 'kelurahan'){
				$tabel_target_parent = 'am_kel_target';
				$id_target_parent = 'idKel';
				$id_prov = substr($id, 0,2);
				/*$this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');*/
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
				$where_guraklih .= "and id_village = $id";
				$query_lanjutan .= "where 1=1 AND tps!='999' $where group by tps";
				$tipe_pilpres ='tps';
			}
			
			$target = $this->db->query("SELECT target from $tabel_target_parent where $id_target_parent = '$id'")->row('target');
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon where id_area='$id_prov'")->row('jml');
			if($tabel){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');	
			}else{
				$jum_dpt = 0;
			}

			if($tabel != NULL){
				if($tipe == 'kelurahan'){
					$tabel_ = $this->db->query("SELECT id, tps as name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}else{
					$tabel_ = $this->db->query("SELECT b.id, b.logo, name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}
				
				if($tabel_ != NULL){
					$i = 0;
					$jml_jokowi =0;
					$jml_prabowo =0;
					foreach ($tabel_ as $key => $value) {
						$id_hasil = $value->id;
						$data['tabel'][$i]['id'] = $i+1;
						$data['tabel'][$i]['id_wilayah'] = $value->id;
						$data['tabel'][$i]['button'] = '<a class="btn btn-sm btn-danger btn-rekomendasi" data-id="'.$value->id.'" data-nama="'.$value->name.'" href="javascript:void(0)">Rekomendasi</a>';
						if($tipe != 'kelurahan'){
							$data['tabel'][$i]['target'] = $this->db->query("SELECT target from $tabel_target where $id_target = '$id_hasil'")->row('target');
							$data['tabel'][$i]['logo_wilayah'] = $value->logo;

							$jokowi = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'jokowi','2014');
							$prabowo = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'prabowo','2014');
							$jml_jokowi=$jml_jokowi+$jokowi;
							$jml_prabowo=$jml_prabowo+$prabowo;
							$data['tabel'][$i]['jokowi'] = $jokowi;
							$data['tabel'][$i]['prabowo'] = $prabowo;
							$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'sah','2014');
							$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'tidak_sah','2014');
						}else{
							$asumsi_suara_sah = round($value->jml*0.75);
							$target_perolehan_suara = round($asumsi_suara_sah/$jum_paslon+($asumsi_suara_sah*0.05));
							$data['tabel'][$i]['target'] = $target;
							$data['tabel'][$i]['logo_wilayah'] = '';
							$jokowi = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'jokowi','2014');
							$prabowo = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'prabowo','2014');
							$jml_jokowi=$jml_jokowi+$jokowi;
							$jml_prabowo=$jml_prabowo+$prabowo;
							$data['tabel'][$i]['jokowi'] = $jokowi;
							$data['tabel'][$i]['prabowo'] = $prabowo;
							$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'sah','2014');
							$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'tidak_sah','2014');
						}
						
						$data['tabel'][$i]['wilayah'] = $value->name;
						if($tipe == 'kelurahan'){
							$data['tabel'][$i]['jum_tps'] = 1;
						}else{
							$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT COUNT(1) as jml from $tabel where $jenis=$id_hasil")->row('jml');
						}
						
						if($value->jml != NULL){
							$data['tabel'][$i]['jum_dpt'] = $value->jml;
						}else{
							$data['tabel'][$i]['jum_dpt'] = 0;
						}
						$i++;
					}
				}else{
					$data['tabel'][0]['id'] = '';
					$data['tabel'][0]['id_wilayah'] = '';
					$data['tabel'][0]['target'] = '';
					$data['tabel'][0]['button'] = '';
					$data['tabel'][0]['logo_wilayah'] = '';
					$data['tabel'][0]['wilayah'] = '';
					$data['tabel'][0]['jum_tps'] = 0;
					$data['tabel'][0]['jum_dpt'] = 0;
					$data['tabel'][0]['jokowi'] = 0;
					$data['tabel'][0]['prabowo'] = 0;
					$data['tabel'][0]['sah'] = 0;
					$data['tabel'][0]['tidak_sah'] =0;
					$jml_jokowi=0;
					$jml_prabowo=0;
				}
			}else{
				$data['tabel'][0]['id'] = '';
				$data['tabel'][0]['id_wilayah'] = '';
				$data['tabel'][0]['target'] = '';
				$data['tabel'][0]['button'] = '';
				$data['tabel'][0]['logo_wilayah'] = '';
				$data['tabel'][0]['wilayah'] = '';
				$data['tabel'][0]['jum_tps'] = 0;
				$data['tabel'][0]['jum_dpt'] = 0;
				$data['tabel'][0]['jokowi'] = 0;
				$data['tabel'][0]['prabowo'] = 0;
				$data['tabel'][0]['sah'] = 0;
				$data['tabel'][0]['tidak_sah'] =0;
				$jml_jokowi=0;
				$jml_prabowo=0;
			}
			
		}
		
		$data['jumlah_dpt'] = $jum_dpt;
		$data['jumlah_paslon'] = $jum_paslon;
		$data['jumlah_jokowi'] = $jml_jokowi;
		$data['jumlah_prabowo'] = $jml_prabowo;
		
		if($tabel){
			$data['asumsi_suara'] = round($jum_dpt*0.75);
			if($id == 0){
				$data['target_suara'] = $target_nasional;
				$data['presentase_target'] = $this->db->query("SELECT ROUND(AVG(target),2) AS rata FROM am_prov_target")->row('rata');
			}else{
				
				$data['presentase_target'] = $target;
				$data['target_suara'] = $data['asumsi_suara']*$target/100;
				
			}
			
			// $data['target_suara'] = round($jum_dpt*0.75/$jum_paslon+($jum_dpt*0.75*0.05));
			
		}else{
			$data['target_suara'] = 0;
			$data['asumsi_suara'] = 0;
		}
		
		$data['suara_guraklih'] = $this->Dashboard_model->get_jmlperolehan($id,$tipe);
		$data['jum_petugas_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where is_deleted='0' AND type = 'kortps' $where_guraklih")->row('jml');
		$data['jum_koordinator_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where is_deleted='0' AND type <> 'kortps' $where_guraklih")->row('jml');
		$data['kekurangan_potensi_suara'] = $data['target_suara'] - $data['suara_guraklih'];
		$data['kekurangan_suara'] = $data['jumlah_dpt'] - $data['suara_guraklih'];
		$data['jum_ketokohan_group'] = $this->Dashboard_model->get_jmlketokohan_group($id,$tipe);
		$data['list_ketokohan'] = $this->db->query("SELECT * from m_pertokohan where is_deleted = '0'")->result();
		
		
		// $data['jumlah_influencer'] = ;
		return $data;	
	}

	function get_data_pilpres($id,$nama,$tipe){
		$where = '';
		$where_guraklih = '';
		$query_lanjutan = '';
		if($id == 0){
			$tabel = $this->db->get('m_dpt')->result();	
			$tabel_target = 'am_prov_target';
			$id_target = 'idProv';
			$jum_dpt = 0;
			$jml_jokowi =0;
			$jml_prabowo =0;
			$i = 0;
			$target_nasional = 0;
			foreach ($tabel as $key => $value) {
				if($value->tabel_sum != NULL){
					$id_prov = $value->idProv;
					$sum_tabel = $value->tabel_sum;
					$hasil = $this->db->query("SELECT SUM(dpt) as jumlah from $sum_tabel")->row('jumlah');
					$jum_dpt=$jum_dpt+$hasil;
					$detail_prov = $this->db->get_where('m_provinces', array('id' => $value->idProv))->row();
					$new_table = $value->tabel_sum;
					$jokowi = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','jokowi','2014');
					$prabowo = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','prabowo','2014');
					$jml_jokowi=$jml_jokowi+$jokowi;
					$jml_prabowo=$jml_prabowo+$prabowo;

					$data['tabel'][$i]['id'] = $i+1;
					$data['tabel'][$i]['id_wilayah'] = $value->idProv;
					$data['tabel'][$i]['logo_wilayah'] = $detail_prov->logo;
					$data['tabel'][$i]['wilayah'] = $value->provinsi;
					$data['tabel'][$i]['jokowi'] = $jokowi;
					$data['tabel'][$i]['prabowo'] = $prabowo;
					$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','sah','2014');
					$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($id_prov,'provinsi','tidak_sah','2014');

					$data['tabel'][$i]['button'] = '<a class="btn btn-sm btn-danger btn-rekomendasi" data-id="'.$value->idProv.'" data-nama="'.$value->provinsi.'" href="javascript:void(0)">Rekomendasi</a>';
					$data['tabel'][$i]['target'] = $this->db->query("SELECT target from $tabel_target where $id_target = '$id_prov'")->row('target');
					if($new_table != NULL){
						$data['tabel'][$i]['jum_dpt'] = $this->db->query("SELECT sum(dpt) as jml from $new_table")->row('jml');
						$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT count(1) as jml from $new_table")->row('jml');
					}else{
						$data['tabel'][$i]['jum_dpt'] = 0;
						$data['tabel'][$i]['jum_tps'] = 0;
					}
					$target_nasional += $data['tabel'][$i]['target']/100*$data['tabel'][$i]['jum_dpt'];
					$i++;	
				}	

			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon")->row('jml');
		}else{
			$tipe_pilpres = '';
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel_target = 'am_kab_target';
				$id_target = 'idKab';
				$tabel_target_parent = 'am_prov_target';
				$id_target_parent = 'idProv';
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where_guraklih .= "and id_provinces = $id";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id WHERE tps!='999' group by idKab";
				$jenis = 'idKab';
				$tipe_pilpres ='kabupaten';
			}else if($tipe == 'kabupaten'){
				$tabel_target = 'am_kec_target';
				$id_target = 'idKec';
				$tabel_target_parent = 'am_kab_target';
				$id_target_parent = 'idKab';
				$id_prov = substr($id, 0,2) ;//$this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
				$where_guraklih .= "and id_regency = $id";
				$query_lanjutan .= "inner join m_districts b on a.idKec=b.id where 1=1 AND tps!='999' $where group by idKec";
				$jenis = 'idKec';
				$tipe_pilpres ='kecamatan';
			}else if($tipe == 'kecamatan'){
				$tabel_target = 'am_kel_target';
				$id_target = 'idKel';
				$tabel_target_parent = 'am_kec_target';
				$id_target_parent = 'idKec';
				$id_prov = substr($id, 0,2) ;//$this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
				$where_guraklih .= "and id_district = $id";
				$query_lanjutan .= "inner join m_villages b on a.idKel=b.id where 1=1 AND tps!='999' $where group by idKel";
				$jenis = 'idKel';
				$tipe_pilpres ='kelurahan';
			}else if($tipe == 'kelurahan'){
				$tabel_target_parent = 'am_kel_target';
				$id_target_parent = 'idKel';
				$id_prov = substr($id, 0,2);
				/*$this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');*/
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
				$where_guraklih .= "and id_village = $id";
				$query_lanjutan .= "where 1=1 AND tps!='999' $where group by tps";
				$tipe_pilpres ='tps';
			}else if ($tipe == 'dapil') {
				$id_prov ='';
				$idKab="";
				$result = $this->db->query("SELECT a.id_provinces, a.`id_regency` FROM dapil_has_area a WHERE a.id_dapil=$id GROUP BY a.id_regency")->result();
				$i=0;
				foreach ($result as $key => $value) {
					$id_prov = $value->id_provinces;
					if ($i==0) {
						$idKab = "'".$value->id_regency."'";
					} else {
						$idKab = $idKab.",'".$value->id_regency."'";
					}
					$i++;
				}

				$tabel_target = 'am_kab_target';
				$id_target = 'idKab';
				$tabel_target_parent = 'am_prov_target';
				$id_target_parent = 'idProv';
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where .= "and idKab in ($idKab)";
				$where_guraklih .= "and id_provinces = $id_prov and id_regency in (".$idKab.")";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id WHERE tps!='999' AND b.id in (".$idKab.")group by idKab";
				$jenis = 'idKab';
				$tipe_pilpres ='kabupaten';
			}

			$target = $this->db->query("SELECT target from $tabel_target_parent where $id_target_parent = '$id'")->row('target');
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon where id_area='$id_prov'")->row('jml');
			if($tabel){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');	
			}else{
				$jum_dpt = 0;
			}

			if($tabel != NULL){
				if($tipe == 'kelurahan'){
					$tabel_ = $this->db->query("SELECT id, tps as name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}else{
					$tabel_ = $this->db->query("SELECT b.id, b.logo, name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}
				
				if($tabel_ != NULL){
					$i = 0;
					$jml_jokowi =0;
					$jml_prabowo =0;
					foreach ($tabel_ as $key => $value) {
						$id_hasil = $value->id;
						$data['tabel'][$i]['id'] = $i+1;
						$data['tabel'][$i]['id_wilayah'] = $value->id;
						$data['tabel'][$i]['button'] = '<a class="btn btn-sm btn-danger btn-rekomendasi" data-id="'.$value->id.'" data-nama="'.$value->name.'" href="javascript:void(0)">Rekomendasi</a>';
						if($tipe != 'kelurahan'){
							$data['tabel'][$i]['target'] = $this->db->query("SELECT target from $tabel_target where $id_target = '$id_hasil'")->row('target');
							$data['tabel'][$i]['logo_wilayah'] = $value->logo;

							$jokowi = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'jokowi','2014');
							$prabowo = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'prabowo','2014');
							$jml_jokowi=$jml_jokowi+$jokowi;
							$jml_prabowo=$jml_prabowo+$prabowo;
							$data['tabel'][$i]['jokowi'] = $jokowi;
							$data['tabel'][$i]['prabowo'] = $prabowo;
							$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'sah','2014');
							$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($value->id,$tipe_pilpres,'tidak_sah','2014');
						}else{
							$asumsi_suara_sah = round($value->jml*0.75);
							$target_perolehan_suara = round($asumsi_suara_sah/$jum_paslon+($asumsi_suara_sah*0.05));
							$data['tabel'][$i]['target'] = $target;
							$data['tabel'][$i]['logo_wilayah'] = '';
							$jokowi = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'jokowi','2014');
							$prabowo = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'prabowo','2014');
							$jml_jokowi=$jml_jokowi+$jokowi;
							$jml_prabowo=$jml_prabowo+$prabowo;
							$data['tabel'][$i]['jokowi'] = $jokowi;
							$data['tabel'][$i]['prabowo'] = $prabowo;
							$data['tabel'][$i]['sah'] = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'sah','2014');
							$data['tabel'][$i]['tidak_sah'] = $this->Dashboard_model->get_suara_pilpres($id.'_'.$value->name,$tipe_pilpres,'tidak_sah','2014');
						}
						
						$data['tabel'][$i]['wilayah'] = $value->name;
						if($tipe == 'kelurahan'){
							$data['tabel'][$i]['jum_tps'] = 1;
						}else{
							$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT COUNT(1) as jml from $tabel where $jenis=$id_hasil")->row('jml');
						}
						
						if($value->jml != NULL){
							$data['tabel'][$i]['jum_dpt'] = $value->jml;
						}else{
							$data['tabel'][$i]['jum_dpt'] = 0;
						}
						$i++;
					}
				}else{
					$data['tabel'][0]['id'] = '';
					$data['tabel'][0]['id_wilayah'] = '';
					$data['tabel'][0]['target'] = '';
					$data['tabel'][0]['button'] = '';
					$data['tabel'][0]['logo_wilayah'] = '';
					$data['tabel'][0]['wilayah'] = '';
					$data['tabel'][0]['jum_tps'] = 0;
					$data['tabel'][0]['jum_dpt'] = 0;
					$data['tabel'][0]['jokowi'] = 0;
					$data['tabel'][0]['prabowo'] = 0;
					$data['tabel'][0]['sah'] = 0;
					$data['tabel'][0]['tidak_sah'] =0;
					$jml_jokowi=0;
					$jml_prabowo=0;
				}
			}else{
				$data['tabel'][0]['id'] = '';
				$data['tabel'][0]['id_wilayah'] = '';
				$data['tabel'][0]['target'] = '';
				$data['tabel'][0]['button'] = '';
				$data['tabel'][0]['logo_wilayah'] = '';
				$data['tabel'][0]['wilayah'] = '';
				$data['tabel'][0]['jum_tps'] = 0;
				$data['tabel'][0]['jum_dpt'] = 0;
				$data['tabel'][0]['jokowi'] = 0;
				$data['tabel'][0]['prabowo'] = 0;
				$data['tabel'][0]['sah'] = 0;
				$data['tabel'][0]['tidak_sah'] =0;
				$jml_jokowi=0;
				$jml_prabowo=0;
			}
			
		}
		
		$data['jumlah_dpt'] = $jum_dpt;
		$data['jumlah_paslon'] = $jum_paslon;
		$data['jumlah_jokowi'] = $jml_jokowi;
		$data['jumlah_prabowo'] = $jml_prabowo;
		
		if($tabel){
			$asumsi_suara = round($jum_dpt*0.75);
			$data['asumsi_suara'] = $asumsi_suara;
			if($id == 0){
				$data['target_suara'] = round(($asumsi_suara/2)+($asumsi_suara *0.05));
				$data['presentase_target'] = $this->db->query("SELECT ROUND(AVG(target),2) AS rata FROM am_prov_target")->row('rata');
			}else{
				
				$data['presentase_target'] = $target;
				$data['target_suara'] = round(($asumsi_suara/2)+($asumsi_suara *0.05));
				
			}
			
			// $data['target_suara'] = round($jum_dpt*0.75/$jum_paslon+($jum_dpt*0.75*0.05));
			
		}else{
			$data['target_suara'] = 0;
			$data['asumsi_suara'] = 0;
		}
		
		$data['suara_guraklih'] = $this->Dashboard_model->get_jmlperolehan($id,$tipe);
		$data['jum_petugas_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where is_deleted='0' AND type = 'kortps' $where_guraklih")->row('jml');
		$data['jum_koordinator_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where is_deleted='0' AND type <> 'kortps' $where_guraklih")->row('jml');
		$data['kekurangan_potensi_suara'] = $data['target_suara'] - $data['suara_guraklih'];
		$data['kekurangan_suara'] = $data['jumlah_dpt'] - $data['suara_guraklih'];
		$data['jum_ketokohan_group'] = $this->Dashboard_model->get_jmlketokohan_group($id,$tipe);
		$data['list_ketokohan'] = $this->db->query("SELECT * from m_pertokohan where is_deleted = '0'")->result();
		
		
		// $data['jumlah_influencer'] = ;
		return $data;	
	}

	/*
    * Function for get detail guraklih data (Hari Hendryan)
    */
	function get_detail_guraklih($id, $tipe_wilayah, $type){
		$q ='';
		if ($type=='0') {
			$q = " type = 'kortps' ";
		} else {
			$q = " type <> 'kortps' ";
		}

		if ($tipe_wilayah=='kabupaten') {
			$query = "SELECT a.cp, a.phone, a.nik, a.address, a.id_village , a.id_district, a.id_regency,
				 (SELECT kabupaten from am_kab_target where idKab = a.id_regency) as kab, 
				 (SELECT kecamatan from am_kec_target where idKec = a.id_district) as kec,
				 (SELECT kelurahan from am_kel_target where idKel = a.id_village) as kel,  b.tps, type  FROM m_team a, m_team_has_tps b WHERE  a.id_regency = $id AND ".$q." AND a.is_deleted='0' AND b.is_deleted='0' AND a.id = b.id_team";
		} else if ($tipe_wilayah=='kecamatan') {
			$query = "SELECT a.cp, a.phone, a.nik, a.address, a.id_village , a.id_district, a.id_regency,
				 (SELECT kabupaten from am_kab_target where idKab = a.id_regency) as kab, 
				 (SELECT kecamatan from am_kec_target where idKec = a.id_district) as kec,
				 (SELECT kelurahan from am_kel_target where idKel = a.id_village) as kel, b.tps, type  FROM m_team a, m_team_has_tps b WHERE  a.id_district = $id AND ".$q." AND a.is_deleted='0' AND b.is_deleted='0' AND a.id = b.id_team";
		} else {
			$query = "SELECT a.cp, a.phone, a.nik, a.address, a.id_village , a.id_district, a.id_regency,
				 (SELECT kabupaten from am_kab_target where idKab = a.id_regency) as kab, 
				 (SELECT kecamatan from am_kec_target where idKec = a.id_district) as kec,
				 (SELECT kelurahan from am_kel_target where idKel = a.id_village) as kel, b.tps, type  FROM m_team a, m_team_has_tps b WHERE  a.id_village = $id AND ".$q." AND a.is_deleted='0' AND b.is_deleted='0' AND a.id = b.id_team";
		}
			
		$data = $this->db->query($query)->result();
		return($data);
	}

	function get_rekomendasi($id,$nama,$tipe){
		if($id == 0){
			$wilayah = $this->db->get('m_provinces')->result();
			$i=0;
			foreach ($wilayah as $key => $value) {
				$wilayah[$i]->rekomendasi = $this->db->get_where('rekomendasi', array('id_area' => $value->id ))->row('rekomendasi');
				$i++;
			}
		}else{
			// if($tipe == 'provinsi'){
			// 	$wilayah = $this->db->get_where('m_provinces', array('id' => $id))->row();
			// }else if($tipe == 'kabupaten'){
			// 	$wilayah = $this->db->get_where('m_regencies', array('id' => $id))->row();
			// }else if($tipe == 'kecamatan'){
			// 	$wilayah = $this->db->get_where('m', array('id' => $id))->row();
			// }else if($tipe == 'kelurahan'){
			// 	$wilayah = $this->db->get_where('m_provinces', array('id' => $id))->row();
			// }
			$wilayah = $this->db->get_where('rekomendasi', array('id_area' => $id))->row();
		}

		$data['tabel'] = $wilayah;
		return $data;
	}
}