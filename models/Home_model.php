<?php
defined('BASEPATH') or exit('No direct script access allowed');

//model for admin user
class Home_model extends CI_Model
{
    public function get_detail_influencer($id_prov)
    {
        $query = "SELECT name,phone,gender,id_pertokohan,inclination, (SELECT name from m_regencies where intangible.`id_city` = m_regencies.id) as kab, (SELECT name from m_districts where intangible.`id_districts` = `m_districts`.id ) as kec, (SELECT name from m_villages where intangible.`id_village` = `m_villages`.id ) as kel from intangible where is_deleted='0' AND id_provinces ='$id_prov'";
        $data = $this->db->query($query)->result();
        $i = 0;
        foreach ($data as $key => $value) {
            $data[$i]->pertokohan = $this->db->get_where("m_pertokohan", array('id_pertokohan' => $value->id_pertokohan))->row('nama');
            $data[$i]->kecondongan = $this->db->get_where("m_parpol", array('id_parpol' => $value->inclination))->row('name');
            $i++;
        }
        return ($data);

    }

    public function get_data_home()
    {
        $data['survey_pilpres_14'] = $this->db->query("select count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where p5 <> 162 and p5 <> 163 and p5 <> 236) * 100 ),2) as jumlah, b.nama
			from trans_survey_clean a
			inner join m_pilih b on a.p5 = b.id
			where b.id <> 162 and b.id <> 163 and b.id <> 236
			group by p5")->result();
        $data['survey_pilpres_19'] = $this->db->query("select IFNULL(b.image_paslon,'avatarblank.jpg') img_paslon,count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where p6 <> 225 and p6 <> 218 and p6 <> 206 and p6 <> 226 and p6 <> 205 and p6 <> 227) * 100 ),2) as jumlah, b.nama
			from trans_survey_clean a
			left join m_pilih b on a.p6 = b.id
			LEFT JOIN m_paslon c ON a.`id_paslon` = c.`id_paslon`
			where b.id <> 225 and b.id <> 218 and b.id <> 206 and b.id <> 226 and b.id <> 205 and b.id <> 227
			group by p6 order by jumlah desc limit 10")->result();
        $data['survey_parpol_14'] = $this->db->query("select c.color,c.picture, count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where p3 <> 40 and p3 <> 41 and p3 <> 42 and p3 <> 43 and p3 <> 44 and p3 <> 45 and p3 <> 235) * 100 ),2) as jumlah, b.nama
			from trans_survey_clean a
			inner join m_pilih b on a.p3 = b.id
			left join m_parpol c on c.alias = b.nama
			where b.id <> 40 and b.id <> 41 and b.id <> 42 and b.id <> 43 and b.id <> 44 and b.id <> 45 and b.id <>235
			group by p3 order by jumlah desc")->result();
        $data['survey_parpol_19'] = $this->db->query("select c.color,c.picture, count(1) as total, round(( count(1)/(SELECT count(1) from trans_survey_clean where p4 <> 60 and p4 <> 61 and p4 <> 62 and p4 <> 63 and p4 <> 64 and p4 <> 65) * 100 ),2) as jumlah, b.nama
			from trans_survey_clean a
			inner join m_pilih b on a.p4 = b.id
			left join m_parpol c on c.alias = b.nama
			where b.id <> 60 and b.id <> 61 and b.id <> 62 and b.id <> 63 and b.id <> 64 and b.id <> 65
			group by p4 order by jumlah desc")->result();
        $data['paslon'] = $this->db->query("SELECT a.id_paslon, a.pasangan_alias,a.kepala_alias,a.wakil_alias,a.nama_wakil,a.area,a.image_paslon,a.id_area, COUNT(1) AS jml
											FROM m_paslon a ,m_parpol b, m_pengusung c, m_area d
											WHERE a.`id_paslon` = c.`id_paslon`
											AND b.`id_parpol` = c.`id_parpol`
											AND b.`id_parpol` = 3
											AND a.`id_area` = d.`idProv`
											GROUP BY a.`id_paslon`")->result();

        return $data;
    }
}
