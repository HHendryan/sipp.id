<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Simulasi_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Survei_model');
        $this->load->model('Dashboard_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }
	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function thousandsCurrencyFormat($num) {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('K', 'M', 'B', 'T');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}
	
	function get_data($id,$nama,$tipe){
		$where = '';
		$where_guraklih = '';
		$query_lanjutan = '';
		if($id == 0){
			$tabel = $this->db->get('m_dpt')->result();	
			$jum_dpt = 0;
			$i = 0;
			foreach ($tabel as $key => $value) {
				if($value->tabel_sum != NULL){
					$sum_tabel = $value->tabel_sum;
					$hasil = $this->db->query("SELECT SUM(dpt) as jumlah from $sum_tabel")->row('jumlah');
					$jum_dpt=$jum_dpt+$hasil;
					$new_table = $value->tabel_sum;
					$data['tabel'][$i]['id'] = $i+1;
					$data['tabel'][$i]['id_wilayah'] = $i+1;
					$data['tabel'][$i]['wilayah'] = $value->provinsi;
					if($new_table != NULL){
						$data['tabel'][$i]['jum_dpt'] = $this->db->query("SELECT sum(dpt) as jml from $new_table")->row('jml');
						$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT count(1) as jml from $new_table")->row('jml');
					}else{
						$data['tabel'][$i]['jum_dpt'] = 0;
						$data['tabel'][$i]['jum_tps'] = 0;
					}
					$i++;	
				}	

			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon")->row('jml');
		}else{
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where_guraklih .= "and id_provinces = $id";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id group by idKab";
				$jenis = 'idKab';
			}else if($tipe == 'kabupaten'){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
				$where_guraklih .= "and id_regency = $id";
				$query_lanjutan .= "inner join m_districts b on a.idKec=b.id where 1=1 $where group by idKec";
				$jenis = 'idKec';
			}else if($tipe == 'kecamatan'){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
				$where_guraklih .= "and id_district = $id";
				$query_lanjutan .= "inner join m_villages b on a.idKel=b.id where 1=1 $where group by idKel";
				$jenis = 'idKel';
			}else if($tipe == 'kelurahan'){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
				$where_guraklih .= "and id_village = $id";
				$query_lanjutan .= "where 1=1 $where group by tps";
			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon where id_area='$id_prov'")->row('jml');
			if($tabel){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');	
			}else{
				$jum_dpt = 0;
			}

			if($tabel != NULL){
				if($tipe == 'kelurahan'){
					$tabel_ = $this->db->query("SELECT id, tps as name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}else{
					$tabel_ = $this->db->query("SELECT b.id, name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}
				
				if($tabel_ != NULL){
					$i = 0;
					foreach ($tabel_ as $key => $value) {
						$id_hasil = $value->id;
						$data['tabel'][$i]['id'] = $i+1;
						$data['tabel'][$i]['wilayah'] = $value->name;
						if($tipe == 'kelurahan'){
							$data['tabel'][$i]['jum_tps'] = 1;
						}else{
							$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT COUNT(1) as jml from $tabel where $jenis=$id_hasil")->row('jml');
						}
						
						if($value->jml != NULL){
							$data['tabel'][$i]['jum_dpt'] = $value->jml;
						}else{
							$data['tabel'][$i]['jum_dpt'] = 0;
						}
						$i++;
					}
				}else{
					$data['tabel'][0]['id'] = '';
					$data['tabel'][0]['wilayah'] = '';
					$data['tabel'][0]['jum_tps'] = 0;
					$data['tabel'][0]['jum_dpt'] = 0;
				}
			}else{
				$data['tabel'][0]['id'] = '';
				$data['tabel'][0]['wilayah'] = '';
				$data['tabel'][0]['jum_tps'] = 0;
				$data['tabel'][0]['jum_dpt'] = 0;
			}
			
		}
		
		$data['jumlah_dpt'] = $jum_dpt;
		$data['jumlah_paslon'] = $jum_paslon;
		
		if($tabel){
			$data['target_suara'] = round($jum_dpt*0.75/$jum_paslon+($jum_dpt*0.75*0.05));
		}else{
			$data['target_suara'] = 0;
		}
		
		$data['suara_guraklih'] = $this->Dashboard_model->get_jmlperolehan($id,$tipe);
		$data['jum_petugas_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where type = 'kortps' $where_guraklih")->row('jml');
		$data['jum_koordinator_guraklih'] = $this->db->query("SELECT COUNT(1) as jml from m_team where type <> 'kortps' $where_guraklih")->row('jml');
		$data['kekurangan_potensi_suara'] = $data['target_suara'] - $data['suara_guraklih'];
		$data['kekurangan_suara'] = $data['jumlah_dpt'] - $data['suara_guraklih'];
		$data['jum_ketokohan_group'] = $this->Dashboard_model->get_jmlketokohan_group($id,$tipe);
		
		
		// $data['jumlah_influencer'] = ;
		return $data;	
	}
}