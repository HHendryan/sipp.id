<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Legislator_model extends CI_Model {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // $this->load->model('Auth_model');
        $this->load->model('Survei_model');
        // $this->load->library('datatables');

        // $data = $this->session->userdata('teknopol');
        // if(!$data){
        //   redirect('');
        // }
       	// $this->load->view('template/menu',$data,true);
    }
	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function thousandsCurrencyFormat($num) {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('K', 'M', 'B', 'T');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_data($id,$nama,$tipe){
		$where = '';
		if($id != 0 ){
			if($tipe == 'provinsi'){
				$where .= "and c.id_provinces = $id";
			}else if ($tipe == 'kabupaten'){
				$where .= "and c.id_regency = $id";
			}else if ($tipe == 'kecamatan'){
				$where .= "and c.id_districts = $id";
			}
		}
		// $data['legislator'] = $this->db->query("SELECT * from m_legislator a 
		// 										left join m_dapil b on a.id_dapil=b.id_dapil
		// 										left join dapil_has_area c on c.id_dapil=b.id_dapil
		// 										where 1=1 $where
		// 										group by id_legislator")->result();
		$data['legislator'] = 1;
		return $data;
	}

}